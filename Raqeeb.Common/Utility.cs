﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml;

namespace Raqeeb.Common
{
    public static class Utility
    {
        private static Random random = new Random();
        private const int NumberOfRetries = 3;
        private const int DelayOnRetry = 1000;
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public static string GenerateSecretKey(int bytes)
        {

            // Create a random key using a random number generator. This would be the
            //  secret key shared by sender and receiver.
            byte[] secretkeybytes = new Byte[bytes];
            string secretkey = null;
            //RNGCryptoServiceProvider is an implementation of a random number generator.
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                // The array is now filled with cryptographically strong random bytes.
                rng.GetBytes(secretkeybytes);
                secretkey = HttpServerUtility.UrlTokenEncode(secretkeybytes);

            }

            return secretkey;
        }

        [Obsolete("void Method To Create Mapping Between Source And Destination <T Source,T Destination>")]
        public static void CreateMap<TSource, TDestination>()
        {
            AutoMapper.Mapper.CreateMap<TSource, TDestination>();
        }

        [Obsolete("return TDestination Type From  Mapping Between Source And Destination <T Source,T Destination>(TSource source)")]
        public static TDestination Map<TSource, TDestination>(TSource source)
        {
            return AutoMapper.Mapper.Map<TSource, TDestination>(source);
        }

        public static string ShiftFromDateTime()
        {
            DateTime currentDateTime = DateTime.Now;
            string shiftType = "Night";
            TimeSpan start = TimeSpan.Parse("06:00"); // 6 AM
            TimeSpan end = TimeSpan.Parse("18:00");   // 2 AM
            TimeSpan now = currentDateTime.TimeOfDay;

            if (now >= start && now <= end)
            {
                shiftType = "Day";
            }

            return shiftType;

        }
        public static string GetLocationName(double latitude, double longitude)
        {
            XmlDocument doc = new XmlDocument();

            try
            {
                const string APIKEY = "AIzaSyByETi5G2LcFTWJy5I3G5zltjmhcWhFAfE";
                doc.Load("https://maps.googleapis.com/maps/api/geocode/xml?latlng=" + latitude + "," + longitude + "&sensor=false" + "&key=" + APIKEY);
                XmlNode element = doc.SelectSingleNode("//GeocodeResponse/status");
                if (element.InnerText == "ZERO_RESULTS")
                {
                    return ("No data available for the specified location");
                }
                else
                {

                    element = doc.SelectSingleNode("//GeocodeResponse/result/formatted_address");


                    //Console.ReadKey();
                    return (element.InnerText);
                }

            }
            catch (Exception ex)
            {
                return ("(Address lookup failed: ) " + ex.Message);
            }
        }

        public static bool WriteLog(string strFileName, string strMessage)
        {
            try
            {
                FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", Path.GetTempPath(), strFileName), FileMode.Append, FileAccess.Write);
                StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
                objStreamWriter.WriteLine(strMessage);
                objStreamWriter.Close();
                objFilestream.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static void WriteLog(Exception ex)
        {
            TextWriter oldOut = Console.Out;

            string c_path = @"c:\temp\MyTest.txt";
            for (int i = 1; i <= NumberOfRetries; ++i)
            {
                try
                {
                    // Do stuff with file
                    break; // When done we can break loop
                }
                catch (IOException e) when (i <= NumberOfRetries)
                {
                    // You may check error code to filter some exceptions, not every error
                    // can be recovered.
                    Thread.Sleep(DelayOnRetry);
                }
            }

            // This text is added only once to the file.
            if (!File.Exists(c_path))
            {
                // Create a file to write to.
                string createText = ex + Environment.NewLine;
                File.WriteAllText(c_path, createText);
            }

            // This text is always added, making the file longer over time
            // if it is not deleted.
            string appendText = ex + Environment.NewLine;
            File.AppendAllText(c_path, appendText);

            // Open the file to read from.
            string readText = File.ReadAllText(c_path);
            Console.WriteLine(readText);

        }

        public static void WriteLogWithMessage(Exception ex, string UID)
        {
            TextWriter oldOut = Console.Out;

            string c_path = @"c:\temp\MyTest.txt";
            for (int i = 1; i <= NumberOfRetries; ++i)
            {
                try
                {
                    // Do stuff with file
                    break; // When done we can break loop
                }
                catch (IOException e) when (i <= NumberOfRetries)
                {
                    // You may check error code to filter some exceptions, not every error
                    // can be recovered.
                    Thread.Sleep(DelayOnRetry);
                }
            }

            // This text is added only once to the file.
            if (!File.Exists(c_path))
            {
                // Create a file to write to.
                string createText = DateTime.Now.ToString() + ex + " " + UID + Environment.NewLine;
                File.WriteAllText(c_path, createText);
            }

            // This text is always added, making the file longer over time
            // if it is not deleted.
            string appendText = DateTime.Now.ToString() + ex + " " + UID + Environment.NewLine;
            File.AppendAllText(c_path, appendText);

            // Open the file to read from.
            string readText = File.ReadAllText(c_path);
            Console.WriteLine(readText);

        }

        public static string ConvertImageToBase64(string filePath)
        {
            //var regex = new Regex(filePath, RegexOptions.IgnoreCase);
            // String unescapedString = Regex.Unescape(filePath);

            string base64ImageRepresentation = "";
            try
            {
                byte[] imageArray = System.IO.File.ReadAllBytes(@filePath);
                base64ImageRepresentation = Convert.ToBase64String(imageArray);
            }
            catch (Exception ex)
            {

                return "";
            }

            return base64ImageRepresentation = "data:image/jpeg;charset=utf-8;base64," + base64ImageRepresentation;
        }
        public static string ConvertVideoToBase64(string filePath)
        {
            //var regex = new Regex(filePath, RegexOptions.IgnoreCase);
            // String unescapedString = Regex.Unescape(filePath);

            string base64ImageRepresentation = "";
            try
            {
                byte[] imageArray = System.IO.File.ReadAllBytes(@filePath);
                base64ImageRepresentation = Convert.ToBase64String(imageArray);
            }
            catch (Exception ex)
            {

                return "";
            }

            return base64ImageRepresentation = "data:video/mp4;base64," + base64ImageRepresentation;
        }

    }
}
