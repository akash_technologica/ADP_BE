﻿namespace Raqeeb.Common
{
    public static class Constant
    {
    }

    public static class SystemErrors
    {
        public const string UnhandledException = "UNHANDLED_EXCEPTION";
        public const string InvalidUserCredentials = "INVALID_CREDENTIALS";
        public const string InvalidUserApiKeys = "INVALID_USER_API_KEYS";

        public const string ApplicationAlreadyRegistered = "ERROR_ALREADY_REGISTERED";
        public const string InvalidEntity = "ERROR_MISSING_DATA";
    }
}
