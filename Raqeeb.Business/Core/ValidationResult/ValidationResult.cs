﻿using System.Collections;
using System.Collections.Generic;

namespace Raqeeb.Business
{
    public class ValidationResult
    {
        public bool IsPassed { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public IList<DictionaryEntry> Messages { get; set; }
    }
}
