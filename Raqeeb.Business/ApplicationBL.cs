﻿using Raqeeb.Framework.Core;
using Raqeeb.Common;
using Raqeeb.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Business
{
    public static class ApplicationBL
    {
        public static void CheckIfApplicationExist(bool isApplicationExist)
        {
            if (isApplicationExist)
                throw new BussinessException(SystemErrors.ApplicationAlreadyRegistered);
        }


        public static void ValidateApplicationEntity(Application application)
        {
            if (string.IsNullOrEmpty(application.UserName) ||
                 string.IsNullOrEmpty(application.MobileNumber) ||
                 string.IsNullOrEmpty(application.Password) ||
                 string.IsNullOrEmpty(application.EnglishName))
            {
                throw new BussinessException(SystemErrors.InvalidEntity);
            }
        }
    }
}
