﻿using Raqeeb.Framework.Core;
using System;
using System.ComponentModel.Composition;
using System.ServiceModel;

namespace Raqeeb.Manager
{
    public class ManagerBase : IDisposable, IServiceContract
    {
        bool disposed = false;

        public ManagerBase()
        {
            OperationContext context = OperationContext.Current;

            if (ObjectBase.Container != null)
                ObjectBase.Container.SatisfyImportsOnce(this);
        }
        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {

                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}
