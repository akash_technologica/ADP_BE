﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class IOElementsMasterManager: ManagerBase
    {
        readonly IOElementsMasterDAL ioDAL;
        public IOElementsMasterManager()
        {
            ioDAL = new IOElementsMasterDAL();
        }

        public List<IOElementsMaster> GetList()
        {

            var list = ioDAL.GetAll();
            return list;
        }


        public IOElementsMaster Add(IOElementsMaster country)
        {

            return ioDAL.Add(country);
        }


        public IOElementsMaster Update(IOElementsMaster country)
        {

            return ioDAL.Update(country);
        }
    }
}
