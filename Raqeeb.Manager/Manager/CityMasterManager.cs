﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class CityMasterManager : ManagerBase
    {
        readonly CityMasterDAL cityMasterDAL;
        public CityMasterManager()
        {
            cityMasterDAL = new CityMasterDAL();
        }

        public List<CityMaster> GetCityByCountry(string countryId)
        {
            var list = cityMasterDAL.GetByCountryId(countryId);
            return list;
        }

        public CityMaster Add(CityMaster city)
        {
            return cityMasterDAL.Add(city);
        }

        public CityMaster Delete(CityMaster city)
        {
            return cityMasterDAL.Delete(city);
        }

        public CityMaster Update(CityMaster city)
        {

            return cityMasterDAL.Update(city);
        }
    }
}
