﻿using Raqeeb.Domain.Entities;
using Raqeeb.DataAccess;
using Raqeeb.Framework.Core;
using Raqeeb.Common;
using System;
using System.Collections.Generic;
using Raqeeb.Domain.Dtos;
using Raqeeb.DataAccess.DataAccess;
using System.Linq;

namespace Raqeeb.Manager
{
    public class UserManager : ManagerBase
    {
        readonly UserDAL userDAL;
        readonly GroupMasterDAL groupMasterDAL;
        readonly RolesDAL rolesDAL;
        public UserManager()
        {
            userDAL = new UserDAL();
            groupMasterDAL = new GroupMasterDAL();
            rolesDAL = new RolesDAL();
        }
        public UserDto Add(UserDto user)
        {
            var UserDAL = new UserDAL();
            return UserDAL.Add(user);
        }

        public User Inactive(User user)
        {
            var UserDAL = new UserDAL();
            return UserDAL.Inactive(user);
        }

        public UserModel GetAll()
        {
            try
            {
                List<Domain.DataModel.User> list = userDAL.GetAll();
                List<UserDto> userList = new List<UserDto>();
                UserModel userModel = new UserModel();
                List<Domain.DataModel.Role_Master> roleList = rolesDAL.GetRolesType();

                DateTime loginTimeforNull = new DateTime();
                userModel.ActiveContacts = 0;
                userModel.ActiveUsers = 0;
                userModel.InactiveUsers = 0;
                userModel.InactiveContacts = 0;
                userModel.ContactsLoggedInToday = 0;
                            
                foreach (var item in list)
                {
                    List<string> role_List = new List<string>();
                    role_List.Add(item.Roles);
                    UserDto userDto = new UserDto()
                    {
                        UserName = item.UserName,
                        ArabicName = item.ArabicName,
                        Email = item.Email,
                        EnglishName = item.EnglishName,
                        IsActive = item.IsActive,
                        IsLoggedIn = item.IsLoggedIn.Value,
                        LastLoginTime = item.LastLoginTime == null ? loginTimeforNull : item.LastLoginTime,
                        MobileNumber = item.MobileNumber,
                        RoleName = item.Roles,
                        Roles = role_List,
                        RoleId = roleList.Where(c=>c.Role == item.Roles).Select(c=>c.Id).FirstOrDefault(),
                        Id = item.C_id,
                        GroupId = item.GroupId.Value,
                        GroupName = groupMasterDAL.GetGroupName(item.GroupId)
                    };


                    

                    userList.Add(userDto);
                    if (item.IsActive)
                    {
                        userModel.ActiveUsers += 1;
                    }
                    else
                    {
                        userModel.InactiveUsers += 1;
                    }
                }
                userModel.UserList = userList;
                return userModel;
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        public User Login(string userName, string password)
        {
            Crypto crypto = new Crypto();

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
            {
                throw new BussinessException(SystemErrors.InvalidUserCredentials);
            }


            UserDAL userDAL = new UserDAL();
            var user = userDAL.GetAccess(userName, crypto.Encrypt(password, "o6806642kbM7c5"));

            if (user == null)
            {
                throw new BussinessException(SystemErrors.InvalidUserCredentials);
            }
            else
            {
                user.Password = null;
                user.LastLoginTime = DateTime.Now.AddHours(4); 
                user.AccessTokenStamp = Guid.NewGuid().ToString();

                userDAL.UpdateUser(user);
                return user;
            }
        }

        public User Update(User user)
        {
            UserDAL userDAL = new UserDAL();
            return userDAL.UpdateUser(user);
        }

        public UserDto Delete(UserDto user)
        {
            UserDAL userDAL = new UserDAL();
            return userDAL.Delete(user);
        }

        public UserDto UpdateUserData(UserDto user)
        {
            UserDAL userDAL = new UserDAL();
            return userDAL.UpdateUserData(user);
        }

        public User CheckUserExist(User user)
        {
            UserDAL userDAL = new UserDAL();
            return userDAL.CheckUserExist(user);
        }

        public User SendConfirmationEmail(User user)
        {
            UserDAL userDAL = new UserDAL();
            return userDAL.SendConfirmationEmail(user);
        }

        public User GetById(string idString)
        {
            UserDAL userDAL = new UserDAL();
            return userDAL.GetById(idString);
        }

        public User ValidateConfirmationToken(string token)
        {
            UserDAL userDAL = new UserDAL();
            return userDAL.ValidateConfirmationToken(token);
        }

        public User updatePassword(string newPassword, string token)
        {
            UserDAL userDAL = new UserDAL();
            return userDAL.updatePassword(newPassword, token);
        }

        public User ChangePassword(UserDto userDto)
        {
            Crypto crypto = new Crypto();

            if (string.IsNullOrEmpty(userDto.Password) || string.IsNullOrEmpty(userDto.OldPassword))
            {
                throw new BussinessException(SystemErrors.InvalidUserCredentials);
            }


            UserDAL userDAL = new UserDAL();
            var user = userDAL.GetAccess(userDto.UserName, crypto.Encrypt(userDto.OldPassword, "o6806642kbM7c5"));

            if (user == null)
            {
                throw new BussinessException(SystemErrors.InvalidUserCredentials);
            }
            else
            {
                user.Password = userDto.Password;
                userDAL.ChangePassword(user);
                return user;
            }
        }


        public UserPermissions UpdateUserPermissions(UserPermissions userPermissions)
        {
            UserDAL userDAL = new UserDAL();
            userPermissions = GetPermissionData(userPermissions);
            return userDAL.UpdateUserPermissions(userPermissions);
        }

        public UserPermissions AddPermissions(UserPermissions userPermissions)
        {
            UserDAL userDAL = new UserDAL();
            userPermissions = GetPermissionData(userPermissions);

            return userDAL.AddUserPermissions(userPermissions);
        }

        public UserPermissions GetPermissionData(UserPermissions userPermissions)
        {

            if (userPermissions.Report_eventlog == false)
            {
                userPermissions.Report_eventlogMenuActive = false;
            }
            else
            {
                userPermissions.Report_eventlogMenuActive = true;
            }
            if (userPermissions.Report_trip == false)
            {
                userPermissions.Report_tripMenuActive = false;
            }
            else
            {
                userPermissions.Report_tripMenuActive = true;
            }
            if (userPermissions.Report_violation == false)
            {
                userPermissions.Report_violationMenuActive = false;
            }
            else
            {
                userPermissions.Report_violationMenuActive = true;
            }
            if (userPermissions.Rolesall == false && userPermissions.Rolesview == false)
            {
                userPermissions.RolesMenuActive = false;
            }
            else
            {
                userPermissions.RolesMenuActive = true;
            }
            if (userPermissions.Usersall == false && userPermissions.Usersview == false)
            {
                userPermissions.UsersMenuActive = false;
            }
            else
            {
                userPermissions.UsersMenuActive = true;
            }
            if (userPermissions.Vehiclesall == false && userPermissions.Vehiclesview == false)
            {
                userPermissions.VehiclesMenuActive = false;
            }
            else
            {
                userPermissions.VehiclesMenuActive = true;
            }
            if (userPermissions.Rolesall == false && userPermissions.Rolesview == false)
            {
                userPermissions.RolesMenuActive = false;
            }
            else
            {
                userPermissions.RolesMenuActive = true;
            }
            if (userPermissions.Dispatcherall == false && userPermissions.Dispatcherview == false)
            {
                userPermissions.DispatcherMenuActive = false;
            }
            else
            {
                userPermissions.DispatcherMenuActive = true;
            }
            if (userPermissions.Driverall == false && userPermissions.Driverview == false)
            {
                userPermissions.DriverMenuActive = false;
            }
            else
            {
                userPermissions.DriverMenuActive = true;
            }
            if (userPermissions.Deviceall == false && userPermissions.Deviceview == false)
            {
                userPermissions.DeviceMenuActive = false;
            }
            else
            {
                userPermissions.DeviceMenuActive = true;
            }
            return userPermissions;
        }

        public UserPermissions GetPermissionById(string id)
        {
            UserDAL userDAL = new UserDAL();
            return userDAL.GetPermissionById(id);
        }
    }
}
