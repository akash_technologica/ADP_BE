﻿using MongoDB.Bson;
using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using Raqeeb.Domain.Filters;
using Raqeeb.Domain.ViewModel;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class VehicleManager : ManagerBase
    {

        VehicleDAL VehicleDAL = new VehicleDAL();
        DriverDAL driverDal = new DriverDAL();
        GeofenceDAL geofenceDAL = new GeofenceDAL();
        LivePositionDAL livePositionDAL = new LivePositionDAL();
        public List<Vehicle> InsertBulk(List<Vehicle> Vehicles)
        {
            Vehicles = VehicleDAL.InsertBulk(Vehicles);
            return Vehicles;
        }

        public Vehicle GetByCode(int? code)
        {
            Vehicle result = new Vehicle();

            result = VehicleDAL.GetByCode(code);

            return result;
        }
        public VehicleSummary GetALL()
        {
            List<Domain.DataModel.Vehicle> result = VehicleDAL.GetAll();

            return GetVehicleSummary(result);
        }

        public VehicleSummary GetALL(int group)
        {
            List<Domain.DataModel.Vehicle> result = VehicleDAL.GetAll(group);

            return GetVehicleSummary(result);
        }

        private VehicleSummary GetVehicleSummary(List<Domain.DataModel.Vehicle> vehicles)
        {
            VehicleSummary vehicleSummary = new VehicleSummary();
            List<VehicleDto> vehicleDto = new List<VehicleDto>();
            GroupMasterDAL groupMasterDAL = new GroupMasterDAL();
            DateTime updateDate = new DateTime();

            foreach (var item in vehicles)
            {
                VehicleDto vehicleDtos = new VehicleDto()
                {
                    Registration = item.CarLicense,
                    ChasisNumber = item.ChasisNumber,
                    DeviceId = item.DeviceId,
                    Online = item.Online,
                    UpdateDate = item.LastCommandDate != null ? item.LastCommandDate.Value : updateDate,
                    Sim_IP = item.Sim_IP_Address,
                    PlateCode = item.PlateCode,
                    PlateNumber = item.PlateNumber,
                    InstallationDate = item.Installation_Date,
                    Callibration = item.Callibration,
                    GroupName = item.GruoupId != null ? groupMasterDAL.GetGroupName(item.GruoupId) : "",
                    Latitude = item.Latitude != null ? (double?)item.Latitude.Value : 0.0,
                    Longitude = item.Longitude != null ? (double?)item.Longitude.Value : 0.0,
                    Speed = item.Speed,
                    Idle = item.Idle,
                    EngineOn = item.EngineOn,


                };

                if (item.Idle.Value)
                {
                    vehicleSummary.Idle += 1;
                }
                else if (item.Speed > 0)
                {
                    vehicleSummary.Moving += 1;
                }
                else if (!item.EngineOn.Value)
                {
                    vehicleSummary.Stopped += 1;
                }
                vehicleDto.Add(vehicleDtos);
            }
            vehicleSummary.Vehicles = vehicleDto;
            vehicleSummary.TotalVehicles = vehicles.Count();

            return vehicleSummary;
        }

        public BusTreeViewModel GetVehicleTree()
        {
            GroupMasterDAL groupMasterDAL = new GroupMasterDAL();

            List<Domain.DataModel.Vehicle> vehicleList = VehicleDAL.GetAll();
            List<Domain.DataModel.GroupMaster> groupList = groupMasterDAL.GetAll();

            BusTreeViewModel busTreeViewModel = new BusTreeViewModel();

            List<ChildrenItem> childrenItemList = new List<ChildrenItem>();



            busTreeViewModel.leaf = false;
            busTreeViewModel.title = "root";



            foreach (var item in groupList)
            {
                ChildrenItem childrenItemObj = new ChildrenItem()
                {
                    title = item.GroupName,
                    groupId = item.GroupId,
                    leaf = false
                };

                childrenItemList.Add(childrenItemObj);
            }

            try
            {
                foreach (var child in childrenItemList)
                {
                    ChildrenItem childrenItem = new ChildrenItem();
                    List<ChildrenItems> childrenItemsList = new List<ChildrenItems>();

                    int count = 0;
                    List<Domain.DataModel.Vehicle> groupVehicles = vehicleList.Where(c => c.GruoupId == child.groupId).ToList();

                    foreach (var item in groupVehicles)
                    {
                        ChildrenItems childrenItems = new ChildrenItems();
                        childrenItems.leaf = true;
                        childrenItems.id = count;
                        childrenItems.isActive = true;
                        childrenItems.name = item.CarLicense;
                        childrenItems.title = item.CarLicense;

                        childrenItemsList.Add(childrenItems);
                        count++;
                    }

                    child.children = childrenItemsList;
                    //childrenItemList.Add(childrenItem);


                }

                List<SubChildrenItems> subChildrenList = new List<SubChildrenItems>();

                for (int i = 0; i <= 3; i++)
                {
                    switch (i)
                    {
                        case 0:
                            SubChildrenItems subChildrenItems = new SubChildrenItems()
                            {
                                id = i,
                                title = "Front",
                                leaf = true,
                                name = "Front",
                            };
                            subChildrenList.Add(subChildrenItems);
                            break;
                        case 1:
                            SubChildrenItems subChildrenItem2 = new SubChildrenItems()
                            {
                                id = i,
                                title = "Back",
                                leaf = true,
                                name = "Back",
                            };
                            subChildrenList.Add(subChildrenItem2);
                            break;
                        case 2:
                            SubChildrenItems subChildrenItem3 = new SubChildrenItems()
                            {
                                id = i,
                                title = "Side",
                                leaf = true,
                                name = "Side",
                            };
                            subChildrenList.Add(subChildrenItem3);
                            break;
                        case 3:
                            SubChildrenItems subChildrenItem4 = new SubChildrenItems()
                            {
                                id = i,
                                title = "Rear",
                                leaf = true,
                                name = "Rear",
                            };
                            subChildrenList.Add(subChildrenItem4);
                            break;

                        case 5: break;
                        case 6: break;
                    }

                }

                foreach (var item in childrenItemList)
                {

                    foreach (var childItem in item.children)
                    {
                        childItem.children = subChildrenList;
                    }

                }


                busTreeViewModel.children = childrenItemList;

                return busTreeViewModel;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public TreeRoot GetVehicleTreeModel()
        {
            GroupMasterDAL groupMasterDAL = new GroupMasterDAL();

            List<Domain.DataModel.Vehicle> vehicleList = VehicleDAL.GetAll();
            List<Domain.DataModel.GroupMaster> groupList = groupMasterDAL.GetAll();

            TreeRoot busTreeViewModel = new TreeRoot();
            List<DataItem> dataList = new List<DataItem>();
            DataItem data = new DataItem();

            List<ChildrenTreeItem> childrenItemList = new List<ChildrenTreeItem>();

            data.label = "Buses";
            data.data = "Documents Folder";
            data.expandedIcon = "pi pi-folder-open";
            data.collapsedIcon = "pi pi-folder";
            data.key = "Buses";
            foreach (var item in groupList)
            {
                ChildrenTreeItem childrenItemObj = new ChildrenTreeItem()
                {
                    label = item.GroupName,
                    groupId = item.GroupId,
                    leaf = false,
                    data = "Work Folder",
                    expandedIcon = "pi pi-folder-open",
                    collapsedIcon = "pi pi-folder",
                    key = item.GroupName
                };

                childrenItemList.Add(childrenItemObj);
            }

            try
            {
                foreach (var child in childrenItemList)
                {
                    ChildrenItem childrenItem = new ChildrenItem();
                    List<ChildrenTreeLastItem> childrenItemsList = new List<ChildrenTreeLastItem>();

                    int count = 0;
                    List<Domain.DataModel.Vehicle> groupVehicles = vehicleList.Where(c => c.GruoupId == child.groupId).ToList();

                    foreach (var item in groupVehicles)
                    {
                        ChildrenTreeLastItem childrenItems = new ChildrenTreeLastItem
                        {
                            leaf = true,
                            id = count,
                            isActive = true,
                            label = item.CarLicense,
                            data = item.CarLicense,
                            BackCameraUrl = Constant.STREAMAX_BASE_URL + ":" + Constant.PORT + "/" + "live.flv?devid=" + item.DeviceId + "&chl=" + Constant.BACK_CAMERA_CHANNEL + "&st=0&audio=0",
                            FrontCameraUrl = Constant.STREAMAX_BASE_URL + ":" + Constant.PORT + "/" + "live.flv?devid=" + item.DeviceId + "&chl=" + Constant.FRONT_CAMERA_CHANNEL + "&st=0&audio=0",
                            SideCameraUrl = Constant.STREAMAX_BASE_URL + ":" + Constant.PORT + "/" + "live.flv?devid=" + item.DeviceId + "&chl=" + Constant.SIDE_CAMERA_CHANNEL + "&st=0&audio=0",
                            RearCameraUrl = Constant.STREAMAX_BASE_URL + ":" + Constant.PORT + "/" + "live.flv?devid=" + item.DeviceId + "&chl=" + Constant.REAR_CAMERA_CHANNEL + "&st=0&audio=0",
                            Online = item.Online,
                            icon = "pi pi-file",
                            key = item.CarLicense
                        };

                        childrenItemsList.Add(childrenItems);
                        count++;
                    }

                    child.children = childrenItemsList;
                    //childrenItemList.Add(childrenItem);


                }


                data.children = childrenItemList;

                // busTreeViewModel.children = childrenItemList;
                dataList.Add(data);
                busTreeViewModel.data = dataList;

                return busTreeViewModel;
            }
            catch (Exception ex)
            {

                return busTreeViewModel;
            }

        }



        public List<Domain.DataModel.Vehicle> Search(SearchCriteria<VehicleFilter> searchCriteria, int IncludedTime = 0)
        {
            return VehicleDAL.Search(searchCriteria, IncludedTime);
        }

        public List<Vehicle> GetVehiclesByStatus(bool idle, bool reporting)
        {
            return VehicleDAL.GetVehiclesByStatus(idle, reporting);
        }

        public List<VehicleDto> GetAllLVehiclesLocations()
        {
            List<VehicleDto> result = new List<VehicleDto>();
            List<Domain.DataModel.Vehicle> vehicles = VehicleDAL.GetAll();
            if (vehicles != null && vehicles.Count > 0)
            {
                for (int i = 0; i < vehicles.Count; i++)
                {
                    VehicleDto vehicle = new VehicleDto();
                    Domain.DataModel.LivePosition livePosition = livePositionDAL.GetByCode(vehicles[i].CarLicense);

                    vehicle.Code = 58133;//vehicles[i].CarLicense;
                    vehicle.LastOdoMeter = null;// vehicles[i].LastOdoMeter;
                    vehicle.Registration = vehicles[i].CarLicense;
                    vehicle.UpdateDate = vehicles[i].LastCommandDate.Value;

                    vehicle.Latitude = (double?)livePosition.Latitude;
                    vehicle.Longitude = (double?)livePosition.Longitude;
                    vehicle.LocationTIme = livePosition.LocationTIme.Value;
                    vehicle.Speed = livePosition.Speed;
                    vehicle.Reporting = true;
                    vehicle.VehicleStatus = "Active";
                    vehicle.Idle = false;
                    result.Add(vehicle);
                    //vehicle.Prm = vehicles[i].RPM;
                    //vehicle.CurrentEvents = vehicles[i].CurrentEvents;

                    //if (vehicles[i].Reporting == null)
                    //{
                    //    vehicle.Reporting = false;
                    //}
                    //else
                    //{
                    //    vehicle.Reporting = vehicles[i].Reporting;
                    //}

                    //vehicle.EngineOn = vehicles[i].EngineOn;
                    //vehicle.Idle = vehicles[i].Idle;
                    //vehicle.FMS_Serial_Number = vehicles[i].FMS_Serial_Number;
                    //vehicle.SM_Serial_Number = vehicles[i].SM_Serial_Number;
                    //vehicle.VehicleStatus = vehicles[i].VehicleStatus;

                    //if (vehicles[i].Counter.HasValue)
                    //{
                    //    vehicle.Counter = vehicles[i].Counter;
                    //}
                    //else
                    //{
                    //    vehicle.Counter = 0;
                    //}
                    //vehicle.IsGeofenceEnabled = vehicles[i].IsGeofenceViolation;


                    //if(vehicles[i].VehicleStatus == "Active")
                    //{
                    //    if(vehicles[i].DriverCode != null)
                    //    {
                    //        vehicle.DriverName = driverDal.GetByCode(vehicles[i].DriverCode.Value).Name;
                    //    }
                    //    vehicle.EventCount = vehicles[i].EventCount;
                    //    result.Add(vehicle);
                    //}

                }

            }
            return result;


        }

        public List<VehicleDto> GetGeofencing(string code)
        {
            List<Domain.DataModel.Vehicle> vehicles = VehicleDAL.GetAll();
            List<VehicleDto> geofencingVehicles = new List<VehicleDto>();
            List<Geofence> geofenceList = geofenceDAL.GetAllGeofence();


            foreach (var item in vehicles)
            {
                var geofenceItem = geofenceList.Where(c => c.IdString == code).FirstOrDefault(topList =>
                                                  topList.VehicleCodes.Any());

                VehicleDto vehicleDto = new VehicleDto()
                {
                    //Code = item.Code,
                    //Registration = item.Registration,
                    //Latitude = item.Latitude,
                    //Longitude = item.Longitude
                };

                if (geofenceItem != null)
                {
                    vehicleDto.IsGeofenceEnabled = true;
                }
                else
                {
                    vehicleDto.IsGeofenceEnabled = false;
                }
                geofencingVehicles.Add(vehicleDto);


            }
            return geofencingVehicles;


        }

        public List<VehicleUtilization> GetStatistics(SearchCriteria<VehicleFilter> searchCriteria)
        {
            List<VehicleUtilization> vehicleScoreList = new List<VehicleUtilization>();

            foreach (var item in searchCriteria.Filters.VehicleCode)
            {
                Vehicle vehicle = VehicleDAL.GetByCode(item);
                VehicleUtilization score = VehicleDAL.GetVehicleStatisticsByCode(searchCriteria, item);
                if (score != null)
                {
                    score.Registration = vehicle.Registration;
                    vehicleScoreList.Add(score);
                }

            }
            return vehicleScoreList;
            //return VehicleDAL.GetVehicleStatisticsByCode(searchCriteria);
        }

        public List<VehicleUtilization> CriticalVehicleByMonth(int month, int year)
        {
            return VehicleDAL.CriticalVehicleByMonth(month, year);
        }

        //public List<VehicleDto> GetVehiclesEventsByClassificationId(int classificationId)
        //{
        //    // get all live positions/Pagination 
        //    LivePositionManager livePositionManager = new LivePositionManager();
        //    SetupManager setupManager = new SetupManager();
        //    VehicleManager vehicleManager = new VehicleManager();
        //    EventManager eventManager = new EventManager();

        //    List<LivePosition> livePositions = new List<LivePosition>();
        //    List<VehicleDto> result = new List<VehicleDto>();
        //    Setup currentSetup = new Setup();
        //    VehicleDto vehicleDto = null;

        //    livePositions = livePositionManager.GetAllByType((int)PositionType.Vehicle);

        //    List<Setup> setupArr = setupManager.GetAll();

        //    currentSetup = setupArr.Where(a => (int)a.ClassificationType == classificationId).FirstOrDefault();


        //    if (livePositions.Count > 0)
        //    {
        //        for (int i = 0; i < livePositions.Count; i++)
        //        {
        //            int? LivePositionVehicleCode = livePositions[i].VehicleCode;
        //            Vehicle vehicle = vehicleManager.GetByCode(LivePositionVehicleCode);

        //            List<Event> vehicleEvents = eventManager.GetEventsByVehcileCodeForDashboard(LivePositionVehicleCode, null, null, null);
        //            if (classificationId == (int)EventClassification.Ideal && livePositions[i].Speed == 0 && livePositions[i].Prm == 0)
        //            {
        //                //Ideal 
        //                vehicleDto = new VehicleDto();
        //                vehicleDto.Code = vehicle.Code;
        //                vehicleDto.LastOdoMeter = vehicle.LastOdoMeter;
        //                vehicleDto.Registration = vehicle.Registration;
        //                vehicleDto.Events = vehicleEvents;
        //                vehicleDto.TotalVehicleEvents = vehicleEvents.Count;
        //                vehicleDto.Latitude = livePositions[i].Latitude;
        //                vehicleDto.Longitude = livePositions[i].Longitude;
        //                vehicleDto.Speed = livePositions[i].Speed;
        //                vehicleDto.Prm = livePositions[i].Prm;

        //                result.Add(vehicleDto);
        //            }
        //            else if (classificationId == (int)EventClassification.Normal && livePositions[i].Speed == 0 && livePositions[i].Prm > 0 && vehicleEvents.Count > 0)
        //            {
        //                // Normal  
        //                vehicleDto = new VehicleDto();
        //                vehicleDto.Code = vehicle.Code;
        //                vehicleDto.LastOdoMeter = vehicle.LastOdoMeter;
        //                vehicleDto.Registration = vehicle.Registration;
        //                vehicleDto.Events = vehicleEvents;
        //                vehicleDto.TotalVehicleEvents = vehicleEvents.Count;
        //                vehicleDto.Latitude = livePositions[i].Latitude;
        //                vehicleDto.Longitude = livePositions[i].Longitude;
        //                vehicleDto.Speed = livePositions[i].Speed;
        //                vehicleDto.Prm = livePositions[i].Prm;
        //                result.Add(vehicleDto);
        //            }
        //            else if (vehicleEvents.Count > 0)
        //            {
        //                vehicleDto = new VehicleDto();
        //                vehicleDto.Code = vehicle.Code;
        //                vehicleDto.LastOdoMeter = vehicle.LastOdoMeter;
        //                vehicleDto.Registration = vehicle.Registration;
        //                vehicleDto.Events = vehicleEvents;
        //                vehicleDto.TotalVehicleEvents = vehicleEvents.Count;
        //                vehicleDto.Latitude = livePositions[i].Latitude;
        //                vehicleDto.Longitude = livePositions[i].Longitude;
        //                vehicleDto.Speed = livePositions[i].Speed;
        //                vehicleDto.Prm = livePositions[i].Prm;
        //                DashboardManager dashoardManager = new DashboardManager();
        //                bool vehicleEventClassification = dashoardManager.CheckVehicleEventsClassification(vehicleEvents, currentSetup, livePositions[i], setupArr);
        //                if (vehicleEventClassification)
        //                {
        //                    result.Add(vehicleDto);
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}


        public List<VehicleDto> GetVehiclesEventsByClassificationId(VehicleFilter vehicleFilter)
        {
            LivePositionManager livePositionManager = new LivePositionManager();
            SetupManager setupManager = new SetupManager();
            EventManager eventManager = new EventManager();
            VehicleManager vehicleManager = new VehicleManager();
            DashboardManager dashboardManager = new DashboardManager();


            // get all live positions/Pagination 
            PagedSearchResultMongo<LivePosition> livePositions = new PagedSearchResultMongo<LivePosition>();
            List<VehicleDto> result = new List<VehicleDto>();
            Setup currentSetup = new Setup();
            VehicleDto vehicleDto = null;

            SearchCriteria<LivePositionFilter> searchCriteria = new SearchCriteria<LivePositionFilter>();
            searchCriteria.Filters = new LivePositionFilter();
            searchCriteria.Filters.Type = (int)PositionType.Vehicle;


            if (!string.IsNullOrEmpty(vehicleFilter.Shift))
            {
                if (vehicleFilter.Shift.ToLower() == "night")
                {
                    searchCriteria.Filters.StartDate = new DateTime();
                    searchCriteria.Filters.EndDate = new DateTime();

                    searchCriteria.Filters.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
                    searchCriteria.Filters.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0).AddDays(1);
                }
                else if (vehicleFilter.Shift.ToLower() == "day")
                {
                    searchCriteria.Filters.StartDate = new DateTime();
                    searchCriteria.Filters.EndDate = new DateTime();
                    searchCriteria.Filters.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0);
                    searchCriteria.Filters.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
                }
                livePositions = livePositionManager.Search(searchCriteria, Convert.ToInt32(ConfigurationManager.AppSettings["DecreasedTimeFromBackEnd"]));
            }
            else
            {
                searchCriteria.Filters.StartDate = vehicleFilter.StartDate;
                searchCriteria.Filters.EndDate = vehicleFilter.EndDate;
                livePositions = livePositionManager.Search(searchCriteria);

            }


            List<Setup> setupArr = setupManager.GetAll();

            //  currentSetup = setupManager.GetByClassificationEventTyp(vehicleFilter.ClassificationEventType.Value);
            currentSetup = setupArr.Where(a => (int)a.ClassificationType == vehicleFilter.ClassificationEventType.Value).FirstOrDefault();


            // }
            if (livePositions.Collection.Count > 0)
            {
                for (int i = 0; i < livePositions.Collection.Count; i++)
                {
                    int? LivePositionVehicleCode = livePositions.Collection[i].VehicleCode;
                    Vehicle vehicle = vehicleManager.GetByCode(LivePositionVehicleCode);

                    List<Event> vehicleEvents = eventManager.GetEventsByVehcileCodeForDashboard(LivePositionVehicleCode, vehicleFilter.StartDate, vehicleFilter.EndDate, vehicleFilter.Shift);
                    if (vehicleFilter.ClassificationEventType == (int)EventClassification.Ideal && livePositions.Collection[i].Speed == 0 && livePositions.Collection[i].Prm == 0)
                    {
                        //Ideal 
                        vehicleDto = new VehicleDto();
                        vehicleDto.Code = vehicle.Code;
                        vehicleDto.LastOdoMeter = vehicle.LastOdoMeter;
                        vehicleDto.Registration = vehicle.Registration;
                        vehicleDto.Events = vehicleEvents;
                        vehicleDto.TotalVehicleEvents = vehicleEvents.Count;
                        vehicleDto.Latitude = livePositions.Collection[i].Latitude;
                        vehicleDto.Longitude = livePositions.Collection[i].Longitude;
                        vehicleDto.Speed = livePositions.Collection[i].Speed;
                        vehicleDto.Prm = livePositions.Collection[i].Prm;

                        result.Add(vehicleDto);
                    }
                    else if (vehicleFilter.ClassificationEventType == (int)EventClassification.Normal && livePositions.Collection[i].Speed == 0 && livePositions.Collection[i].Prm > 0 && vehicleEvents.Count > 0)
                    {
                        // Normal  
                        vehicleDto = new VehicleDto();
                        vehicleDto.Code = vehicle.Code;
                        vehicleDto.LastOdoMeter = vehicle.LastOdoMeter;
                        vehicleDto.Registration = vehicle.Registration;
                        vehicleDto.Events = vehicleEvents;
                        vehicleDto.TotalVehicleEvents = vehicleEvents.Count;
                        vehicleDto.Latitude = livePositions.Collection[i].Latitude;
                        vehicleDto.Longitude = livePositions.Collection[i].Longitude;
                        vehicleDto.Speed = livePositions.Collection[i].Speed;
                        vehicleDto.Prm = livePositions.Collection[i].Prm;
                        result.Add(vehicleDto);
                    }
                    else if (vehicleEvents.Count > 0)
                    {
                        vehicleDto = new VehicleDto();
                        vehicleDto.Code = vehicle.Code;
                        vehicleDto.LastOdoMeter = vehicle.LastOdoMeter;
                        vehicleDto.Registration = vehicle.Registration;
                        vehicleDto.Events = vehicleEvents;
                        vehicleDto.TotalVehicleEvents = vehicleEvents.Count;
                        vehicleDto.Latitude = livePositions.Collection[i].Latitude;
                        vehicleDto.Longitude = livePositions.Collection[i].Longitude;
                        vehicleDto.Speed = livePositions.Collection[i].Speed;
                        vehicleDto.Prm = livePositions.Collection[i].Prm;

                        bool vehicleEventClassification = dashboardManager.CheckVehicleEventsClassification(vehicleEvents, currentSetup, livePositions.Collection[i], setupArr);
                        if (vehicleEventClassification)
                        {
                            result.Add(vehicleDto);
                        }
                    }
                }
            }
            return result;
        }













        public List<BsonDocument> GetALlVehiclesLocations_v2()
        {
            List<BsonDocument> result = new List<BsonDocument>();

            result = VehicleDAL.GetALlVehiclesLocations_v2();

            return result;
        }

        public Vehicle GetByRegistration(string registration)
        {
            Vehicle result = new Vehicle();

            result = VehicleDAL.GetByRegistration(registration);

            return result;
        }

        public Vehicle UpdateVehicle(Vehicle vehicleData)
        {
            Vehicle result = new Vehicle();

            result = VehicleDAL.UpdateVehicle(vehicleData);

            return result;
        }
        public Vehicle Add(Vehicle vehicle)
        {
            return VehicleDAL.InsertOne(vehicle);
        }
        public void UpdateVehicleClassification(Vehicle vehicleData)
        {
            Vehicle result = new Vehicle();

            result = VehicleDAL.UpdateVehicleClassification(vehicleData);


        }

        public VehicleUtilization CheckVehicleStatisticsExists(int? code, int month, int year)
        {
            return VehicleDAL.CheckVehicleStatisticsExists(code, month, year);
        }

        public void UpdateVehicleStatistics(VehicleUtilization scoreitem)
        {
            VehicleDAL.UpdateVehicleStatistics(scoreitem);
        }

        public void InsertwVehicleStatistics(VehicleUtilization vehicleScore)
        {
            VehicleDAL.InsertwVehicleStatistics(vehicleScore);
        }

        public List<Domain.DataModel.Vehicle> GetVehiclesByGroupId(int groupId)
        {
            return VehicleDAL.GetVechilsByGroupId(groupId);
        }


        internal VehicleSummary GetVehicleByGroup(int group)
        {
            throw new NotImplementedException();
        }
    }
}









//public List<VehicleDto> GetAllLVehiclesLocations()
//{
//    List<VehicleDto> result = new List<VehicleDto>();
//    List<Vehicle> vehicles = VehicleDAL.GetAll();
//    LivePositionManager positionManager = new LivePositionManager();
//    int positiontype = (int)PositionType.Vehicle;
//    List<LivePosition> positions = positionManager.GetAllByType(positiontype);

//    if (vehicles != null && vehicles.Count > 0 && positions != null && positions.Count > 0)
//    {
//        for (int i = 0; i < vehicles.Count; i++)
//        {
//            VehicleDto vehicle = new VehicleDto();
//            vehicle.Code = vehicles[i].Code;
//            vehicle.LastOdoMeter = vehicles[i].LastOdoMeter;
//            vehicle.Registration = vehicles[i].Registration;
//            vehicle.UpdateDate = vehicles[i].UpdateDate;
//            for (int j = 0; j < positions.Count; j++)
//            {
//                if (positions[j].VehicleCode == vehicles[i].Code)
//                {

//                    vehicle.Latitude = positions[j].Latitude;
//                    vehicle.Longitude = positions[j].Longitude;
//                    vehicle.LocationTIme = positions[j].LocationTIme;
//                }
//                result.Add(vehicle);
//            }

//        }

//    }
//    return result;


//}