﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class VehicleBrandMasterManager : ManagerBase
    {
        readonly VehicleBrandDAL vehicleBrandDAL;
        public VehicleBrandMasterManager()
        {
            vehicleBrandDAL = new VehicleBrandDAL();
        }

        public List<VehicleBrandMaster> GetAll()
        {
            var list = vehicleBrandDAL.GetAll();
            return list;
        }

        public VehicleBrandMaster Add(VehicleBrandMaster brand)
        {
            return vehicleBrandDAL.Add(brand);
        }

        public VehicleBrandMaster Delete(VehicleBrandMaster brand)
        {
            return vehicleBrandDAL.Delete(brand);
        }

        public VehicleBrandMaster Update(VehicleBrandMaster brand)
        {

            return vehicleBrandDAL.Update(brand);
        }

        public List<Raqeeb.Domain.DataModel.VehicleBrandMaster> GetBrandList()
        {
            return vehicleBrandDAL.GetBrandList();
            
        }
    }
}
