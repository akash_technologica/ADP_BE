﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class SeeingMachineManager
    {
        Seeing_Machine_Data_DAL sm_data = new Seeing_Machine_Data_DAL();

        public List<Seeing_Machine_Data> InsertBulk(List<Seeing_Machine_Data> contractors)
        {

            contractors = sm_data.InsertBulk(contractors);

            return contractors;
        }

        public List<Seeing_Machine_Data> GetList()
        {
            List<Seeing_Machine_Data> Seeing_Machine_Data = new List<Seeing_Machine_Data>();
            Seeing_Machine_Data = sm_data.GetAll();
            return Seeing_Machine_Data;
        }

        public Seeing_Machine_Data GetSM_DataByCodeAndTime(string registration, DateTime? startDate, DateTime? endDate)
        {
            Seeing_Machine_Data _data = sm_data.GetSM_DataByCodeAndTime(registration, startDate, endDate);
            return _data;
        }
    }
}
