﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class DashboardManager : ManagerBase
    {
        EventDAL eventDAL;
        TripDAL tripDAL;
        DriverDAL driverDAL;
        ScoreSetUpDAL scoresetupDAL;
        LivePositionDAL livepositionDAL;
        VehicleManager vehicleManager;
        DashboardDAL dashboardDAL;
        VehicleDAL vehicleDAL;
        DeviceDAL deviceDAL;
        public DashboardManager()
        {
            eventDAL = new EventDAL();
            tripDAL = new TripDAL();
            driverDAL = new DriverDAL();
            scoresetupDAL = new ScoreSetUpDAL();
            livepositionDAL = new LivePositionDAL();
            dashboardDAL = new DashboardDAL();
            vehicleManager = new VehicleManager();
            vehicleDAL = new VehicleDAL();
            deviceDAL = new DeviceDAL();
        }

        string[] monthNames =
         System.Globalization.CultureInfo.CurrentCulture
        .DateTimeFormat.MonthGenitiveNames;


        ObjectCache cache = MemoryCache.Default;
        public VehicleSummary GetDashboardData(VehicleFilter vehicleFilter)
        {
            // get vehicles 
            VehicleSummary vehicleList;
            vehicleList = vehicleManager.GetALL();
            //if (cache.Contains(Constant.VehicleCacheKey))
            //    vehicleList = (List<Vehicle>)cache.Get(Constant.VehicleCacheKey);
            //else
            //{
                

            //    cache.Set(Constant.VehicleCacheKey, vehicleList, new CacheItemPolicy() { RemovedCallback = new CacheEntryRemovedCallback(CacheRemovedCallback) /* your other parameters here */});
                
            //    CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            //    cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(1);
            //    cache.Add(Constant.VehicleCacheKey, vehicleList, cacheItemPolicy);
            //    cacheItemPolicy.RemovedCallback = new CacheEntryRemovedCallback(CacheRemovedCallback);

            //}

            List<VehicleDto> result = new List<VehicleDto>();
            // get all live positions/Pagination 
            VehicleDto vehicleDto = null;

            //foreach (var vehicleItem in vehicleList)
            //{           
            //        if (vehicleItem.Critical != null && vehicleItem.Critical == true)
            //        {
            //            vehicleDto = new VehicleDto();
            //            vehicleDto.Code = vehicleItem.Code;
            //            vehicleDto.Registration = vehicleItem.Registration;
            //            vehicleDto.EventCount = vehicleItem.EventCount;
            //            vehicleDto.VehicleState = (int)EventClassification.Critical;
            //            result.Add(vehicleDto);
            //        }

                
              
            //        if (vehicleItem.Elevated != null && vehicleItem.Elevated == true)
            //        {
            //            vehicleDto = new VehicleDto();
            //            vehicleDto.Code = vehicleItem.Code;
            //            vehicleDto.Registration = vehicleItem.Registration;
            //            vehicleDto.EventCount = vehicleItem.EventCount;
            //            vehicleDto.VehicleState = (int)EventClassification.Elivated;
            //            result.Add(vehicleDto);
            //        }
                
                
            //        if (vehicleItem.VehicleStatus == "Active" && vehicleItem.Normal == true && (vehicleItem.Reporting == true || vehicleItem.EventCount >= 0))
            //        {
            //            vehicleDto = new VehicleDto();
            //            vehicleDto.Code = vehicleItem.Code;
            //            vehicleDto.Registration = vehicleItem.Registration;
            //            vehicleDto.VehicleState = (int)EventClassification.Normal;
            //            vehicleDto.EventCount = vehicleItem.EventCount;
            //            result.Add(vehicleDto);
            //        }
                
                
            //        if (vehicleItem.VehicleStatus == "Active" && (vehicleItem.Normal == null || vehicleItem.Normal == false)
            //            && (vehicleItem.Critical == null || vehicleItem.Critical == false) && (vehicleItem.Elevated == null || vehicleItem.Elevated == false))

            //        {
            //            vehicleDto = new VehicleDto();
            //            vehicleDto.Code = vehicleItem.Code;
            //            vehicleDto.Registration = vehicleItem.Registration;
            //            vehicleDto.VehicleState = (int)EventClassification.Ideal;
            //            vehicleDto.EventCount = vehicleItem.EventCount;
            //            result.Add(vehicleDto);
            //        }              

            //}

            return vehicleList;
        }

        private void CacheRemovedCallback(CacheEntryRemovedArguments arguments)
        {
            VehicleSummary vehicleList;
            vehicleList = vehicleManager.GetALL();
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(15);
            cache.Add(Constant.VehicleCacheKey, vehicleList, cacheItemPolicy);
        }

        public List<VehicleDto> GetElevatedData(VehicleFilter vehicleFilter)
        {
            LivePositionManager livePositionManager = new LivePositionManager();
            SetupManager setupManager = new SetupManager();
            EventManager eventManager = new EventManager();
            VehicleManager vehicleManager = new VehicleManager();

            List<Vehicle> vehicleList = vehicleManager.GetVehiclesByStatus(false, true); // idle, reporting

            // get all live positions/Pagination 
            PagedSearchResultMongo<LivePosition> livePositions = new PagedSearchResultMongo<LivePosition>();
            List<VehicleDto> result = new List<VehicleDto>();
            Setup currentSetup = new Setup();
            VehicleDto vehicleDto = null;

            SearchCriteria<LivePositionFilter> searchCriteria = new SearchCriteria<LivePositionFilter>();
            searchCriteria.Filters = new LivePositionFilter();
            searchCriteria.Filters.Type = (int)PositionType.Vehicle;




            if (!string.IsNullOrEmpty(vehicleFilter.Shift))
            {
                if (vehicleFilter.Shift.ToLower() == "night")
                {
                    searchCriteria.Filters.StartDate = new DateTime();
                    searchCriteria.Filters.EndDate = new DateTime();

                    searchCriteria.Filters.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
                    searchCriteria.Filters.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0).AddDays(1);
                }
                else if (vehicleFilter.Shift.ToLower() == "day")
                {
                    searchCriteria.Filters.StartDate = new DateTime();
                    searchCriteria.Filters.EndDate = new DateTime();
                    searchCriteria.Filters.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0);
                    searchCriteria.Filters.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
                }
                //    livePositions = livePositionManager.Search(searchCriteria, Convert.ToInt32(ConfigurationManager.AppSettings["DecreasedTimeFromBackEnd"]));
            }
            else
            {
                searchCriteria.Filters.StartDate = vehicleFilter.StartDate;
                searchCriteria.Filters.EndDate = vehicleFilter.EndDate;
                livePositions = livePositionManager.Search(searchCriteria);

            }

            List<Setup> setupArr = setupManager.GetAll();

            //  currentSetup = setupManager.GetByClassificationEventTyp(vehicleFilter.ClassificationEventType.Value);
            currentSetup = setupArr.Where(a => (int)a.ClassificationType == vehicleFilter.ClassificationEventType.Value).FirstOrDefault();



            PagedSearchResultMongo<LivePosition> livePositionsnew = new PagedSearchResultMongo<LivePosition>();
            livePositionsnew = livePositionManager.Search(searchCriteria, Convert.ToInt32(ConfigurationManager.AppSettings["DecreasedTimeFromBackEnd"]));



            foreach (var vehicleItem in vehicleList)
            {
                List<Event> vehicleEvents = eventManager.GetEventsByVehcileCodeForDashboard(vehicleItem.Code, searchCriteria.Filters.StartDate, searchCriteria.Filters.EndDate, vehicleFilter.Shift);
                // searchCriteria.Filters.VehicleCode = vehicleItem.Code;
                //long fatigueCount = eventManager.GetEventCountByEventTypeCode(searchCriteria, (int)EventType.Driver_Fatigue, vehicleFilter.Shift);
                //long FOVCount = eventManager.GetEventCountByEventTypeCode(searchCriteria, (int)EventType.FOV_Exception, vehicleFilter.Shift);
                //long DistractionCount = eventManager.GetEventCountByEventTypeCode(searchCriteria, (int)EventType.Distraction, vehicleFilter.Shift);
                if (vehicleEvents.Count > 0)
                {
                    bool vehicleEventClassification = CheckVehicleEventsClassification(vehicleEvents, currentSetup, livePositionsnew.Collection[0], setupArr);
                    if (vehicleFilter.ClassificationEventType == (int)EventClassification.Elivated && vehicleEventClassification && vehicleItem.Reporting.Value && vehicleItem.Counter < 12)
                    {
                        vehicleDto = new VehicleDto();
                        vehicleDto.Code = vehicleItem.Code;
                        vehicleDto.Registration = vehicleItem.Registration;
                        result.Add(vehicleDto);
                    }
                }





            }
            return result;
        }
        public List<VehicleDto> GetNormalData(VehicleFilter vehicleFilter)
        {
            LivePositionManager livePositionManager = new LivePositionManager();
            SetupManager setupManager = new SetupManager();
            EventManager eventManager = new EventManager();
            VehicleManager vehicleManager = new VehicleManager();

            List<Vehicle> vehicleList = vehicleManager.GetVehiclesByStatus(false, true); // idle, reporting

            // get all live positions/Pagination 
            PagedSearchResultMongo<LivePosition> livePositions = new PagedSearchResultMongo<LivePosition>();
            List<VehicleDto> result = new List<VehicleDto>();
            Setup currentSetup = new Setup();
            VehicleDto vehicleDto = null;

            SearchCriteria<LivePositionFilter> searchCriteria = new SearchCriteria<LivePositionFilter>();
            searchCriteria.Filters = new LivePositionFilter();
            searchCriteria.Filters.Type = (int)PositionType.Vehicle;




            if (!string.IsNullOrEmpty(vehicleFilter.Shift))
            {
                if (vehicleFilter.Shift.ToLower() == "night")
                {
                    searchCriteria.Filters.StartDate = new DateTime();
                    searchCriteria.Filters.EndDate = new DateTime();

                    searchCriteria.Filters.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
                    searchCriteria.Filters.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0).AddDays(1);
                }
                else if (vehicleFilter.Shift.ToLower() == "day")
                {
                    searchCriteria.Filters.StartDate = new DateTime();
                    searchCriteria.Filters.EndDate = new DateTime();
                    searchCriteria.Filters.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0);
                    searchCriteria.Filters.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
                }
                //    livePositions = livePositionManager.Search(searchCriteria, Convert.ToInt32(ConfigurationManager.AppSettings["DecreasedTimeFromBackEnd"]));
            }
            else
            {
                searchCriteria.Filters.StartDate = vehicleFilter.StartDate;
                searchCriteria.Filters.EndDate = vehicleFilter.EndDate;
                livePositions = livePositionManager.Search(searchCriteria);

            }

            List<Setup> setupArr = setupManager.GetAll();

            //  currentSetup = setupManager.GetByClassificationEventTyp(vehicleFilter.ClassificationEventType.Value);
            currentSetup = setupArr.Where(a => (int)a.ClassificationType == vehicleFilter.ClassificationEventType.Value).FirstOrDefault();



            PagedSearchResultMongo<LivePosition> livePositionsnew = new PagedSearchResultMongo<LivePosition>();
            livePositionsnew = livePositionManager.Search(searchCriteria, Convert.ToInt32(ConfigurationManager.AppSettings["DecreasedTimeFromBackEnd"]));

            foreach (var vehicleItem in vehicleList)
            {
                List<Event> vehicleEvents = eventManager.GetEventsByVehcileCodeForDashboard(vehicleItem.Code, searchCriteria.Filters.StartDate, searchCriteria.Filters.EndDate, vehicleFilter.Shift);
                // searchCriteria.Filters.VehicleCode = vehicleItem.Code;
                //long fatigueCount = eventManager.GetEventCountByEventTypeCode(searchCriteria, (int)EventType.Driver_Fatigue, vehicleFilter.Shift);
                //long FOVCount = eventManager.GetEventCountByEventTypeCode(searchCriteria, (int)EventType.FOV_Exception, vehicleFilter.Shift);
                //long DistractionCount = eventManager.GetEventCountByEventTypeCode(searchCriteria, (int)EventType.Distraction, vehicleFilter.Shift);
                if (vehicleEvents.Count > 0)
                {
                    bool vehicleEventClassification = CheckVehicleEventsClassification(vehicleEvents, currentSetup, livePositionsnew.Collection[0], setupArr);
                    if (vehicleFilter.ClassificationEventType == (int)EventClassification.Normal && !vehicleEventClassification && vehicleItem.Reporting.Value && vehicleItem.Counter < 12)
                    {
                        vehicleDto = new VehicleDto();
                        vehicleDto.Code = vehicleItem.Code;
                        vehicleDto.Registration = vehicleItem.Registration;
                        result.Add(vehicleDto);
                    }
                }



            }










            return result;
        }
        public VehicleSummary GetIdleData(VehicleFilter vehicleFilter)
        {
            LivePositionManager livePositionManager = new LivePositionManager();
            SetupManager setupManager = new SetupManager();
            EventManager eventManager = new EventManager();
            VehicleManager vehicleManager = new VehicleManager();

            VehicleSummary vehicleList = vehicleManager.GetALL();

            // get all live positions/Pagination 
            PagedSearchResultMongo<LivePosition> livePositions = new PagedSearchResultMongo<LivePosition>();
            List<VehicleDto> result = new List<VehicleDto>();
            Setup currentSetup = new Setup();
            VehicleDto vehicleDto = null;

            SearchCriteria<LivePositionFilter> searchCriteria = new SearchCriteria<LivePositionFilter>();
            searchCriteria.Filters = new LivePositionFilter();
            searchCriteria.Filters.Type = (int)PositionType.Vehicle;




            if (!string.IsNullOrEmpty(vehicleFilter.Shift))
            {
                if (vehicleFilter.Shift.ToLower() == "night")
                {
                    searchCriteria.Filters.StartDate = new DateTime();
                    searchCriteria.Filters.EndDate = new DateTime();

                    searchCriteria.Filters.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
                    searchCriteria.Filters.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0).AddDays(1);
                }
                else if (vehicleFilter.Shift.ToLower() == "day")
                {
                    searchCriteria.Filters.StartDate = new DateTime();
                    searchCriteria.Filters.EndDate = new DateTime();
                    searchCriteria.Filters.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0);
                    searchCriteria.Filters.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
                }
                //    livePositions = livePositionManager.Search(searchCriteria, Convert.ToInt32(ConfigurationManager.AppSettings["DecreasedTimeFromBackEnd"]));
            }
            else
            {
                searchCriteria.Filters.StartDate = vehicleFilter.StartDate;
                searchCriteria.Filters.EndDate = vehicleFilter.EndDate;
                livePositions = livePositionManager.Search(searchCriteria);

            }

            List<Setup> setupArr = setupManager.GetAll();

            //  currentSetup = setupManager.GetByClassificationEventTyp(vehicleFilter.ClassificationEventType.Value);
            currentSetup = setupArr.Where(a => (int)a.ClassificationType == vehicleFilter.ClassificationEventType.Value).FirstOrDefault();



            PagedSearchResultMongo<LivePosition> livePositionsnew = new PagedSearchResultMongo<LivePosition>();
            livePositionsnew = livePositionManager.Search(searchCriteria, Convert.ToInt32(ConfigurationManager.AppSettings["DecreasedTimeFromBackEnd"]));



            // idle vehicle
            //if (vehicleFilter.ClassificationEventType == (int)EventClassification.Ideal)
            //{
            //    foreach (var vehicleItem in vehicleList)
            //    {
            //        if (vehicleItem.Reporting == false || vehicleItem.Idle == true || vehicleItem.Reporting == null || vehicleItem.Idle == null)
            //        {
            //            vehicleDto = new VehicleDto();
            //            vehicleDto.Code = vehicleItem.Code;
            //            vehicleDto.Registration = vehicleItem.Registration;
            //            result.Add(vehicleDto);
            //        }


            //    }
            //}
            // idle vehicle end










            return vehicleList;
        }
        public bool CheckVehicleEventsClassification(List<Event> events, Setup currnetSetup, LivePosition livePositions, List<Setup> setupArr)
        {
            int totalFatigueEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Driver_Fatigue)).Count();
            int totalDistractionEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Distraction)).Count();
            int totalForwardCollisionsEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Forward_Collision_Warning)).Count();
            int totalFOVExceptionEvents = events.Where(e => e.EventTypeCode == (int)(EventType.FOV_Exception)).Count();
            int totalHarshAccelerationEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Harsh_Acceleration)).Count();
            int totalHarshBreakingEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Harsh_Braking)).Count();
            int totalLeftLaneDepartureEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Left_Lane_Departure)).Count();
            int totalMobileeyeTemperAlertEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Mobileeye_Tamper_Alert)).Count();
            int totalNearMisscEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Near_Missc)).Count();
            int totalOverRevvingEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Over_Revving)).Count();
            int totalOverSpeedingEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Over_Speeding)).Count();
            int totalPanicEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Panic_Button)).Count();
            int totalPedestrainInDangerZoneEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Pedestrian_In_Danger_Zone)).Count();
            int totalPedestrainInForwardCollisionWarningEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Pedestrian_In_Forward_Collision_Warning)).Count();
            int totalRightLaneDepartureEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Right_Lane_Departure)).Count();
            int totalTrafficSignRecognitionEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Traffic_Sign_Recognition)).Count();
            int totalTrafficSignRecognitionWarningEvents = events.Where(e => e.EventTypeCode == (int)(EventType.Traffic_Sign_Recognition_Warning)).Count();

            // check if current is critical
            if (currnetSetup.ClassificationType == EventClassification.Critical)
            {
                if ((currnetSetup.EventSetups[0].MinEventsNumber != 0 && totalFatigueEvents >= currnetSetup.EventSetups[0].MinEventsNumber) ||
                (currnetSetup.EventSetups[1].MinEventsNumber != 0 && totalDistractionEvents >= currnetSetup.EventSetups[1].MinEventsNumber) ||
               (currnetSetup.EventSetups[2].MinEventsNumber != 0 && totalForwardCollisionsEvents >= currnetSetup.EventSetups[2].MinEventsNumber) ||
               (currnetSetup.EventSetups[3].MinEventsNumber != 0 && totalFOVExceptionEvents >= currnetSetup.EventSetups[3].MinEventsNumber) ||
               (currnetSetup.EventSetups[4].MinEventsNumber != 0 && totalHarshAccelerationEvents >= currnetSetup.EventSetups[4].MinEventsNumber) ||
               (currnetSetup.EventSetups[5].MinEventsNumber != 0 && totalHarshBreakingEvents >= currnetSetup.EventSetups[5].MinEventsNumber) ||
               (currnetSetup.EventSetups[6].MinEventsNumber != 0 && totalLeftLaneDepartureEvents >= currnetSetup.EventSetups[6].MinEventsNumber) ||
               (currnetSetup.EventSetups[7].MinEventsNumber != 0 && totalMobileeyeTemperAlertEvents >= currnetSetup.EventSetups[7].MinEventsNumber) ||
               (currnetSetup.EventSetups[8].MinEventsNumber != 0 && totalNearMisscEvents >= currnetSetup.EventSetups[8].MinEventsNumber) ||
               (currnetSetup.EventSetups[9].MinEventsNumber != 0 && totalOverRevvingEvents >= currnetSetup.EventSetups[9].MinEventsNumber) ||
               (currnetSetup.EventSetups[10].MinEventsNumber != 0 && totalOverSpeedingEvents >= currnetSetup.EventSetups[10].MinEventsNumber) ||
               (currnetSetup.EventSetups[11].MinEventsNumber != 0 && totalPanicEvents >= currnetSetup.EventSetups[11].MinEventsNumber) ||
               (currnetSetup.EventSetups[12].MinEventsNumber != 0 && totalPedestrainInDangerZoneEvents >= currnetSetup.EventSetups[12].MinEventsNumber) ||
               (currnetSetup.EventSetups[13].MinEventsNumber != 0 && totalPedestrainInForwardCollisionWarningEvents >= currnetSetup.EventSetups[13].MinEventsNumber) ||
                (currnetSetup.EventSetups[14].MinEventsNumber != 0 && totalRightLaneDepartureEvents >= currnetSetup.EventSetups[14].MinEventsNumber) ||
                (currnetSetup.EventSetups[15].MinEventsNumber != 0 && totalTrafficSignRecognitionWarningEvents >= currnetSetup.EventSetups[15].MinEventsNumber))
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (currnetSetup.ClassificationType == EventClassification.Elivated)  // check if current is Elivated
            {
                // get critical setup ..
                Setup crititicalSetup = setupArr.Where(a => a.ClassificationType == EventClassification.Critical).FirstOrDefault();

                // get normal setup ..
                //  Setup normalSetup = setupArr.Where(a => a.ClassificationType == EventClassification.Normal).FirstOrDefault();

                // compare values to be Inbetween of them ..
                if ((currnetSetup.EventSetups[0].MinEventsNumber != 0 && totalFatigueEvents >= currnetSetup.EventSetups[0].MinEventsNumber && totalFatigueEvents < crititicalSetup.EventSetups[0].MinEventsNumber) ||
                (currnetSetup.EventSetups[1].MinEventsNumber != 0 && totalDistractionEvents >= currnetSetup.EventSetups[1].MinEventsNumber && totalDistractionEvents < crititicalSetup.EventSetups[1].MinEventsNumber) ||
                (currnetSetup.EventSetups[2].MinEventsNumber != 0 && totalForwardCollisionsEvents >= currnetSetup.EventSetups[2].MinEventsNumber && totalForwardCollisionsEvents < crititicalSetup.EventSetups[2].MinEventsNumber) ||
               (currnetSetup.EventSetups[3].MinEventsNumber != 0 && totalFOVExceptionEvents >= currnetSetup.EventSetups[3].MinEventsNumber && totalFOVExceptionEvents < crititicalSetup.EventSetups[3].MinEventsNumber) ||
               (currnetSetup.EventSetups[4].MinEventsNumber != 0 && totalHarshAccelerationEvents >= currnetSetup.EventSetups[4].MinEventsNumber && totalHarshAccelerationEvents < crititicalSetup.EventSetups[4].MinEventsNumber) ||
               (currnetSetup.EventSetups[5].MinEventsNumber != 0 && totalHarshBreakingEvents >= currnetSetup.EventSetups[5].MinEventsNumber && totalHarshBreakingEvents < crititicalSetup.EventSetups[5].MinEventsNumber) ||
               (currnetSetup.EventSetups[6].MinEventsNumber != 0 && totalLeftLaneDepartureEvents >= currnetSetup.EventSetups[6].MinEventsNumber && totalLeftLaneDepartureEvents < crititicalSetup.EventSetups[6].MinEventsNumber) ||
               (currnetSetup.EventSetups[7].MinEventsNumber != 0 && totalMobileeyeTemperAlertEvents >= currnetSetup.EventSetups[7].MinEventsNumber && totalMobileeyeTemperAlertEvents < crititicalSetup.EventSetups[7].MinEventsNumber) ||
               (currnetSetup.EventSetups[8].MinEventsNumber != 0 && totalNearMisscEvents >= currnetSetup.EventSetups[8].MinEventsNumber && totalNearMisscEvents < crititicalSetup.EventSetups[8].MinEventsNumber) ||
               (currnetSetup.EventSetups[9].MinEventsNumber != 0 && totalOverRevvingEvents >= currnetSetup.EventSetups[9].MinEventsNumber && totalOverRevvingEvents < crititicalSetup.EventSetups[9].MinEventsNumber) ||
               (currnetSetup.EventSetups[10].MinEventsNumber != 0 && totalOverSpeedingEvents >= currnetSetup.EventSetups[10].MinEventsNumber && totalOverSpeedingEvents < crititicalSetup.EventSetups[10].MinEventsNumber) ||
               (currnetSetup.EventSetups[11].MinEventsNumber != 0 && totalPanicEvents >= currnetSetup.EventSetups[11].MinEventsNumber && totalPanicEvents < crititicalSetup.EventSetups[11].MinEventsNumber) ||
               (currnetSetup.EventSetups[12].MinEventsNumber != 0 && totalPedestrainInDangerZoneEvents >= currnetSetup.EventSetups[12].MinEventsNumber && totalPedestrainInDangerZoneEvents < crititicalSetup.EventSetups[12].MinEventsNumber) ||
               (currnetSetup.EventSetups[13].MinEventsNumber != 0 && totalPedestrainInForwardCollisionWarningEvents >= currnetSetup.EventSetups[13].MinEventsNumber && totalPedestrainInForwardCollisionWarningEvents < crititicalSetup.EventSetups[13].MinEventsNumber) ||
                (currnetSetup.EventSetups[14].MinEventsNumber != 0 && totalRightLaneDepartureEvents >= currnetSetup.EventSetups[14].MinEventsNumber && totalRightLaneDepartureEvents < crititicalSetup.EventSetups[14].MinEventsNumber) ||
                (currnetSetup.EventSetups[15].MinEventsNumber != 0 && totalTrafficSignRecognitionWarningEvents >= currnetSetup.EventSetups[15].MinEventsNumber && totalTrafficSignRecognitionWarningEvents < crititicalSetup.EventSetups[15].MinEventsNumber))
                {
                    return true;
                }
                else
                {

                    return false;
                }
            }
            else
            {
                if (totalFatigueEvents > 0)
                {
                    return true;
                }
                return false;
            }

        }

        public List<Event> GetViolationData()
        {

            return eventDAL.GetViolationData();


        }

        public DashboardDto GetViolationDashboardData(int group)
        {
            DashboardDto dashboardDto = new DashboardDto();
            try
            {
                VehicleSummary vehicleList = new VehicleSummary();
                List<Domain.DataModel.Device> devices = new List<Domain.DataModel.Device>();
                List<int> violationCount = new List<int>();

                switch (group)
                {
                    case 0:
                        vehicleList =  vehicleManager.GetALL();
                        devices = deviceDAL.GetList();
                        violationCount = eventDAL.GetViolationCount();
                        dashboardDto.Alarms = eventDAL.GetTotalAlarms();
                        break;

                    case 1: 
                        vehicleList =  vehicleManager.GetALL(group);
                        devices = deviceDAL.GetList(vehicleList.Vehicles);
                        violationCount = eventDAL.GetViolationCount("AD");
                        dashboardDto.Alarms = eventDAL.GetTotalAlarms(devices);
                        break;
                    case 2:
                        vehicleList = vehicleManager.GetALL(group);
                        devices = deviceDAL.GetList(vehicleList.Vehicles);
                        violationCount = eventDAL.GetViolationCount("AA");
                        dashboardDto.Alarms = eventDAL.GetTotalAlarms(devices);
                        break;
                    case 3:
                        vehicleList = vehicleManager.GetALL(group);
                        devices = deviceDAL.GetList(vehicleList.Vehicles);
                        violationCount = eventDAL.GetViolationCount("ADH");
                        dashboardDto.Alarms = eventDAL.GetTotalAlarms(devices);
                        break;
                }

                List<Domain.DataModel.Dashboard> result = dashboardDAL.GetAllDashboardData();
              
                
                dashboardDto.ViolationCount = violationCount[0];
                dashboardDto.ValidatedViolationCount = violationCount[1];
                dashboardDto.ProcessedViolationCount = violationCount[2];
                dashboardDto.UnProcessedViolationCount = violationCount[3];

                // devices = deviceDAL.GetList();

                //List<VehicleDto> vehicleList = vehicleManager.GetAllLVehiclesLocations();


                int currentMonth = DateTime.Now.Month;
                int currentYear = DateTime.Now.Year;

                List<string> monthList = new List<string>();
                List<long> violationCountList = new List<long>();

                
                List<Domain.DataModel.Dashboard> dashboardsDataList = new List<Domain.DataModel.Dashboard>();




                //for (int i = 0; i <= 11; i++)
                //{
                //    monthList.Add(monthNames[i]);
                //    violationCountList.Add(0);
                //    Domain.DataModel.Dashboard obj = new Domain.DataModel.Dashboard();
                //    dashboardsDataList.Add(obj);
                //    //  dashboardDto.MonthsList.Add(monthNames[i]);
                //}
                //foreach (var item in result)
                //{
                //    if (item.YearName == currentYear)
                //    {

                //        switch (item.MonthName)
                //        {

                //            case 1:
                //                violationCountList[0] = (long)item.TotalEvents;
                //                //  dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[0] = item;

                //                break;
                //            case 2:
                //                violationCountList[1] = (long)item.TotalEvents;
                //                //  dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[1] = item; break;
                //            case 3:
                //                violationCountList[2] = (long)item.TotalEvents;
                //                //    dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[2] = item; break;
                //            case 4:
                //                violationCountList[3] = (long)item.TotalEvents;
                //                //   dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[3] = item; break;
                //            case 5:
                //                violationCountList[4] = (long)item.TotalEvents;
                //                //    dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[4] = item; break;
                //            case 6:
                //                violationCountList[5] = (long)item.TotalEvents;
                //                //     dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[5] = item; break;
                //            case 7:
                //                // violationCountList.Add(item.TotalEvents);
                //                //     dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                violationCountList[6] = (long)item.TotalEvents;
                //                dashboardsDataList[6] = item; break;
                //            case 8:
                //                violationCountList[7] = (long)item.TotalEvents;
                //                //    dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[7] = item; break;
                //            case 9:
                //                violationCountList[8] = (long)item.TotalEvents;
                //                //     dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[8] = item; break;
                //            case 10:
                //                violationCountList[9] = (long)item.TotalEvents;
                //                //    dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[9] = item; break;
                //            case 11:
                //                violationCountList[10] = (long)item.TotalEvents;
                //                //     dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[10] = item; break;
                //            case 12:
                //                violationCountList[11] = (long)item.TotalEvents;
                //                //     dashboardDto.ViolationsList.Add(item.TotalEvents);
                //                dashboardsDataList[11] = item; break;

                //        }
                //    }
                //}

                dashboardDto.DashboardDataList = dashboardsDataList;
                dashboardDto.MonthsList = monthList;             

                // dashboardDto.ViolationsCountList = violationCountList;
                //dashboardDto.VehicleList = vehicleList;
                dashboardDto.OnlineCount = vehicleList.Vehicles.Where(c => c.Online.Value == true).Count();
                dashboardDto.OfflineCount = vehicleList.Vehicles.Where(c => c.Online.Value == false).Count();
                // get the idle status


                dashboardDto.OfflineDeviceCount = devices.Where(c => c.Online == false).Count();
                dashboardDto.OnlineDeviceCount = devices.Where(c => c.Online == true).Count();
                dashboardDto.TotalDeviceCount = devices.Count();

                dashboardDto.IdleVehicleCount = vehicleList.Vehicles.Where(c => c.Idle == true).Count();
                dashboardDto.MovingVehicles = vehicleList.Vehicles.Where(c => c.EngineOn == true).Count();
                dashboardDto.StoppedVehicles = vehicleList.Vehicles.Where(c => c.EngineOn == false).Count();
                dashboardDto.VehicleCount = vehicleList.Vehicles.Count();


                return dashboardDto;
            }
            catch (Exception ex)
            {

                return dashboardDto;
            }


          
        }

        public List<Trip> GetTripData()
        {
            var result = tripDAL.GetTripData();
            return result;
        }
        public List<Driver> GetDriverData()
        {
            var result = driverDAL.GetDriverData();
            return result;
        }
        //public List<ScoreSetUp> GetScoreSetUpData()
        //{
        //    var result = scoresetupDAL.GetScoreSetUpData();
        //    return result;
        //}

        public List<LivePosition> GetLivePositionData()
        {
            var result = livepositionDAL.GetLivePositionData();
            return result;
        }

        public Dashboard CheckVehicleStatisticsExists(int month, int year)
        {
            DashboardDAL dashboardDAL = new DashboardDAL();
            return dashboardDAL.CheckVehicleStatisticsExists(month, year);
        }

        public void Insert_UpdateDashboard(Dashboard dashboard, bool v)
        {
            DashboardDAL dashboardDAL = new DashboardDAL();
            dashboardDAL.Insert_Update_One(dashboard, v);
        }

        public List<DriverScore> GetCriticalDrivers()
        {
            int sMonth = Convert.ToInt32(DateTime.Now.ToString("MM"));
            int sYear = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
            return driverDAL.CriticalDriverByMonth(sMonth, sYear);

        }
        public List<VehicleUtilization> GetCriticalVehicles()
        {
            int sMonth = Convert.ToInt32(DateTime.Now.ToString("MM"));
            int sYear = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
            return vehicleDAL.CriticalVehicleByMonth(sMonth, sYear);

        }
    }
}
