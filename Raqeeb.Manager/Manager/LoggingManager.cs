﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class LoggingManager : ManagerBase
    {
        LoggingDAL loggingDAL = new LoggingDAL();

        public Log Insert(Log log)
        {
            log = loggingDAL.Insert(log);
            return log;
        }

        public PagedSearchResultMongo<Log> Search()
        {
            return loggingDAL.Search();
        }
    }
}
