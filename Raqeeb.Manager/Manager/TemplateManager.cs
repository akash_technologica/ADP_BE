﻿using Raqeeb.Framework.Core;
using Raqeeb.Framework.Core.Data.Entites;
using Raqeeb.Framework.Core.Security;
using Raqeeb.Business;
using Raqeeb.Common;
using Raqeeb.DataAccess;
using Raqeeb.Domain;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using System;

namespace Raqeeb.Manager
{
    public class TemplateManager : ManagerBase
    {
        public Contractor Add(Contractor template)
        {
            //#region Validation
            ////   ApplicationBL.ValidateApplicationEntity(template);
            //#endregion

            //#region Mapping
            //template.CreationDate = DateTime.Now;
            //template.CreatedBy = IdentityManager.GetLoggedInUserId();
            //#endregion

            //TemplateDAL templateDAL = new TemplateDAL();
            //var tempalte = templateDAL.Add(template);

            //return template;

            return null;
        }

        public PagedSearchResultMongo<Contractor> Search(Paging<TemplateFilter> paging)
        {
            return new TemplateDAL().Search(paging);
        }
    }
}
