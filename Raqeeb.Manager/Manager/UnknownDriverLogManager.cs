﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class UnknownDriverLogManager
    {
        UnKnownDriverDAL unknownDriverDAL = new UnKnownDriverDAL();

        public List<UnknownDriverLog> InsertBulk(List<UnknownDriverLog> logs)
        {
            logs = unknownDriverDAL.InsertBulk(logs);
            return logs;
        }

        public string GetdriverCode(int vehicleCode, DateTime startDate)
        {
          return unknownDriverDAL.GetdriverCode(vehicleCode, startDate);
        }
    }
}
