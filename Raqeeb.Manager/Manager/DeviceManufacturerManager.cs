﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class DeviceManufacturerManager : ManagerBase
    {
        readonly DeviceManufacturerDAL deviceManufactureDAL;
        public DeviceManufacturerManager()
        {
            deviceManufactureDAL = new DeviceManufacturerDAL();
        }

        public List<DeviceManufacturerMaster> GetManufacturer()
        {
            var list = deviceManufactureDAL.GetAll();
            return list;
        }

        public DeviceManufacturerMaster Add(DeviceManufacturerMaster manufacturer)
        {
            return deviceManufactureDAL.Add(manufacturer);
        }


        public DeviceManufacturerMaster Update(DeviceManufacturerMaster manufacturer)
        {

            return deviceManufactureDAL.Update(manufacturer);
        }
    }
}