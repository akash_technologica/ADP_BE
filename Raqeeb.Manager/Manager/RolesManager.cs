﻿using Raqeeb.Domain.Entities;
using Raqeeb.DataAccess;
using Raqeeb.Framework.Core;
using Raqeeb.Common;
using System;
using System.Collections.Generic;
using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;

namespace Raqeeb.Manager
{
    public class RolesManager
    {
        public List<RolesDto> GetAll()
        {
            RolesDAL RolesDAL = new RolesDAL();
            MenuMasterDAL menuMasterDAL = new MenuMasterDAL();
            List<RolesDto> rolesDtosList = new List<RolesDto>();
            var list = RolesDAL.GetAll();



            foreach (var menu in list)
            {
                RolesDto rolesDto = new RolesDto();
               // rolesDto.IdString = menu.IdString;
                rolesDto.Role = menu.Role;
                rolesDto.IsActive = menu.IsActive;
              //  rolesDto.Id = menu.IdString;
                //List<MenuMaster> menuList = new List<MenuMaster>();
                //if (menu.MenuId != null)
                //{
                //    if (menu.MenuId.Count > 0)
                //    {
                //        foreach (var item in menu.MenuId)
                //        {
                //            MenuMaster menuMaster = menuMasterDAL.GetMenuById(item);
                //            menuList.Add(menuMaster);
                //        }
                //        rolesDto.Menus = menuList;
                //    }
                //}

                rolesDtosList.Add(rolesDto);
            }
            return rolesDtosList;
        }

        public List<Domain.DataModel.Role_Master> GetRolesType()
        {
            RolesDAL RolesDAL = new RolesDAL();
            var list = RolesDAL.GetRolesType();
            return list;
        }


        public Roles Add(Roles role)
        {
            var RolesDAL = new RolesDAL();
            return RolesDAL.Add(role);
        }

        public Roles Delete(Roles role)
        {
            var RolesDAL = new RolesDAL();
            return RolesDAL.Delete(role);
        }

        public Roles Inactive(Roles role)
        {
            var RolesDAL = new RolesDAL();
            return RolesDAL.Inactive(role);
        }

        public Roles Update(Roles role)
        {
            var RolesDAL = new RolesDAL();
            return RolesDAL.Update(role);
        }

        public List<MenuMasterDto> GetMenuByUser_Role(string userid)
        {
            UserDAL userDAL = new UserDAL();
            var RolesDAL = new RolesDAL();
            Sub_Menu_Master_DAL sub_Menu_Master_DAL = new Sub_Menu_Master_DAL();

            User user = userDAL.GetByUserName(userid);
            UserPermissions user_permissions = userDAL.GetPermissionById(user.IdString);

            MenuMasterDAL menuMasterDAL = new MenuMasterDAL();
            List<MenuMasterDto> menuMasterDtosList = new List<MenuMasterDto>();

            List<MenuMaster> menuMastersList = menuMasterDAL.GetAll();

            foreach (var item in menuMastersList)
            {
                List<SubMenuDto> subMenuDtosList = new List<SubMenuDto>();

                MenuMasterDto menuMasterDto = new MenuMasterDto()
                {
                    IsActive = item.IsActive,
                    MenuID = item.IdString,
                    MenuName = item.MenuName,

                };

                switch (item.MenuName)
                {
                    case "Fleet":
                        if ((user_permissions.Deviceall == false && user_permissions.Deviceview == false) ||
                           (user_permissions.Driverall == false && user_permissions.Driverview == false) ||
                           (user_permissions.Vehiclesall == false && user_permissions.Vehiclesview == false))
                        {
                            menuMasterDto.IsActive = false;
                        }
                        break;
                    case "Dispatcher":
                        menuMasterDto.RoutingUrl = item.RoutingUrl;
                        if ((user_permissions.Dispatcherall == false && user_permissions.Dispatcherview == false))
                        {
                            menuMasterDto.IsActive = false;
                        }
                        break;
                    case "Administration":
                        if ((user_permissions.Rolesall == false && user_permissions.Rolesview == false) &&
                           (user_permissions.Usersall == false && user_permissions.Usersview == false))
                        {
                            menuMasterDto.IsActive = false;
                        }
                        break;
                    case "Dashboards":
                        menuMasterDto.RoutingUrl = item.RoutingUrl;
                        break;
                    case "Reports": break;

                }

                List<SubMenuMaster> subMenuMastersList = sub_Menu_Master_DAL.GetSubMenuById(item.IdString);

                if (subMenuMastersList.Count > 0)
                {
                    foreach (var sub_menu in subMenuMastersList)
                    {

                        SubMenuDto subMenuDto = new SubMenuDto()
                        {
                            SubMenuName = sub_menu.SubMenuName,
                            SubMenuUrl = sub_menu.SubMenuUrl

                        };

                        switch (sub_menu.SubMenuName)
                        {
                            case "Driver":
                                if ((user_permissions.Driverall == false && user_permissions.Driverview == false))
                                {
                                    subMenuDto.IsActive = false;
                                }
                                break;
                            case "Vehicles":
                                if ((user_permissions.Vehiclesall == false && user_permissions.Vehiclesview == false))
                                {
                                    menuMasterDto.IsActive = false;
                                }
                                break;
                            case "Devices":
                                if ((user_permissions.Deviceall == false && user_permissions.Deviceview == false))
                                {
                                    menuMasterDto.IsActive = false;
                                }
                                break;
                            case "Users":
                                if ((user_permissions.Usersall == false && user_permissions.Usersview == false))
                                {
                                    menuMasterDto.IsActive = false;
                                }
                                break;
                            case "Roles":
                                if ((user_permissions.Rolesall == false && user_permissions.Rolesview == false))
                                {
                                    menuMasterDto.IsActive = false;
                                }
                                break;
                            case "Events Log":
                                if ((user_permissions.Report_eventlog == false && user_permissions.Report_eventlog == false))
                                {
                                    menuMasterDto.IsActive = false;
                                }
                                break;
                            case "Trip":
                                if ((user_permissions.Deviceall == false && user_permissions.Deviceview == false))
                                {
                                    menuMasterDto.IsActive = false;
                                }
                                break;
                            case "Violation":
                                if ((user_permissions.Deviceall == false && user_permissions.Deviceview == false))
                                {
                                    menuMasterDto.IsActive = false;
                                }
                                break;
                           
                        }


                        subMenuDtosList.Add(subMenuDto);
                    }
                }

                menuMasterDto.SubMenusList = subMenuDtosList;
                menuMasterDtosList.Add(menuMasterDto);
            }

            return menuMasterDtosList;
        }
    }
}
