﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class GeofenceManager : ManagerBase
    {
        GeofenceDAL geofenceDAL = new GeofenceDAL();

        public Geofence Insert(Geofence geofence)
        {
            return geofenceDAL.Insert(geofence);
        }

        public List<Geofence> GetGeofenceList()
        {
            return geofenceDAL.GetAllGeofence();
        }

        public Geofence Update(Geofence geofence)
        {
            return geofenceDAL.UpdateAsync(geofence);
        }

        public Geofence Delete(Geofence geofence)
        {
            return geofenceDAL.DeleteGeofence(geofence);
        }

        public Geofence UpdateGeofencing(Geofence geofence)
        {
            return geofenceDAL.UpdateGeofencing(geofence);
        }
    }
}
