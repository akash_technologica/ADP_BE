﻿using Raqeeb.DataAccess.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class DebugCodeMasterManager : ManagerBase
    {
        DebugMasterDAL codeMasterDAL = new DebugMasterDAL();    
        public List<Raqeeb.Domain.DataModel.DebugCode_Master> GetAll()
        {
            return codeMasterDAL.GetList();

        }
    }
}
