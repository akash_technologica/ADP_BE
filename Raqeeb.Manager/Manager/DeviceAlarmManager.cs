﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.DataModel;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class DeviceAlarmManager : ManagerBase
    {
        DeviceAlarmDAL deviceAlarmDAL = new DeviceAlarmDAL();

        public AlarmDetails Search(SearchCriteria<AlarmFilter> searchCriteria)
        {
            return deviceAlarmDAL.Search(searchCriteria);
        }

        public List<DeviceAlarmDetail> exportData(int group)
        {
            return deviceAlarmDAL.exportAll(group);
        }
    }
}
