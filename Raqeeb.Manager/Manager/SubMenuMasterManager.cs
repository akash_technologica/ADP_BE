﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class SubMenuMasterManager
    {
        public List<SubMenuMaster> GetAll()
        {
            Sub_Menu_Master_DAL MenuDAL = new Sub_Menu_Master_DAL();
            var list = MenuDAL.GetAll();
            return list;
        }

        public List<SubMenuMaster> GetMenuType()
        {
            Sub_Menu_Master_DAL MenuDAL = new Sub_Menu_Master_DAL();
            var list = MenuDAL.GetMenuType();
            return list;
        }


        public SubMenuMaster Add(SubMenuMaster sub_menu)
        {
            Sub_Menu_Master_DAL MenuDAL = new Sub_Menu_Master_DAL();
            return MenuDAL.Add(sub_menu);
        }

        public SubMenuMaster Delete(SubMenuMaster sub_menu)
        {
            Sub_Menu_Master_DAL MenuDAL = new Sub_Menu_Master_DAL();
            return MenuDAL.Delete(sub_menu);
        }

        public SubMenuMaster Inactive(SubMenuMaster menu)
        {
            Sub_Menu_Master_DAL MenuDAL = new Sub_Menu_Master_DAL();
            return MenuDAL.Inactive(menu);
        }

        public SubMenuMaster Update(SubMenuMaster menu)
        {
            Sub_Menu_Master_DAL MenuDAL = new Sub_Menu_Master_DAL();
            return MenuDAL.Update(menu);
        }

        public List<SubMenuMaster> GetById(string id)
        {
            Sub_Menu_Master_DAL MenuDAL = new Sub_Menu_Master_DAL();
            List<SubMenuMaster> list = MenuDAL.GetSubMenuById(id);
            return list;
        }
    }
}
