﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class DriverManager : ManagerBase
    {

        DriverDAL driverDAL = new DriverDAL();

        public List<Driver> InsertBulk(List<Driver> drivers)
        {

            drivers = driverDAL.InsertBulk(drivers);

            return drivers;
        }



        public Driver GetByCode(int code)
        {
            Driver driver = new Driver();
            driver = driverDAL.GetByCode(code);

            return driver;
        }

        public List<Driver> GetAllDrivers()
        {

            return driverDAL.GetAllDrivers();
        }

        public PagedSearchResultMongo<Driver> Search(SearchCriteria<DriverFilter> searchCriteria)
        {
            return driverDAL.Search(searchCriteria);
        }

        public DriverScore CheckDriverScoreExists(int code, int month, int year)
        {
            return driverDAL.CheckDriverScoreExists(code, month,year);
        }

        public List<DriverScore> CriticalDriverByMonth(int month, int year)
        {
            return driverDAL.CriticalDriverByMonth(month, year);
        }

        public void UpdateDriverScore(DriverScore scoreitem)
        {
            driverDAL.UpdateDriverScore(scoreitem);
        }

        public void InsertDriverScore(DriverScore driverScore)
        {
            driverDAL.InsertDriverScore(driverScore);
        }

        public List<DriverScore> GetDriverStatisticsByCode(SearchCriteria<DriverFilter> searchCriteria)
        {
            List<DriverScore> driverScoreList = new List<DriverScore>();

            foreach (var item in searchCriteria.Filters.DriverCode)
            {
               Driver driver = driverDAL.GetByRegistrationNumber(item);
               DriverScore score =  driverDAL.GetDriverStatisticsByCode(searchCriteria,item);
                if(score != null)
                {
                    score.Name = driver.Name;
                    driverScoreList.Add(score);
                }
              
            }

            return driverScoreList;



          // return driverDAL.GetDriverStatisticsByCode(searchCriteria);
        }

        public List<DriverVehicleHistory> GetDriverVehicleHistory(string idString)
        {
            return driverDAL.GetDriverScoreVehicleHistory(idString);
        }
    }
}
