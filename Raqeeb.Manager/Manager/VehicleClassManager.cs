﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class VehicleClassManager : ManagerBase
    {
        readonly VehicleClassDAL vehicleClassDAL = new VehicleClassDAL();

        public VehicleClass Insert(VehicleClass vehicleClass)
        {
            return vehicleClassDAL.Insert(vehicleClass);
        }

        public List<VehicleClass> GetVehicleClassList()
        {
            return vehicleClassDAL.GetAllVehicleClass();
        }

        public VehicleClass Update(VehicleClass vehicleClass)
        {
            return vehicleClassDAL.UpdateAsync(vehicleClass);
        }

        public VehicleClass Delete(VehicleClass vehicleClass)
        {
            return vehicleClassDAL.DeleteVehicleClass(vehicleClass);
        }

        public VehicleClass UpdateVehicleClass(VehicleClass vehicleClass)
        {
            return vehicleClassDAL.UpdateVehicleClass(vehicleClass);
        }
    }
}
