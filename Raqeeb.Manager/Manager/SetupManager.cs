﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class SetupManager : ManagerBase
    {
        SetupDAL setupDAL = new SetupDAL();

        public List<Setup> GetAll()
        {
            return setupDAL.GetAll();
        }

        public void Update(Setup setup)
        {
            setupDAL.Update(setup);
        }

        public Setup GetByClassificationEventTyp(int classificationType)
        {

            return setupDAL.GetByClassificationEventTyp(classificationType);
        }



        public void Add(int classificationType)
        {
            Setup setup = new Setup();
            setup.ClassificationType = (EventClassification)(classificationType);
            setup.EventSetups = new List<EventSetup>();
            EventSetup eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Driver_Fatigue;
            eventSetup.EventType = "Fatigue";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Distraction;
            eventSetup.EventType = "Distraction";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);


            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Forward_Collision_Warning;
            eventSetup.EventType = "Forward Collision Warning";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.FOV_Exception;
            eventSetup.EventType = "FOV Exception";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Harsh_Acceleration;
            eventSetup.EventType = "Harsh Acceleration";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Harsh_Braking;
            eventSetup.EventType = "Harsh Braking";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Left_Lane_Departure;
            eventSetup.EventType = "Left Lane Departure";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Mobileeye_Tamper_Alert;
            eventSetup.EventType = "Mobileeye Tamper Alert";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Near_Missc;
            eventSetup.EventType = "Near Missc";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);


            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Over_Revving;
            eventSetup.EventType = "Over Revving";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Over_Speeding;
            eventSetup.EventType = "Over Speeding";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Panic_Button;
            eventSetup.EventType = "Panic Button";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Pedestrian_In_Danger_Zone;
            eventSetup.EventType = "Pedestrian In Danger Zone";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Pedestrian_In_Forward_Collision_Warning;
            eventSetup.EventType = "Pedestrian In Forward Collision Warning";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Right_Lane_Departure;
            eventSetup.EventType = "Right Lane Departure";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Traffic_Sign_Recognition;
            eventSetup.EventType = "Traffic Sign Recognition";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            eventSetup = new EventSetup();
            eventSetup.EventTypeCode = EventType.Traffic_Sign_Recognition_Warning;
            eventSetup.EventType = "Traffic Sign Recognition Warning";
            eventSetup.MinEventsNumber = 0;
            setup.EventSetups.Add(eventSetup);

            setupDAL.Add(setup);
        }
    }


    
}
