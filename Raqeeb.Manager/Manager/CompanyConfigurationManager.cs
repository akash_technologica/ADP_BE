﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class CompanyConfigurationManager : ManagerBase
    {

        readonly CompanyConfigurationDAL companyConfigurationDAL;
        public CompanyConfigurationManager()
        {
            companyConfigurationDAL = new CompanyConfigurationDAL();
        }
        public List<CompanyConfiguration> GetAccountList()
        {
            var list = companyConfigurationDAL.GetAll();
            return list;
        }
        public CompanyConfiguration Add(CompanyConfiguration company)
        {
            Guid obj = Guid.NewGuid();
            company.AccountId = obj.ToString();
            CompanyConfiguration companyConfiguration = new CompanyConfiguration();

            company.LicenseStart = DateTime.Now;
            switch (company.Validity)
            {
                case 1: company.LicenseEnd = DateTime.Now.AddYears(company.Validity); break;
                case 2: company.LicenseEnd = DateTime.Now.AddYears(company.Validity); break;
                case 3: company.LicenseEnd = DateTime.Now.AddYears(company.Validity); break;
                case 4: company.LicenseEnd = DateTime.Now.AddYears(company.Validity); break;
            }
            try
            {
                companyConfiguration = companyConfigurationDAL.Add(company);
                List<string> roles = new List<string>();

                roles.Add("Admin");
                User user = new User()
                {
                    CompanyId = companyConfiguration.IdString,
                    IsActive = true,
                    Email = company.Email,
                    MobileNumber = company.Mobile.ToString(),
                    EnglishName = company.CompanyName,
                    UserName = company.Email,
                    Roles = roles,
                    Password = ConfigurationManager.AppSettings["NewUserPassword"].ToString()

            };

                UserManager userManager = new UserManager();
              //  userManager.Add(user);
                return companyConfiguration;

            }
            catch (Exception)
            {

                throw;
            }

        }



        public CompanyConfiguration Delete(CompanyConfiguration company)
        {
            return companyConfigurationDAL.Delete(company);
        }
        public CompanyConfiguration Update(CompanyConfiguration company)
        {
            return companyConfigurationDAL.Update(company);
        }

    }
}
