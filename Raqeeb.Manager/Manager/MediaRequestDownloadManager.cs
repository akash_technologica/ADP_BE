﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.DataModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using Raqeeb.Domain.Enums;
using Newtonsoft.Json;
using Raqeeb.Domain.ViewModel;
using Raqeeb.Domain.Filters;
using Raqeeb.Domain.Dtos;

namespace Raqeeb.Manager.Manager
{
    public class MediaRequestDownloadManager : ManagerBase
    {
        MediaDownloadRequestDAL mediaDownloadRequestDal = new MediaDownloadRequestDAL();
        VehicleDAL VehicleDAL = new VehicleDAL();


        public MediaRequestDownloadDetails GetMediaDownloadRequests(SearchCriteria<MediaDownloadRequestFilter> searchCriteria)
        {
            var requests = mediaDownloadRequestDal.GetAllDownloadRequestData(searchCriteria);
            return requests;
        }


        public void addnewDownloadRequest(MediaDownloadRequest mediaDownloadRequest)
        {


            var deviceId = VehicleDAL.GetDeviceIdByLicense(mediaDownloadRequest.CarLicense);
            ObjectCache cache = MemoryCache.Default;

            string startTime = mediaDownloadRequest.StartTime.ToString("yyyy-MM-dd HH:mm:ss");
            string endTime = mediaDownloadRequest.EndTime.ToString("yyyy-MM-dd HH:mm:ss");


            int[] chls = new int[] { 1, 2, 3, 4 };

            // Array array = teridList;

            var key = GetAuthToken();

            var client = new RestClient(Raqeeb.Manager.Constant.Add_Video_Download_Task);


            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var body = new
            {
                key = key,
                terid = deviceId,

                filetype = 2,
                starttime = startTime,
                endtime = endTime,
                name = mediaDownloadRequest.FilePath,
                chl = chls
            };
            request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var result = JsonConvert.DeserializeObject<MediaRequestDataViewModel>(response.Content);
            if (result.errorcode == (int)ErrorCode.Success)
            {
                mediaDownloadRequest.TaskId = result.data.taskid;
                mediaDownloadRequestDal.AddNewDownloadRequest(mediaDownloadRequest);
            }
        }

        public string GetAuthToken()
        {
            var client = new RestClient(Raqeeb.Manager.Constant.END_POINT);
            var request = new RestRequest(Raqeeb.Manager.Constant.TOKEN_URL, Method.GET).AddParameter("username", Raqeeb.Manager.Constant.TOKEN_EMAIL).AddParameter("password", Raqeeb.Manager.Constant.TOKEN_PASSWORD);
            client.Timeout = -1;
            var queryResult = client.Execute(request);
            var session_jobject = JsonConvert.DeserializeObject<AuthenticationModel>(queryResult.Content);
            return session_jobject.data.key;

        }

        public List<Raqeeb.Domain.DataModel.MediaDownloadRequest> exportData()
        {
            return mediaDownloadRequestDal.exportAll();
        }
    }
}
