﻿using MongoDB.Bson;
using Raqeeb.Common;
using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class TripManager : ManagerBase
    {

        TripDAL TripDAL = new TripDAL();

        public List<Trip> InsertBulk(List<Trip> trips)
        {
            trips = TripDAL.InsertBulk(trips);
            return trips;
        }

        public Trip GetByCode(long? code)
        {
            Trip result = new Trip();

            result = TripDAL.GetByCode(code);

            return result;
        }


        public PagedSearchResultMongo<TripDto> Search(SearchCriteria<TripFilter> searchCriteria, int IncludedTime = 0)
        {
            List<Trip> tripListData = new List<Trip>();
            DateTime startDate = searchCriteria.Filters.StartDate.Value;
            DateTime endDate = searchCriteria.Filters.EndDate.Value;

            if (searchCriteria.Filters.DriverCodes != null && searchCriteria.Filters.VehicleCodes != null)
            {
                if (searchCriteria.Filters.DriverCodes.Length > 0 || searchCriteria.Filters.VehicleCodes.Length > 0)
                {                  
                    foreach (var vehicle_item in searchCriteria.Filters.VehicleCodes)
                    {
                        searchCriteria.Filters.VehicleCode = vehicle_item;
                        searchCriteria.Filters.StartDate = startDate;
                        searchCriteria.Filters.EndDate = endDate;

                        if (searchCriteria.Filters.DriverCodes.Length > 0)
                        {
                            foreach (var driver_item in searchCriteria.Filters.DriverCodes)
                            {
                                searchCriteria.Filters.DriverCode = driver_item;
                                List<Trip> _trips = TripDAL.Search(searchCriteria, IncludedTime);
                                if (_trips.Count > 0)
                                {
                                    tripListData.AddRange(_trips);
                                }


                            }
                        }
                        else
                        {
                            List<Trip> _trips = TripDAL.Search(searchCriteria, IncludedTime);
                            if (_trips.Count > 0)
                            {
                                tripListData.AddRange(_trips);
                            }

                        }


                    }
                }
                else
                {
                    tripListData = TripDAL.Search(searchCriteria, IncludedTime);
                }
            }

            else
            {
                tripListData = TripDAL.Search(searchCriteria, IncludedTime);
            }






        
            PagedSearchResultMongo<TripDto> tripList = new PagedSearchResultMongo<TripDto>();
         

            //  Trip trip = new Trip();
            List<TripDto> tripLists = new List<TripDto>();

            foreach (var trip in tripListData)
            {
                bool iSExists = false;
                if (tripLists.Count > 0)
                {
                    iSExists = tripLists.Exists(c => c.TripCode == trip.Code);
                }
                if (!iSExists)
                {
                    TripDto result = new TripDto();

                    result.TripCode = trip.Code;
                    result.StartDate = trip.StartDate.Value;
                    result.EndDate = trip.EndDate;
                    result.StartPositionCode = trip.StartPositionCode;
                    result.EndPositionCode = trip.EndPositionCode;
                    result.VehicleCode = trip.VehicleCode;
                    result.TripDuration = trip.Duration;
                    result.TripDrivingTime = trip.DrivingTime;
                    result.TripStandingTime = trip.StandingTime;
                    result.TripDistanceDriven = Math.Round(trip.DistanceDriven.Value);
                    result.TripMaxSpeed = trip.MaxSpeed;
                    result.TripMaxPrm = trip.MaxPrm;
                    result.TripStartOdoMeter = trip.StartOdoMeter;
                    result.TripEndOdoMeter = trip.EndOdoMeter;

                    

                    TimeSpan elapsed = TimeSpan.FromSeconds(Convert.ToDouble(result.TripDuration));
                    string elapsedFormatted = elapsed.ToString(@"m\:ss");

                    if (result.TripDuration != null)
                        result.durationForTrip = GetTimeFormat(result.TripDuration.Value);
                    if (result.TripDrivingTime != null)
                        result.durationForDriving = GetTimeFormat(result.TripDrivingTime.Value);
                    if (result.TripStandingTime != null)
                        result.TimeForStanding = GetTimeFormat(result.TripStandingTime.Value);

                    VehicleManager vehicleManager = new VehicleManager();
                    DriverManager driverManager = new DriverManager();
                    Driver driver = new Driver();
                    Vehicle vehicle = vehicleManager.GetByCode(result.VehicleCode);
                    if (trip.DriverCode != null)
                    {
                        driver = driverManager.GetByCode(trip.DriverCode.Value);
                        result.DriverName = driver.Name;
                    }
                    else
                    {
                        result.DriverName = "Unknown Driver";
                    }

                    if (vehicle != null)
                    {
                        result.VehicleRegistration = vehicle.Registration;
                    }


                    if (result.StartPositionCode != null && result.EndPositionCode != null)
                    {
                        LivePositionManager positionManager = new LivePositionManager();
                       // result.StartPositionDetails = positionManager.GetByCode(result.StartPositionCode);
                       // result.EndPositionDetails = positionManager.GetByCode(result.EndPositionCode);
                        if(result.StartPositionDetails != null && result.EndPositionDetails != null)
                        {
                            if(result.StartPositionDetails.Latitude != null && result.EndPositionDetails.Latitude != null)
                            {
                                result.StartLocation = Utility.GetLocationName(result.StartPositionDetails.Latitude.Value, result.StartPositionDetails.Longitude.Value);
                                result.EndLocation = Utility.GetLocationName(result.EndPositionDetails.Latitude.Value, result.EndPositionDetails.Longitude.Value);

                            }

                        }

                    }
                    //EventManager eventManager = new EventManager();

                    //EventFilter eventFilter = new EventFilter();
                    //eventFilter.StartDate = trip.StartDate.Value;
                    //eventFilter.EndDate = trip.EndDate.Value;
                    //eventFilter.TripCode = trip.Code;
                    //eventFilter.VehicleCode = trip.VehicleCode;

                    //SubTripManager subTripManager = new SubTripManager();
                    //List<SubTrip> subtrips = subTripManager.GetByTripCode(result.TripCode.Value);
                    //result.SubTrips = subtrips;

                    if (searchCriteria.Filters.UnknownDriver == true)
                    {
                        tripLists.Add(result);
                    }
                    else if (searchCriteria.Filters.UnknownDriver == false && trip.DriverCode != null)
                    {
                        tripLists.Add(result);
                    }
                }



            }
            tripList.Collection = tripLists;
            return tripList;
        }

        public List<List<LatLongList>> GetTripOrdinatesByCode(long code)
        {
            Trip trip = TripDAL.GetByCode(code);
            LivePositionManager livePositionManager = new LivePositionManager();


            List<LivePosition> gpsList = livePositionManager.GetCoOrdinatesListByStartAndEndPosition(trip.StartPosition.Value, trip.EndPosition.Value, trip.VehicleCode.Value);
            TripCoOridinate coOridinates = new TripCoOridinate();
            List<LatLongList> tripPosList = new List<LatLongList>();
            foreach (var data in gpsList)
            {
                if (data.Latitude != null && data.Latitude != null)
                {

                    LatLongList tripCoOridinates = new LatLongList()
                    {
                        Latitide = data.Latitude.Value,
                        Longitude = data.Longitude.Value,
                        Speed = data.Speed.Value,
                        RPM = data.Prm.Value,
                        LocationDateTime = data.LocationTIme,
                        Odometer = data.OdoMeter.Value

                    };
                    tripPosList.Add(tripCoOridinates);

                }

            }

            List<List<LatLongList>> myList = new List<List<LatLongList>>();
            
            List<LatLongList> latlngList = new List<LatLongList>();
            if(tripPosList != null)
            {
               
                if (tripPosList.Count > 0)
                {
                    foreach (var item in tripPosList)
                    {
                        var obj = new LatLongList
                        {
                            Latitide = item.Latitide,
                            Longitude = item.Longitude,
                            LocationDateTime = item.LocationDateTime,
                            Odometer = item.Odometer,
                            RPM = item.RPM,
                            Speed = item.Speed
                        };
                        //var tt = new List<LatLongList>()
                        //{

                        //}
                        myList.Add(new List<LatLongList> { obj });
                    }

                }
            }
          



            return myList;
        }

        public string GetTimeFormat(int time)
        {
            TimeSpan elapsed = TimeSpan.FromSeconds(Convert.ToDouble(time));
            string elapsedFormatted = elapsed.ToString(@"hh\:mm\:ss");


            //TimeSpan elapsed = TimeSpan.FromSeconds(Convert.ToDouble(time));
            //int hours = (int)elapsed.TotalHours;
            //int minutes = (int)elapsed.TotalMinutes;

            //string elapsedFormatted = string.Join(":",hours,minutes);
            return elapsedFormatted;
        }
        public PagedSearchResultMongo<Trip> GetTripsForDashboard(SearchCriteria<TripFilter> searchCriteria, int IncludedTime = 0)
        {
            return TripDAL.GetTripsForDashboard(searchCriteria, IncludedTime);
        }


        


        public Trip GetTripByCode(long? code)
        {
            Trip result = new Trip();
            result = TripDAL.GetTripByCode(code);

            return result;
        }



        public TripDto GetTripDetailsByCode(long code, bool isVehicleDisplayed)
        {
            TripDto result = new TripDto();
            Trip trip = new Trip();

            trip = TripDAL.GetByCode(code);

            result.TripCode = trip.Code;
            result.StartDate = trip.StartDate.Value;
            result.EndDate = trip.EndDate;
            result.StartPositionCode = trip.StartPositionCode;
            result.EndPositionCode = trip.EndPositionCode;
            result.VehicleCode = trip.VehicleCode;
            result.TripDuration = trip.Duration;
            result.TripDrivingTime = trip.DrivingTime;
            result.TripStandingTime = trip.StandingTime;
            result.TripDistanceDriven = trip.DistanceDriven;
            result.TripMaxSpeed = trip.MaxSpeed;
            result.TripMaxPrm = trip.MaxPrm;

            VehicleManager vehicleManager = new VehicleManager();
            Vehicle vehicle = vehicleManager.GetByCode(result.VehicleCode);
            if (vehicle != null)
            {
                result.VehicleRegistration = vehicle.Registration;
            }


            if (result.StartPositionCode != null && result.EndPositionCode != null)
            {
                LivePositionManager positionManager = new LivePositionManager();
               // result.StartPositionDetails = positionManager.GetByCode(result.StartPositionCode);
               // result.EndPositionDetails = positionManager.GetByCode(result.EndPositionCode);
            }
            EventManager eventManager = new EventManager();

            EventFilter eventFilter = new EventFilter();
            eventFilter.StartDate = trip.StartDate.Value;
            eventFilter.EndDate = trip.EndDate.Value;
            eventFilter.TripCode = (isVehicleDisplayed) ? null : trip.Code;
            eventFilter.VehicleCode = (isVehicleDisplayed) ? trip.VehicleCode : null;

            SubTripManager subTripManager = new SubTripManager();
            List<SubTrip> subtrips = subTripManager.GetByTripCode(result.TripCode.Value);
            result.SubTrips = subtrips;

            List<Event> events = eventManager.GetByDateAndCodeAndVehicle(eventFilter, isVehicleDisplayed, Convert.ToInt32(ConfigurationManager.AppSettings["DecreasedTimeFromBackEnd"]));
            if (events != null && events.Count > 0)
            {
                result.Events = new List<EventDto>();

                for (int i = 0; i < events.Count; i++)
                {
                    EventDto eventObj = new EventDto();
                    eventObj.EventStartDate = events[i].StartDate;
                    eventObj.EventEndDate = events[i].EndDate;
                    eventObj.EventTypeCode = events[i].EventTypeCode;
                    eventObj.Shift = events[i].Shift;
                    eventObj.EventSource = events[i].EventSource;

                    EventMasterManager eventMasterManager = new EventMasterManager();
                    EventMaster eventMaster = eventMasterManager.GetByCode(eventObj.EventTypeCode);
                    if (eventMaster != null)
                    {
                        eventObj.EventDescription = eventMaster.Description;
                    }


                    if (events[i].StartPositionCode != null)
                    {
                        LivePositionManager positionManager = new LivePositionManager();
                        Domain.DataModel.LivePosition startPosition = positionManager.GetByCode(trip.StartPositionCode);
                        if (startPosition != null)
                        {
                          //  eventObj.StartPositionDetails = startPosition;
                        }
                    }
                    if (events[i].EndPositionCode != null)
                    {
                        LivePositionManager positionManager = new LivePositionManager();
                        Domain.DataModel.LivePosition endPosition = positionManager.GetByCode(trip.EndPositionCode);
                        if (endPosition != null)
                        {
                          //  eventObj.EndPositionDetails = endPosition;
                        }
                    }
                    result.Events.Add(eventObj);
                }
            }
            return result;
        }

        #region Dashboard 
        // second part in dashboard
        public object GetTotalVehiclesByTripDateDashboard(SearchCriteria<TripFilter> searchCriteria)
        {
            return TripDAL.GetTotalVehiclesByTripDateDashboard(searchCriteria);
        }

        // third part in Dashboard
        public PagedSearchResultMongo<TripDto> GetEventsWithVehiclesDashboard(SearchCriteria<TripFilter> searchCriteria)
        {
            PagedSearchResultMongo<TripDto> result = new PagedSearchResultMongo<TripDto>();

            PagedSearchResultMongo<TripDto> trips = new PagedSearchResultMongo<TripDto>();
            result.Collection = new List<TripDto>();
            // get all events based on pagination . order by startDate 

            trips = Search(searchCriteria);

            if (trips != null && trips.Collection != null && trips.Collection.Count > 0)
            {
                for (int i = 0; i < trips.Collection.Count; i++)
                {
                    TripDto tripDto = new TripDto();

                  //  tripDto.TripCode = trips.Collection[i].Code;
                    tripDto.StartDate = trips.Collection[i].StartDate;
                    tripDto.EndDate = trips.Collection[i].EndDate;
                    tripDto.StartPositionCode = trips.Collection[i].StartPositionCode;
                    tripDto.EndPositionCode = trips.Collection[i].EndPositionCode;
                    tripDto.VehicleCode = trips.Collection[i].VehicleCode;

                    // get Vehicle registration from vehicles By VehicleCode

                    VehicleManager vehicleManager = new VehicleManager();

                    Vehicle vehicle = vehicleManager.GetByCode(trips.Collection[i].VehicleCode);
                    if (vehicle != null)
                    {
                        tripDto.VehicleRegistration = vehicle.Registration;
                    }
                    // get event type code for this trip 

                    EventManager eventManager = new EventManager();
                    long totalVehicleEventsForTrip = 0;
                    EventFilter filter = new EventFilter();
                    filter.StartDate = tripDto.StartDate;
                    filter.EndDate = tripDto.EndDate;
                    filter.VehicleCode = tripDto.VehicleCode;
                    filter.TripCode = tripDto.TripCode;
                    List<Event> tripEvents = eventManager.GetByDateAndCodeAndVehicle(filter, false);
                    if (tripEvents != null)
                    {
                        if (tripEvents.Count == 0)
                        {
                            tripDto.TripEventType = (int)EventClassification.Normal;
                        }
                        else
                        {
                            int fatigueCount = tripEvents.Where(x => x.EventTypeCode == (int)EventType.Driver_Fatigue).Count();
                            int distraction = tripEvents.Where(x => x.EventTypeCode == (int)EventType.Distraction).Count();
                            if (fatigueCount > 2 || distraction > 3)
                            {
                                tripDto.TripEventType = (int)EventClassification.Critical;
                            }
                            else if (fatigueCount == 1 || (distraction != 0 && distraction <= 2))
                            {
                                tripDto.TripEventType = (int)EventClassification.Elivated;
                            }
                        }

                    }

                    if (tripDto.TripCode != null && tripDto.TripCode != 0)
                    {
                        filter.StartDate = tripDto.StartDate;
                        filter.EndDate = tripDto.EndDate;
                        totalVehicleEventsForTrip = eventManager.GetEventsCount(filter);
                    }
                    tripDto.TotalVehicleEventsForTrip = totalVehicleEventsForTrip;
                    result.Collection.Add(tripDto);
                }
            }
            result.NumberOfRecords = trips.NumberOfRecords;
            return result;
        }


        public List<BsonDocument> GetTripsAndEventCategories(SearchCriteria<TripFilter> searchCriteria)
        {
            List<BsonDocument> result = new List<BsonDocument>();

            result = TripDAL.GetTripsAndEventCategories(searchCriteria);

            return result;
        }

        public List<BsonDocument> GetEventsByTripVehicles()
        {
            List<BsonDocument> result = new List<BsonDocument>();

            result = TripDAL.GetEventsByTripVehicles();

            return result;
        }

        public List<Trip> GetTripByLastUpdateCode(long minId)
        {
            List<Trip> result = new List<Trip>();
            result = TripDAL.GetTripListByCode(minId);

            return result;
        }

        public void UpdateTripCoOdinateList(TripCoOridinate item)
        {
            TripDAL.UpdateTripCoOdinateList(item);
        }

        public List<Trip> GetAllTripByMonth(int code, bool check_for_dri_Veh)
        {
            return TripDAL.GetAllTripByMonth(code, check_for_dri_Veh);
        }

        #endregion

    }
}
