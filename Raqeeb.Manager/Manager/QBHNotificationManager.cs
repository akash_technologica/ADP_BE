﻿using System;
using System.Collections.Generic;
using Raqeeb.Framework.Core;
using Raqeeb.DataAccess;
using Raqeeb.Domain;

namespace Raqeeb.Manager
{
    public class QbhNotificationManager : ManagerBase
    {
        public Application Add(Application application)
        {
            #region TestingData

            application.EnglishName = "QASEM APPLICATION";
            application.ArabicName = "تطبيق الحموري";
            application.SubscriptionStartDate = DateTime.Now;
            application.SubscriptionEndDate = DateTime.Now.AddYears(1);
            application.Status = true;
            application.ApiKey = new Crypto().GenerateKey();
            application.SecretKey = new Crypto().Encrypt(DateTime.Now.ToFileTime().ToString(), "o6806642kbM7c5");
            application.CreationDate = DateTime.Now;
            //application.CreatedBy = 1;
            application.Password = "Test@1234";
            application.Password = new Crypto().Encrypt(application.Password, "o6806642kbM7c5");
            application.UserName = "test.notitification@gmail.com";
            application.UserGuid = new Guid().ToString();
            application.EmailAddress = "test.notitification@gmail.com";
            application.LastLogin = DateTime.Now;
            application.MobileNumber = "00971562123212";

            #endregion

            var qbhNotificationDal = new QBHNotificationDAL();
            return qbhNotificationDal.Add(application);
        }

        public Application Update(Application application)
        {
            var qbhNotificationDal = new QBHNotificationDAL();
            return qbhNotificationDal.Update(application);
        }

        public Application GetApplicationById(string id)
        {
            var qbhNotificationDal = new QBHNotificationDAL();
            return qbhNotificationDal.GetApplicationById(id);
        }

        public List<T> GetLookup<T>()
        {
            var qbhNotificationDal = new QBHNotificationDAL();
            return qbhNotificationDal.GetAll<T>();
        }
    }
}