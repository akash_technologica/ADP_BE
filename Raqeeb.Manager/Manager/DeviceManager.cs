﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class DeviceManager : ManagerBase
    {
        DeviceDAL deviceDAL = new DeviceDAL();
        SimCardManager simCardManager = new SimCardManager();

        public DeviceStatisticsModel GetAll()
        {
            DeviceStatisticsModel deviceStatistics = new DeviceStatisticsModel();

            try
            {
                List<Domain.DataModel.Device> devices = deviceDAL.GetList();

                deviceStatistics.TotalOnline = devices.Where(c => c.Online == true).Count();
                deviceStatistics.TotalOffline = devices.Where(c => c.Online == false).Count();
                deviceStatistics.TotalDevices = devices.Count();
                deviceStatistics.DeviceList = devices;

                return deviceStatistics;
            }
            catch (Exception ex)
            {
               
                return deviceStatistics;
            }
          


        }
        public DeviceStatisticsModel GetAll(int group)
        {
            DeviceStatisticsModel deviceStatistics = new DeviceStatisticsModel();

            try
            {
                List<Domain.DataModel.Device> devices = deviceDAL.GetList(group);

                deviceStatistics.TotalOnline = devices.Where(c => c.Online == true).Count();
                deviceStatistics.TotalOffline = devices.Where(c => c.Online == false).Count();
                deviceStatistics.TotalDevices = devices.Count();
                deviceStatistics.DeviceList = devices;

                return deviceStatistics;
            }
            catch (Exception ex)
            {

                return deviceStatistics;
            }



        }
        public List<Device> GetList(string entityId)
        {
            var list = deviceDAL.GetLists(entityId);
            return list;
        }
        public Device Add(Device device)
        {
            Device deviceInfo = deviceDAL.Add(device);
            simCardManager.ActiveSim(device.SimCardCode);

            return deviceInfo;
        }

        public Device Update(Device device)
        {

            return deviceDAL.Update(device);
        }

        public List<DeviceLogViewModel> Search(SearchCriteria<DeviceFilter> searchCriteria)
        {
            
            List<Domain.DataModel.Device> devices = new List<Domain.DataModel.Device>();
            List<Domain.DataModel.DeviveOnlineOfflineLog> logList = new List<Domain.DataModel.DeviveOnlineOfflineLog>();
            List<DeviceLogViewModel> deviceLogList = new List<DeviceLogViewModel>();

            if (searchCriteria.Filters.UserGroup == 0)
            {
                devices = deviceDAL.GetList();
               
            }
            else
            {
                devices = deviceDAL.GetList(searchCriteria.Filters.UserGroup);

            }
            logList = deviceDAL.Search(searchCriteria);

            foreach (var item in logList)
            {
                DeviceLogViewModel deviceLogViewModel = new DeviceLogViewModel()
                {
                    CarLicense = devices.Where(c => c.TerninalId == item.DeviceId).Select(c => c.CarLicense).FirstOrDefault(),
                    DeviceId = item.DeviceId,
                    LogDate = item.Date.Value,
                    Id = item.Id,
                    Online = item.Type != 1? false:true

                };
                deviceLogList.Add(deviceLogViewModel);
            }

            return deviceLogList;
        }
    }
}
