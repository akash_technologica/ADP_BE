﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class DeviceModelManager : ManagerBase
    {
        readonly DeviceModelDAL deviceModelDAL;
        public DeviceModelManager()
        {
            deviceModelDAL = new DeviceModelDAL();
        }

        public List<DeviceModel> GetDeviceModel(string manufacturerId)
        {
            var list = deviceModelDAL.GetAll(manufacturerId);
            return list;
        }

        public DeviceModel Add(DeviceModel model)
        {
            return deviceModelDAL.Add(model);
        }
       

        public DeviceModel Update(DeviceModel model)
        {

            return deviceModelDAL.Update(model);
        }
    }
}
