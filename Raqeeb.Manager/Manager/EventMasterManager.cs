﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class EventMasterManager : ManagerBase
    {

        EventMasterDAL eventMasterDAL = new EventMasterDAL();

        public List<EventMaster> InsertBulk(List<EventMaster> eventMasters)
        {

            eventMasters = eventMasterDAL.InsertBulk(eventMasters);

            return eventMasters;
        }


        public List<Raqeeb.Domain.DataModel.Event_Master> GetAll()
        {         
            return eventMasterDAL.GetAll();
        
        }
        public EventMaster GetByCode(long? code)
        {
            EventMaster eventMaster = new EventMaster();
            eventMaster = eventMasterDAL.GetByCode(code);

            return eventMaster;
        }


        public List<SubEventTypeMaster> InsertSubEventBulk(List<SubEventTypeMaster> subEventMasters)
        {

            subEventMasters = eventMasterDAL.InsertSubEventBulk(subEventMasters);

            return subEventMasters;
        }

        public List<SubEventTypeMaster> GetSubEventByEventCode(int eventTypeCode)
        {

            return eventMasterDAL.GetSubEventByEventCode(eventTypeCode);

        }
        public SubEventTypeMaster GetSubEventCode(int subEventTypeCode)
        {

            return eventMasterDAL.GetSubEventCode(subEventTypeCode);

        }








    }
}
