﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class WebServiceSetupManager : ManagerBase
    {
        WebServiceSetupDAL webServiceSetupDAL = new WebServiceSetupDAL();

        public WebServiceSetup Insert(WebServiceSetup webServiceSetup)
        {
            webServiceSetup = webServiceSetupDAL.Insert(webServiceSetup);
            return webServiceSetup;
        }

        public WebServiceSetup GetByType(WebServiceType type)
        {
            WebServiceSetup webServiceSetup = new WebServiceSetup();
            webServiceSetup = webServiceSetupDAL.GetByType(type);
            return webServiceSetup;
        }

        public WebServiceSetup Update(WebServiceSetup webServiceSetup)
        {
            webServiceSetup = webServiceSetupDAL.Update(webServiceSetup);
            return webServiceSetup;
        }

        public PagedSearchResultMongo<WebServiceSetup> Search()
        {
            return webServiceSetupDAL.Search();
        }

    }
}
