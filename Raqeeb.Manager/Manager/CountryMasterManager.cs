﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class CountryMasterManager: ManagerBase
    {
        readonly CountryMasterDAL countryMasterDAL;
        public CountryMasterManager()
        {
            countryMasterDAL = new CountryMasterDAL();
        }
     
        public List<CountryMaster> GetCountryList()
        {
          
            var list = countryMasterDAL.GetAll();
            return list;
        }


        public CountryMaster Add(CountryMaster country)
        {
           
            return countryMasterDAL.Add(country);
        }

        public CountryMaster Delete(CountryMaster country)
        {
         
            return countryMasterDAL.Delete(country);
        }

       
        public CountryMaster Update(CountryMaster country)
        {
           
            return countryMasterDAL.Update(country);
        }

        public CountryMaster AddCity(CityMaster city)
        {
           return countryMasterDAL.AddCity(city);
       
        }

        public List<CityMaster> GetCityByCountry(CountryMaster country)
        {
            return countryMasterDAL.GetCityByCountryId(country);
        }

        public CityMaster UpdateCity(CityMaster city)
        {
            return countryMasterDAL.UpdateCity(city);
        }
    }
}
