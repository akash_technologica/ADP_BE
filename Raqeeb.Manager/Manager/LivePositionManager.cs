﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class LivePositionManager : ManagerBase
    {

        LivePositionDAL livePositionDAL = new LivePositionDAL();

        public List<LivePosition> InsertBulk(List<LivePosition> livePositions)
        {
            livePositions = livePositionDAL.InsertBulk(livePositions);
            return livePositions;
        }


        public Domain.DataModel.LivePosition GetByCode(string code)
        {
            Domain.DataModel.LivePosition result = new Domain.DataModel.LivePosition();

            result = livePositionDAL.GetByCode(code);

            return result;
        }

        public LivePosition GetByVehicleCode(long code)
        {
            LivePosition result = new LivePosition();

            result = livePositionDAL.GetByVehicleCode(code);

            return result;
        }

        public List<LivePosition> GetAllByType(int positiontype)
        {
           return livePositionDAL.GetAllByType(positiontype);
        }


        public PagedSearchResultMongo<LivePosition> Search(SearchCriteria<LivePositionFilter> searchCriteria, int IncludedTime = 0)
        {
            return livePositionDAL.Search(searchCriteria, IncludedTime);
        }


        public int? GetDriverId(int vehicleId, DateTime startDate)
        {
            return livePositionDAL.GetDriverId(vehicleId, startDate);
        }

        public List<LivePosition> GetCoOrdinatesListByStartAndEndPosition(long startPositionCode, long endPositionCode,int vehicleCode)
        {

            List<LivePosition> list = livePositionDAL.GetDataBetweenStartAndEnd(startPositionCode, endPositionCode, vehicleCode);

            return list;
        }

        public List<LivePosition> GetListByStartAndEndPosition(string startPositionCode, string endPositionCode, int? vehicleCode)
        {
            List<LivePosition> list = livePositionDAL.GetBetweenStartAndEnd(startPositionCode, endPositionCode, vehicleCode.Value);

            return list;
        }
    }
}
