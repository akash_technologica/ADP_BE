﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class VehicleModelManager : ManagerBase
    {
        readonly VehicleModelDAL vehicleModelDAL = new VehicleModelDAL();

        public VehicleModel Insert(VehicleModel vehicleModel)
        {
            return vehicleModelDAL.Insert(vehicleModel);
        }

        public List<VehicleModel> GetvehicleModelList()
        {
            return vehicleModelDAL.GetAllvehicleModel();
        }
        public List<VehicleModel> GetModelByBrand(string code)
        {
            return vehicleModelDAL.GetAllvehicleModelByBrand(code);
        }
        public VehicleModel Update(VehicleModel vehicleModel)
        {
            return vehicleModelDAL.UpdateAsync(vehicleModel);
        }

        public VehicleModel Delete(VehicleModel vehicleModel)
        {
            return vehicleModelDAL.DeletevehicleModel(vehicleModel);
        }

        public VehicleModel UpdateVehicleModel(VehicleModel vehicleModel)
        {
            return vehicleModelDAL.UpdateVehicleModel(vehicleModel);
        }
    }
}
