﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class ScoreSetUpManager : ManagerBase
    {
        ScoreSetUpDAL scoreSetupDal = new ScoreSetUpDAL();

        public ScoreSetUp Add(ScoreSetUp scoreSetUp)
        {
            scoreSetupDal.Add(scoreSetUp);
            return scoreSetUp;
        }
        public List<ScoreSetUp> GetAll()
        {
            var list = scoreSetupDal.GetAll();
            return list;

        }
        public ScoreSetUp Update(ScoreSetUp scoreSetUp)
        {
            scoreSetupDal.Update(scoreSetUp);
            return scoreSetUp;
        }
        public bool CheckExisting(int eventTypeId)
        {
            var list = scoreSetupDal.GetAll();
            var isExist = false;
            foreach (var ss in list)
            {
                if (eventTypeId == ss.EventTypeId) return isExist = true;

               
            }
            return false;
        }

        public List<string> AddBulk(List<string> scoreSetUp)
        {
            List<ScoreSetUpMaster> list = scoreSetupDal.GetScoreSetUpData();
           
            foreach (var item in list)
            {
                if (scoreSetUp.Contains(item.IdString))
                {
                    item.IsIncludedForScoring = true;
                    scoreSetupDal.UpdateScoreSetUP(item);
                }
                else
                {
                    item.IsIncludedForScoring = false;
                    scoreSetupDal.UpdateScoreSetUP(item);
                }
            }
      
            
            return scoreSetUp;
        }
        public List<ScoreSetUpDto> GetScoreSetUp()
        {
            List<ScoreSetUpMaster> list = scoreSetupDal.GetScoreSetUpData();
            EventMasterDAL eventMasterDAL = new EventMasterDAL();

            List<ScoreSetUpDto> ScoreSetUpDtoList = new List<ScoreSetUpDto>();

            foreach (var item in list)
            {
                
                string eventType = eventMasterDAL.GetEventByIdString(item.EventsId);


                if(eventType != null)
                {
                    ScoreSetUpDto scoreSetUpDtos = new ScoreSetUpDto()
                    {
                       EventsId = item.EventsId,
                       EventTypeName = eventType,
                       IdString = item.IdString,
                       IsIncludedForScoring = item.IsIncludedForScoring
                    };

                    ScoreSetUpDtoList.Add(scoreSetUpDtos);
                }
            }
            return ScoreSetUpDtoList;

        }
    }
}
