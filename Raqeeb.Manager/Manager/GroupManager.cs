﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.DataModel;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class GroupManager : ManagerBase
    {
        readonly  GroupMasterDAL groupDAL;
        public GroupManager()
        {
            groupDAL = new GroupMasterDAL();
        }

        public List<GroupMaster> GetGroupList()
        {
            List<GroupMaster> list = groupDAL.GetAll();
            return list;
        }
        
        public GroupMaster Add(GroupMaster group)
        {
            return groupDAL.Add(group);

        }

        public GroupMaster Delete(GroupMaster group)
        {
            return groupDAL.Delete(group);
        }
        public GroupMaster Update(GroupMaster group)
        {
            return groupDAL.UpdateGroup(group);
        }
    }
}
