﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
  public class ContractorManager : ManagerBase
  {

    ContractorDAL contractorDAL = new ContractorDAL();

    public List<Contractor> InsertBulk(List<Contractor> contractors)
    {

      contractors = contractorDAL.InsertBulk(contractors);

      return contractors;
    }



    public Contractor GetByCode(string code)
    {
      Contractor contractor = new Contractor();
      contractor = contractorDAL.GetByCode(code);

      return contractor;
    }



  }
}
