﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.SymbolStore;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Raqeeb.Domain.Enums;
using System.Runtime.Caching;
using Raqeeb.Common;
using Raqeeb.Domain.ViewModel;
using System.IO.Compression;
using Aspose.Zip;
using Aspose.Zip.Saving;

namespace Raqeeb.Manager.Manager
{
    public class EventManager : ManagerBase
    {
        EventDAL eventDAL;
        ObjectCache cache;
        public EventManager()
        {
            cache = MemoryCache.Default;
            eventDAL = new EventDAL();
        }


        public List<Event> InsertBulk(List<Event> events)
        {
            events = eventDAL.InsertBulk(events);
            return events;
        }
        public Event Insert(Event events)
        {
            events = eventDAL.InsertOne(events);
            return events;
        }

        public List<Event> GetAllEvents()
        {
            var events = eventDAL.GetAllEvents();
            return events;
        }

        public List<Event> GetAllEventsByDate()
        {
            var events = eventDAL.GetAllEventsByDate();
            return events;
        }

        public List<UnknownDriverDto> GetEventsLog(SearchCriteria<EventFilter> searchCriteria)
        {
            List<UnknownDriverLog> unknownDriverLogsList = new List<UnknownDriverLog>();
            if (searchCriteria.Filters.VehicleCodes.Length > 0)
            {
                foreach (var item in searchCriteria.Filters.VehicleCodes)
                {
                    searchCriteria.Filters.VehicleCode = item;
                    List<UnknownDriverLog> unknownDriverLogList = new List<UnknownDriverLog>();
                    unknownDriverLogList = eventDAL.SearchLogs(searchCriteria);

                    if (unknownDriverLogList.Count > 0)
                    {
                        // UnknownDriverLog unknownDriverLog = unknownDriverLogList[0];
                        unknownDriverLogsList.Add(unknownDriverLogList[0]);
                    }

                }
            }
            else
            {
                unknownDriverLogsList = eventDAL.SearchLogs(searchCriteria);
            }



            List<UnknownDriverDto> unknownDriverDtoList = new List<UnknownDriverDto>();
            VehicleDAL vehicleDAL = new VehicleDAL();

            foreach (var item in unknownDriverLogsList)
            {
                if (!unknownDriverDtoList.Exists(c => c.VehicleId == item.VehicleId))
                {
                    UnknownDriverDto unknownDriverDto = new UnknownDriverDto()
                    {
                        Registration = vehicleDAL.GetByCode(item.VehicleId).Registration,
                        VehicleId = item.VehicleId,
                        DriverCode = item.DriverCode,
                        Lat = item.Lat,
                        Long = item.Long,
                        LogDate = item.LogDate,
                        _id = item.IdString

                    };

                    unknownDriverDtoList.Add(unknownDriverDto);
                }



            }


            return unknownDriverDtoList;
        }

        public Event GetByCode(string code)
        {
            Event result = new Event();

            result = eventDAL.GetByCode(code);

            return result;
        }

        public long GetEventsTripByVehicleCode(EventFilter eventFilter)
        {
            return eventDAL.GetEventsTripByVehicleCode(eventFilter);
        }

        public long GetEventsCount(EventFilter eventFilter)
        {
            return eventDAL.GetEventsCount(eventFilter);
        }


        public PagedSearchResultMongo<Event> Search(SearchCriteria<EventFilter> searchCriteria, int IncludedTime = 0)
        {
            return eventDAL.Search(searchCriteria, IncludedTime);
        }

        public List<Event> GetByDateAndCodeAndVehicle(EventFilter filter, bool isVehicleDisplayed, int IncludedTime = 0)
        {
            return eventDAL.GetByDateAndCodeAndVehicle(filter, isVehicleDisplayed, IncludedTime);
        }


        public EventDto GetEventDetailsByCode(string code)
        {
            EventDto result = new EventDto();
            Event objEvent = new Event();
            EventMasterManager eventMasterManager = new EventMasterManager();


            objEvent = eventDAL.GetByCode(code);

            result.EventCode = objEvent.Code;
            result.EventStartDate = objEvent.StartDate.Value;
            result.EventEndDate = objEvent.EndDate;
            result.EventTypeCode = objEvent.EventTypeCode;
            result.EventSource = objEvent.EventSource;
            result.Shift = objEvent.Shift;
            result.originalCode = objEvent.OriginalCode;
            // mapping for SEEING MACHINE .. 
            result.Duration = objEvent.Duration;

            TimeSpan time = TimeSpan.FromSeconds(result.Duration.Value);
            result.duration_in_hours = time.Hours;
            result.duration_in_minutes = time.Minutes;
            result.duration_in_seconds = time.Seconds;


            result.SerialNumber = objEvent.SerialNumber;
            result.ContractorName = objEvent.ContractorName;
            result.ConfirmationState = objEvent.ConfirmationState;
            result.DetectedDateTime = objEvent.DetectedDateTime;
            result.Alarms = objEvent.Alarms;
            result.DetectedPositionCode = objEvent.DetectedPositionCode;
            result.Tracking = objEvent.Tracking;
            result.TravelDistance = objEvent.TravelDistance;
            result.subEventTypes = eventMasterManager.GetSubEventByEventCode(objEvent.EventTypeCode.Value);

            if (objEvent.EventAccepted || objEvent.EventRejected)
            {
                EventChangeLog log = eventDAL.GetEventLogs(result.EventCode);

                if (log != null)
                {
                    EventLogDto eventLogDto = new EventLogDto()
                    {
                        Comments = log.Comments,

                        UpdatedBy = log.UpdatedBy,
                        UpdatedDate = log.UpdatedDate
                    };

                    if (objEvent.EventAccepted)
                    {
                        SubEventTypeMaster subE_data = eventMasterManager.GetSubEventCode(log.SubEventCode);
                        eventLogDto.SubEventCode = subE_data.Description;
                        switch (log.ClassificationType)
                        {
                            case 1:
                                eventLogDto.ClassificationType = "Acknowledge"; break;
                            case 2:
                                eventLogDto.ClassificationType = "Classify"; break;
                            case 3:
                                eventLogDto.ClassificationType = "Reclassify"; break;

                        }
                    }
                    else
                    {
                        eventLogDto.ClassificationType = "Rejected";
                    }
                    result.EventLog = eventLogDto;
                }




            }
            else
            {
                result.EventStatus = "Pending";
            }


            if (objEvent.EventTypeCode == (int)EventType.Distraction || objEvent.EventTypeCode == (int)EventType.Driver_Fatigue)
            {
                result.subEventTypes = eventMasterManager.GetSubEventByEventCode(objEvent.EventTypeCode.Value);

                if (objEvent.EventTypeCode == 25)
                {
                    result.SuggestedEventTypeCode = 22;
                    result.SuggestedEventTypeDescription = "Fatigue";
                    result.suggestedSubEventTypes = eventMasterManager.GetSubEventByEventCode(22);

                }
                else if (objEvent.EventTypeCode == 22)
                {
                    result.SuggestedEventTypeCode = 25;
                    result.SuggestedEventTypeDescription = "Distraction";
                    result.suggestedSubEventTypes = eventMasterManager.GetSubEventByEventCode(25);
                }
                result.IsClassificationAllowed = true;
                result.IsReclassificationAllowed = true;
                result.IsAcknowledged = false;

            }

            else
            {

                result.IsAcknowledged = true;
                result.IsClassificationAllowed = false;
                result.IsReclassificationAllowed = false;

            }



            TimeSpan t = TimeSpan.FromSeconds(objEvent.StationaryTime);

            string StationaryTime = string.Format("{0:D2}:{1:D2}:{2:D2}",
                            t.Hours,
                            t.Minutes,
                            t.Seconds);

            result.StationaryTime = StationaryTime; // ASK AMMAR
            result.Bearing = objEvent.Bearing;
            result.TripTime = objEvent.TripTime;
            result.Travel = objEvent.Travel;
            result.ageAgo = GetTimeSince(result.EventStartDate.Value);

            // result.Age = ;
            result.Speed = objEvent.Speed;
            result.TimeIntoShift = objEvent.TimeIntoShift;
            result.GPSCoverage = objEvent.GPSCoverage;




            EventMaster eventMaster = eventMasterManager.GetByCode(result.EventTypeCode);
            if (eventMaster != null)
            {
                result.EventDescription = eventMaster.Description;
            }
            VehicleManager vehicleManager = new VehicleManager();

            Vehicle vehicle = vehicleManager.GetByCode(objEvent.VehicleCode);
            if (vehicle != null)
            {
                result.VehicleRegistration = vehicle.Registration;
            }
            // get trip details 
            TripManager tripManager = new TripManager();
            if (objEvent.TripCode != null)
            {
                Trip trip = tripManager.GetTripByCode(objEvent.TripCode);
                if (trip != null)
                {
                    result.TripStartPositionCode = trip.StartPositionCode;
                    result.TripEndPositionCode = trip.EndPositionCode;
                    result.TripStartDate = trip.StartDate;
                    result.TripEndDate = trip.EndDate;
                    result.TripDuration = trip.Duration;
                    result.TripDrivingTime = trip.DrivingTime;
                    result.TripStandingTime = trip.StandingTime;
                    result.TripDistanceDriven = trip.DistanceDriven;
                    result.TripMaxSpeed = trip.MaxSpeed;
                    result.TripMaxPrm = trip.MaxPrm;
                }

            }

            // Get Medias for Events .
            EventMediaManager eventMediaManager = new EventMediaManager();
            result.Medias = new List<Media>();
            List<Media> eventMedias = objEvent.MediaData;
            List<Media> mediaData = new List<Media>();
            if (eventMedias != null)
            {
                if (result.EventTypeCode == 25 || result.EventTypeCode == 22)
                {
                    for (int j = 0; j <= 0; j++)
                    {
                        Media mediaObj = new Media();
                        mediaObj = eventMedias[j];
                        mediaData.Add(mediaObj);
                    }
                }
                else
                {
                    for (int j = 0; j <= 1; j++)
                    {
                        Media mediaObj = new Media();
                        mediaObj = eventMedias[j];
                        mediaData.Add(mediaObj);
                    }
                }

                result.Medias = mediaData;
            }

            //if (objEvent.StartPositionCode != null)
            //{
            //    LivePositionManager positionManager = new LivePositionManager();
            //    LivePosition startPosition = positionManager.GetByCode(objEvent.StartPositionCode);
            //    if (startPosition != null)
            //    {
            //        result.StartPositionDetails = startPosition;
            //    }
            //}
            //if (objEvent.EndPositionCode != null)
            //{
            //    LivePositionManager positionManager = new LivePositionManager();
            //    LivePosition endPosition = positionManager.GetByCode(objEvent.EndPositionCode);
            //    if (endPosition != null)
            //    {
            //        result.EndPositionDetails = endPosition;
            //    }
            //}


            if (objEvent.DriverCode.HasValue)
            {
                DriverManager driverManager = new DriverManager();
                Driver driverinfo = driverManager.GetByCode(objEvent.DriverCode.Value);

                if (driverinfo != null)
                {

                    result.DriverName = driverinfo.Name;
                }
            }
            else if (objEvent.UnknownDriverCode != null)
            {

                result.DriverName = objEvent.UnknownDriverCode;
            }
            else
            {
                result.DriverName = "Unknown Driver";
            }


            return result;
        }

        public  string DownloadFilesAsync(string evidenceID)
        {
            Domain.DataModel.EventEvidence events = eventDAL.GetViolationById(evidenceID);
            var path = events.FilePath.Substring(0, events.FilePath.LastIndexOf(@"\"));

            var files = Directory.GetFiles(path);
            var zipFileMemoryStream = new MemoryStream();
            using (ZipArchive archive = new ZipArchive(zipFileMemoryStream, ZipArchiveMode.Update, leaveOpen: true))
            {
                foreach (var file in files)
                {
                    var entry = archive.CreateEntry(Path.GetFileName(file));
                    using (var entryStream = entry.Open())
                    using (var fileStream = System.IO.File.OpenRead(file))
                    {
                        fileStream.CopyTo(entryStream);
                    }
                }
            }

            string des_path = @"C:\inetpub\wwwroot\abudhabiMedia\" + events.EvidenceID + events.BusId  + ".zip";
           // zipFileMemoryStream.Seek(0, SeekOrigin.Begin);

            using (var fileStream = new FileStream(des_path, FileMode.Create))
            {
                zipFileMemoryStream.Seek(0, SeekOrigin.Begin);
                zipFileMemoryStream.CopyTo(fileStream);
            }

            string downloadUrl = "http://192.168.2.16/abudhabiMedia/" + events.EvidenceID + events.BusId + ".zip";


            return downloadUrl;


           
            
        }
        

        public List<EventsHistoryDto> GetEventsHistory(SearchCriteria<EventFilter> searchCriteria)
        {
            List<Domain.DataModel.EventEvidence> events = new List<Domain.DataModel.EventEvidence>();
            // get all events based on pagination . order by startDate 
            events = eventDAL.GetViolationDataByEventType(searchCriteria.Filters.EventTypeCode.Value);// Search(searchCriteria);
            List<EventsHistoryDto> eventsHistoryList = new List<EventsHistoryDto>();
            EventMasterManager eventMasterManager = new EventMasterManager();

            foreach (var item in events)
            {
                EventsHistoryDto eventsHistoryDto = new EventsHistoryDto();

                if (item.Lat != null && item.Long != null)
                {
                    //LivePositionManager positionManager = new LivePositionManager();
                    //Domain.DataModel.LivePosition startPosition = positionManager.GetByCode(item.StartPositionCode);
                    //EventMaster eventMaster = eventMasterManager.GetByCode(item.EventTypeCode);

                    
                        eventsHistoryDto.Latitude = (double)item.Lat.Value;
                        eventsHistoryDto.Longitude = (double)item.Long.Value;
                                              
                        eventsHistoryDto.EventDescription = item.EvidenceName;
                        
                        eventsHistoryList.Add(eventsHistoryDto);
                    
                }
                
            }

            return eventsHistoryList;
        }

        public ViolationSummary SearchEventPage(SearchCriteria<EventFilter> searchCriteria, int IncludedTime = 0)
        {
            List<Domain.DataModel.EventEvidence> eventData = new List<Domain.DataModel.EventEvidence>(); 
            eventData = eventDAL.SearchEventForPage(searchCriteria, IncludedTime);          
            
            List<EventDetailViewModel> list_result = new List<EventDetailViewModel>();

            ViolationSummary eventSummary = new ViolationSummary();

            foreach (var item in eventData)
            {
                try
                {
                    EventDetailViewModel eventDetailViewModel = new EventDetailViewModel()
                    {
                        AlarmTime = item.AlarmTime,
                        BusId = item.BusId,
                        C27Spic = item.C27Spic == null || item.C27Spic == "" ? "": Common.Utility.ConvertImageToBase64(@item.C27Spic),
                        C27Wpic = item.C27Wpic == null || item.C27Wpic == "" ? "" : Common.Utility.ConvertImageToBase64(@item.C27Wpic),
                        C28OverviewPic = item.C28OverviewPic == null || item.C28OverviewPic == "" ? "" : Common.Utility.ConvertImageToBase64(@item.C28OverviewPic),
                        C28Overview = item.C28Overview == null || item.C28Overview == "" ? "" : Common.Utility.ConvertVideoToBase64(@item.C28Overview),
                        City = item.City,
                        Comments = item.Comments,
                        Debug_Code = item.Debug_Code,
                        District = item.District,
                        EventsId = item.EventsId,
                        EvidenceID = item.EvidenceID,
                        EvidenceType = item.EvidenceType,
                        InValidViolationCode = item.InValidViolationCode,
                        InValidViolationComments = item.InValidViolationComments,
                        LastUpdate = item.LastUpdate,
                        Lat = item.Lat,
                        Long = item.Long,
                        NotProcessed = item.NotProcessed,
                        ValidationStatus = item.ValidationStatus,
                        VehicleNumber = item.VehicleNumber,
                        ProcessedStatus = item.ProcessedStatus,
                        Street = item.Street,
                        TG_Distance = item.TG_Distance,
                        TG_Duration = item.TG_Duration,
                        TG_Speed = item.TG_Speed,
                        LegalCode = item.LegalCode,
                        RoadId = item.RoadID,
                        RoadRegion = item.RoadRegion,
                        ViolationDirection = item.ViolationDirection,
                        ViolationLane = item.ViolationLane,                                                                 

                    };

                    if(item.ValidationStatus == true)
                    {
                        eventSummary.ValidationStatus += 1;                       
                    }
                    if(item.ProcessedStatus == true)
                    {
                        eventSummary.ProcessedStatus += 1;
                    }
                    if(item.EvidenceType == "Stop Arm Violation")
                    {
                        eventSummary.StopArmViolation += 1;
                    }
                    else if (item.EvidenceType == "Safe zone alarm")
                    {
                        eventSummary.SafeZoneAlarm += 1;
                    }

                    list_result.Add(eventDetailViewModel);


                }

                catch (Exception ex)
                {
                    Raqeeb.Common.Utility.WriteLog(ex);
                    continue;
                }

            }

            eventSummary.ViolationList = list_result;
            eventSummary.TotalViolation = list_result.Count();

            return eventSummary;
        }

        public Reclassification PostAcceptEvent(Reclassification data)
        {
            return eventDAL.PostAcceptEvent(data);
        }

        public object PostRejectEvent(Reclassification data)
        {
            return eventDAL.PostRejectEvent(data);
        }

        public Reclassification PostReclassificationEvent(Reclassification data)
        {
            return eventDAL.PostReclassificationEvent(data);
        }

        public List<EventDto> GetEventDetailsByVehicle(SearchCriteria<EventFilter> searchCriteria)
        {
            List<EventDto> list_result = new List<EventDto>();
            EventMasterManager eventMasterManager = new EventMasterManager();
            //Event objEvent = new Event();


            VehicleDAL vehicleDAL = new VehicleDAL();
            string vehRegis = searchCriteria.Filters.VehicleRegistration;
            var vehicleData = vehicleDAL.GetByRegistration(vehRegis);
            searchCriteria.Filters.VehicleCode = vehicleData.Code;

            PagedSearchResultMongo<Event> events = new PagedSearchResultMongo<Event>();
            // get all events based on pagination . order by startDate 
            events = Search(searchCriteria);

            foreach (var objEvent in events.Collection)
            {

                if (objEvent.MediaData != null)
                {
                    EventDto result = new EventDto();


                    result.EventCode = objEvent.Code;
                    result.EventStartDate = objEvent.StartDate.Value;
                    result.EventEndDate = objEvent.EndDate;
                    result.EventTypeCode = objEvent.EventTypeCode;
                    result.EventSource = objEvent.EventSource;
                    result.Shift = objEvent.Shift;
                    result.originalCode = objEvent.OriginalCode;
                    // mapping for SEEING MACHINE .. 
                    result.Duration = objEvent.Duration;

                    TimeSpan time = TimeSpan.FromSeconds(result.Duration.Value);
                    result.duration_in_hours = time.Hours;
                    result.duration_in_minutes = time.Minutes;
                    result.duration_in_seconds = time.Seconds;


                    result.SerialNumber = objEvent.SerialNumber;
                    result.ContractorName = objEvent.ContractorName;
                    result.ConfirmationState = objEvent.ConfirmationState;
                    result.DetectedDateTime = objEvent.DetectedDateTime;
                    result.Alarms = objEvent.Alarms;
                    result.DetectedPositionCode = objEvent.DetectedPositionCode;
                    result.Tracking = objEvent.Tracking;
                    result.TravelDistance = objEvent.TravelDistance;
                    result.subEventTypes = eventMasterManager.GetSubEventByEventCode(objEvent.EventTypeCode.Value);

                    if (objEvent.EventTypeCode == 25 || objEvent.EventTypeCode == 22)
                    {
                        result.subEventTypes = eventMasterManager.GetSubEventByEventCode(objEvent.EventTypeCode.Value);

                        if (objEvent.EventTypeCode == 25)
                        {
                            result.SuggestedEventTypeCode = 22;
                            result.SuggestedEventTypeDescription = "Fatigue";
                            result.suggestedSubEventTypes = eventMasterManager.GetSubEventByEventCode(22);

                        }
                        else if (objEvent.EventTypeCode == 22)
                        {
                            result.SuggestedEventTypeCode = 25;
                            result.SuggestedEventTypeDescription = "Distraction";
                            result.suggestedSubEventTypes = eventMasterManager.GetSubEventByEventCode(25);
                        }
                        result.IsClassificationAllowed = true;
                        result.IsReclassificationAllowed = true;
                        result.IsAcknowledged = false;

                    }

                    else
                    {

                        result.IsAcknowledged = true;
                        result.IsClassificationAllowed = false;
                        result.IsReclassificationAllowed = false;

                    }

                    TimeSpan t = TimeSpan.FromSeconds(objEvent.StationaryTime);

                    string StationaryTime = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                    t.Hours,
                                    t.Minutes,
                                    t.Seconds);

                    result.StationaryTime = StationaryTime; // ASK AMMAR
                    result.Bearing = objEvent.Bearing;
                    result.TripTime = objEvent.TripTime;
                    result.Travel = objEvent.Travel;
                    result.ageAgo = GetTimeSince(result.EventStartDate.Value);

                    // result.Age = ;
                    result.Speed = objEvent.Speed;
                    result.TimeIntoShift = objEvent.TimeIntoShift;
                    result.GPSCoverage = objEvent.GPSCoverage;




                    EventMaster eventMaster = eventMasterManager.GetByCode(result.EventTypeCode);
                    if (eventMaster != null)
                    {
                        result.EventDescription = eventMaster.Description;
                    }
                    VehicleManager vehicleManager = new VehicleManager();

                    Vehicle vehicle = vehicleManager.GetByCode(objEvent.VehicleCode);
                    if (vehicle != null)
                    {
                        result.VehicleRegistration = vehicle.Registration;
                    }
                    // get trip details 
                    //TripManager tripManager = new TripManager();
                    //if (objEvent.TripCode != null)
                    //{
                    //    Trip trip = tripManager.GetTripByCode(objEvent.TripCode);
                    //    if (trip != null)
                    //    {
                    //        result.TripStartPositionCode = trip.StartPositionCode;
                    //        result.TripEndPositionCode = trip.EndPositionCode;
                    //        result.TripStartDate = trip.StartDate;
                    //        result.TripEndDate = trip.EndDate;
                    //        result.TripDuration = trip.Duration;
                    //        result.TripDrivingTime = trip.DrivingTime;
                    //        result.TripStandingTime = trip.StandingTime;
                    //        result.TripDistanceDriven = trip.DistanceDriven;
                    //        result.TripMaxSpeed = trip.MaxSpeed;
                    //        result.TripMaxPrm = trip.MaxPrm;
                    //    }

                    //}

                    // Get Medias for Events .
                    EventMediaManager eventMediaManager = new EventMediaManager();
                    result.Medias = new List<Media>();
                    List<Media> eventMedias = objEvent.MediaData;
                    List<Media> mediaData = new List<Media>();
                    if (eventMedias.Count > 0)
                    {
                        if (result.EventTypeCode == 25 || result.EventTypeCode == 22)
                        {
                            for (int j = 0; j <= 0; j++)
                            {
                                Media mediaObj = new Media();
                                mediaObj = eventMedias[j];
                                mediaData.Add(mediaObj);
                            }
                        }
                        else
                        {
                            for (int j = 0; j <= 1; j++)
                            {
                                Media mediaObj = new Media();
                                mediaObj = eventMedias[j];
                                mediaData.Add(mediaObj);
                            }
                        }

                        result.Medias = mediaData;
                    }

                    if (objEvent.StartPositionCode != null)
                    {
                        LivePositionManager positionManager = new LivePositionManager();
                        Domain.DataModel.LivePosition startPosition = positionManager.GetByCode(objEvent.StartPositionCode);
                        if (startPosition != null)
                        {
                            //result.StartPositionDetails = startPosition;
                        }
                    }
                    if (objEvent.EndPositionCode != null)
                    {
                        LivePositionManager positionManager = new LivePositionManager();
                        Domain.DataModel.LivePosition endPosition = positionManager.GetByCode(objEvent.EndPositionCode);
                        if (endPosition != null)
                        {
                            //result.EndPositionDetails = endPosition;
                        }
                    }


                    if (objEvent.DriverCode.HasValue)
                    {
                        DriverManager driverManager = new DriverManager();
                        Driver driverinfo = driverManager.GetByCode(objEvent.DriverCode.Value);

                        if (driverinfo != null)
                        {

                            result.DriverName = driverinfo.Name;
                        }
                    }
                    else
                    {

                        result.DriverName = objEvent.UnknownDriverCode.ToString();
                    }

                    list_result.Add(result);
                }

            }

            return list_result;


        }

        public long GetEventCountByEventTypeCode(int eventTypeCode, string shift, int vehicleCode)
        {
            return eventDAL.GetEventCountByEventTypeCode(eventTypeCode, shift, vehicleCode);
        }

        public Event UpdateEventsWithMedia(Event item)
        {
            return eventDAL.UpdateEventsWithMedia(item);
        }

        public List<Event> GetAllSM_EventsFromFMS(long? minId)
        {
            return eventDAL.GetAllSM_EventsFromFMS(minId);
        }

        public List<Event> GetEventsByVehcileCodeForDashboard(int? VehicleCode, DateTime? DateTimeFrom = null, DateTime? DateTimeTo = null, string Shift = null, int IncludedTime = 0)
        {
            return eventDAL.GetEventsByVehcileCodeForDashboard(VehicleCode, DateTimeFrom, DateTimeTo, Shift, IncludedTime);
        }

        public List<Event> GetEventsByVehcileCode(int? VehicleCode, DateTime? DateTimeFrom = null, DateTime? DateTimeTo = null, string Shift = null, int IncludedTime = 0)
        {
            return eventDAL.GetEventsByVehcileCode(VehicleCode, DateTimeFrom, DateTimeTo, Shift, IncludedTime);
        }
        #region Dashboard 
        // first part of Dashboard
        public PagedSearchResultMongo<EventDto> GetEventsDashboard(SearchCriteria<EventFilter> searchCriteria)
        {

            PagedSearchResultMongo<EventDto> result = new PagedSearchResultMongo<EventDto>();
            DriverManager driverManager = new DriverManager();
            result.Collection = new List<EventDto>();
            PagedSearchResultMongo<Event> events = new PagedSearchResultMongo<Event>();
            EventMediaManager eventMediaManager = new EventMediaManager();

            events = Search(searchCriteria);


            //if (cache.Contains(Constant.EventsCacheKey))
            //    events = (PagedSearchResultMongo<Event>)cache.Get(Constant.EventsCacheKey);
            //else
            //{
            //    events = Search(searchCriteria);

            //    // cache.Set(Constant.VehicleCacheKey, vehicleList, new CacheItemPolicy() { RemovedCallback = new CacheEntryRemovedCallback(CacheRemovedCallback) /* your other parameters here */});
            //    // Store data in the cache    
            //    CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            //    cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(3);
            //    cache.Add(Constant.EventsCacheKey, events, cacheItemPolicy);
            //    cacheItemPolicy.RemovedCallback = new CacheEntryRemovedCallback(CacheRemovedCallback);

            //}






            // get all events based on pagination . order by startDate 
            // events = Search(searchCriteria);

            if (events != null && events.Collection != null && events.Collection.Count > 0)
            {
                for (int i = 0; i < events.Collection.Count; i++)
                {
                    EventDto eventDto = new EventDto();

                    eventDto.EventCode = events.Collection[i].Code;
                    eventDto.EventStartDate = events.Collection[i].StartDate.Value;
                    eventDto.EventEndDate = events.Collection[i].EndDate;
                    eventDto.EventTypeCode = events.Collection[i].EventTypeCode;
                    eventDto.originalCode = events.Collection[i].OriginalCode;
                    eventDto.Medias = events.Collection[i].MediaData;
                    eventDto.ageAgo = GetTimeSince(eventDto.EventStartDate.Value);
                    if (events.Collection[i].DriverCode != null)
                    {
                        Driver dd = driverManager.GetByCode(events.Collection[i].DriverCode.Value);
                        eventDto.DriverName = dd.Name;
                        eventDto.DriverMobile = dd.Mobile;
                    }
                    else if (events.Collection[i].UnknownDriverCode != null)
                    {
                        eventDto.DriverName = events.Collection[i].UnknownDriverCode.ToString();
                    }
                    else
                    {
                        eventDto.DriverName = "NA";
                    }

                    if (eventDto.EventTypeCode == 25 || eventDto.EventTypeCode == 22)
                    {
                        if (eventDto.EventTypeCode == 25)
                        {
                            eventDto.SuggestedEventTypeCode = 22;
                            eventDto.SuggestedEventTypeDescription = "Fatigue";
                        }
                        else
                        {
                            eventDto.SuggestedEventTypeCode = 25;
                            eventDto.SuggestedEventTypeDescription = "Distraction";
                        }
                        eventDto.IsClassificationAllowed = true;
                        eventDto.IsReclassificationAllowed = true;
                        eventDto.IsAcknowledged = false;

                    }

                    else
                    {
                        eventDto.IsAcknowledged = true;
                        eventDto.IsClassificationAllowed = false;
                        eventDto.IsReclassificationAllowed = false;

                    }


                    if (events.Collection[i].EventTypeCode == 25 || events.Collection[i].EventTypeCode == 22)

                        eventDto.ShowExamine = true;
                    else
                        eventDto.ShowExamine = false;

                    eventDto.Shift = events.Collection[i].Shift;
                    eventDto.EventSource = events.Collection[i].EventSource;

                    // get description from eventMaster By Code .
                    EventMasterManager eventMasterManager = new EventMasterManager();
                    EventMaster eventMaster = eventMasterManager.GetByCode(eventDto.EventTypeCode);
                    if (eventMaster != null)
                    {
                        eventDto.EventDescription = eventMaster.Description;
                    }
                    // get Vehicle registration from vehicles By VehicleCode

                    VehicleManager vehicleManager = new VehicleManager();
                    List<EventSummaryList> eventSummaryLists = new List<EventSummaryList>();

                    Vehicle vehicle = vehicleManager.GetByCode(events.Collection[i].VehicleCode);

                    if (result.Collection.Count > 0)
                    {
                        if (result.Collection.Exists(c => c.VehicleRegistration == vehicle.Registration))
                        {
                            var resOut = result.Collection.Find(c => c.VehicleRegistration == vehicle.Registration);

                            if (resOut.EventSummaryLists.Exists(c => c.EventTypeCode == eventDto.EventTypeCode))
                            {
                                var res2 = resOut.EventSummaryLists.Find(c => c.EventTypeCode == eventDto.EventTypeCode);
                                res2.EventCount += 1;
                            }

                            else
                            {
                                EventSummaryList eventSummary = new EventSummaryList()
                                {
                                    EventCount = 1,
                                    EventName = eventDto.EventDescription,
                                    EventTypeCode = eventDto.EventTypeCode,

                                };
                                resOut.EventSummaryLists.Add(eventSummary);
                            }
                        }
                        else
                        {
                            eventDto.VehicleRegistration = vehicle.Registration;
                            EventSummaryList eventSummary = new EventSummaryList()
                            {
                                EventCount = 1,
                                EventName = eventDto.EventDescription,
                                EventTypeCode = eventDto.EventTypeCode
                            };
                            eventSummaryLists.Add(eventSummary);
                            eventDto.EventSummaryLists = eventSummaryLists;

                            result.Collection.Add(eventDto);
                        }


                    }

                    else
                    {
                        if (vehicle != null)
                        {
                            eventDto.VehicleRegistration = vehicle.Registration;
                            EventSummaryList eventSummary = new EventSummaryList()
                            {
                                EventCount = 1,
                                EventName = eventDto.EventDescription,
                                EventTypeCode = eventDto.EventTypeCode
                            };
                            eventSummaryLists.Add(eventSummary);
                            eventDto.EventSummaryLists = eventSummaryLists;
                        }
                        result.Collection.Add(eventDto);
                    }



                    // get trip details 
                    //TripManager tripManager = new TripManager();
                    //Trip trip = tripManager.GetTripByCodeAndDate(eventDto.EventTripCode, eventDto.EventStartDate);
                    //if (trip != null)
                    //{
                    //  eventDto.StartPositionCode = trip.StartPositionCode;
                    //  eventDto.EndPositionCode = trip.EndPositionCode;
                    //}

                    // media

                    //List<Media> medias = eventMediaManager.GetByEventCode(events.Collection[i].OriginalCode.Value);

                    //List<Media> mediaData = new List<Media>();
                    //if (medias.Count > 0)
                    //{
                    //    if (eventDto.EventTypeCode == 25 || eventDto.EventTypeCode == 22)
                    //    {
                    //        for (int j = 0; j <= 0; j++)
                    //        {
                    //            Media mediaObj = new Media();
                    //            mediaObj = medias[j];
                    //            mediaData.Add(mediaObj);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        for (int j = 0; j <= 1; j++)
                    //        {
                    //            Media mediaObj = new Media();
                    //            mediaObj = medias[j];
                    //            mediaData.Add(mediaObj);
                    //        }
                    //    }

                    //    eventDto.Medias = mediaData;
                    //}




                }
            }
            result.NumberOfRecords = events.NumberOfRecords;
            return result;
        }

        private void CacheRemovedCallback(CacheEntryRemovedArguments arguments)
        {
            PagedSearchResultMongo<Event> events = new PagedSearchResultMongo<Event>();
            SearchCriteria<EventFilter> searchCriteria = new SearchCriteria<EventFilter>();
            searchCriteria.Filters.Shift = Utility.ShiftFromDateTime();
            if (searchCriteria.Filters.Shift == "Night")
            {
                searchCriteria.Filters.StartDate = DateTime.Now.Date.AddHours(18);
                searchCriteria.Filters.EndDate = DateTime.Now.AddDays(1).Date.AddHours(6);
            }
            else
            {
                searchCriteria.Filters.StartDate = DateTime.Now.Date.AddHours(6);
                searchCriteria.Filters.EndDate = DateTime.Now.Date.AddHours(18);
            }
            events = Search(searchCriteria);
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(3);
            cache.Add(Constant.EventsCacheKey, events, cacheItemPolicy);
        }


        public List<Domain.DataModel.EventEvidence> GetEventsForDashboard(SearchCriteria<EventFilter> searchCriteria)
        {                            
            
            List<Domain.DataModel.EventEvidence> events = eventDAL.SearchEventForPage(searchCriteria);
            DashboardEventDtos dashboardEventDtos = new DashboardEventDtos();

            //foreach (var event_item in events)
            //{
                
            //    switch (event_item.EventTypeCode)
            //    {
            //        case (int)EventType.Distraction:
            //            dashboardEventDtos.Distraction += 1;
            //            break;
            //        case (int)EventType.Driver_Fatigue:
            //            dashboardEventDtos.Fatigue += 1;
            //            break;
            //        case (int)EventType.Forward_Collision_Warning:
            //            dashboardEventDtos.ForwardCollisionWarning += 1;
            //            break;

            //        case (int)EventType.FOV_Exception:
            //            dashboardEventDtos.Fov += 1;
            //            break;
            //        case (int)EventType.Harsh_Acceleration:
            //            dashboardEventDtos.HarshAcceleration += 1;

            //            break;
            //        case (int)EventType.Harsh_Braking:
            //            dashboardEventDtos.HarshBrake += 1;
            //            break;
            //        case (int)EventType.Left_Lane_Departure:
            //            dashboardEventDtos.LeftLaneDeparture += 1;
            //            break;
            //        case (int)EventType.Mobileeye_Tamper_Alert:
            //            dashboardEventDtos.Mobileeye += 1;
            //            break;
            //        case (int)EventType.Near_Missc: break;
            //        case (int)EventType.Over_Revving:
            //            dashboardEventDtos.OverRevving += 1;
            //            // over_reving_distance += total_distance;
            //            break;
            //        case (int)EventType.Over_Speeding:
            //            dashboardEventDtos.OverSpeed += 1;
            //            break;
            //        case (int)EventType.Panic_Button: break;
            //        case (int)EventType.Pedestrian_In_Danger_Zone:
            //            dashboardEventDtos.PedestrianInDangerZone += 1;
            //            break;
            //        case (int)EventType.Pedestrian_In_Forward_Collision_Warning:
            //            dashboardEventDtos.PedestrianInForwardCollisionWarning += 1;
            //            break;
            //        case (int)EventType.PowerFailure: break;
            //        case (int)EventType.Right_Lane_Departure:
            //            dashboardEventDtos.RightLaneDeparture += 1;
            //            break;
            //        case (int)EventType.Traffic_Sign_Recognition: break;
            //        case (int)EventType.Traffic_Sign_Recognition_Warning: break;
            //    }
            //}

             return events;


        }

        public Event getExistingGeofenceEvent(int? code, int geofence_Violation)
        {
            return eventDAL.getExistingGeofenceEvent(code, geofence_Violation);
        }



        #endregion



        // Events by Driver Id

        public DriverProfileDto GetEventDetailsByDriverId(int code)
        {
            return eventDAL.GetEventDetailsByDriverId(code);
        }

        public Event GetSM_EventsByDateAndVehicle(DateTime startDate, DateTime endDate, int? vehicleCode, int eventType)
        {

            List<Event> eventList = eventDAL.GetSM_EventsByDateAndVehicle(startDate, endDate, vehicleCode, eventType);
            Event eventData = new Event();
            if (eventList != null)
            {
                foreach (var item in eventList)
                {
                    if (startDate.TimeOfDay.Seconds == item.StartDate.Value.TimeOfDay.Seconds && endDate.TimeOfDay.Seconds == item.EndDate.Value.TimeOfDay.Seconds)
                    {
                        eventData = item;
                    }

                }
            }
            return eventData;

        }

        public void UpdateGeofenceEvent(Event e_event)
        {
            eventDAL.UpdateGeofenceEvent(e_event);
        }

        public void UpdateEventsWithDriver(Event eventData)
        {
            eventDAL.UpdateEventWithDriver(eventData);
        }

        public List<Event> GetAllEventsByVehicleCode(int vehicleCode)
        {
            return eventDAL.GetAllEventsByVehicleCode(vehicleCode);
        }
        // return how much time passed since date object
        public static string GetTimeSince(DateTime objDateTime)
        {
            // here we are going to subtract the passed in DateTime from the current time converted to UTC
            TimeSpan ts = DateTime.Now.Subtract(objDateTime);
            int intDays = ts.Days;
            int intHours = ts.Hours;
            int intMinutes = ts.Minutes;
            int intSeconds = ts.Seconds;

            if (intDays > 0)
                return string.Format("{0} days", intDays);

            if (intHours > 0)
                return string.Format("{0} hours", intHours);

            if (intMinutes > 0)
                return string.Format("{0} minutes", intMinutes);

            if (intSeconds > 0)
                return string.Format("{0} seconds", intSeconds);

            // let's handle future times..just in case
            if (intDays < 0)
                return string.Format("in {0} days", Math.Abs(intDays));

            if (intHours < 0)
                return string.Format("in {0} hours", Math.Abs(intHours));

            if (intMinutes < 0)
                return string.Format("in {0} minutes", Math.Abs(intMinutes));

            if (intSeconds < 0)
                return string.Format("in {0} seconds", Math.Abs(intSeconds));

            return "a bit";
        }

        public List<Event> GetAllEventsByMonth(int code, bool checkForVehDri)
        {
            return eventDAL.GetAllEventsByMonth(code, checkForVehDri);
        }
    }
}
