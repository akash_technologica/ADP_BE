﻿using Raqeeb.Framework.Core;
using Raqeeb.Business;
using Raqeeb.Common;
using Raqeeb.DataAccess;
using Raqeeb.Domain;
using System;

namespace Raqeeb.Manager
{
    public class ApplicationManager : ManagerBase
    {
        public ApplicationDto Add(Application application)
        {
            #region Validation
            ApplicationBL.CheckIfApplicationExist(CheckIfApplicationExist(application.UserName));
            ApplicationBL.ValidateApplicationEntity(application);
            #endregion

            Utility.CreateMap<Application, ApplicationDto>();
            #region Mapping
            application.SubscriptionStartDate = DateTime.Now;
            application.SubscriptionEndDate = DateTime.Now.AddYears(1);
            application.Status = true;
            application.ApiKey = Utility.GenerateSecretKey(20);
            application.SecretKey = Utility.GenerateSecretKey(20);
            application.CreationDate = DateTime.Now;
            application.Password = new Crypto().Encrypt(application.Password, "o6806642kbM7c5");
            application.UserGuid = new Guid().ToString();
            application.EmailAddress = application.UserName;
            #endregion

            ApplicationDAL applicationDAL = new ApplicationDAL();
            Application app = applicationDAL.Add(application);
            ApplicationDto applicationDto = new ApplicationDto();
            applicationDto = Utility.Map<Application, ApplicationDto>(app);

            return applicationDto;
        }

        public Application Update(Application application)
        {
            ApplicationDAL applicationDAL = new ApplicationDAL();
            return applicationDAL.Update(application);
        }

        public Application GetApplicationById(string id)
        {
            ApplicationDAL applicationDAL = new ApplicationDAL();
            return applicationDAL.GetApplicationById(id);
        }

        public ApplicationDto Login(string userName, string password)
        {
            Crypto crypto = new Crypto();

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
            {
                throw new BussinessException(SystemErrors.InvalidUserCredentials);
            }

            ApplicationDAL applicationDAL = new ApplicationDAL();
            var app = applicationDAL.GetAccess(userName, crypto.Encrypt(password, "o6806642kbM7c5"));
            if (app == null)
            {
                throw new BussinessException(SystemErrors.InvalidUserCredentials);
            }
            else
            {
                Utility.CreateMap<Application, ApplicationDto>();
                ApplicationDto applicationDto = new ApplicationDto();
                applicationDto = Utility.Map<Application, ApplicationDto>(app);
                return applicationDto;
            }
        }

        public ApplicationDto GetAccessByApiKeys(string apiKey, string secretkey)
        {
            Crypto crypto = new Crypto();

            if (string.IsNullOrEmpty(apiKey) || string.IsNullOrEmpty(secretkey))
            {
                throw new BussinessException(SystemErrors.InvalidUserApiKeys);
            }

            ApplicationDAL applicationDAL = new ApplicationDAL();
            var app = applicationDAL.GetAccessByApiKeys(apiKey, secretkey);
            if (app == null)
            {
                throw new BussinessException(SystemErrors.InvalidUserApiKeys);
            }
            else
            {
                Utility.CreateMap<Application, ApplicationDto>();
                ApplicationDto applicationDto = new ApplicationDto();
                applicationDto = Utility.Map<Application, ApplicationDto>(app);
                return applicationDto;
            }
        }


        public bool CheckIfApplicationExist(string email)
        {
            Application application = GetApplicationByEmail(email);
            if (application != null)
                return true;
            else
                return false;
        }

        public Application GetApplicationByEmail(string email)
        {
            ApplicationDAL applicationDAL = new ApplicationDAL();
            return applicationDAL.GetApplicationByEmail(email);
        }
    }
}
