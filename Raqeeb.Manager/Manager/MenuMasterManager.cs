﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class MenuMasterManager
    {
        public List<MenuMaster> GetAll()
        {
            MenuMasterDAL MenuDAL = new MenuMasterDAL();
            var list = MenuDAL.GetAll();
            return list;
        }

        public List<MenuMaster> GetMenuType()
        {
            MenuMasterDAL MenuDAL = new MenuMasterDAL();
            var list = MenuDAL.GetMenuType();
            return list;
        }


        public MenuMaster Add(MenuMaster menu)
        {
            MenuMasterDAL MenuDAL = new MenuMasterDAL();
            return MenuDAL.Add(menu);
        }

        public MenuMaster Delete(MenuMaster menu)
        {
            MenuMasterDAL MenuDAL = new MenuMasterDAL();
            return MenuDAL.Delete(menu);
        }

        public MenuMaster Inactive(MenuMaster menu)
        {
            MenuMasterDAL MenuDAL = new MenuMasterDAL();
            return MenuDAL.Inactive(menu);
        }

        public MenuMaster Update(MenuMaster menu)
        {
            MenuMasterDAL MenuDAL = new MenuMasterDAL();
            return MenuDAL.Update(menu);
        }
    }
}
