﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class EventMediaManager : ManagerBase
    {

        MediaDAL mediaDAL = new MediaDAL();
        EventMediaDAL eventMediaDAL = new EventMediaDAL();
        public List<EventMedia> InsertBulk(List<EventMedia> eventMedia)
        {
            eventMedia = mediaDAL.InsertBulk(eventMedia);
            return eventMedia;
        }

        public List<Media> GetByEventCode(long eventCode)
        {
            List<Media> result = new List<Media>();

            result = eventMediaDAL.GetByEventCode(eventCode);

            return result;
        }
        public List<Media> InsertBulkMedia(List<Media> eventMedia)
        {
            eventMedia = mediaDAL.InsertBulkMedia(eventMedia);
            return eventMedia;
        }


    }
}
