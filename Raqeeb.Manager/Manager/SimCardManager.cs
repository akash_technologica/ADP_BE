﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class SimCardManager : ManagerBase
    {
        readonly SimCardMasterDAL simMasterDAL;
        public SimCardManager()
        {
            simMasterDAL = new SimCardMasterDAL();
        }

        public List<SimCard_Master> GetAll(string code)
        {
            var list = simMasterDAL.GetAll(code);
            return list;
        }
        public List<SimCard_Master> GetSim(string code)
        {
            var list = simMasterDAL.GetSim(code);
            return list;
        }

        public SimCard_Master Add(SimCard_Master sim)
        {
            return simMasterDAL.Add(sim);
        }

        internal void ActiveSim(string simCardCode)
        {
            simMasterDAL.Active(simCardCode);
        }

        public SimCard_Master Update(SimCard_Master sim)
        {
            return simMasterDAL.Update(sim);
        }
    }
}
