﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class GsmMasterManager : ManagerBase
    {
        readonly GsmMasterDAL gsmMasterDAL;
        public GsmMasterManager()
        {
            gsmMasterDAL = new GsmMasterDAL();
        }

        public List<GSM_Master> GetAll()
        {
            var list = gsmMasterDAL.GetAll();
            return list;
        }

        public GSM_Master Add(GSM_Master gsm)
        {
            return gsmMasterDAL.Add(gsm);
        }

        public GSM_Master Update(GSM_Master gsm)
        {
            return gsmMasterDAL.Update(gsm);
        }
    }
}
