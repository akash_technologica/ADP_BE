﻿using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class GeofenceMonitoring
    {
        public bool PointInPolygon(CoOrdinates p, List<CoOrdinates> poly)
        {
            int n = poly.Count();

            poly.Add(new CoOrdinates { Lat = poly[0].Lat, Lng = poly[0].Lng });
            CoOrdinates[] v = poly.ToArray();

            int wn = 0;    // the winding number counter

            // loop through all edges of the polygon
            for (int i = 0; i < n; i++)
            {   // edge from V[i] to V[i+1]
                if (v[i].Lat <= p.Lat)
                {         // start y <= P.y
                    if (v[i + 1].Lat > p.Lat)      // an upward crossing
                        if (isLeft(v[i], v[i + 1], p) > 0)  // P left of edge
                            ++wn;            // have a valid up intersect
                }
                else
                {                       // start y > P.y (no test needed)
                    if (v[i + 1].Lat <= p.Lat)     // a downward crossing
                        if (isLeft(v[i], v[i + 1], p) < 0)  // P right of edge
                            --wn;            // have a valid down intersect
                }
            }
            if (wn != 0)
                return true;
            else
                return false;

        }

        private static int isLeft(CoOrdinates P0, CoOrdinates P1, CoOrdinates P2)
        {
            double calc = ((P1.Lng - P0.Lng) * (P2.Lat - P0.Lat)
                    - (P2.Lng - P0.Lng) * (P1.Lat - P0.Lat));
            if (calc > 0)
                return 1;
            else if (calc < 0)
                return -1;
            else
                return 0;
        }

        public bool CheckInside(Circle circle, double longitude, double latitude)
        {
            return CalculateDistance(
                circle.Lon, circle.Lat, longitude, latitude
            ) < circle.Radius;
        }
        private double CalculateDistance(double longitude1, double latitude1,double longitude2, double latitude2)
        {
            double c =
                Math.Sin(ToRadians(latitude1)) *
                Math.Sin(ToRadians(latitude2)) +
                    Math.Cos(ToRadians(latitude1)) *
                    Math.Cos(ToRadians(latitude2)) *
                    Math.Cos(ToRadians(longitude2) -
                        ToRadians(longitude1));
            c = c > 0 ? Math.Min(1, c) : Math.Max(-1, c);
            return 6371 * 1000 * Math.Acos(c);
        }

        private double ToRadians(double deg)
        {
            return deg * (Math.PI / 180);
        }
      

    }
}
