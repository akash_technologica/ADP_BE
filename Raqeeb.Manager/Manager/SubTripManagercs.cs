﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class SubTripManager : ManagerBase
    {

        SubTripDAL subTripDAL = new SubTripDAL();

        public List<SubTrip> InsertBulk(List<SubTrip> subTrips)
        {
            subTrips = subTripDAL.InsertBulk(subTrips);
            return subTrips;
        }


        public SubTrip GetByCode(long code)
        {
            SubTrip result = new SubTrip();

            result = subTripDAL.GetByCode(code);

            return result;
        }


        public List<SubTrip> GetByTripCode(long code)
        {
            return subTripDAL.GetByTripCode(code);
        }

    }
}
