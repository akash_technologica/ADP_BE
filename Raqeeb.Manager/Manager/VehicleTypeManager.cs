﻿using Raqeeb.DataAccess.DataAccess;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager.Manager
{
    public class VehicleTypeManager : ManagerBase
    {
        readonly VehicleTypeDAL vehicleTypeDAL = new VehicleTypeDAL();

        public VehicleType Insert(VehicleType vehicleType)
        {
            return vehicleTypeDAL.Insert(vehicleType);
        }

        public List<VehicleType> GetVehicleTypeList()
        {
            return vehicleTypeDAL.GetAllVehicleType();
        }

        public VehicleType Update(VehicleType vehicleType)
        {
            return vehicleTypeDAL.UpdateAsync(vehicleType);
        }

        public VehicleType Delete(VehicleType vehicleType)
        {
            return vehicleTypeDAL.DeleteVehicleType(vehicleType);
        }

        public VehicleType UpdateVehicleType(VehicleType vehicleType)
        {
            return vehicleTypeDAL.UpdateVehicleType(vehicleType);
        }
    }
}
