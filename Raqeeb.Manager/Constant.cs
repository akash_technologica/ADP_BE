﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Manager
{
    public class Constant
    {
        public const string VehicleCacheKey = "vehiclesData";
        public const string EventsCacheKey = "eventsData";
        public const string CacheKey = "authKey";

        public const string END_POINT = "http://185.173.32.12:12056/";

        public const string TOKEN_URL = END_POINT + "api/v1/basic/key";
        public const string VEHICLE_GROUP_URL = END_POINT + "api/v1/basic/groups";
        public const string DEVICE_ADD_URL = END_POINT + "api/v1/basic/devices";
        public const string LIVE_POSITION_URL = END_POINT + "api/v1/basic/gps/detail";
        public const string ALARM_STATISTICS_URL = END_POINT + "api/v1/basic/alarm/count";
        public const string ALARM_DETAILS_URL = END_POINT + "api/v1/basic/alarm/detail";
        public const string DEVICE_ONLINE_OFFLINE_LOG_URL = END_POINT + "api/v1/basic/state/log";
        public const string DEVICE_ONLINE_OFFLINE_NOW_URL = END_POINT + "api/v1/basic/state/now";
        public const string DEVICE_LAST_COMMAND_URL = END_POINT + "api/v1/basic/state/last";

        public const string DRIVER_UPDATE_URL = END_POINT + "api/driver.updatedriver";

        public const string EVENTS_URL = END_POINT + "api/event.getaccountevents";
        public const string CAMLIST_URL = END_POINT + "api/cam.getcamlist";
        public const string UPLOAD_VIDEO_URL = END_POINT + "api/cam.requestupload";
        public const string GETDRIVER_URL = END_POINT + "api/driver.getdriverlist";
        public const string GETVEHICLE_URL = END_POINT + "api/vehicle.getvehiclelist";
        public const string GETCAMERA_URL = END_POINT + "api/cam.getcamlist";
        public const string GETLIVELOCATION_URL = END_POINT + "api/location.getlocation";
        public const string GETCAMERA_STATUS_URL = END_POINT + "api/location.getconnectionstatus";
        public const string GETCAMERA_ONLINE_OFFLINE_URL = END_POINT + "api/location.getconnectiondetails";
        public const string GETCAMERA_SNAPSHOT_URL = END_POINT + "api/snapshot.getsnapshots";
        // Returns list of available tag events names saved in the logged in account
        public const string GET_TAG_LIST = END_POINT + "api/event.gettagslist";
        //Returns an array of events, filtered by tags/priority/start DateTime/end DateTime
        public const string GET_EVENTS_TAG = END_POINT + "api/event.geteventsbytags";
        // Returns an array of dashboard statistics, filtered by last 7/30/360 days
        public const string GET_TAG_DASHBOARD = END_POINT + "api/graph.gettagsdashboard";
        //Add/register a new camera
        public const string GET_ADD_CAMERA = END_POINT + "api/cam.addcam";
        //Update camera data.
        public const string GET_UPDATE_CAMERA = END_POINT + "api/cam.updatecam";
        // Link the camera to an existing vehicle
        public const string GET_UPDATE_VEHICLE_CAMERA = END_POINT + "api/cam.updatecamvehicle";
        // Link the driver to an existing vehicle
        public const string GET_UPDATE_VEHICLE_DRIVER = END_POINT + "api/driver.updatedrivervehicle";
        // Delete registered camera.
        public const string GET_DELETE_CAMERA = END_POINT + "api/cam.deletecam";
        // Returns an array of cameras, filtered by camera id.
        public const string GET_CAMERA_BY_ID = END_POINT + "api/cam.getcambyid";
        // Set On/Off camera silent mode
        public const string GET_UPDATE_SILENT_MODE_CAMERA = END_POINT + "api/cam.updatesilentmode";
        // send a command to the camera as an example start/stop a live stream.
        public const string GET_CAMERA_SEND_COMMAND = END_POINT + "api/cam.sendcommand";
        //Get the command status and the current camera configuration.
        public const string GET_CAMERA_GET_COMMAND = END_POINT + "api/cam.getcommand";
        // Gets all trips done by certain vehicle(s) by sending the vehicle ids
        public const string GET_TRIP_BY_VEHICLE_ID = END_POINT + "api/trip.unitsTrips";

        public const string GET_TRIP_BY_ZONE = END_POINT + "api/trip.tripZones";

        // This endpoint returns the activities for certain units with filters
        public const string GET_ACTIVITY_BY_UNIT_ID = END_POINT + "api/activity.getActivites";

        // This endpoint returns the available activities types registered and tracked by the system or by this account id right now
        public const string GET_Last_ACTIVITY_BY_UserID = END_POINT + "api/activity.getLastActivitesTypes";

        /// <summary>
        /// This endpoint returns all the live activities that are still ongoing and haven’t been ended yet.
        /// </summary>
        public const string GET_Last_ACTIVITY_Live = END_POINT + "api/activity.getLastActivites";

        public const string Add_Video_Download_Task = END_POINT + "api/v1/basic/record/task";
        public const string Check_Video_Download_Task_State = END_POINT + "api/v1/basic/record/taskstate"; 
        public static string[] ACTIVITYTYPELIST = { "Vehicle-Acc", "Vehicle-Idle-Engine-On" }; /*{ "Hourly-Activity", "Over-Speed-Activity", "Vehicle-Acc", "Monthly-Activity", "Vehicle-Engine-On", "Daily-Activity", "Vehicle-Idle-Engine-On", "Driving-Activity", "Geo-Zone", "Yearly-Activity", "Vehicle-Air-On", "Vehicle-Brake-On" };*/

        public const string INVALID_REPORTS_PATH = @"D:\Logs\ValidationReports\inValidReports\";
        public const string VALID_REPORTS_PATH = @"D:\Logs\ValidationReports\validReports\";

        public const int MONTH_TIME = 2;

        public const int APP_VERSION = 49;
        public const string TOKEN_EMAIL = "officetest";
        public const string TOKEN_PASSWORD = "officetest123!";
        //public const string USER_ID = "-200";
        //public const bool RAW = true;




        public const string STREAMAX_BASE_URL = "http://185.173.32.12";
        public const string PORT = "12062";
        public const string BACK_CAMERA_CHANNEL = "15";
        public const string FRONT_CAMERA_CHANNEL = "15";
        public const string SIDE_CAMERA_CHANNEL = "15";
        public const string REAR_CAMERA_CHANNEL = "15";

    }
}
