﻿using System.Collections.Generic;

namespace Raqeeb.Framework.Core
{
    public class BussinessExceptionNotLocalized
    {

        public bool ReturnMessageOnly { get; set; }
        public string MessageCode { get; set; }
        public virtual List<ErrorData> Errors { get; set; }
        public BussinessExceptionNotLocalized(string meesageCode, string exception = null, object data = null, bool messageOnly = false)
        {
            if (Errors == null)
            {
                Errors = new List<ErrorData>();
            }

            ReturnMessageOnly = messageOnly;
            Errors.Add(new ErrorData(meesageCode, meesageCode));
            MessageCode = meesageCode;
        }

        public BussinessExceptionNotLocalized(List<ErrorData> _bussinessExceptions)
        {
            Errors = _bussinessExceptions;

        }
    }
}
