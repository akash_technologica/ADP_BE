﻿using System;
using System.Collections.Generic;

namespace Raqeeb.Framework.Core
{
    public class AuthorizationValidationException : ApplicationException
    {
        public AuthorizationValidationException(string message)
            : this(message, null)
        {
        }
        public AuthorizationValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    [Serializable]
    public class BussinessException : Exception
    {
        public bool ReturnMessageOnly { get; set; }
        public string MessageCode { get; set; }
        public virtual List<ErrorData> Errors { get; set; }
        public BussinessException(string meesageCode, string exception = null, object data = null, bool messageOnly = false)
        {
            if (Errors == null)
            {
                Errors = new List<ErrorData>();
            }

            ReturnMessageOnly = messageOnly; // // ReturnMessageOnly added only for print report from notification
            Errors.Add(new ErrorData(meesageCode, this, data, ""));
            MessageCode = meesageCode;
        }

        public BussinessException(List<ErrorData> _bussinessExceptions)
        {
            Errors = _bussinessExceptions;

        }
    }
}
