﻿using System;
using System.Configuration;

namespace Raqeeb.Framework.Core
{
    public class ErrorData
    {
        public string RefNumber { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
        public object Data { get; set; }
        public string MessageCode { get; set; }
        public object StackTrace { get; set; }

        public ErrorData(string messageCode)
        {
            Message = messageCode;
        }
        public ErrorData(string messageCode, string message)
        {
            MessageCode = messageCode;
            Message = message;
        }
        public ErrorData(string messageCode, Exception ex, string refNumber)
        {
            bool readErrorResource = bool.Parse(ConfigurationManager.AppSettings["ReadErrorResource"]);

            if (readErrorResource)
            {
                Message = messageCode;
            }
            RefNumber = refNumber;
            bool enableStackTrace = bool.Parse(ConfigurationManager.AppSettings["EnableStackTrace"]);
            if (enableStackTrace)
            {
                Error = ex.Message;
                StackTrace = ex.StackTrace;
            }
            MessageCode = messageCode;


        }
        public ErrorData(string messageCode, Exception ex, object data, string refNumber)
        {
            bool readErrorResource = bool.Parse(ConfigurationManager.AppSettings["ReadErrorResource"]);
            RefNumber = refNumber;
            if (readErrorResource)
            {
                Message = messageCode;
            }
            bool enableStackTrace = bool.Parse(ConfigurationManager.AppSettings["EnableStackTrace"]);
            if (enableStackTrace)
            {
                StackTrace = ex.StackTrace;
                Error = ex.Message;
            }

            Data = data;
            MessageCode = messageCode;
        }

        public ErrorData(Exception ex, string refNumber)
        {
            RefNumber = refNumber;
            bool enableStackTrace = bool.Parse(ConfigurationManager.AppSettings["EnableStackTrace"]);
            if (enableStackTrace)
            {
                StackTrace = ex.StackTrace;
                Error = ex.Message;
            }
        }
    }
}
