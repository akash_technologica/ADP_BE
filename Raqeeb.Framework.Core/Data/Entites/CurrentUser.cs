﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Framework.Core.Data.Entites
{
    public class CurrentUser
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public int ApplicationId { get; set; }
        public string ArabicName { get; set; }
        public string EnglishName { get; set; }
        public string MobileNumber { get; set; }
        public string ApiKey { get; set; }
        public string Secretkey { get; set; }

        public DateTime SubscriptionStartDate { get; set; }
        public DateTime SubscriptionEndDate { get; set; }
    }
}
