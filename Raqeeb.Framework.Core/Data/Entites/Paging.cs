﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Framework.Core.Data.Entites
{
    public class Paging<T>
    {
        #region Members

        private string _SortColumn;

        #endregion

        #region Properties 

        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public SortOrder SortOrder { get; set; }

        public string SortColumn
        {
            get
            {
                if (!string.IsNullOrEmpty(_SortColumn))
                {
                    return _SortColumn;
                }

                return "_id";
            }
            set
            {
                _SortColumn = value;
            }
        }

        public int ItemsToSkip
        {
            get { return ((PageNo - 1) * PageSize); }
        }

        public SortDefinition<T> SortCriteria
        {
            get
            {
                if (SortOrder == SortOrder.Asc)
                {
                    return Builders<T>.Sort.Ascending(SortColumn);
                }

                return Builders<T>.Sort.Descending(SortColumn);
            }
        }

        public T SearchFilters { get; set; }

        #endregion
    }
}
