﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Framework.Core.Data.Entites
{
    public enum SortOrder : int
    {
        Desc = 1,
        Asc = 2
    }
}
