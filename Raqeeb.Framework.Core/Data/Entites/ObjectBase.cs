﻿using System.ComponentModel.Composition.Hosting;

namespace Raqeeb.Framework.Core
{
    public abstract class ObjectBase
    {
        public ObjectBase()
        {
        }
        protected bool _IsDirty = false;
        public static CompositionContainer Container { get; set; }
    }
}
