﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Framework.Core.Data.Entites
{
    public class PagedSearchResultMongo<T> where T : class
    {
        public long NumberOfRecords { get; set; }

        public List<T> Collection { get; set; }
    }
}
