using MongoDB.Bson.Serialization.Attributes;
using System;


namespace Raqeeb.Framework.Core
{
    [Serializable]
    public class AuditableEntityBase : IdentifiableEntityBase
    {
        [BsonElement(elementName: "CREATION_DATE")]
        public DateTime CreationDate { get; set; }

        [BsonElement(elementName: "CREATED_BY")]
        public string CreatedBy { get; set; }

        [BsonElement(elementName: "MODIFICATION_DATE")]
        public DateTime? ModificationDate { get; set; }

        [BsonElement(elementName: "MODIFIED_BY")]
        public string ModifiedBy { get; set; }

        [BsonElement(elementName: "DELETION_DATE")]
        public DateTime? DeletionDate { get; set; }

        [BsonElement(elementName: "DELETED_BY")]
        public string DeletedBy { get; set; }
    }
}
