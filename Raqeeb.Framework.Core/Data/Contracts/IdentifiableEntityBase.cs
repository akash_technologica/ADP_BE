using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Raqeeb.Framework.Core
{
    [Serializable]
    public  class IdentifiableEntityBase
    {
        [BsonId]
        [BsonElement(elementName: "_id")]
        public BsonObjectId Id { get; set; }

        string _IdString;
        public string IdString
        {
            get
            {
                if (Id != null)
                    return Id.ToString();
                else
                    return _IdString;
            }
            set
            {
                _IdString = value;
            }
        }
    }
    
}
