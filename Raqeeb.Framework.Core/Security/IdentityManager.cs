﻿using Microsoft.AspNet.Identity;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Security.Claims;
using System.Web;

namespace Raqeeb.Framework.Core.Security
{
    public static class IdentityManager
    {
        public static CurrentUser GetCurrentUser()
        {
            var identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            CurrentUser currentUser = new CurrentUser();
            currentUser.Id = Convert.ToString(HttpContext.Current.User.Identity.GetUserId());
            currentUser.UserName = HttpContext.Current.User.Identity.GetUserName();
            return currentUser;
        }

        public static string GetLoggedInUserId()
        {
            return HttpContext.Current.User.Identity.GetUserId();
        }

    }
}
