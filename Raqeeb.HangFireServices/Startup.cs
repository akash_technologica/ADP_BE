﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Mongo;
using Microsoft.Owin;
using Owin;
using Raqeeb.Framework.Core;
using Raqeeb.HangFireServices.Services;

[assembly: OwinStartup(typeof(Raqeeb.HangFireServices.Startup))]

namespace Raqeeb.HangFireServices
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            FMSSynchService fmsSynchService = new FMSSynchService();           
            string connectionString = new Crypto().Decrypt(ConfigurationManager.AppSettings["HangFireMongoServerConnectionString"].ToString(), "o6806642kbM7c5");

            string dataBaseName = new Crypto().Decrypt(ConfigurationManager.AppSettings["HangFireDataBaseName"].ToString(), "o6806642kbM7c5");
           // GlobalConfiguration.Configuration.UseSqlServerStorage(@"Server=sql-2017-lsn01,1434; Database=BusViolation; Integrated Security=TRUE;");
            GlobalConfiguration.Configuration.UseSqlServerStorage(@"Server=.; Database=ADP; Integrated Security=TRUE;");
            app.UseHangfireDashboard();
            app.UseHangfireServer();




            BackgroundJob.Schedule(() => Console.WriteLine("This background job would execute after a delay."), TimeSpan.FromMilliseconds(1000));

            RecurringJob.AddOrUpdate("AUTH_TOKEN", () => fmsSynchService.GetAuthToken(), Cron.MinuteInterval(60));
            RecurringJob.AddOrUpdate("VEHICLE_GROUP", () => fmsSynchService.GetVehicleGroup(), Cron.MinuteInterval(60));
            RecurringJob.AddOrUpdate("Device_MASTER", () => fmsSynchService.GetDevice(), Cron.MinuteInterval(60));
            RecurringJob.AddOrUpdate("LIVE_POSITION", () => fmsSynchService.GetGPSInformation(), Cron.MinuteInterval(60));
            RecurringJob.AddOrUpdate("ALARM_STATISTICS", () => fmsSynchService.GetAlarmStatistics(), Cron.MinuteInterval(60));
            RecurringJob.AddOrUpdate("ALARM_DETAIL_INFO", () => fmsSynchService.AlarmDetailInfo(), Cron.MinuteInterval(60));
            RecurringJob.AddOrUpdate("DEVICE_ONLINE_OFFLINE_LOG", () => fmsSynchService.GetDeviceOnlineOfflineLog(), Cron.MinuteInterval(60));
            RecurringJob.AddOrUpdate("DEVICE_STATUS_NOW", () => fmsSynchService.GetDeviceOnlineNowState(), Cron.MinuteInterval(10));
            RecurringJob.AddOrUpdate("READ_JSON", () => fmsSynchService.ReadJsonFile(), Cron.HourInterval(Convert.ToInt32(ConfigurationManager.AppSettings["READ_JSON"])));
            RecurringJob.AddOrUpdate("CHECK_PROCESSED_VIOLATIONS", () => fmsSynchService.CheckProcessedFile(), Cron.HourInterval(Convert.ToInt32(ConfigurationManager.AppSettings["CHECK_PROCESSED_VIOLATIONS"])));
            RecurringJob.AddOrUpdate("READ_OLD_JSON", () => fmsSynchService.ReadOldJsonFile(), Cron.HourInterval(Convert.ToInt32(ConfigurationManager.AppSettings["READ_OLD_JSON"])));
            RecurringJob.AddOrUpdate("READ_LOGS", () => fmsSynchService.UpdateViolationsWithValidationStatus(), Cron.Daily(Convert.ToInt32(ConfigurationManager.AppSettings["READ_LOGS"])));
            RecurringJob.AddOrUpdate("CHECK_VIDEO_DOWNLOAD_STATUS", () => fmsSynchService.CheckVideoDownloadStatus(), Cron.Daily(Convert.ToInt32(ConfigurationManager.AppSettings["CHECK_VIDEO_DOWNLOAD_STATUS"])));

        }
    }
}
