﻿using Hangfire;
using Newtonsoft.Json;
using Raqeeb.Configuration;
using Raqeeb.Domain.DataModel;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using Raqeeb.Domain.Filters;
using Raqeeb.Domain.ViewModel;
using Raqeeb.Manager.Manager;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Data.Entity;
using Raqeeb.HangFireServices.Common;
using Microsoft.VisualBasic.FileIO;

namespace Raqeeb.HangFireServices.Services
{
    public class FMSSynchService
    {
        #region Declaration Managers
        WebServiceSetupManager webServiceSetupManager;
        ADPEntities aDPEntities;
        string violationPath = "";
        string validReportsPath = "";
        string invalidReportsPath = "";
        DateTime twoMonthsBeforeNow;
        string file_count = "";
        string take_count = "";

        AuthenticationModel session_jobject = new AuthenticationModel();
        #endregion
        public FMSSynchService()
        {

            if (Manager.Constant.MONTH_TIME != DateTime.Now.Month)
            {
                webServiceSetupManager = new WebServiceSetupManager();
                aDPEntities = new ADPEntities();
                violationPath = ConfigurationManager.AppSettings["VIOLATION_FOLDER_PATH"].ToString();
                validReportsPath = ConfigurationManager.AppSettings["VALID_REPORTS_PATH"].ToString();
                invalidReportsPath = ConfigurationManager.AppSettings["INVALID_REPORTS_PATH"].ToString();
                twoMonthsBeforeNow = DateTime.Now.AddMonths(Convert.ToInt32(ConfigurationManager.AppSettings["MONTH_BEFORE"]));
                file_count = ConfigurationManager.AppSettings["FILE_COUNT"].ToString();
                take_count = ConfigurationManager.AppSettings["TAKE_COUNT"].ToString();
            }
        }



        #region WEB SRV METHOD
        // get auth Token list from StreamMax
        public string GetAuthToken()
        {

            var client = new RestClient(Raqeeb.Manager.Constant.END_POINT);
            var request = new RestRequest(Raqeeb.Manager.Constant.TOKEN_URL, Method.GET).AddParameter("username", Raqeeb.Manager.Constant.TOKEN_EMAIL).AddParameter("password", Raqeeb.Manager.Constant.TOKEN_PASSWORD);
            client.Timeout = -1;
            var queryResult = client.Execute(request);
            session_jobject = JsonConvert.DeserializeObject<AuthenticationModel>(queryResult.Content);

            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddHours(1.0);
            if (cache.Contains(Raqeeb.Manager.Constant.CacheKey))
            {
                cache.Remove(Raqeeb.Manager.Constant.CacheKey, null);

            }
            else
            {
                // cache.Remove(Raqeeb.Manager.Constant.CacheKey, session_jobject.data.key);
                cache.Add(Raqeeb.Manager.Constant.CacheKey, session_jobject.data.key, cacheItemPolicy);
            }




            if (session_jobject.errorcode != (int)ErrorCode.Success)
            {
                string logMsg = DateTime.Now.ToString() + " " + Raqeeb.Manager.Constant.TOKEN_URL + " " + "Response Code:" + session_jobject.errorcode;
                Raqeeb.Common.Utility.WriteLog("Log", logMsg);
            }

            return session_jobject.data.key;
        }

        // get Vehicle Group list from StreamMax
        public void GetVehicleGroup()
        {
            ObjectCache cache = MemoryCache.Default;

            if (!cache.Contains(Raqeeb.Manager.Constant.CacheKey))
                GetAuthToken();

            var client = new RestClient(Raqeeb.Manager.Constant.END_POINT);

            var request = new RestRequest(Raqeeb.Manager.Constant.VEHICLE_GROUP_URL, Method.GET).AddParameter("key", (string)cache.Get(Raqeeb.Manager.Constant.CacheKey));
            client.Timeout = -1;
            var queryResult = client.Execute(request);
            Domain.ViewModel.VehicleGroupModel vehicleGroup = JsonConvert.DeserializeObject<Domain.ViewModel.VehicleGroupModel>(queryResult.Content);


            if (vehicleGroup.errorcode == (int)ErrorCode.Success)
            {

                foreach (var item in vehicleGroup.data)
                {
                    var isGroupExists = aDPEntities.VehicleGroups.Find(item.groupid);
                    if (isGroupExists == null)
                    {
                        Domain.DataModel.VehicleGroup vehGrp = new Domain.DataModel.VehicleGroup()
                        {
                            GroupId = item.groupid,

                            GroupFatherId = item.groupfatherid
                        };

                        aDPEntities.VehicleGroups.Add(vehGrp);
                        aDPEntities.SaveChanges();
                    }


                }
            }
            else
            {
                string logMsg = DateTime.Now.ToString() + " " + Raqeeb.Manager.Constant.VEHICLE_GROUP_URL + " " + "Response Code:" + vehicleGroup.errorcode;
                Raqeeb.Common.Utility.WriteLog("Log", logMsg);
            }

        }

        // get device list from StreamMax
        public void GetDevice()
        {
            ObjectCache cache = MemoryCache.Default;
            if (!cache.Contains(Raqeeb.Manager.Constant.CacheKey))
                GetAuthToken();
            var client = new RestClient(Raqeeb.Manager.Constant.END_POINT);

            var request = new RestRequest(Raqeeb.Manager.Constant.DEVICE_ADD_URL, Method.GET).AddParameter("key", (string)cache.Get(Raqeeb.Manager.Constant.CacheKey));
            client.Timeout = -1;
            var queryResult = client.Execute(request);
            DeviceViewModel device = JsonConvert.DeserializeObject<DeviceViewModel>(queryResult.Content);
            if (device.errorcode == (int)ErrorCode.Success)
            {
                foreach (var item in device.data)
                {
                    var isDeviceExists = aDPEntities.Devices.Find(item.deviceid);

                    if (isDeviceExists == null)
                    {
                        Domain.DataModel.Device deviceObj = new Domain.DataModel.Device()
                        {
                            DeviceId = item.deviceid,
                            CarLicense = item.carlicense,
                            ChannelCount = item.channelcount,
                            CName = item.cname,
                            DevicePassword = item.devicepassword,
                            DeviceType = item.devicetype,
                            DeviceUsername = item.deviceusername,
                            En = item.en,
                            GroupId = aDPEntities.Vehicles.Where(c => c.CarLicense == item.carlicense).Select(c => c.GruoupId).FirstOrDefault(),
                            LinkType = item.linktype,
                            PlateColor = item.platecolor,
                            RegisterIp = item.registerip,
                            RegisterPort = item.registerport,
                            Sim = item.sim,
                            TerninalId = item.terid,
                            TransmitIp = item.transmitip,
                            TransmitPort = item.transmitport,
                        };
                        aDPEntities.Devices.Add(deviceObj);
                        aDPEntities.SaveChanges();
                    }
                }
            }
            else
            {
                string logMsg = DateTime.Now.ToString() + " " + Raqeeb.Manager.Constant.DEVICE_ADD_URL + " " + "Response Code:" + device.errorcode;
                Raqeeb.Common.Utility.WriteLog("Log", logMsg);
            }

        }

        // get the gps detail information

        public void GetGPSInformation()
        {
            ObjectCache cache = MemoryCache.Default;
            List<Domain.DataModel.Vehicle> vehicleList = aDPEntities.Vehicles.Where(c => c.DeviceId != null).ToList();
            string startTime = "";
            string endTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            foreach (var item in vehicleList)
            {
                int count = aDPEntities.LivePositions.Where(c => c.VehicleCode == item.CarLicense.Substring(item.CarLicense.Length - 4)).Count();

                if (count > 0)
                {
                    List<Domain.DataModel.LivePosition> livePositionList = aDPEntities.LivePositions.Where(c => c.VehicleCode == item.CarLicense).ToList();
                    startTime = livePositionList.Last().GPS_Time.Value.AddSeconds(1).ToString("yyyy-MM-dd HH:mm:ss");
                }
                else
                {
                    startTime = DateTime.Now.AddDays(-60).Date.ToString("yyyy-MM-dd HH:mm:ss");
                }


                if (!cache.Contains(Raqeeb.Manager.Constant.CacheKey))
                    GetAuthToken();

                var client = new RestClient(Raqeeb.Manager.Constant.END_POINT);

                var request = new RestRequest(Raqeeb.Manager.Constant.LIVE_POSITION_URL, Method.POST)
                                .AddParameter("key", (string)cache.Get(Raqeeb.Manager.Constant.CacheKey))
                                .AddParameter("terid", item.DeviceId)
                                .AddParameter("starttime", startTime)
                                .AddParameter("endtime", endTime);
                client.Timeout = -1;
                var queryResult = client.Execute(request);
                LivePositionViewModel livePositions = JsonConvert.DeserializeObject<LivePositionViewModel>(queryResult.Content);



                if (livePositions.errorcode == (int)ErrorCode.Success)
                {
                    foreach (var position in livePositions.data)
                    {
                        Domain.DataModel.LivePosition posObj = new Domain.DataModel.LivePosition()
                        {
                            VehicleCode = item.CarLicense,
                            LocationTIme = Convert.ToDateTime(position.time),
                            Altitude = position.altitude,
                            Heading = position.direction,
                            Latitude = Convert.ToDecimal(position.gpslat),
                            Longitude = Convert.ToDecimal(position.gpslng),
                            GPS_Time = Convert.ToDateTime(Convert.ToDateTime(position.gpstime).ToString("yyyy-MM-dd HH:mm:ss")),
                            Speed = position.speed,
                            DoorStatus = position.state,
                            OdoMeter = position.mileage,
                        };

                        if (position.speed > 0)
                        {
                            posObj.EngineOn = true;
                            item.EngineOn = true;
                        }
                        else if (position.speed == 0 && item.Online.Value)
                        {
                            posObj.EngineOn = true;
                            posObj.Idle = true;
                            item.Idle = true;
                        }
                        else
                        {
                            posObj.EngineOn = false;
                            item.EngineOn = false;


                        }

                        item.Speed = position.speed;
                        item.Latitude = Convert.ToDecimal(position.gpslat);
                        item.Longitude = Convert.ToDecimal(position.gpslng);

                        aDPEntities.LivePositions.Add(posObj);
                        aDPEntities.SaveChanges();
                    }
                }
                else
                {
                    string logMsg = DateTime.Now.ToString() + " " + Raqeeb.Manager.Constant.LIVE_POSITION_URL + " " + "Response Code:" + livePositions.errorcode;
                    Raqeeb.Common.Utility.WriteLog("Log", logMsg);
                }
            }


        }

        public void GetAlarmStatistics()
        {
            ObjectCache cache = MemoryCache.Default;
            List<Domain.DataModel.Vehicle> vehicleList = aDPEntities.Vehicles.ToList();

            string startTime = "";
            string endTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            foreach (var item in vehicleList)
            {
                int count = aDPEntities.DeviceAlarmStatistics.Where(c => c.DeviceId == item.DeviceId).Count();

                List<DeviceAlarmStatistic> alarmStatisticsList = new List<DeviceAlarmStatistic>();
                if (count > 0)
                {
                    alarmStatisticsList = aDPEntities.DeviceAlarmStatistics.Where(c => c.DeviceId == item.DeviceId).ToList();
                    startTime = alarmStatisticsList.Last().AlarmDate.Value.AddSeconds(1).ToString("yyyy-MM-dd HH:mm:ss");
                }
                else
                {
                    startTime = DateTime.Now.AddDays(-30).Date.ToString("yyyy-MM-dd HH:mm:ss");
                }
                string[] teridList = new string[] { item.DeviceId };

                // Array array = teridList;

                if (!cache.Contains(Raqeeb.Manager.Constant.CacheKey))
                    GetAuthToken();

                var client = new RestClient(Raqeeb.Manager.Constant.ALARM_STATISTICS_URL);
                //var request = new RestRequest(Method.POST);
                //request.AddHeader("Content-Type", "application/json");


                //request.AddParameter("key", (string)cache.Get(Raqeeb.Manager.Constant.CacheKey));
                //request.AddParameter("terid", teridList);
                //request.AddParameter("type", 4);
                //request.AddParameter("starttime", startTime);
                //request.AddParameter("endtime", endTime);


                client.Timeout = -1;

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var body = new
                {
                    key = (string)cache.Get(Raqeeb.Manager.Constant.CacheKey),
                    terid = teridList,
                    type = 4,
                    starttime = startTime,
                    endtime = endTime
                };

                //  request.AddParameter("application/json", "{\r\n    \"key\": \"zT908g2j9nj56ahn0XCP7IPos8tqKjk15z%2BqWcs1jS4Ogqd%2Fz%2BLEZA%3D%3D\",\r\n    \"terid\": [\r\n        \"009C00031B\"\r\n    ],\r\n    \"type\": 4,\r\n    \"starttime\": \"2021-02-11 00:00:00\",\r\n    \"endtime\": \"2021-03-13 16:30:22\"\r\n}", ParameterType.RequestBody);

                request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);





                var queryResult = client.Execute(request);


                AlarmStatisticsViewModel alarmStats = JsonConvert.DeserializeObject<AlarmStatisticsViewModel>(queryResult.Content);



                if (alarmStats.errorcode == (int)ErrorCode.Success)
                {


                    foreach (var alarms in alarmStats.data)
                    {
                        DeviceAlarmStatistic deviceAlarmStatistic;
                        if (alarmStatisticsList.Count > 0)
                        {

                            if (alarmStatisticsList.Last().AlarmDate.Value.Date.ToString("yyyy-MM-dd") == alarms.date)
                            {
                                deviceAlarmStatistic = alarmStatisticsList.Last();
                                deviceAlarmStatistic.AlarmCount = alarms.count;
                                aDPEntities.Entry(deviceAlarmStatistic).State = EntityState.Modified;
                                aDPEntities.SaveChanges();
                            }
                            else
                            {
                                deviceAlarmStatistic = new DeviceAlarmStatistic()
                                {
                                    DeviceId = alarms.terid,
                                    AlarmDate = Convert.ToDateTime(Convert.ToDateTime(alarms.date).ToString("yyyy-MM-dd HH:mm:ss")),
                                    AlarmCount = alarms.count
                                };


                                aDPEntities.DeviceAlarmStatistics.Add(deviceAlarmStatistic);
                                aDPEntities.SaveChanges();
                            }


                        }
                        else
                        {
                            deviceAlarmStatistic = new DeviceAlarmStatistic()
                            {
                                DeviceId = alarms.terid,
                                AlarmDate = Convert.ToDateTime(Convert.ToDateTime(alarms.date).ToString("yyyy-MM-dd HH:mm:ss")),
                                AlarmCount = alarms.count
                            };


                            aDPEntities.DeviceAlarmStatistics.Add(deviceAlarmStatistic);
                            aDPEntities.SaveChanges();
                        }

                    }
                }
                else
                {
                    string logMsg = DateTime.Now.ToString() + " " + Raqeeb.Manager.Constant.LIVE_POSITION_URL + " " + "Response Code:" + alarmStats.errorcode;
                    Raqeeb.Common.Utility.WriteLog("Log", logMsg);
                }
            }
        }

        public void AlarmDetailInfo()
        {
            ObjectCache cache = MemoryCache.Default;
            List<Domain.DataModel.Vehicle> vehicleList = aDPEntities.Vehicles.Where(c => c.DeviceId != null).ToList();

            string startTime = "";
            string endTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            foreach (var item in vehicleList)
            {

                int count = aDPEntities.DeviceAlarmDetails.Where(c => c.DeviceId == item.DeviceId).Count();

                List<Domain.DataModel.DeviceAlarmDetail> alarmDetailList = new List<Domain.DataModel.DeviceAlarmDetail>();
                if (count > 0)
                {
                    alarmDetailList = aDPEntities.DeviceAlarmDetails.Where(c => c.DeviceId == item.DeviceId).ToList();
                    startTime = alarmDetailList.Last().Time.Value.AddSeconds(1).ToString("yyyy-MM-dd HH:mm:ss");
                }
                else
                {
                    startTime = DateTime.Now.AddDays(-30).Date.ToString("yyyy-MM-dd HH:mm:ss");
                }
                string[] teridList = new string[] { item.DeviceId };
                int[] alarmType = new int[] { };

                // Array array = teridList;

                if (!cache.Contains(Raqeeb.Manager.Constant.CacheKey))
                    GetAuthToken();

                var client = new RestClient(Raqeeb.Manager.Constant.ALARM_DETAILS_URL);

                client.Timeout = -1;

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var body = new
                {
                    key = (string)cache.Get(Raqeeb.Manager.Constant.CacheKey),
                    terid = teridList,
                    type = alarmType,
                    starttime = startTime,
                    endtime = endTime
                };

                //  request.AddParameter("application/json", "{\r\n    \"key\": \"zT908g2j9nj56ahn0XCP7IPos8tqKjk15z%2BqWcs1jS4Ogqd%2Fz%2BLEZA%3D%3D\",\r\n    \"terid\": [\r\n        \"009C00031B\"\r\n    ],\r\n    \"type\": 4,\r\n    \"starttime\": \"2021-02-11 00:00:00\",\r\n    \"endtime\": \"2021-03-13 16:30:22\"\r\n}", ParameterType.RequestBody);

                request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);

                var queryResult = client.Execute(request);


                AlarmDetailViewModel alarmDetails = JsonConvert.DeserializeObject<AlarmDetailViewModel>(queryResult.Content);

                if (alarmDetails.errorcode == (int)ErrorCode.Success)
                {
                    foreach (var alarms in alarmDetails.data)
                    {

                        DeviceAlarmDetail deviceAlarmStatistic = new DeviceAlarmDetail()
                        {
                            DeviceId = alarms.terid,
                            AlarmContent = alarms.content,
                            Altitude = alarms.altitude,
                            CmdType = alarms.cmdtype,
                            Direction = alarms.direction,
                            GpsTime = Convert.ToDateTime(alarms.gpstime),
                            Latitude = Convert.ToDecimal(alarms.gpslat),
                            Longitude = Convert.ToDecimal(alarms.gpslng),
                            RecordSpeed = alarms.recordspeed,
                            Speed = alarms.speed,
                            State = alarms.state,
                            Type = alarms.type,
                            Time = Convert.ToDateTime(alarms.time),
                            GroupId = aDPEntities.Devices.Where(c => c.DeviceId == alarms.terid).Select(c => c.GroupId).FirstOrDefault()
                        };


                        aDPEntities.DeviceAlarmDetails.Add(deviceAlarmStatistic);
                        aDPEntities.SaveChanges();


                    }
                }
                else
                {
                    string logMsg = DateTime.Now.ToString() + " " + Raqeeb.Manager.Constant.LIVE_POSITION_URL + " " + "Response Code:" + alarmDetails.errorcode;
                    Raqeeb.Common.Utility.WriteLog("Log", logMsg);
                }
            }
        }

        public void GetDeviceOnlineOfflineLog()
        {
            ObjectCache cache = MemoryCache.Default;
            List<Domain.DataModel.Vehicle> vehicleList = aDPEntities.Vehicles.ToList();

            string startTime = "";
            string endTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            foreach (var item in vehicleList)
            {
                int count = aDPEntities.DeviveOnlineOfflineLogs.Where(c => c.DeviceId == item.DeviceId).Count();

                List<DeviveOnlineOfflineLog> logList = new List<DeviveOnlineOfflineLog>();
                if (count > 0)
                {
                    logList = aDPEntities.DeviveOnlineOfflineLogs.Where(c => c.DeviceId == item.DeviceId).ToList();
                    startTime = logList.Last().Date.Value.AddSeconds(1).ToString("yyyy-MM-dd HH:mm:ss");
                }
                else
                {
                    startTime = DateTime.Now.AddDays(-30).Date.ToString("yyyy-MM-dd HH:mm:ss");
                }
                string[] teridList = new string[] { item.DeviceId };



                if (!cache.Contains(Raqeeb.Manager.Constant.CacheKey))
                    GetAuthToken();

                var client = new RestClient(Raqeeb.Manager.Constant.DEVICE_ONLINE_OFFLINE_LOG_URL);
                client.Timeout = -1;

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var body = new
                {
                    key = (string)cache.Get(Raqeeb.Manager.Constant.CacheKey),
                    terid = teridList,
                    type = 4,
                    starttime = startTime,
                    endtime = endTime
                };

                request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);
                var queryResult = client.Execute(request);
                DeviceOnlineOfflineLogViewModel deviceLogs = JsonConvert.DeserializeObject<DeviceOnlineOfflineLogViewModel>(queryResult.Content);
                if (deviceLogs.errorcode == (int)ErrorCode.Success)
                {


                    foreach (var log in deviceLogs.data)
                    {
                        DeviveOnlineOfflineLog deviveOnlineOfflineLog;
                        if (logList.Count > 0)
                        {

                            if (logList.Last().Date.Value.Date.ToString("yyyy-MM-dd HH:mm:ss") != log.time)
                            {
                                deviveOnlineOfflineLog = logList.Last();
                                deviveOnlineOfflineLog.Type = log.type;
                                deviveOnlineOfflineLog.Date = Convert.ToDateTime(log.time);
                                aDPEntities.Entry(deviveOnlineOfflineLog).State = EntityState.Modified;
                                aDPEntities.SaveChanges();
                            }

                        }
                        else
                        {
                            deviveOnlineOfflineLog = new DeviveOnlineOfflineLog()
                            {
                                DeviceId = log.terid,
                                GroupId = aDPEntities.Devices.Where(c => c.DeviceId == log.terid).Select(c => c.GroupId).FirstOrDefault(),
                                Date = Convert.ToDateTime(Convert.ToDateTime(log.time).ToString("yyyy-MM-dd HH:mm:ss")),
                                Type = log.type
                            };


                            aDPEntities.DeviveOnlineOfflineLogs.Add(deviveOnlineOfflineLog);
                            aDPEntities.SaveChanges();
                        }

                    }
                }
                else
                {
                    string logMsg = DateTime.Now.ToString() + " " + Raqeeb.Manager.Constant.LIVE_POSITION_URL + " " + "Response Code:" + deviceLogs.errorcode;
                    Raqeeb.Common.Utility.WriteLog("Log", logMsg);
                }
            }


        }

        public void GetDeviceOnlineNowState()
        {
            ObjectCache cache = MemoryCache.Default;
            List<Domain.DataModel.Vehicle> vehicleList = aDPEntities.Vehicles.Where(c => c.DeviceId != null).ToList();


            foreach (var item in vehicleList)
            {

                int count = aDPEntities.DeviveOnlineOfflineLogs.Where(c => c.DeviceId == item.DeviceId).Count();

                List<DeviveOnlineOfflineLog> logList = new List<DeviveOnlineOfflineLog>();

                string[] teridList = new string[] { item.DeviceId };



                if (!cache.Contains(Raqeeb.Manager.Constant.CacheKey))
                    GetAuthToken();

                var client = new RestClient(Raqeeb.Manager.Constant.DEVICE_ONLINE_OFFLINE_NOW_URL);
                client.Timeout = -1;

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var body = new
                {
                    key = (string)cache.Get(Raqeeb.Manager.Constant.CacheKey),
                    terid = teridList,
                };

                request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);

                var queryResult = client.Execute(request);


                DeviceOnlineOfflineLogViewModel deviceLogs = JsonConvert.DeserializeObject<DeviceOnlineOfflineLogViewModel>(queryResult.Content);

                string lastTime = GetDeviceLastCommand(teridList);
                if (lastTime != "")
                {
                    item.LastCommandDate = Convert.ToDateTime(lastTime);

                }

                if (deviceLogs.errorcode == (int)ErrorCode.Success)
                {
                    Domain.DataModel.Device device = aDPEntities.Devices.Where(c => c.DeviceId == item.DeviceId).FirstOrDefault();

                    if (deviceLogs.data.Count > 0)
                    {
                        item.Online = true;
                        device.Online = true;
                        device.LastCommandDate = Convert.ToDateTime(lastTime);

                    }
                    else
                    {
                        item.Online = false;
                        device.Online = false;
                    }

                    aDPEntities.Entry(item).State = EntityState.Modified;
                    aDPEntities.SaveChanges();


                }
                else
                {
                    string logMsg = DateTime.Now.ToString() + " " + Raqeeb.Manager.Constant.LIVE_POSITION_URL + " " + "Response Code:" + deviceLogs.errorcode;
                    Raqeeb.Common.Utility.WriteLog("Log", logMsg);
                }
            }
        }

        public string GetDeviceLastCommand(string[] deviceId)
        {
            ObjectCache cache = MemoryCache.Default;
            string time = "";
            var client = new RestClient(Raqeeb.Manager.Constant.DEVICE_LAST_COMMAND_URL);
            client.Timeout = -1;

            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var body = new
            {
                key = (string)cache.Get(Raqeeb.Manager.Constant.CacheKey),
                terid = deviceId,
            };

            request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);

            var queryResult = client.Execute(request);


            DeviceOnlineOfflineLogViewModel deviceLogs = JsonConvert.DeserializeObject<DeviceOnlineOfflineLogViewModel>(queryResult.Content);

            if (deviceLogs.errorcode == (int)ErrorCode.Success)
            {
                if (deviceLogs.data.Count > 0)
                {
                    time = deviceLogs.data[0].time;
                }
                else
                {
                    time = "";
                }
                return time;

            }
            else
            {
                string logMsg = DateTime.Now.ToString() + " " + Raqeeb.Manager.Constant.LIVE_POSITION_URL + " " + "Response Code:" + deviceLogs.errorcode;
                Raqeeb.Common.Utility.WriteLog("Log", logMsg);
                return time;
            }

        }
        public void ReadJsonFile()
        {
            EventEvidence lastFileName = aDPEntities.EventEvidences.Select(c => c).FirstOrDefault();

            string curr_date = DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                var directories = CustomSearcher.GetDirectoriesOnly(@violationPath + "\\" + curr_date);
                SaveFiles(directories);
            }
            catch (Exception ex)
            {

                Raqeeb.Common.Utility.WriteLog(ex);
            }


        }

        public void ReadOldJsonFile()
        {
            try
            {
                List<string> datesFolderList = CustomSearcher.GetDirectoriesOnly(@violationPath);

                var lastTwoMonthList = datesFolderList.Skip(Math.Max(0, datesFolderList.Count() - Convert.ToInt32(file_count))).Take(Convert.ToInt32(take_count));

                foreach (var violationFolder_item in lastTwoMonthList)
                {
                    List<string> violationFolderList = CustomSearcher.GetDirectoriesOnly(@violationFolder_item);

                    SaveFiles(violationFolderList);

                }

            }
            catch (Exception ex)
            {
                Raqeeb.Common.Utility.WriteLog(ex);

            }

        }

        public void CheckProcessedFile()
        {

            List<EventEvidence> unProcessed_ViolationsList = aDPEntities.EventEvidences.Where(c => c.Debug_Code != Constant.SUCCESS_PROCESSED_CODE && c.AlarmTime.Value >= twoMonthsBeforeNow).Select(c => c).ToList();

            foreach (var unprocessed_item in unProcessed_ViolationsList)
            {
                try
                {
                    var addressFile = unprocessed_item.FilePath.Split('\\');

                    var absolute_path = addressFile[0] + "\\" + addressFile[1] + "\\" + addressFile[2] + "\\" + addressFile[3] + "\\" + Constant.DEBUG_JSON;
                    var actualPath = unprocessed_item.FilePath;
                    if (addressFile[4] == Constant.ADDRESS_FILE)
                    {
                        addressFile[4] = addressFile[3];
                        actualPath = addressFile[0] + "\\" + addressFile[1] + "\\" + addressFile[2] + "\\" + addressFile[3] + "\\" + addressFile[3];
                    }

                    if (unprocessed_item.IterationCount.Value <= 24)
                    {

                        RecheckFiles(unprocessed_item, absolute_path, actualPath);


                    }
                }
                catch (Exception ex)
                {
                    continue;
                }


            }



        }

        private void RecheckFiles(EventEvidence eventEvidences, string debugFile, string actualFile)
        {

            if (File.Exists(@actualFile + ".json"))
            {
                using (StreamReader r = File.OpenText(@actualFile + ".json"))
                {
                    string json = r.ReadToEnd();

                    Item items = JsonConvert.DeserializeObject<Item>(json);


                    if (items != null)
                    {
                        //bool isExist = aDPEntities.EventEvidences.Where(c => c.EvidenceID == items.EvidenceID).Any();


                        eventEvidences.AlarmID = items.AlarmID;
                        eventEvidences.AlarmName = items.AlarmName;
                        eventEvidences.AlarmTime = Convert.ToDateTime(items.AlarmTime);
                        eventEvidences.BusId = items.CarLicense;
                        eventEvidences.EvidenceName = items.EvidenceName;
                        eventEvidences.EvidenceType = items.EvidenceType.ToString();
                        //ExtraInfo = items.ExtraInfo.ToString(),
                        eventEvidences.Lat = Convert.ToDecimal(items.Lat);
                        eventEvidences.Long = Convert.ToDecimal(items.Long);
                        eventEvidences.FileName = items.EvidenceID;
                        eventEvidences.C27Spic = items.C27Spic;
                        eventEvidences.LegalCode = items.LegalCode;
                        eventEvidences.RadarKind = items.RadarKind;
                        eventEvidences.RoadID = items.RoadID;
                        eventEvidences.RoadRegion = items.RoadRegion;
                        eventEvidences.ViolationDirection = items.ViolationDirection;
                        eventEvidences.ViolationLane = items.ViolationLane;
                        eventEvidences.ViolationTypeID = items.ViolationTypeID;
                        eventEvidences.TG_Distance = items.TG_Distance;
                        eventEvidences.TG_Duration = items.TG_Duration;
                        eventEvidences.TG_Speed = items.TG_Speed;
                        eventEvidences.Street = items.Street;
                        eventEvidences.District = items.District;
                        eventEvidences.FilePath = actualFile;
                        eventEvidences.FileName = items.EvidenceID;

                        if (items.ExtraInfo != null)
                        {
                            eventEvidences.City = items.ExtraInfo.City;
                            eventEvidences.Type = items.ExtraInfo.Type;
                            eventEvidences.VehicleNumber = items.ExtraInfo.VN;
                            eventEvidences.Category = items.ExtraInfo.Category;
                        }

                        if (items.C27V != "" && items.C27V != null)
                        {
                            eventEvidences.C27V = @actualFile + @"\" + items.C27V;
                        }
                        if (items.C27Spic != "" && items.C27Spic != null)
                        {
                            eventEvidences.C27Spic = @actualFile + @"\" + items.C27Spic;
                        }
                        if (items.C27Wpic != "" && items.C27Wpic != null)
                        {
                            eventEvidences.C27Wpic = @actualFile + @"\" + items.C27Wpic;
                        }
                        if (items.C28Back != "" && items.C28Back != null)
                        {
                            eventEvidences.C28Back = @actualFile + @"\" + items.C28Back;
                        }
                        if (items.C28Overview != "" && items.C28Overview != null)
                        {
                            eventEvidences.C28Overview = @actualFile + @"\" + items.C28Overview;
                        }

                        if (items.C28OverviewPic != "" && items.C28OverviewPic != null)
                        {
                            eventEvidences.C28OverviewPic = @actualFile + @"\" + items.C28OverviewPic;
                        }



                        if (File.Exists(debugFile))
                        {
                            using (StreamReader rs = File.OpenText(@debugFile))
                            {
                                string debug_json = rs.ReadToEnd();
                                Debug debug_items = JsonConvert.DeserializeObject<Debug>(debug_json);

                                if (debug_items != null)
                                {

                                    //  eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                                    eventEvidences.Comments = debug_items.Message;
                                    eventEvidences.Debug_Code = debug_items.debugCode;
                                    eventEvidences.Debug_Time = debug_items.Time;
                                    eventEvidences.LastUpdate = DateTime.Now;
                                    eventEvidences.IterationCount += 1;
                                }
                                else if (eventEvidences.Category == null)
                                {
                                    // eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                                    eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                                    eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                                    eventEvidences.LastUpdate = DateTime.Now;
                                    eventEvidences.IterationCount += 1;

                                }

                            }
                        }
                        else
                        {
                            //eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                            eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                            eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                            eventEvidences.LastUpdate = DateTime.Now;
                            eventEvidences.IterationCount += 1;
                        }

                        if (eventEvidences.Category != null)
                        {
                            eventEvidences.Debug_Code = Constant.SUCCESS_PROCESSED_CODE;
                            eventEvidences.Comments = Constant.SUCCESS_PROCESSED;
                            eventEvidences.ProcessedStatus = true;
                        }

                        if (eventEvidences.Debug_Code == Constant.SUCCESS_PROCESSED_CODE)
                        {
                            eventEvidences.ProcessedStatus = true;
                        }
                        else
                        {
                            eventEvidences.ProcessedStatus = false;
                        }


                        aDPEntities.Entry(eventEvidences).State = EntityState.Modified;
                        try
                        {
                            aDPEntities.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            aDPEntities.Dispose();
                            aDPEntities = new ADPEntities();

                        }



                    }
                    else
                    {
                        if (File.Exists(@debugFile))
                        {
                            using (StreamReader rs = File.OpenText(@debugFile))
                            {
                                string debug_json = rs.ReadToEnd();
                                Debug debug_items = JsonConvert.DeserializeObject<Debug>(debug_json);

                                if (eventEvidences.Debug_Code == Constant.SUCCESS_PROCESSED_CODE)
                                {
                                    eventEvidences.ProcessedStatus = true;
                                }
                                else
                                {
                                    eventEvidences.ProcessedStatus = false;
                                }

                                if (debug_items != null)
                                {

                                    //  eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                                    eventEvidences.Comments = debug_items.Message;
                                    eventEvidences.Debug_Code = debug_items.debugCode;
                                    eventEvidences.Debug_Time = debug_items.Time;
                                    eventEvidences.LastUpdate = DateTime.Now;
                                    eventEvidences.IterationCount += 1;
                                }
                                else
                                {
                                    //  eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                                    eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                                    eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                                    eventEvidences.LastUpdate = DateTime.Now;
                                    eventEvidences.IterationCount += 1;

                                }

                            }
                        }
                        else
                        {
                            // eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                            if (eventEvidences.Debug_Code == Constant.SUCCESS_PROCESSED_CODE)
                            {
                                eventEvidences.ProcessedStatus = true;
                            }
                            else
                            {
                                eventEvidences.ProcessedStatus = false;
                            }
                            eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                            eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                            eventEvidences.LastUpdate = DateTime.Now;
                            eventEvidences.IterationCount += 1;
                        }
                    }




                    //  object extraInfo = JsonConvert.DeserializeObject<object>(items.ExtraInfo)


                }

            }
            else if (File.Exists(debugFile))
            {
                using (StreamReader r = File.OpenText(@debugFile))
                {
                    string json = r.ReadToEnd();
                    try
                    {
                        Debug items = JsonConvert.DeserializeObject<Debug>(json);

                        if (items != null)
                        {
                            // eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];

                            eventEvidences.LastUpdate = DateTime.Now;
                            eventEvidences.IterationCount = 0;

                            if (items.debugCode == null)
                            {
                                eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                                eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                            }
                            else
                            {
                                eventEvidences.Comments = items.Message;
                                eventEvidences.Debug_Code = items.debugCode;
                                eventEvidences.Debug_Time = items.Time;
                            }
                            if (eventEvidences.Debug_Code == Constant.SUCCESS_PROCESSED_CODE)
                            {
                                eventEvidences.ProcessedStatus = true;
                            }
                            else
                            {
                                eventEvidences.ProcessedStatus = false;
                            }

                        }
                        else
                        {
                            //  eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                            eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                            eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                            eventEvidences.LastUpdate = DateTime.Now;
                            eventEvidences.IterationCount += 1;
                            if (eventEvidences.Debug_Code == Constant.SUCCESS_PROCESSED_CODE)
                            {
                                eventEvidences.ProcessedStatus = true;
                            }
                            else
                            {
                                eventEvidences.ProcessedStatus = false;
                            }

                        }
                        aDPEntities.Entry(eventEvidences).State = EntityState.Modified;
                        aDPEntities.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        Raqeeb.Common.Utility.WriteLog(ex);

                    }

                }

            }

            else
            {
                // eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                eventEvidences.LastUpdate = DateTime.Now;
                eventEvidences.IterationCount += 1;
                if (eventEvidences.Debug_Code == Constant.SUCCESS_PROCESSED_CODE)
                {
                    eventEvidences.ProcessedStatus = true;
                }
                else
                {
                    eventEvidences.ProcessedStatus = false;
                }

                try
                {
                    aDPEntities.Entry(eventEvidences).State = EntityState.Modified;
                    aDPEntities.SaveChanges();

                }
                catch (Exception ex)
                {

                    Raqeeb.Common.Utility.WriteLog(ex);

                }

            }




            //WebServiceSetUp lastFileName = new WebServiceSetUp();


        }


        private void SaveFiles(List<string> violationFolderList)
        {
            string lastfile = "";

            foreach (var violationFile_item in violationFolderList)
            {
                var addressFile = violationFile_item.Split('\\');
                lastfile = addressFile[3];
                EventEvidence eventEvidences = new EventEvidence();
                eventEvidences.EvidenceID = addressFile[3];
                try
                {
                    if (File.Exists(@violationFile_item + "\\" + lastfile + ".json"))
                    {
                        ReadViolationFile(violationFile_item, addressFile, Constant.VIOLATION_NEW_JSON_TYPE);

                    }
                    else if (File.Exists(@violationFile_item + "\\" + "EvidenceInfo.json"))
                    {
                        ReadViolationFile(violationFile_item, addressFile, Constant.VIOLATION_OLD_JSON_TYPE);
                    }

                    else if (File.Exists(@violationFile_item + "\\" + Constant.DEBUG_JSON))
                    {
                        using (StreamReader r = File.OpenText(@violationFile_item + "\\" + Constant.DEBUG_JSON))
                        {
                            string json = r.ReadToEnd();
                            try
                            {
                                Debug items = JsonConvert.DeserializeObject<Debug>(json);

                                if (items != null)
                                {
                                    eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];

                                    eventEvidences.LastUpdate = DateTime.Now;
                                    eventEvidences.IterationCount = 0;

                                    if (items.debugCode == null)
                                    {
                                        eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                                        eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                                    }
                                    else
                                    {
                                        eventEvidences.Comments = items.Message;
                                        eventEvidences.Debug_Code = items.debugCode;
                                        eventEvidences.Debug_Time = items.Time;
                                    }

                                }
                                else
                                {
                                    eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                                    eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                                    eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                                    eventEvidences.LastUpdate = DateTime.Now;
                                    eventEvidences.IterationCount = 0;

                                }

                                if (eventEvidences.Debug_Code == Constant.SUCCESS_PROCESSED_CODE)
                                {
                                    eventEvidences.ProcessedStatus = true;
                                }
                                else
                                {
                                    eventEvidences.ProcessedStatus = false;
                                }

                                aDPEntities.EventEvidences.Add(eventEvidences);
                                aDPEntities.SaveChanges();

                            }
                            catch (Exception ex)
                            {
                                Raqeeb.Common.Utility.WriteLog(ex);
                                continue;
                            }

                        }

                    }

                    else
                    {
                        eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                        eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                        eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                        eventEvidences.LastUpdate = DateTime.Now;
                        eventEvidences.IterationCount = 0;

                        if (eventEvidences.Debug_Code == Constant.SUCCESS_PROCESSED_CODE)
                        {
                            eventEvidences.ProcessedStatus = true;
                        }
                        else
                        {
                            eventEvidences.ProcessedStatus = false;
                        }

                        try
                        {
                            aDPEntities.EventEvidences.Add(eventEvidences);
                            aDPEntities.SaveChanges();

                        }
                        catch (Exception ex)
                        {

                            Raqeeb.Common.Utility.WriteLog(ex);
                            continue;
                        }

                    }
                }
                catch (Exception)
                {
                    continue;
                }



                //WebServiceSetUp lastFileName = new WebServiceSetUp();

            }
        }

        private void ReadViolationFile(string violationFile_item, string[] addressFile, string fileType)
        {
            if (fileType == Constant.VIOLATION_OLD_JSON_TYPE)
            {
                addressFile[3] = Constant.ADDRESS_FILE;
            }
            using (StreamReader r = File.OpenText(@violationFile_item + "\\" + addressFile[3] + ".json"))
            {
                string json = r.ReadToEnd();
                try
                {
                    Item items = JsonConvert.DeserializeObject<Item>(json);
                    EventEvidence eventEvidences = new EventEvidence();

                    if (items != null)
                    {
                        bool isExist = aDPEntities.EventEvidences.Where(c => c.EvidenceID == items.EvidenceID).Any();
                        if (!isExist)
                        {
                            eventEvidences = new EventEvidence()
                            {
                                AlarmID = items.AlarmID,
                                AlarmName = items.AlarmName,
                                AlarmTime = Convert.ToDateTime(items.AlarmTime),
                                BusId = items.CarLicense,
                                EvidenceID = items.EvidenceID,
                                EvidenceName = items.EvidenceName,
                                EvidenceType = items.EvidenceType.ToString(),
                                //ExtraInfo = items.ExtraInfo.ToString(),
                                Lat = Convert.ToDecimal(items.Lat),
                                Long = Convert.ToDecimal(items.Long),
                                FileName = addressFile[3],
                                C27Spic = items.C27Spic,
                                LegalCode = items.LegalCode,
                                RadarKind = items.RadarKind,
                                RoadID = items.RoadID,
                                RoadRegion = items.RoadRegion,
                                ViolationDirection = items.ViolationDirection,
                                ViolationLane = items.ViolationLane,
                                ViolationTypeID = items.ViolationTypeID,
                                TG_Distance = items.TG_Distance,
                                TG_Duration = items.TG_Duration,
                                TG_Speed = items.TG_Speed,
                                Street = items.Street,
                                District = items.District
                                //  City = items.ExtraInfo.City,
                                //  Type = items.ExtraInfo.Type,
                                // VehicleNumber = items.ExtraInfo.VN,
                                // Category = items.ExtraInfo.Category
                            };

                            if (addressFile[3] != Constant.ADDRESS_FILE)
                            {
                                eventEvidences.City = items.ExtraInfo.City;
                                eventEvidences.Type = items.ExtraInfo.Type;
                                eventEvidences.VehicleNumber = items.ExtraInfo.VN;
                                eventEvidences.Category = items.ExtraInfo.Category;
                            }

                            if (items.C27V != "" && items.C27V != null)
                            {
                                eventEvidences.C27V = @violationPath + addressFile[2] + @"\" + items.EvidenceID + @"\" + items.C27V;
                            }
                            if (items.C27Spic != "" && items.C27Spic != null)
                            {
                                eventEvidences.C27Spic = @violationPath + addressFile[2] + @"\" + items.EvidenceID + @"\" + items.C27Spic;
                            }
                            if (items.C27Wpic != "" && items.C27Wpic != null)
                            {
                                eventEvidences.C27Wpic = @violationPath + addressFile[2] + @"\" + items.EvidenceID + @"\" + items.C27Wpic;
                            }
                            if (items.C28Back != "" && items.C28Back != null)
                            {
                                eventEvidences.C28Back = @violationPath + addressFile[2] + @"\" + items.EvidenceID + @"\" + items.C28Back;
                            }
                            if (items.C28Overview != "" && items.C28Overview != null)
                            {
                                eventEvidences.C28Overview = @violationPath + addressFile[2] + @"\" + items.EvidenceID + @"\" + items.C28Overview;
                            }

                            if (items.C28OverviewPic != "" && items.C28OverviewPic != null)
                            {
                                eventEvidences.C28OverviewPic = @violationPath + addressFile[2] + @"\" + items.EvidenceID + @"\" + items.C28OverviewPic;
                            }



                            if (File.Exists(@violationFile_item + "\\" + Constant.DEBUG_JSON))
                            {
                                using (StreamReader rs = File.OpenText(@violationFile_item + "\\" + Constant.DEBUG_JSON))
                                {
                                    string debug_json = rs.ReadToEnd();
                                    Debug debug_items = JsonConvert.DeserializeObject<Debug>(debug_json);

                                    if (debug_items != null)
                                    {

                                        eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                                        eventEvidences.Comments = debug_items.Message;
                                        eventEvidences.Debug_Code = debug_items.debugCode;
                                        eventEvidences.Debug_Time = debug_items.Time;
                                        eventEvidences.LastUpdate = DateTime.Now;
                                        eventEvidences.IterationCount = 0;

                                    }
                                    else if (eventEvidences.Category == null)
                                    {
                                        eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                                        eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                                        eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                                        eventEvidences.LastUpdate = DateTime.Now;
                                        eventEvidences.IterationCount = 0;

                                    }



                                }

                            }
                            else
                            {
                                eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                                eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                                eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                                eventEvidences.LastUpdate = DateTime.Now;
                                eventEvidences.IterationCount = 0;
                            }

                            if (eventEvidences.Category != null)
                            {
                                eventEvidences.Debug_Code = Constant.SUCCESS_PROCESSED_CODE;
                                eventEvidences.Comments = Constant.SUCCESS_PROCESSED;
                                eventEvidences.ProcessedStatus = true;
                            }

                            if (eventEvidences.Debug_Code == Constant.SUCCESS_PROCESSED_CODE)
                            {
                                eventEvidences.ProcessedStatus = true;
                            }
                            else
                            {
                                eventEvidences.ProcessedStatus = false;
                            }

                            if ((eventEvidences.Lat == 0 && eventEvidences.Long == 0) || eventEvidences.Debug_Code == "76")
                            {
                                var vehicle = aDPEntities.Vehicles.FirstOrDefault(x => x.CarLicense == eventEvidences.BusId);
                                DeviceAlarmDetail deviceAlarmDetail = new DeviceAlarmDetail()
                                {
                                    Type = 76,
                                    DeviceId = vehicle.DeviceId,
                                    GroupId = vehicle.GruoupId,
                                    AlarmContent = eventEvidences.Comments,
                                };
                                aDPEntities.DeviceAlarmDetails.Add(deviceAlarmDetail);
                            }


                            aDPEntities.EventEvidences.Add(eventEvidences);
                            try
                            {
                                aDPEntities.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                aDPEntities.Dispose();
                                aDPEntities = new ADPEntities();

                            }


                        }
                    }
                    else
                    {
                        if (File.Exists(@violationFile_item + "\\" + Constant.DEBUG_JSON))
                        {
                            using (StreamReader rs = File.OpenText(@violationFile_item + "\\" + Constant.DEBUG_JSON))
                            {
                                string debug_json = rs.ReadToEnd();
                                Debug debug_items = JsonConvert.DeserializeObject<Debug>(debug_json);

                                if (debug_items != null)
                                {

                                    eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                                    eventEvidences.Comments = debug_items.Message;
                                    eventEvidences.Debug_Code = debug_items.debugCode;
                                    eventEvidences.Debug_Time = debug_items.Time;
                                    eventEvidences.LastUpdate = DateTime.Now;
                                    eventEvidences.IterationCount = 0;
                                }
                                else
                                {
                                    eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                                    eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                                    eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                                    eventEvidences.LastUpdate = DateTime.Now;
                                    eventEvidences.IterationCount = 0;

                                }

                            }
                        }
                        else
                        {
                            eventEvidences.FilePath = @violationFile_item + "\\" + addressFile[3];
                            eventEvidences.Comments = Constant.FILE_NOT_PROCESSED_STATUS;
                            eventEvidences.Debug_Code = Constant.FILE_NOT_PROCESSED_STATUS_CODE;
                            eventEvidences.LastUpdate = DateTime.Now;
                            eventEvidences.IterationCount = 0;
                        }
                    }


                    if (eventEvidences.Debug_Code == Constant.SUCCESS_PROCESSED_CODE)
                    {
                        eventEvidences.ProcessedStatus = true;
                    }
                    else
                    {
                        eventEvidences.ProcessedStatus = false;
                    }


                    aDPEntities.EventEvidences.Add(eventEvidences);
                    try
                    {
                        aDPEntities.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        aDPEntities.Dispose();
                        aDPEntities = new ADPEntities();

                    }

                }
                catch (Exception ex)
                {
                    Raqeeb.Common.Utility.WriteLog(ex);

                }

                //  object extraInfo = JsonConvert.DeserializeObject<object>(items.ExtraInfo)


            }

        }

        public void UpdateViolationsWithValidationStatus()
        {
            // get the list which are not validated
            List<ValidViolationCSV> validViolationCSVsList = new List<ValidViolationCSV>();
            List<InValidViolationCSV> inValidViolationCSVsList = new List<InValidViolationCSV>();

            try
            {
                // load  and read  the validReports csv
                //var datesFolderList = CustomSearcher.GetDirectoriesOnly(Constant.VALID_REPORTS_LOG_FOLDER_PATH);
                string curr_date = DateTime.Now.ToString("yyyy-MM-dd");

                var valid_CSV_path = @validReportsPath + curr_date + ".csv"; // Habeeb, "Dubai Media City, Dubai"
                using (TextFieldParser csvParser = new TextFieldParser(valid_CSV_path))
                {
                    csvParser.CommentTokens = new string[] { "#" };
                    csvParser.SetDelimiters(new string[] { "," });
                    csvParser.HasFieldsEnclosedInQuotes = true;

                    // Skip the row with the column names
                    csvParser.ReadLine();

                    while (!csvParser.EndOfData)
                    {
                        // Read current line fields, pointer moves to the next line.
                        ValidViolationCSV validViolationCSV = new ValidViolationCSV();
                        string[] fields = csvParser.ReadFields();
                        validViolationCSV.UID = fields[0];
                        validViolationCSV.Date = fields[1];

                        validViolationCSVsList.Add(validViolationCSV);

                    }
                    validViolationCSVsList.Reverse();
                }

                // load and read  the inValidReports csv

                var invalid_CSV_path = @invalidReportsPath + curr_date + ".csv";
                using (TextFieldParser csvParser = new TextFieldParser(invalid_CSV_path))
                {
                    csvParser.CommentTokens = new string[] { "#" };
                    csvParser.SetDelimiters(new string[] { "," });
                    csvParser.HasFieldsEnclosedInQuotes = true;

                    // Skip the row with the column names
                    csvParser.ReadLine();

                    while (!csvParser.EndOfData)
                    {
                        // Read current line fields, pointer moves to the next line.
                        InValidViolationCSV invalidViolationCSV = new InValidViolationCSV();

                        string[] fields = csvParser.ReadFields();
                        invalidViolationCSV.UID = fields[0];
                        invalidViolationCSV.Date = fields[1];
                        invalidViolationCSV.InValidCode = fields[3];
                        invalidViolationCSV.InvalidReason = fields[4];

                        inValidViolationCSVsList.Add(invalidViolationCSV);

                    }
                    inValidViolationCSVsList.Reverse();
                }

                foreach (var valid in validViolationCSVsList)
                {
                    try
                    {
                        EventEvidence eventEvidence = aDPEntities.EventEvidences.Where(c => c.EvidenceID == valid.UID && c.ValidationStatus == null).Select(c => c).FirstOrDefault();
                        if (eventEvidence != null)
                        {
                            eventEvidence.ValidationStatus = true;

                            aDPEntities.Entry(eventEvidence).State = EntityState.Modified;
                            aDPEntities.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Raqeeb.Common.Utility.WriteLogWithMessage(ex, valid.UID);
                        continue;
                    }

                }

                foreach (var invalid in inValidViolationCSVsList)
                {
                    try
                    {
                        EventEvidence eventEvidence = aDPEntities.EventEvidences.Where(c => c.EvidenceID == invalid.UID && c.ValidationStatus == null).Select(c => c).FirstOrDefault();
                        if (eventEvidence != null)
                        {
                            eventEvidence.ValidationStatus = false;
                            eventEvidence.InValidViolationComments = invalid.InvalidReason;
                            eventEvidence.InValidViolationCode = invalid.InValidCode;
                            aDPEntities.Entry(eventEvidence).State = EntityState.Modified;
                            aDPEntities.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Raqeeb.Common.Utility.WriteLogWithMessage(ex, invalid.UID);
                        continue;
                    }

                }


            }
            catch (Exception ex)
            {
                Raqeeb.Common.Utility.WriteLog(ex);

            }
        }



        public void CheckVideoDownloadStatus()
        {
            var mediaDownloadRequest = aDPEntities.MediaDownloadRequests.Where(c => c.Status == "-6" || c.Status == null).ToList();

            if (mediaDownloadRequest.Count > 0)
            {
                ObjectCache cache = MemoryCache.Default;
                var key = GetAuthToken();
                var client = new RestClient(Raqeeb.Manager.Constant.Check_Video_Download_Task_State);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var body = new
                {
                    key = key,
                    parms = mediaDownloadRequest.Select(p => new { taskid = p.TaskId, date = p.StartTime })
                };
                request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var result = JsonConvert.DeserializeObject<MediaDownloadCheckStatusViewModel>(response.Content);
                if (result.errorcode == (int)ErrorCode.Success)
                {
                    foreach (var item in result.data)
                    {
                        var newMedia = mediaDownloadRequest.FirstOrDefault(x => x.TaskId == item.taskid);
                        newMedia.Status = result.data.FirstOrDefault(w => w.taskid == newMedia.TaskId).state.ToString();
                        aDPEntities.Entry(newMedia).State = EntityState.Modified;
                        aDPEntities.SaveChanges();
                    }
                }
            }
        }
        #endregion
        #region MANAGE SETUP TABLE
        public WebServiceSetup GetWebSrvByType(WebServiceType webServiceType)
        {
            var result = webServiceSetupManager.GetByType(webServiceType);
            return result;
        }
        public WebServiceSetup InsertWebSrvSetup(WebServiceSetup webServiceSetup)
        {
            var result = webServiceSetupManager.Insert(webServiceSetup);
            return result;
        }
        public WebServiceSetup UpdateWebSrvSetup(WebServiceSetup webServiceSetup)
        {
            var result = webServiceSetupManager.Update(webServiceSetup);
            return result;
        }
        #endregion
        #region Private Helper Methods

        #endregion



    }

}