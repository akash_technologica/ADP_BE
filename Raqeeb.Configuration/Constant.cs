﻿namespace Raqeeb.Configuration
{
    public class Constant
    {
        public const string VIOLATION_FOLDER_PATH = @"d:\EvidenceData\";
        public static readonly string VIOLATION_NEW_JSON_TYPE = "NEW";
        public static readonly string VIOLATION_OLD_JSON_TYPE = "OLD";
        public const string VALID_REPORTS_LOG_FOLDER_PATH = @"d:\Logs\ValidationReports\validReports\";
        public const string INVALID_REPORTS_LOG_FOLDER_PATH = @"d:\Logs\ValidationReports\invalidReports\";
        public const string FILE_NOT_PROCESSED_STATUS = "FILE NOT PROCESSED";
        public const string ADDRESS_FILE = "EvidenceInfo";
        public const string DEBUG_JSON = "debug.json";
        public const string SUCCESS_PROCESSED = "Violation Processed Successfully";
        public const string SUCCESS_PROCESSED_CODE = "201";
        public const string FILE_NOT_PROCESSED_STATUS_CODE = "81";
      
    }
}
