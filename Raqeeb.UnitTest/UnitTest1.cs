﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using Raqeeb.Manager.Manager;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Text;
using Vehicle = Raqeeb.Domain.Entities.Vehicle;

namespace Raqeeb.UnitTest
{
    [TestClass]
    public class UnitTest1
    {

        //[TestMethod]
        //public async Task TestMethod1()
        //{

        //    IMongoDatabase db = MongoDbManager.GetMongoDataBase();
        //    IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>("EVENT");

        //    // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

        //    var options = new AggregateOptions()
        //    {
        //        AllowDiskUse = true
        //    };

        //    PipelineDefinition<BsonDocument, BsonDocument> pipeline = new BsonDocument[]
        //    {
        //        new BsonDocument("$project", new BsonDocument()
        //                .Add("_id", 0)
        //                .Add("EVENT", "$$ROOT")),
        //        new BsonDocument("$lookup", new BsonDocument()
        //                .Add("localField", "EVENT.EventTypeCode")
        //                .Add("from", "EVENT_MASTER")
        //                .Add("foreignField", "Code")
        //                .Add("as", "EVENT_MASTER")),
        //        new BsonDocument("$unwind", new BsonDocument()
        //                .Add("path", "$EVENT_MASTER")
        //                .Add("preserveNullAndEmptyArrays", new BsonBoolean(false))),
        //        new BsonDocument("$project", new BsonDocument()
        //                .Add("EVENT.Code", "$EVENT.Code")
        //                .Add("EVENT_MASTER.Description", "$EVENT_MASTER.Description"))
        //    };

        //    using (var cursor = await collection.AggregateAsync(pipeline, options))
        //    {
        //        while (await cursor.MoveNextAsync())
        //        {
        //            var batch = cursor.Current;
        //            foreach (BsonDocument document in batch)
        //            {
        //                Console.WriteLine(document.ToJson());
        //            }
        //        }
        //    }
        //}
        //[TestMethod]
        //public async Task TestMethodJoinCategories()
        //{


        //    IMongoDatabase db = MongoDbManager.GetMongoDataBase();
        //    IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>("TRIP");

        //    // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

        //    var options = new AggregateOptions()
        //    {
        //        AllowDiskUse = true
        //    };

        //    PipelineDefinition<BsonDocument, BsonDocument> pipeline = new BsonDocument[]
        //    {
        //        new BsonDocument("$project", new BsonDocument()
        //                .Add("_id", 0)
        //                .Add("TRIP", "$$ROOT")),
        //        new BsonDocument("$lookup", new BsonDocument()
        //                .Add("localField", "TRIP.Code")
        //                .Add("from", "EVENT")
        //                .Add("foreignField", "TripCode")
        //                .Add("as", "EVENT")),
        //        new BsonDocument("$unwind", new BsonDocument()
        //                .Add("path", "$EVENT")
        //                .Add("preserveNullAndEmptyArrays", new BsonBoolean(false))),
        //        new BsonDocument("$group", new BsonDocument()
        //                .Add("_id", new BsonDocument()
        //                        .Add("TRIP\u1390Code", "$TRIP.Code")
        //                        .Add("EVENT\u1390EventTypeCode", "$EVENT.EventTypeCode")
        //                )
        //                .Add("COUNT(*)", new BsonDocument()
        //                        .Add("$sum", 1)
        //                )),
        //        new BsonDocument("$project", new BsonDocument()
        //                .Add("COUNT(*)", "$COUNT(*)")
        //                .Add("TRIP.Code", "$_id.TRIP\u1390Code")
        //                .Add("EVENT.EventTypeCode", "$_id.EVENT\u1390EventTypeCode")
        //                .Add("_id", 0))
        //    };

        //    using (var cursor = await collection.AggregateAsync(pipeline, options))
        //    {
        //        while (await cursor.MoveNextAsync())
        //        {
        //            var batch = cursor.Current;
        //            foreach (BsonDocument document in batch)
        //            {
        //                Console.WriteLine(document.ToJson());
        //            }
        //        }
        //    }
        //}


        //[TestMethod]
        //public void TestAddContractors()
        //{
        //    ContractorManager contractorManager = new ContractorManager();
        //    RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
        //    using (new OperationContextScope(obj.InnerChannel))
        //    {
        //        var contractorMasterDataResult = obj.GetContractorsMasterData("PTARaqeeb", "PTA123Raqeeb");

        //        Contractor contractor = null;
        //        List<Contractor> contractorList = new List<Contractor>();

        //        foreach (ContractorMasterDataResult oContractor in contractorMasterDataResult)
        //        {
        //            contractor = new Contractor();
        //            contractor.Code = oContractor.CONTRACTOR_CODE;
        //            contractor.EndDate = oContractor.CONTRACT_END_DATE.HasValue ? Convert.ToDateTime(oContractor.CONTRACT_END_DATE.Value) : (DateTime?)null;
        //            contractor.StartDate = oContractor.CONTRACT_START_DATE.HasValue ? Convert.ToDateTime(oContractor.CONTRACT_START_DATE.Value) : (DateTime?)null;
        //            contractor.VendorCode = oContractor.VENDOR_CODE;
        //            contractor.Name = oContractor.COTRACTOR_NAME;
        //            contractorList.Add(contractor);
        //        }
        //        contractorManager.InsertBulk(contractorList);
        //    }
        //}


        //[TestMethod]
        //public void TestAddDrivers()
        //{
        //    DriverManager driverManager = new DriverManager();
        //    RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
        //    using (new OperationContextScope(obj.InnerChannel))
        //    {
        //        var DriversMasterDataList = obj.GetDriversMasterData("PTARaqeeb", "PTA123Raqeeb");

        //        Driver driver = null;
        //        List<Driver> driverList = new List<Driver>();

        //        foreach (DriverMasterDataResult odriver in DriversMasterDataList)
        //        {
        //            driver = new Driver();
        //            driver.ADSDNumber = odriver.ADSD_NUMBER;
        //            driver.Code = odriver.DRIVER_CODE;
        //            driver.ContractorCode = odriver.CONTRACTOR_CODE;
        //            driver.DateOfBirth = odriver.DATE_OF_BIRTH.HasValue ? Convert.ToDateTime(odriver.DATE_OF_BIRTH.Value) : (DateTime?)null;

        //            if (odriver.LAST_UPDATED.HasValue)
        //            {
        //                driver.LastUpdated = convertIntToDate(odriver.LAST_UPDATED.Value);
        //            }

        //            if (odriver.LICENTE_EXPIRY_DATE.HasValue)
        //            {
        //                driver.LicenseExpiryDate = convertIntToDate(odriver.LICENTE_EXPIRY_DATE.Value);
        //            }

        //            if (odriver.LICENSE_ISSUE_DATE.HasValue)
        //            {
        //                driver.LicenseIssueDate = convertIntToDate(odriver.LICENSE_ISSUE_DATE.Value);
        //            }

        //            driver.Mobile = odriver.MOBILE;
        //            driver.Name = odriver.DRIVER_NAME;
        //            driver.VendorCode = odriver.VENDOR_CODE;
        //            driver.Email = odriver.EMAIL;

        //            driverList.Add(driver);
        //        }
        //        driverManager.InsertBulk(driverList);
        //    }
        //}

        //[TestMethod]
        //public void TestADdVehciles()
        //{
        //    VehicleManager vehicleManager = new VehicleManager();
        //    RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
        //    using (new OperationContextScope(obj.InnerChannel))
        //    {
        //        var GetVehiclesMasterDataList = obj.GetVehiclesMasterData("PTARaqeeb", "PTA123Raqeeb");

        //        Vehicle oVehicle = null;
        //        List<Vehicle> oVehicleList = new List<Vehicle>();

        //        foreach (VehicleMasterDataResult _Vehicle in GetVehiclesMasterDataList)
        //        {
        //            oVehicle = new Vehicle();
        //            oVehicle.Code = _Vehicle.VEHICLE_CODE;
        //            oVehicle.ContractorCode = _Vehicle.CONTRACTOR_CODE;
        //            oVehicle.LastOdoMeter = _Vehicle.LAST_ODOMETER;
        //            oVehicle.Registration = _Vehicle.REGISTRATION;
        //            if (_Vehicle.UPDATE_DATE.HasValue)
        //                oVehicle.UpdateDate = convertIntToDate(_Vehicle.UPDATE_DATE.Value);
        //            oVehicle.VendorCode = _Vehicle.VENDOR_CODE;


        //            oVehicleList.Add(oVehicle);
        //        }
        //        vehicleManager.InsertBulk(oVehicleList);
        //    }
        //}

        //[TestMethod]
        //public void TestPositionByDriver()
        //{
        //    LivePositionManager livePositionManager = new LivePositionManager();
        //    RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
        //    using (new OperationContextScope(obj.InnerChannel))
        //    {
        //        var GetLiveGPSPositionDataList = obj.GetLivePositionbyDriver("PTARaqeeb", "PTA123Raqeeb");

        //        LivePosition oLivePosition = null;
        //        List<LivePosition> oLivePositionList = new List<LivePosition>();

        //        foreach (LivePositionbyDriverResult _LivePosition in GetLiveGPSPositionDataList)
        //        {
        //            oLivePosition = new LivePosition();
        //            oLivePosition.Altitude = _LivePosition.ALTITUDE;
        //            oLivePosition.Code = _LivePosition.LIVE_LOCATION_CODE;
        //            oLivePosition.ContractorCode = _LivePosition.CONTRACTOR_CODE;
        //            oLivePosition.DriverCode = _LivePosition.DRIVER_CODE;

        //            oLivePosition.Heading = _LivePosition.HEADING;
        //            oLivePosition.Latitude = _LivePosition.LATITUDE;
        //            if (_LivePosition.LOCATION_TIMESTAMP.HasValue)
        //                oLivePosition.LocationTIme = convertIntToDateTime(_LivePosition.LOCATION_TIMESTAMP.Value);

        //            oLivePosition.Longitude = _LivePosition.LONGITUDE;
        //            oLivePosition.OdoMeter = _LivePosition.ODOMETER;

        //            oLivePosition.Prm = _LivePosition.RPM;
        //            oLivePosition.Speed = _LivePosition.SPEED;


        //            oLivePosition.VehicleCode = _LivePosition.VEHICLE_CODE;
        //            oLivePosition.VendorCode = Convert.ToInt32(_LivePosition.VENDOR_CODE);
        //            oLivePosition.Type = Domain.Enums.PositionType.Driver;

        //            oLivePositionList.Add(oLivePosition);
        //        }
        //        livePositionManager.InsertBulk(oLivePositionList);
        //    }
        //}


        //[TestMethod]
        //public void TestPositionVehicle()
        //{
        //    LivePositionManager livePositionManager = new LivePositionManager();
        //    RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
        //    using (new OperationContextScope(obj.InnerChannel))
        //    {
        //        var GetLivePositionbyVehicleList = obj.GetLivePositionbyVehicle("PTARaqeeb", "PTA123Raqeeb");

        //        LivePosition oLivePosition = null;
        //        List<LivePosition> oLivePositionList = new List<LivePosition>();

        //        foreach (LivePositionbyVehicleResult _LivePosition in GetLivePositionbyVehicleList)
        //        {
        //            oLivePosition = new LivePosition();
        //            oLivePosition.Altitude = _LivePosition.ALTITUDE;
        //            oLivePosition.Code = _LivePosition.LIVE_LOCATION_CODE;
        //            oLivePosition.ContractorCode = _LivePosition.CONTRACTOR_CODE;
        //            oLivePosition.DriverCode = _LivePosition.DRIVER_CODE;

        //            oLivePosition.Heading = _LivePosition.HEADING;
        //            oLivePosition.Latitude = _LivePosition.LATITUDE;
        //            if (_LivePosition.LOCATION_TIMESTAMP.HasValue)
        //                oLivePosition.LocationTIme = convertIntToDateTime(_LivePosition.LOCATION_TIMESTAMP.Value);

        //            oLivePosition.Longitude = _LivePosition.LONGITUDE;
        //            oLivePosition.OdoMeter = _LivePosition.ODOMETER;

        //            oLivePosition.Prm = _LivePosition.RPM;
        //            oLivePosition.Speed = _LivePosition.SPEED;


        //            oLivePosition.VehicleCode = _LivePosition.VEHICLE_CODE;
        //            oLivePosition.VendorCode = Convert.ToInt32(_LivePosition.VENDOR_CODE);
        //            oLivePosition.Type = Domain.Enums.PositionType.Vehicle;

        //            oLivePositionList.Add(oLivePosition);
        //        }
        //        livePositionManager.InsertBulk(oLivePositionList);
        //    }
        //}

        //[TestMethod]
        //public void TestEvents()
        //{
        //    callAddEvent(2680003454);
        //}

        //public void callAddEvent(long id)
        //{
        //    EventManager eventManager = new EventManager();
        //    RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
        //    using (new OperationContextScope(obj.InnerChannel))
        //    {
        //        var GetEventsDataList = obj.GetEventsData("PTARaqeeb", "PTA123Raqeeb", id);

        //        Event oEvent = null;
        //        List<Event> EventList = new List<Event>();

        //        foreach (EventDataResult _event in GetEventsDataList)
        //        {
        //            oEvent = new Event();
        //            oEvent.Code = _event.EVENT_CODE;
        //            oEvent.ContractorCode = _event.CONTRACTOR_CODE;
        //            oEvent.DriverCode = _event.DRIVER_CODE;
        //            if (_event.EVENT_END_DATE.HasValue)
        //                oEvent.EndDate = convertIntToDateTime(_event.EVENT_END_DATE.Value);

        //            oEvent.EndPositionCode = _event.EVENT_END_POSITION_CODE;

        //            if (_event.EVENT_START_DATE.HasValue)
        //                oEvent.StartDate = convertIntToDateTime(_event.EVENT_START_DATE.Value);

        //            oEvent.StartPositionCode = _event.EVENT_START_POSITION_CODE;
        //            oEvent.SubTripCode = _event.SUBTRIP_CODE;
        //            oEvent.TripCode = _event.TRIP_CODE;
        //            oEvent.VehicleCode = _event.VEHICLE_CODE;
        //            oEvent.VendorCode = _event.VendorCode;
        //            oEvent.EventTypeCode = _event.EVENT_TYPE_CODE;
        //            EventList.Add(oEvent);
        //        }
        //        long nextValue = EventList[EventList.Count - 1].Code.Value + 1;
        //        if (EventList.Count > 0)
        //        {
        //            EventList = EventList.Where(c => c.StartDate.Value.ToShortDateString() == DateTime.Now.AddDays(-3).ToShortDateString()).ToList();
        //        }
        //        if (EventList.Count > 0)
        //        {
        //            eventManager.InsertBulk(EventList);
        //        }
        //        //if (EventList.Count < 500)
        //        //{
        //        //    var x = "";
        //        //}
        //        callAddEvent(nextValue);
        //    }
        //}
        //[TestMethod]
        //public void TestGPSLivePostion()
        //{
        //    callAddGPSPosition(2680000531);
        //}

        //public void callAddGPSPosition(long id)
        //{
        //    LivePositionManager livePositionManager = new LivePositionManager();
        //    RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
        //    using (new OperationContextScope(obj.InnerChannel))
        //    {
        //        var GetLiveGPSPositionDataList = obj.GetLiveGPSPositionData("PTARaqeeb", "PTA123Raqeeb", id);

        //        LivePosition oLivePosition = null;
        //        List<LivePosition> oLivePositionList = new List<LivePosition>();

        //        foreach (GPSPositionDataResult _LivePosition in GetLiveGPSPositionDataList)
        //        {
        //            oLivePosition = new LivePosition();
        //            oLivePosition.Altitude = _LivePosition.ALTITUDE;
        //            oLivePosition.Code = _LivePosition.POSITION_CODE.ToString();
        //            oLivePosition.OriginalCode = _LivePosition.POSITION_CODE;
        //            oLivePosition.ContractorCode = _LivePosition.CONTRACTOR_CODE;
        //            oLivePosition.DriverCode = _LivePosition.DRIVER_CODE;
        //            oLivePosition.DriverSeatBelt = _LivePosition.DRIVER_SEAT_BELT;
        //            oLivePosition.FwdEngaged = _LivePosition.FWD_ENGAGED;
        //            oLivePosition.Heading = _LivePosition.HEADING;
        //            oLivePosition.Latitude = _LivePosition.LATITUDE;
        //            if (_LivePosition.LOCATION_TIMESTAMP.HasValue)
        //                oLivePosition.LocationTIme = convertIntToDateTime(_LivePosition.LOCATION_TIMESTAMP.Value);

        //            oLivePosition.Longitude = _LivePosition.LONGITUDE;
        //            oLivePosition.OdoMeter = _LivePosition.ODOMETER;
        //            oLivePosition.PassengerSeatBelt = _LivePosition.PASSENGER_SEAT_BELT;
        //            oLivePosition.Prm = _LivePosition.RPM;
        //            oLivePosition.Speed = _LivePosition.SPEED;
        //            oLivePosition.SubTripCode = _LivePosition.SUBTRIP_CODE;
        //            oLivePosition.TripCode = _LivePosition.TRIP_CODE;
        //            oLivePosition.VehicleCode = _LivePosition.VEHICLE_CODE;
        //            oLivePosition.VendorCode = _LivePosition.VENDOR_CODE;
        //            oLivePosition.Type = Domain.Enums.PositionType.GPS;

        //            oLivePositionList.Add(oLivePosition);
        //        }

        //        long nextValue = oLivePositionList[oLivePositionList.Count - 1].OriginalCode + 1;
        //        if (oLivePositionList.Count > 0)
        //        {
        //            oLivePositionList = oLivePositionList.Where(c => c.LocationTIme.ToShortDateString() == DateTime.Now.AddDays(-3).ToShortDateString()).ToList();
        //        }
        //        if (oLivePositionList.Count > 0)
        //        {
        //            livePositionManager.InsertBulk(oLivePositionList);
        //        }
        //        else
        //        {
        //            string zxc = "";
        //        }


        //        callAddGPSPosition(nextValue);
        //    }
        //}

        //[TestMethod]
        //public void TestAddSubTrips()
        //{
        //    CallAddSubTrip(2680000531);
        //}

        //public void CallAddSubTrip(long id)
        //{
        //    SubTripManager subTripManager = new SubTripManager();
        //    RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
        //    using (new OperationContextScope(obj.InnerChannel))
        //    {
        //        SubTripSummaryDataResult[] GetSubTripSummaryDataList = obj.GetSubTripSummaryData("PTARaqeeb", "PTA123Raqeeb", id);

        //        SubTrip oSubTrip = null;
        //        List<SubTrip> oSubTripList = new List<SubTrip>();

        //        foreach (SubTripSummaryDataResult _SubTrip in GetSubTripSummaryDataList)
        //        {
        //            oSubTrip = new SubTrip();

        //            oSubTrip.Code = _SubTrip.SUBTRIP_CODE;
        //            oSubTrip.ContractorCode = _SubTrip.CONTRACTOR_CODE;
        //            oSubTrip.DistanceDriven = _SubTrip.SUBTRIP_DISTANCE_DRIVEN;
        //            oSubTrip.DriverCode = _SubTrip.DRIVER_CODE;
        //            oSubTrip.Duration = _SubTrip.SUBTRIP_DURATION;

        //            oSubTrip.EndPositionCode = _SubTrip.SUBTRIP_END_POSITION_CODE;
        //            oSubTrip.MaxPrm = _SubTrip.SUBTRIP_MAX_RPM;
        //            oSubTrip.MaxSpeed = _SubTrip.SUBTRIP_MAX_SPEED;

        //            oSubTrip.StartPositionCode = _SubTrip.SUBTRIP_START_POSITION_CODE;
        //            oSubTrip.TripCode = _SubTrip.TRIP_CODE;
        //            oSubTrip.VehicleCode = _SubTrip.VEHICLE_CODE;
        //            oSubTrip.VendorCode = _SubTrip.VENDOR_CODE;



        //            if (_SubTrip.SUBTRIP_END_DATE.HasValue)
        //                oSubTrip.EndDate = convertIntToDateTime(_SubTrip.SUBTRIP_END_DATE.Value);

        //            if (_SubTrip.SUBTRIP_START_DATE.HasValue)
        //                oSubTrip.StartDate = convertIntToDateTime(_SubTrip.SUBTRIP_START_DATE.Value);




        //            oSubTripList.Add(oSubTrip);
        //        }

        //        long nextValue = Convert.ToInt64(oSubTripList[oSubTripList.Count - 1].Code + 1);
        //        if (oSubTripList.Count > 0)
        //        {
        //            oSubTripList = oSubTripList.Where(c => c.StartDate.Value.ToShortDateString() == DateTime.Now.AddDays(-3).ToShortDateString()).ToList();
        //        }
        //        if (oSubTripList.Count > 0)
        //        {
        //            subTripManager.InsertBulk(oSubTripList);
        //        }
        //        else
        //        {
        //            string zxc = "";
        //        }

        //        CallAddSubTrip(nextValue);
        //    }
        //}



        //[TestMethod]
        //public void TestAddTrips()
        //{
        //    CallAddTrip(2680000531);
        //}

        //public void CallAddTrip(long id)
        //{
        //    TripManager tripManager = new TripManager();
        //    RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
        //    using (new OperationContextScope(obj.InnerChannel))
        //    {
        //        TripSummaryDataResult[] GetTripSummaryDataList = obj.GetTripSummaryData("PTARaqeeb", "PTA123Raqeeb", id);

        //        Trip oTrip = null;
        //        List<Trip> oTripList = new List<Trip>();

        //        foreach (TripSummaryDataResult _Trip in GetTripSummaryDataList)
        //        {
        //            oTrip = new Trip();
        //            oTrip.Code = _Trip.TRIP_CODE;
        //            oTrip.ContractorCode = _Trip.CONTRACTOR_CODE;
        //            oTrip.DistanceDriven = _Trip.TRIP_DISTANCE_DRIVEN;
        //            oTrip.DriverCode = _Trip.DRIVER_CODE;
        //            oTrip.DrivingTime = _Trip.TRIP_DRIVING_TIME;
        //            oTrip.Duration = _Trip.TRIP_DURATION;

        //            oTrip.EndOdoMeter = _Trip.TRIP_END_ODOMETER;
        //            oTrip.EndPositionCode = _Trip.TRIP_END_POSITION_CODE;
        //            oTrip.MaxPrm = _Trip.TRIP_MAX_RPM;
        //            oTrip.MaxSpeed = _Trip.TRIP_MAX_SPEED;
        //            oTrip.StandingTime = _Trip.TRIP_STANDING_TIME;


        //            oTrip.StartOdoMeter = _Trip.TRIP_START_ODOMETER;
        //            oTrip.StartPositionCode = _Trip.TRIP_START_POSITION_CODE;
        //            oTrip.VehicleCode = _Trip.VEHICLE_CODE;
        //            oTrip.VendorCode = _Trip.VENDOR_CODE;

        //            if (_Trip.TRIP_END_DATE.HasValue)
        //                oTrip.EndDate = convertIntToDateTime(_Trip.TRIP_END_DATE.Value);

        //            if (_Trip.TRIP_START_DATE.HasValue)
        //                oTrip.StartDate = convertIntToDateTime(_Trip.TRIP_START_DATE.Value);




        //            oTripList.Add(oTrip);
        //        }


        //        long nextValue = Convert.ToInt64(oTripList[oTripList.Count - 1].Code + 1);
        //        if (oTripList.Count > 0)
        //        {
        //            oTripList = oTripList.Where(c => c.StartDate.Value.ToShortDateString() == DateTime.Now.AddDays(-3).ToShortDateString()).ToList();
        //        }
        //        if (oTripList.Count > 0)
        //        {
        //            tripManager.InsertBulk(oTripList);
        //        }
        //        else
        //        {
        //            string zxc = "";
        //        }


        //        CallAddTrip(nextValue);





        //    }
        //}

        //[TestMethod]
        //public void TestEventMaster()
        //{
        //    EventMasterManager eventMasterManager = new EventMasterManager();
        //    RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
        //    using (new OperationContextScope(obj.InnerChannel))
        //    {
        //        var GetEventsMasterData = obj.GetEventsMasterData("PTARaqeeb", "PTA123Raqeeb");

        //        EventMaster oEventMaster = null;
        //        List<EventMaster> EventMasterList = new List<EventMaster>();

        //        foreach (EventMasterDataResult _event in GetEventsMasterData)
        //        {
        //            oEventMaster = new EventMaster();


        //            oEventMaster.Code = _event.EVENT_CODE;
        //            oEventMaster.ContractorCode = _event.CONTRACTOR_CODE;
        //            oEventMaster.Description = _event.EVENT_DESC;
        //            oEventMaster.IsViolation = _event.IS_VIOLATION;


        //            if (_event.VALID_FROM.HasValue)
        //                oEventMaster.ValidForm = convertIntToDate(_event.VALID_FROM.Value);

        //            if (_event.VALID_TO.HasValue)
        //                oEventMaster.ValidTo = convertIntToDate(_event.VALID_TO.Value);

        //            oEventMaster.VendorCode = _event.VENDOR_CODE;

        //            EventMasterList.Add(oEventMaster);
        //        }
        //        eventMasterManager.InsertBulk(EventMasterList);
        //    }
        //}


        //public DateTime convertIntToDate(int value)
        //{
        //    string year = string.Empty;
        //    string month = string.Empty;
        //    string day = string.Empty;

        //    year = value.ToString().Substring(0, 4);
        //    month = value.ToString().Substring(4, 2);
        //    day = value.ToString().Substring(6, 2);
        //    DateTime current = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day), 0, 0, 0, DateTimeKind.Local);


        //    return current;
        //}


        //public DateTime convertIntToDateTime(decimal value)
        //{
        //    string year = string.Empty;
        //    string month = string.Empty;
        //    string day = string.Empty;
        //    string hour = string.Empty;
        //    string min = string.Empty;
        //    string sec = string.Empty;

        //    year = value.ToString().Substring(0, 4);
        //    month = value.ToString().Substring(4, 2);
        //    day = value.ToString().Substring(6, 2);

        //    hour = value.ToString().Substring(8, 2);
        //    min = value.ToString().Substring(10, 2);
        //    sec = value.ToString().Substring(12, 2);
        //    DateTime current = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day), Convert.ToInt32(hour), Convert.ToInt32(min), Convert.ToInt32(sec), DateTimeKind.Local);


        //    return current.AddHours(4);
        //}


        //[TestMethod]
        //public void TestToGetTime()
        //{

        //    //Event result = new Event();
        //    //IMongoDatabase db = MongoDbManager.GetMongoDataBase();
        //    //var filter = Builders<Event>.Filter.Eq("Code", 11);
        //    //result = db.GetCollection<Event>("EVENT").Find<Event>(filter).FirstOrDefault();

        //    //var dateFormat = result.StartDate.Value.GetDateTimeFormats();

        //    //DateTime d2 = result.StartDate.Value.AddHours(4);

        //    //RaqeebTest obj = new RaqeebTest();
        //    //obj.ApplicationDate = convertIntToDateTime(20180117024122);

        //    //IMongoDatabase db = MongoDbManager.GetMongoDataBase();
        //    //db.GetCollection<RaqeebTest>("RAQEEB_TEST").InsertOne(obj);


        //    WebServiceSetupManager webServiceSetupManager = new WebServiceSetupManager();
        //    //WebServiceSetup oWebServiceSetup = new WebServiceSetup();
        //    //oWebServiceSetup.LasItemId = 1;
        //    //oWebServiceSetup.LastSynchTime = DateTime.Now;
        //    //oWebServiceSetup.Name = "GetContractorsMasterData";
        //    //oWebServiceSetup.Type = WebServiceType.GetContractorsMasterData;
        //    //webServiceSetupManager.Insert(oWebServiceSetup);


        //    var result = webServiceSetupManager.GetByType(WebServiceType.GetContractorsMasterData);
        //    result.LastSynchTime = DateTime.Now;
        //    result.LasItemId = 1500;
        //    webServiceSetupManager.Update(result);


        //}



        //[TestMethod]
        //public void AddEventMedia()
        //{
        //    EventMediaManager eventMediaManager = new EventMediaManager();
        //    List<EventMedia> eventMediaList = new List<EventMedia>();
        //    EventMedia oEventMedia = new EventMedia();
        //    oEventMedia.ContentType = "image/png";
        //    oEventMedia.EventCode = 2087615;
        //    oEventMedia.ImageCrop = ImageCrop.cropped;
        //    oEventMedia.Type = EventMediaType.Image;
        //    oEventMedia.Url = "https://drive.google.com/open?id=11aJtTK9OtDsRh0JfIkOq9-YHnx5x_I93";
        //    eventMediaList.Add(oEventMedia); //Here
        //    oEventMedia = new EventMedia();
        //    oEventMedia.ContentType = "image/png";
        //    oEventMedia.EventCode = 2087615;
        //    oEventMedia.ImageCrop = ImageCrop.cropped;
        //    oEventMedia.Type = EventMediaType.Image;
        //    oEventMedia.Url = "https://drive.google.com/open?id=1suzmxkt57LnxdrulSy2rpAsk7IPQesWs";
        //    eventMediaList.Add(oEventMedia); //Here
        //    oEventMedia = new EventMedia();
        //    oEventMedia.ContentType = "image/png";
        //    oEventMedia.EventCode = 2087615;
        //    oEventMedia.ImageCrop = ImageCrop.cropped;
        //    oEventMedia.Type = EventMediaType.Image;
        //    oEventMedia.Url = "https://drive.google.com/open?id=156U6Z33q0f3TsHxcIfqNN8ZATUwQJili";
        //    eventMediaList.Add(oEventMedia); //Here
        //    eventMediaManager.InsertBulk(eventMediaList);

        //}


        //[TestMethod]
        //public void GetEventMedia()
        //{
        //    EventMediaManager eventMediaManager = new EventMediaManager();
        //    List<EventMedia> eventMediaList = new List<EventMedia>();
        //    eventMediaList = eventMediaManager.GetByEventCode(2077934);

        //}


        public void TEST_DATA_VEHICLE_ADD(int code)
        {
            //VehicleManager vehicleManager = new VehicleManager();
            //Vehicle oVehicle = null;
            //List<Vehicle> oVehicleList = new List<Vehicle>();
            //oVehicle = new Vehicle();
            //oVehicle.Code = code;
            //oVehicle.ContractorCode = 1;
            //oVehicle.LastOdoMeter = code + 10;
            //oVehicle.Registration = code.ToString();
            //oVehicle.UpdateDate = DateTime.Now.AddHours(4);
            //oVehicle.VendorCode = 1;
            //oVehicleList.Add(oVehicle);
            //vehicleManager.InsertBulk(oVehicleList);
        }
        public void TEST_DATA_LIVE_POSITION_GPS(long code, DateTime currentDate, int tripTimeInMin)
        {
            //Random lastLat = new Random();
            //Random lastLon = new Random();
            //int lat = lastLat.Next(116400146, 630304598);
            //int lon = lastLon.Next(124464416, 441194152);

            LivePositionManager livePositionManager = new LivePositionManager();

            LivePosition oLivePosition = null;
            List<LivePosition> oLivePositionList = new List<LivePosition>();

            #region  This for Vehicle 101102103
            oLivePosition = new LivePosition();
            oLivePosition.Altitude = 20;
            oLivePosition.Code = "FMS-" + code + "1";
            oLivePosition.OriginalCode = Convert.ToInt64(code + "1");
            oLivePosition.ContractorCode = 1;
            oLivePosition.DriverCode = null;
            oLivePosition.DriverSeatBelt = null;
            oLivePosition.FwdEngaged = null;
            oLivePosition.Heading = null;
            Coordinate location = new Coordinate();
            location = GetRandomLocation();
            oLivePosition.Latitude = location.Latitude;  // Convert.ToDouble("24." + lat);// 24.8861741;
            oLivePosition.Longitude = location.Longitude;   //Convert.ToDouble("55." + lon);// 54.9329477;
            oLivePosition.LocationTIme = currentDate.AddMinutes(-tripTimeInMin);
            oLivePosition.OdoMeter = 8.232;
            oLivePosition.PassengerSeatBelt = null;
            oLivePosition.Prm = 11;
            oLivePosition.Speed = 70;
            oLivePosition.SubTripCode = null;
            oLivePosition.TripCode = null;
            oLivePosition.VehicleCode = Convert.ToInt32(code);
            oLivePosition.VendorCode = 1;
            oLivePosition.Type = Domain.Enums.PositionType.GPS;
            oLivePositionList.Add(oLivePosition);



            oLivePosition = new LivePosition();
            oLivePosition.Altitude = 3;
            oLivePosition.Code = "FMS-" + code + "2";
            oLivePosition.OriginalCode = Convert.ToInt64(code + "2");
            oLivePosition.ContractorCode = 1;
            oLivePosition.DriverCode = null;
            oLivePosition.DriverSeatBelt = null;
            oLivePosition.FwdEngaged = null;
            oLivePosition.Heading = null;
            Coordinate location2 = new Coordinate();
            location2 = GetRandomLocation();
            oLivePosition.Latitude = location2.Latitude;  // Convert.ToDouble("24." + lat);// 24.8861741;
            oLivePosition.Longitude = location2.Longitude;   //Convert.ToDouble("55." + lon);// 54.9329477;
            oLivePosition.LocationTIme = currentDate;
            oLivePosition.OdoMeter = 9.232;
            oLivePosition.PassengerSeatBelt = null;
            oLivePosition.Prm = 15;
            oLivePosition.Speed = 50;
            oLivePosition.SubTripCode = null;
            oLivePosition.TripCode = null;
            oLivePosition.VehicleCode = Convert.ToInt32(code);
            oLivePosition.VendorCode = 1;
            oLivePosition.Type = Domain.Enums.PositionType.GPS;
            oLivePositionList.Add(oLivePosition);
            #endregion


            livePositionManager.InsertBulk(oLivePositionList);

        }
        public void TEST_DATA_LIVE_POSITION_VEHICLE(long code, DateTime currentDate, int tripTimeInMin, int PRM, int Speed, int item)
        {


            //Random lastLat = new Random();
            //Random lastLon = new Random();
            //int lat = lastLat.Next(843559, 630304598);
            //int lon = lastLon.Next(776483, 441194152);
            Coordinate location = new Coordinate();
            location = GetDubaiLocation(item);
            LivePositionManager livePositionManager = new LivePositionManager();

            LivePosition oLivePosition = null;
            List<LivePosition> oLivePositionList = new List<LivePosition>();

            #region  This for Vehicle 101102103

            oLivePosition = new LivePosition();
            oLivePosition.Altitude = 3;
            oLivePosition.Code = "FMS-" + code + "9";
            oLivePosition.OriginalCode = Convert.ToInt64(code + "9");
            oLivePosition.ContractorCode = 1;
            oLivePosition.DriverCode = null;
            oLivePosition.DriverSeatBelt = null;
            oLivePosition.FwdEngaged = null;
            oLivePosition.Heading = null;
            oLivePosition.Latitude = location.Latitude;// 24.8861741;
            oLivePosition.Longitude = location.Longitude;// 54.9329477;
            oLivePosition.LocationTIme = currentDate;
            oLivePosition.OdoMeter = 9.232;
            oLivePosition.PassengerSeatBelt = null;
            oLivePosition.Prm = PRM;
            oLivePosition.Speed = Speed;
            oLivePosition.SubTripCode = null;
            oLivePosition.TripCode = null;
            oLivePosition.VehicleCode = Convert.ToInt32(code);
            oLivePosition.VendorCode = 1;
            oLivePosition.Type = Domain.Enums.PositionType.Vehicle;
            oLivePositionList.Add(oLivePosition);
            #endregion
            livePositionManager.InsertBulk(oLivePositionList);

        }
        public void TEST_DATA_TRIP_ADD(long code, DateTime currentDate, int tripTimeInMin)
        {
            //TripManager tripManager = new TripManager();
            //Trip oTrip = null;
            //List<Trip> oTripList = new List<Trip>();

            //oTrip = new Trip();
            //oTrip.Code = code;
            //oTrip.ContractorCode = 1;
            //oTrip.DistanceDriven = tripTimeInMin;
            //oTrip.DriverCode = null;
            //oTrip.Duration = tripTimeInMin;
            //oTrip.EndOdoMeter = 10.07;
            //oTrip.StartPositionCode = "FMS-" + code + "1"; //Convert.ToInt64(code + "1");
            //oTrip.EndPositionCode = "FMS-" + code + "2";//Convert.ToInt64(code + "2");
            //oTrip.MaxPrm = 30;
            //oTrip.MaxSpeed = 130;
            //oTrip.StandingTime = 141;
            //oTrip.StartOdoMeter = 11;
            //oTrip.VehicleCode = Convert.ToInt32(code);
            //oTrip.VendorCode = 1;
            //oTrip.StartDate = currentDate.AddMinutes(-tripTimeInMin);
            //oTrip.EndDate = currentDate;
            //oTripList.Add(oTrip);
            //   tripManager.InsertBulk(oTripList);
        }
        public void ADD_TEST_DATA_SUB_TRIP(long code, DateTime currentDate, int tripTimeInMin)
        {
            SubTripManager subTripManager = new SubTripManager();

            SubTrip oSubTrip = null;
            List<SubTrip> oSubTripList = new List<SubTrip>();

            oSubTrip = new SubTrip();
            oSubTrip.Code = Convert.ToInt64(code + "1");
            oSubTrip.ContractorCode = 1;
            oSubTrip.DistanceDriven = 0.8099;
            oSubTrip.DriverCode = null;
            oSubTrip.Duration = 10;
            oSubTrip.StartPositionCode = "FMS-" + code + "1";
            oSubTrip.EndPositionCode = "FMS-" + code + "2";
            oSubTrip.MaxPrm = 1600;
            oSubTrip.MaxSpeed = 41;
            oSubTrip.TripCode = code;
            oSubTrip.VehicleCode = Convert.ToInt32(code);
            oSubTrip.VendorCode = 1;
            oSubTrip.StartDate = currentDate.AddMinutes(-tripTimeInMin + 15);
            oSubTrip.EndDate = currentDate.AddMinutes(-tripTimeInMin + 15 + 10);
            oSubTripList.Add(oSubTrip);

            oSubTrip = new SubTrip();
            oSubTrip.Code = Convert.ToInt64(code + "2");
            oSubTrip.ContractorCode = 1;
            oSubTrip.DistanceDriven = 1.8099;
            oSubTrip.DriverCode = null;
            oSubTrip.Duration = 15;
            oSubTrip.StartPositionCode = "FMS-" + code + "1";
            oSubTrip.EndPositionCode = "FMS-" + code + "2";
            oSubTrip.MaxPrm = 1350;
            oSubTrip.MaxSpeed = 90;
            oSubTrip.TripCode = code;
            oSubTrip.VehicleCode = Convert.ToInt32(code);
            oSubTrip.VendorCode = 1;
            oSubTrip.StartDate = currentDate.AddMinutes(-tripTimeInMin + 15 + 20);
            oSubTrip.EndDate = currentDate.AddMinutes(-tripTimeInMin + 15 + +20 + 15);
            oSubTripList.Add(oSubTrip);

            subTripManager.InsertBulk(oSubTripList);
        }
        public void TEST_DATE_EVENT_ADD(long code, DateTime currentDate, int tripTimeInMin, string eventSource)
        {
            EventManager eventManager = new EventManager();
            Event oEvent = null;
            List<Event> EventList = new List<Event>();

            oEvent = new Event();
            oEvent.Code = "FMS-" + code + "1";  //Convert.ToInt64(code + "1");
            oEvent.OriginalCode = Convert.ToInt64(code + "1");
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 8);
            oEvent.StartPositionCode = "FMS-" + code + "1";
            oEvent.EndPositionCode = "FMS-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Driver_Fatigue;
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            EventList.Add(oEvent);

            oEvent = new Event();
            oEvent.Code = "FMS-" + code + "2";  //Convert.ToInt64(code + "1");
            oEvent.OriginalCode = Convert.ToInt64(code + "2");
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 9);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 11);
            oEvent.StartPositionCode = "FMS-" + code + "1";
            oEvent.EndPositionCode = "FMS-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Distraction;
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            EventList.Add(oEvent);


            oEvent = new Event();
            oEvent.Code = "FMS-" + code + "3";  //Convert.ToInt64(code + "1");
            oEvent.OriginalCode = Convert.ToInt64(code + "3");
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 12);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 14);
            oEvent.StartPositionCode = "FMS-" + code + "1";
            oEvent.EndPositionCode = "FMS-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Forward_Collision_Warning;
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            EventList.Add(oEvent);

            //oEvent = new Event();
            //oEvent.Code = "FMS-" + code + "4";  //Convert.ToInt64(code + "1");
            //oEvent.OriginalCode = Convert.ToInt64(code + "4");
            //oEvent.ContractorCode = 1;
            //oEvent.DriverCode = null;
            //oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 15);
            //oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 16);
            //oEvent.StartPositionCode = "FMS-" + code + "1";
            //oEvent.EndPositionCode = "FMS-" + code + "2";
            //oEvent.SubTripCode = null;
            //oEvent.TripCode = null;
            //oEvent.VehicleCode = Convert.ToInt32(code);
            //oEvent.VendorCode = "1";
            //oEvent.EventTypeCode = (int)EventType.Harsh_Braking;
            //oEvent.EventSource = eventSource;
            //if (oEvent.StartDate.HasValue)
            //    oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            //else
            //    oEvent.Shift = "NA";
            //EventList.Add(oEvent);


            //oEvent = new Event();
            //oEvent.Code = "FMS-" + code + "5";  //Convert.ToInt64(code + "1");
            //oEvent.OriginalCode = Convert.ToInt64(code + "5");
            //oEvent.ContractorCode = 1;
            //oEvent.DriverCode = null;
            //oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 18);
            //oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 22);
            //oEvent.StartPositionCode = "FMS-" + code + "1";
            //oEvent.EndPositionCode = "FMS-" + code + "2";
            //oEvent.SubTripCode = null;
            //oEvent.TripCode = null;
            //oEvent.VehicleCode = Convert.ToInt32(code);
            //oEvent.VendorCode = "1";
            //oEvent.EventTypeCode = (int)EventType.Right_Lane_Departure;
            //oEvent.EventSource = eventSource;
            //if (oEvent.StartDate.HasValue)
            //    oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            //else
            //    oEvent.Shift = "NA";
            //EventList.Add(oEvent);

            //oEvent = new Event();
            //oEvent.Code = "FMS-" + code + "6";  //Convert.ToInt64(code + "1");
            //oEvent.OriginalCode = Convert.ToInt64(code + "6");
            //oEvent.ContractorCode = 1;
            //oEvent.DriverCode = null;
            //oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 25);
            //oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 27);
            //oEvent.StartPositionCode = "FMS-" + code + "1";
            //oEvent.EndPositionCode = "FMS-" + code + "2";
            //oEvent.SubTripCode = null;
            //oEvent.TripCode = null;
            //oEvent.VehicleCode = Convert.ToInt32(code);
            //oEvent.VendorCode = "1";
            //oEvent.EventTypeCode = (int)EventType.Over_Speeding;
            //oEvent.EventSource = eventSource;
            //if (oEvent.StartDate.HasValue)
            //    oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            //else
            //    oEvent.Shift = "NA";
            //EventList.Add(oEvent);

            eventManager.InsertBulk(EventList);
        }
        public void TEST_DATE_EVENT_ADD_NORMAL(long code, DateTime currentDate, int tripTimeInMin, string eventSource)
        {
            EventManager eventManager = new EventManager();
            Event oEvent = null;
            List<Event> EventList = new List<Event>();

            oEvent = new Event();
            oEvent.Code = "FMS-" + code + "1";  //Convert.ToInt64(code + "1");
            oEvent.OriginalCode = Convert.ToInt64(code + "1");
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 8);
            oEvent.StartPositionCode = "FMS-" + code + "1";
            oEvent.EndPositionCode = "FMS-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.FOV_Exception;
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            EventList.Add(oEvent);

            oEvent = new Event();
            oEvent.Code = "FMS-" + code + "2";  //Convert.ToInt64(code + "1");
            oEvent.OriginalCode = Convert.ToInt64(code + "2");
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 9);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 11);
            oEvent.StartPositionCode = "FMS-" + code + "1";
            oEvent.EndPositionCode = "FMS-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Mobileeye_Tamper_Alert;
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            EventList.Add(oEvent);


            oEvent = new Event();
            oEvent.Code = "FMS-" + code + "3";  //Convert.ToInt64(code + "1");
            oEvent.OriginalCode = Convert.ToInt64(code + "3");
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 12);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 14);
            oEvent.StartPositionCode = "FMS-" + code + "1";
            oEvent.EndPositionCode = "FMS-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Traffic_Sign_Recognition;
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            EventList.Add(oEvent);

            eventManager.InsertBulk(EventList);
        }
        public void TEST_DATE_EVENT_ADD_ELEVATED(long code, DateTime currentDate, int tripTimeInMin, string eventSource)
        {
            EventManager eventManager = new EventManager();
            Event oEvent = null;
            List<Event> EventList = new List<Event>();

            oEvent = new Event();
            oEvent.Code = "FMS-" + code + "1";  //Convert.ToInt64(code + "1");
            oEvent.OriginalCode = Convert.ToInt64(code + "1");
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 8);
            oEvent.StartPositionCode = "FMS-" + code + "1";
            oEvent.EndPositionCode = "FMS-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Driver_Fatigue;
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            EventList.Add(oEvent);
            eventManager.InsertBulk(EventList);
        }
        public void TEST_DATE_EVENT_ADD_CRITICAL(long code, DateTime currentDate, int tripTimeInMin, string eventSource)
        {
            EventManager eventManager = new EventManager();
            Event oEvent = null;
            List<Event> EventList = new List<Event>();

            oEvent = new Event();
            oEvent.Code = "FMS-" + code + "1";  //Convert.ToInt64(code + "1");
            oEvent.OriginalCode = Convert.ToInt64(code + "1");
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 8);
            oEvent.StartPositionCode = "FMS-" + code + "1";
            oEvent.EndPositionCode = "FMS-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Driver_Fatigue;
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            EventList.Add(oEvent);

            oEvent = new Event();
            oEvent.Code = "FMS-" + code + "2";  //Convert.ToInt64(code + "1");
            oEvent.OriginalCode = Convert.ToInt64(code + "2");
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 9);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 11);
            oEvent.StartPositionCode = "FMS-" + code + "1";
            oEvent.EndPositionCode = "FMS-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Distraction;
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            EventList.Add(oEvent);


            oEvent = new Event();
            oEvent.Code = "FMS-" + code + "3";  //Convert.ToInt64(code + "1");
            oEvent.OriginalCode = Convert.ToInt64(code + "3");
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 12);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 14);
            oEvent.StartPositionCode = "FMS-" + code + "1";
            oEvent.EndPositionCode = "FMS-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Traffic_Sign_Recognition;
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            EventList.Add(oEvent);

            eventManager.InsertBulk(EventList);
        }
        public void TEST_DATE_EVENT_VEHICLE_ADD(long code, DateTime currentDate, int tripTimeInMin)
        {
            //EventManager eventManager = new EventManager();
            //Event oEvent = null;
            //List<Event> EventList = new List<Event>();

            //oEvent = new Event();
            //oEvent.Code = Convert.ToInt64(code + "1");
            //oEvent.ContractorCode = 1;
            //oEvent.DriverCode = null;
            //oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 20);
            //oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 30);
            //oEvent.StartPositionCode = 1005;
            //oEvent.EndPositionCode = 1011;
            //oEvent.SubTripCode = null;
            //oEvent.TripCode = null;
            //oEvent.VehicleCode = Convert.ToInt32(code);
            //oEvent.VendorCode = "1";
            //oEvent.EventTypeCode = (int)EventType.Driver_Fatigue;
            //EventList.Add(oEvent);

            //oEvent = new Event();
            //oEvent.Code = Convert.ToInt64(code + "2");
            //oEvent.ContractorCode = 1;
            //oEvent.DriverCode = null;
            //oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 40);
            //oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 50);
            //oEvent.StartPositionCode = 1005;
            //oEvent.EndPositionCode = 1011;
            //oEvent.SubTripCode = null;
            //oEvent.TripCode = null;
            //oEvent.VehicleCode = Convert.ToInt32(code);
            //oEvent.VendorCode = "1";
            //oEvent.EventTypeCode = (int)EventType.Driver_Fatigue;
            //EventList.Add(oEvent);

            //eventManager.InsertBulk(EventList);

            //// TEST_DATE_EVENT_MEDIA_ADD(Convert.ToInt64(code + "1"));
            ////   TEST_DATE_EVENT_MEDIA_ADD(Convert.ToInt64(code + "2"));

        }

        //public void TEST_DATE_EVENT_MEDIA_ADD(long code)
        //{
        //    EventMediaManager eventMediaManager = new EventMediaManager();
        //    List<EventMedia> eventMediaList = new List<EventMedia>();
        //    EventMedia oEventMedia = new EventMedia();
        //    oEventMedia.ContentType = "image/png";
        //    oEventMedia.EventCode = code;
        //    oEventMedia.ImageCrop = ImageCrop.cropped;
        //    oEventMedia.Type = EventMediaType.Image;
        //    oEventMedia.Url = "http://localhost:4200/assets/images/emp.png";
        //    eventMediaList.Add(oEventMedia); //Here

        //    oEventMedia = new EventMedia();
        //    oEventMedia.ContentType = "image/png";
        //    oEventMedia.EventCode = code;
        //    oEventMedia.ImageCrop = ImageCrop.cropped;
        //    oEventMedia.Type = EventMediaType.Image;
        //    oEventMedia.Url = "http://localhost:4200/assets/images/user.png";
        //    eventMediaList.Add(oEventMedia); //Here

        //    //oEventMedia = new EventMedia();
        //    //oEventMedia.ContentType = "image/png";
        //    //oEventMedia.EventCode = code;
        //    //oEventMedia.ImageCrop = ImageCrop.cropped;
        //    //oEventMedia.Type = EventMediaType.Image;
        //    //oEventMedia.Url = "http://localhost:4200/assets/images/emp.png";
        //    //eventMediaList.Add(oEventMedia); //Here
        //    eventMediaManager.InsertBulk(eventMediaList);
        //}

        public void TEST_DATA_LIVE_POSITION_GPS_SEEING_MACHINE(long code, DateTime currentDate, int tripTimeInMin)
        {
            Random lastLat = new Random();
            Random lastLon = new Random();
            int lat = lastLat.Next(843559, 630304598);
            int lon = lastLon.Next(776483, 441194152);

            LivePositionManager livePositionManager = new LivePositionManager();

            LivePosition oLivePosition = null;
            List<LivePosition> oLivePositionList = new List<LivePosition>();

            #region  This for Vehicle 101102103
            oLivePosition = new LivePosition();
            oLivePosition.Code = "SM-" + code + "1";
            oLivePosition.OriginalCode = 0;
            Coordinate location = new Coordinate();
            location = GetRandomLocation();
            oLivePosition.Latitude = Convert.ToDouble("24." + lat);// 24.8861741;
            oLivePosition.Longitude = Convert.ToDouble("55." + lon);// 54.9329477;
            oLivePosition.Type = Domain.Enums.PositionType.GPS;
            oLivePositionList.Add(oLivePosition);


            oLivePosition = new LivePosition();
            oLivePosition.Code = "SM-" + code + "2";
            oLivePosition.OriginalCode = 0;
            Coordinate location1 = new Coordinate();
            location1 = GetRandomLocation();
            oLivePosition.Latitude = location1.Latitude;
            oLivePosition.Longitude = location1.Longitude;
            oLivePosition.Type = Domain.Enums.PositionType.GPS;
            oLivePositionList.Add(oLivePosition);



            oLivePosition = new LivePosition();
            oLivePosition.Code = "SM-" + code + "3";
            oLivePosition.OriginalCode = 0;
            Coordinate location2 = new Coordinate();
            location2 = GetRandomLocation();
            oLivePosition.Latitude = location2.Latitude;
            oLivePosition.Longitude = location2.Longitude;
            oLivePosition.Type = Domain.Enums.PositionType.GPS;
            oLivePositionList.Add(oLivePosition);
            #endregion


            livePositionManager.InsertBulk(oLivePositionList);

        }
        public void TEST_DATE_EVENT_ADD_SEEING_MACHINE(long code, DateTime currentDate, int tripTimeInMin, string eventSource)
        {
            EventManager eventManager = new EventManager();
            Event oEvent = null;
            List<Event> EventList = new List<Event>();

            oEvent = new Event();
            oEvent.Code = "SM-" + code + "1";
            oEvent.OriginalCode = 0;
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 8);
            oEvent.StartPositionCode = "SM-" + code + "1";
            oEvent.EndPositionCode = "SM-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Driver_Fatigue;

            oEvent.Duration = 1.56;
            oEvent.SerialNumber = "P04025-S00011648";
            oEvent.ContractorName = "(TGA) Public Transport Agency (PTA) Dubai";
            oEvent.ConfirmationState = "pending";
            oEvent.DetectedDateTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Alarms = "Audio, Vibration";
            oEvent.DetectedPositionCode = "SM-" + code + "3";
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            oEvent.Tracking = 94;
            oEvent.TravelDistance = 143.08;
            oEvent.StationaryTime = 22453;
            oEvent.Bearing = "303";
            //  oEvent.TripTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Travel = "17";
            oEvent.Age = 10;
            oEvent.Speed = 33;
            //  oEvent.TimeIntoShift = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.GPSCoverage = 100;
            EventList.Add(oEvent);



            oEvent = new Event();
            oEvent.Code = "SM-" + code + "2";
            oEvent.OriginalCode = 0;
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 8);
            oEvent.StartPositionCode = "SM-" + code + "1";
            oEvent.EndPositionCode = "SM-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Distraction;

            oEvent.Duration = 1.56;
            oEvent.SerialNumber = "P04025-S00011648";
            oEvent.ContractorName = "(TGA) Public Transport Agency (PTA) Dubai";
            oEvent.ConfirmationState = "pending";
            oEvent.DetectedDateTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Alarms = "Vibration";
            oEvent.DetectedPositionCode = "SM-" + code + "3";
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            oEvent.Tracking = 100;
            oEvent.TravelDistance = 160.98;
            oEvent.StationaryTime = 22453;
            oEvent.Bearing = "400";
            //   oEvent.TripTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Travel = "12";
            oEvent.Age = 30;
            oEvent.Speed = 40;
            //   oEvent.TimeIntoShift = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.GPSCoverage = 90;
            EventList.Add(oEvent);

            eventManager.InsertBulk(EventList);
        }

        public void TEST_DATE_EVENT_ADD_SEEING_MACHINE_ELEVATED(long code, DateTime currentDate, int tripTimeInMin, string eventSource)
        {
            EventManager eventManager = new EventManager();
            Event oEvent = null;
            List<Event> EventList = new List<Event>();

            oEvent = new Event();
            oEvent.Code = "SM-" + code + "1";
            oEvent.OriginalCode = 0;
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 8);
            oEvent.StartPositionCode = "SM-" + code + "1";
            oEvent.EndPositionCode = "SM-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Distraction;

            oEvent.Duration = 1.56;
            oEvent.SerialNumber = "P04025-S00011648";
            oEvent.ContractorName = "(TGA) Public Transport Agency (PTA) Dubai";
            oEvent.ConfirmationState = "pending";
            oEvent.DetectedDateTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Alarms = "Audio, Vibration";
            oEvent.DetectedPositionCode = "SM-" + code + "3";
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            oEvent.Tracking = 94;
            oEvent.TravelDistance = 143.08;
            oEvent.StationaryTime = 22453;
            oEvent.Bearing = "303";
            // oEvent.TripTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Travel = "17";
            oEvent.Age = 10;
            oEvent.Speed = 33;
            //   oEvent.TimeIntoShift = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.GPSCoverage = 100;
            EventList.Add(oEvent);
            eventManager.InsertBulk(EventList);
        }

        public void TEST_DATE_EVENT_ADD_SEEING_MACHINE_CRITICAL(long code, DateTime currentDate, int tripTimeInMin, string eventSource)
        {

            EventManager eventManager = new EventManager();
            Event oEvent = null;
            List<Event> EventList = new List<Event>();

            oEvent = new Event();
            oEvent.Code = "SM-" + code + "1";
            oEvent.OriginalCode = 0;
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 8);
            oEvent.StartPositionCode = "SM-" + code + "1";
            oEvent.EndPositionCode = "SM-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Driver_Fatigue;

            oEvent.Duration = 2.2;
            oEvent.SerialNumber = "P04025-S00011648";
            oEvent.ContractorName = "(TGA) Public Transport Agency (PTA) Dubai";
            oEvent.ConfirmationState = "pending";
            oEvent.DetectedDateTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Alarms = "Audio, Vibration";
            oEvent.DetectedPositionCode = "SM-" + code + "3";
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            oEvent.Tracking = 67;
            oEvent.TravelDistance = 156.08;
            oEvent.StationaryTime = 29453;
            oEvent.Bearing = "303";
            //   oEvent.TripTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Travel = "17";
            oEvent.Age = 10;
            oEvent.Speed = 98;
            //   oEvent.TimeIntoShift = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.GPSCoverage = 100;
            EventList.Add(oEvent);



            oEvent = new Event();
            oEvent.Code = "SM-" + code + "2";
            oEvent.OriginalCode = 0;
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 8);
            oEvent.StartPositionCode = "SM-" + code + "1";
            oEvent.EndPositionCode = "SM-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Driver_Fatigue;

            oEvent.Duration = 2.9;
            oEvent.SerialNumber = "P04025-S00011648";
            oEvent.ContractorName = "(TGA) Public Transport Agency (PTA) Dubai";
            oEvent.ConfirmationState = "pending";
            oEvent.DetectedDateTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Alarms = "Audio, Vibration";
            oEvent.DetectedPositionCode = "SM-" + code + "3";
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            oEvent.Tracking = 78;
            oEvent.TravelDistance = 132.98;
            oEvent.StationaryTime = 22444;
            oEvent.Bearing = "400";
            //     oEvent.TripTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Travel = "6";
            oEvent.Age = 11;
            oEvent.Speed = 33;
            //    oEvent.TimeIntoShift = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.GPSCoverage = 56;
            EventList.Add(oEvent);



            oEvent = new Event();
            oEvent.Code = "SM-" + code + "3";
            oEvent.OriginalCode = 0;
            oEvent.ContractorCode = 1;
            oEvent.DriverCode = null;
            oEvent.StartDate = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.EndDate = currentDate.AddMinutes(-tripTimeInMin + 8);
            oEvent.StartPositionCode = "SM-" + code + "1";
            oEvent.EndPositionCode = "SM-" + code + "2";
            oEvent.SubTripCode = null;
            oEvent.TripCode = null;
            oEvent.VehicleCode = Convert.ToInt32(code);
            oEvent.VendorCode = "1";
            oEvent.EventTypeCode = (int)EventType.Distraction;

            oEvent.Duration = 1.99;
            oEvent.SerialNumber = "P04025-S00011648";
            oEvent.ContractorName = "(TGA) Public Transport Agency (PTA) Dubai";
            oEvent.ConfirmationState = "pending";
            oEvent.DetectedDateTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Alarms = "Vibration";
            oEvent.DetectedPositionCode = "SM-" + code + "3";
            oEvent.EventSource = eventSource;
            if (oEvent.StartDate.HasValue)
                oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
            else
                oEvent.Shift = "NA";
            oEvent.Tracking = 70;
            oEvent.TravelDistance = 122;
            oEvent.StationaryTime = 565675;
            oEvent.Bearing = "250";
            // oEvent.TripTime = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.Travel = "9";
            oEvent.Age = 27;
            oEvent.Speed = 50;
            // oEvent.TimeIntoShift = currentDate.AddMinutes(-tripTimeInMin + 5);
            oEvent.GPSCoverage = 88;
            EventList.Add(oEvent);

            eventManager.InsertBulk(EventList);
        }
        [TestMethod]
        public void ADD_FULL_PROCESS_DATA()
        {
            for (int i = 0; i <= 20; i++)
            {
                //IDEAL
                DateTime currentDateTime = DateTime.Now.AddHours(4);
                int tripDurationInMin = 120;
                long code = Convert.ToInt64("60001" + i.ToString());
                TEST_DATA_VEHICLE_ADD(Convert.ToInt32(code));
                TEST_DATA_LIVE_POSITION_GPS(code, currentDateTime, tripDurationInMin);
                TEST_DATA_LIVE_POSITION_VEHICLE(code, currentDateTime, tripDurationInMin, 0, 0, i);
                TEST_DATA_TRIP_ADD(code, currentDateTime, tripDurationInMin);
                ADD_TEST_DATA_SUB_TRIP(code, currentDateTime, tripDurationInMin);
                //  TEST_DATE_EVENT_ADD(code, currentDateTime, tripDurationInMin, "FMS");
            }


            for (int i = 21; i <= 30; i++)
            {
                //NORMAL
                DateTime currentDateTime = DateTime.Now.AddHours(4);
                int tripDurationInMin = 120;
                long code = Convert.ToInt64("9800443" + i.ToString());
                TEST_DATA_VEHICLE_ADD(Convert.ToInt32(code));
                TEST_DATA_LIVE_POSITION_GPS(code, currentDateTime, tripDurationInMin);
                TEST_DATA_LIVE_POSITION_VEHICLE(code, currentDateTime, tripDurationInMin, 50, 0, i);
                TEST_DATA_TRIP_ADD(code, currentDateTime, tripDurationInMin);
                ADD_TEST_DATA_SUB_TRIP(code, currentDateTime, tripDurationInMin);
                TEST_DATE_EVENT_ADD_NORMAL(code, currentDateTime, tripDurationInMin, "FMS");
                // TEST_DATE_EVENT_VEHICLE_ADD(code, currentDateTime, tripDurationInMin);
            }


            for (int i = 32; i <= 37; i++)
            {
                //ELEVATED
                DateTime currentDateTime = DateTime.Now.AddHours(4);
                int tripDurationInMin = 120;
                long code = Convert.ToInt64("9800443" + i.ToString());
                TEST_DATA_VEHICLE_ADD(Convert.ToInt32(code));
                TEST_DATA_LIVE_POSITION_GPS(code, currentDateTime, tripDurationInMin);
                TEST_DATA_LIVE_POSITION_VEHICLE(code, currentDateTime, tripDurationInMin, 30, 70, i);
                TEST_DATA_TRIP_ADD(code, currentDateTime, tripDurationInMin);
                ADD_TEST_DATA_SUB_TRIP(code, currentDateTime, tripDurationInMin);
                TEST_DATE_EVENT_ADD_ELEVATED(code, currentDateTime, tripDurationInMin, "FMS");
                // TEST_DATE_EVENT_VEHICLE_ADD(code, currentDateTime, tripDurationInMin);
            }


            for (int i = 38; i <= 55; i++)
            {
                //CRITICAL
                DateTime currentDateTime = DateTime.Now.AddHours(4);
                int tripDurationInMin = 120;
                long code = Convert.ToInt64("9800443" + i.ToString());
                TEST_DATA_VEHICLE_ADD(Convert.ToInt32(code));
                TEST_DATA_LIVE_POSITION_GPS(code, currentDateTime, tripDurationInMin);
                TEST_DATA_LIVE_POSITION_VEHICLE(code, currentDateTime, tripDurationInMin, 30, 70, i);
                TEST_DATA_TRIP_ADD(code, currentDateTime, tripDurationInMin);
                ADD_TEST_DATA_SUB_TRIP(code, currentDateTime, tripDurationInMin);
                TEST_DATE_EVENT_ADD_CRITICAL(code, currentDateTime, tripDurationInMin, "FMS");
                // TEST_DATE_EVENT_VEHICLE_ADD(code, currentDateTime, tripDurationInMin);
            }
        }




        [TestMethod]
        public void TESTSHIFT()
        {
            DateTime currentDateTime = DateTime.Now.AddHours(4);
            ShiftFromDateTime(currentDateTime);
        }
        public string ShiftFromDateTime(DateTime currentDateTime)
        {
            currentDateTime = currentDateTime.AddHours(-4);
            string shiftType = "Night";
            TimeSpan start = TimeSpan.Parse("06:00"); // 6 AM
            TimeSpan end = TimeSpan.Parse("18:00");   // 2 AM
            TimeSpan now = currentDateTime.TimeOfDay;

            if (now >= start && now <= end)
            {
                shiftType = "Day";
            }

            return shiftType;

        }


        private Coordinate GetRandomLocation()
        {

            Coordinate location1 = new Coordinate();
            location1.Latitude = 25.2548191;
            location1.Longitude = 55.2833251;

            Coordinate location2 = new Coordinate();
            location2.Latitude = 25.1671252;
            location2.Longitude = 55.5193334;

            Coordinate location3 = new Coordinate();
            location3.Latitude = 24.8429033;
            location3.Longitude = 55.2462413;

            Coordinate location4 = new Coordinate();
            location4.Latitude = 24.8839527;
            location4.Longitude = 54.8569114;

            //Coordinate location2, Coordinate location3,Coordinate location4

            Coordinate[] allCoords = { location1, location2, location3, location4 };
            double minLat = allCoords.Min(x => x.Latitude);
            double minLon = allCoords.Min(x => x.Longitude);
            double maxLat = allCoords.Max(x => x.Latitude);
            double maxLon = allCoords.Max(x => x.Longitude);

            Random r = new Random();

            //replase 500 with your number
            Coordinate result = new Coordinate();

            Coordinate point = new Coordinate();
            do
            {
                point.Latitude = r.NextDouble() * (maxLat - minLat) + minLat;
                point.Longitude = r.NextDouble() * (maxLon - minLon) + minLon;
            } while (!IsPointInPolygon(point, allCoords));
            result = point;

            return result;
        }

        //took it from http://codereview.stackexchange.com/a/108903
        //you can use your own one
        private bool IsPointInPolygon(Coordinate point, Coordinate[] polygon)
        {
            int polygonLength = polygon.Length, i = 0;
            bool inside = false;
            // x, y for tested point.
            double pointX = point.Longitude, pointY = point.Latitude;
            // start / end point for the current polygon segment.
            double startX, startY, endX, endY;
            Coordinate endPoint = polygon[polygonLength - 1];
            endX = endPoint.Longitude;
            endY = endPoint.Latitude;
            while (i < polygonLength)
            {
                startX = endX;
                startY = endY;
                endPoint = polygon[i++];
                endX = endPoint.Longitude;
                endY = endPoint.Latitude;
                //
                inside ^= ((endY > pointY) ^ (startY > pointY)) /* ? pointY inside [startY;endY] segment ? */
                          && /* if so, test if it is under the segment */
                          (pointX - endX < (pointY - endY) * (startX - endX) / (startY - endY));
            }
            return inside;
        }


        private Coordinate GetDubaiLocation(int item)
        {
            double Latitude = 0;
            double Longitude = 0;
            Coordinate result = new Coordinate();

            switch (item)
            {
                case 0:
                    Latitude = 25.149480;
                    Longitude = 55.224495;
                    break;
                case 1:
                    Latitude = 25.200490;
                    Longitude = 55.267344;
                    break;
                case 2:
                    Latitude = 25.121895;
                    Longitude = 55.364062;
                    break;
                case 3:
                    Latitude = 25.1716278;
                    Longitude = 55.2361675;
                    break;
                case 4:
                    Latitude = 25.1649545;
                    Longitude = 55.2677623;
                    break;
                case 5:
                    Latitude = 25.2518809;
                    Longitude = 55.5112722;
                    break;
                case 6:
                    Latitude = 25.2550949;
                    Longitude = 55.4300706;
                    break;
                case 7:
                    Latitude = 25.2595381;
                    Longitude = 55.5090525;
                    break;
                case 8:
                    Latitude = 25.1916442;
                    Longitude = 55.5303299;
                    break;
                case 9:
                    Latitude = 25.0895736;
                    Longitude = 55.3784784;
                    break;
                case 10:
                    Latitude = 25.1533782;
                    Longitude = 55.3385996;
                    break;
                case 11:
                    Latitude = 25.2824057;
                    Longitude = 55.3356584;
                    break;

            }
            result.Latitude = Latitude;
            result.Longitude = Longitude;
            return result;
        }
       
    }

    public class Coordinate
    {
        public double Latitude { set; get; }
        public double Longitude { set; get; }
    }

    public enum EventType
    {
        Distraction = 25,
        Driver_Fatigue = 22,
        Forward_Collision_Warning = 14,
        FOV_Exception = 24,
        Harsh_Acceleration = 3,
        Harsh_Braking = 2,
        Left_Lane_Departure = 16,
        Mobileeye_Tamper_Alert = 18,
        Near_Missc = 23,
        Over_Revving = 4,
        Over_Speeding = 1,
        Panic_Button = 13,
        Pedestrian_In_Danger_Zone = 19,
        Pedestrian_In_Forward_Collision_Warning = 20,
        Right_Lane_Departure = 15,
        Traffic_Sign_Recognition = 17,
        Traffic_Sign_Recognition_Warning = 21,
        New_Type = 11,
        SEEING_MACHINE_OTHER = 98701
    }
}




