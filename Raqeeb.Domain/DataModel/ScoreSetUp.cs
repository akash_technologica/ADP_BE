//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Raqeeb.Domain.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ScoreSetUp
    {
        public int Id { get; set; }
        public int EventTypeId { get; set; }
        public int Min_occurrence { get; set; }
        public int Max_occurrence { get; set; }
        public int Score { get; set; }
        public string EventType { get; set; }
    }
}
