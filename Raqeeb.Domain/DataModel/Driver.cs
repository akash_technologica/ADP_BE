//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Raqeeb.Domain.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Driver
    {
        public int Code { get; set; }
        public Nullable<int> VendorCode { get; set; }
        public Nullable<int> ContractorCode { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string Mobile { get; set; }
        public string ADSDNumber { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public Nullable<System.DateTime> LicenseIssueDate { get; set; }
        public Nullable<System.DateTime> LicenseExpiryDate { get; set; }
        public string Email { get; set; }
        public Nullable<int> Current_mileage { get; set; }
        public string EmiratesId { get; set; }
        public string Nationality { get; set; }
        public string License { get; set; }
        public int EntityId { get; set; }
        public Nullable<int> TagId { get; set; }
    }
}
