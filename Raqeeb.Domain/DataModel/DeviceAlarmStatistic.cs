//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Raqeeb.Domain.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class DeviceAlarmStatistic
    {
        public int Id { get; set; }
        public string DeviceId { get; set; }
        public Nullable<System.DateTime> AlarmDate { get; set; }
        public Nullable<int> AlarmCount { get; set; }
    }
}
