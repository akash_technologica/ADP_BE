//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Raqeeb.Domain.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class User_Permission
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public bool Usersview { get; set; }
        public bool Usersall { get; set; }
        public bool Rolesview { get; set; }
        public bool Rolesall { get; set; }
        public bool Deviceview { get; set; }
        public bool Deviceall { get; set; }
        public bool Driverview { get; set; }
        public bool Driverall { get; set; }
        public bool Vehiclesview { get; set; }
        public bool Vehiclesall { get; set; }
        public bool Dispatcherview { get; set; }
        public bool Dispatcherall { get; set; }
        public bool Report_violation { get; set; }
        public bool Report_eventlog { get; set; }
        public bool Report_trip { get; set; }
        public bool DeviceMenuActive { get; set; }
        public bool DispatcherMenuActive { get; set; }
        public bool DriverMenuActive { get; set; }
        public bool Report_eventlogMenuActive { get; set; }
        public bool Report_tripMenuActive { get; set; }
        public bool Report_violationMenuActive { get; set; }
        public bool RolesMenuActive { get; set; }
        public bool UsersMenuActive { get; set; }
        public bool VehiclesMenuActive { get; set; }
    }
}
