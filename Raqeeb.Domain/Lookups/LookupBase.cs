﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Raqeeb.Domain
{
    [Serializable]
    public class LookupBase
    {
        [BsonId]
        [BsonElement(elementName: "_id")]
        public BsonObjectId Id { get; set; }

        [BsonElement(elementName: "ARABIC_DESCRIPTION")]
        public string ArabicDescription { get; set; }

        [BsonElement(elementName: "ENGLISH_DESCRIPTION")]
        public string EnglishDescription { get; set; }

        [BsonElement(elementName: "CODE")]
        public int Code { get; set; }

        [BsonElement(elementName: "CODE_DESC")]
        public string CodeDesc { get; set; }


    }
}
