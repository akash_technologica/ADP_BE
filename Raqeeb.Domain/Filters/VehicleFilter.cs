﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Filters
{
    public class VehicleFilter
    {
        public string Registration { get; set; }

        public int? ClassificationEventType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Shift { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int Code { get; set; }
        public int[] VehicleCode { get; set; }
        public int[] GroupCode { get; set; }
        public int UserGroup { get; set; }
        public string[] LicenseCode { get; set; }
    }
}
