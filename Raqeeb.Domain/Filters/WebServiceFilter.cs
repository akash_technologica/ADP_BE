﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Filters
{
    public class WebServiceFilter
    {
        public int? WebServiceType { get; set; }
    }


    public class LoggingFilter
    {
        public int? LogType { get; set; }
    }
}
