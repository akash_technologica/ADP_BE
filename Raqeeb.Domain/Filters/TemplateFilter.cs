﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Filters
{
    public class TemplateFilter
    {
        public string Name { get; set; }
        public int? NotificationTypeId { get; set; }
        public int? StatusId { get; set; }
    }
}
