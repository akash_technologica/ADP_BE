﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Filters
{
    public class EventFilter
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? EventTypeCode { get; set; }
        public int? VehicleCode { get; set; }
        public DateTime? DateOfToday { get; set; }
        public long? TripCode { get; set; }
        public string Shift { get; set; }
        public string VehicleRegistration { get; set; }
        public int? DriverTypeCode { get; set; }
        public bool UnknownDriver { get; set; }
        public int[] VehicleCodes { get; set; } = new int[] { };
        public string[] PlateNumbers { get; set; } = new string[] { };
        public int[] DriverCodes { get; set; } = new int[] { };
        public int[] EventTypeCodes { get; set; } = new int[] { };
        public int[] GroupCode { get; set; } = new int[] { };
        public object DataTablesParameters { get; set; }
    }
}
