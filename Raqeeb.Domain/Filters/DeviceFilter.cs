﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Filters
{
    public class DeviceFilter
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string[] DeviceNumbers { get; set; }
        public int[] VehicleCodes { get; set; }
        public string[] PlateNumbers { get; set; }
        public int[] GroupCode { get; set; }
        public int UserGroup { get; set; }

    }
}
