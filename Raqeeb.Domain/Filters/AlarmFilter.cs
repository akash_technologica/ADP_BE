﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Filters
{
    public class AlarmFilter
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? AlarmTypeCode { get; set; }
        public int? VehicleCode { get; set; }
        public DateTime? DateOfToday { get; set; }       
        public string VehicleRegistration { get; set; }       
        public int[] VehicleCodes { get; set; }      
        public int[] AlarmTypeCodes { get; set; }
        public int Group { get; set; }
    }
}
