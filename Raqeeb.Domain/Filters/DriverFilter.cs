﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Filters
{
    public class DriverFilter
    {
        public string ADSDNumber { get; set; }

        public int Month { get; set; }
        public int Year { get; set; }
        public int Code { get; set; }
        public int[] DriverCode { get; set; }

    }
}
