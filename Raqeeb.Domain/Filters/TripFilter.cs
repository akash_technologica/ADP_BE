﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Filters
{
    public class TripFilter
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public int? VehicleCode { get; set; }

        public int? DriverCode { get; set; }
        public bool UnknownDriver { get; set; }
        public int[] VehicleCodes { get; set; }
        public int[] DriverCodes { get; set; }
    }
}
