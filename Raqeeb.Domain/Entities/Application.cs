﻿using MongoDB.Bson.Serialization.Attributes;
using Raqeeb.Framework.Core;
using System;

namespace Raqeeb.Domain
{
    [Serializable]
    public class Application : AuditableEntityBase
    {
        [BsonElement(elementName: "ARABIC_NAME")]
        public string ArabicName { get; set; }

        [BsonElement(elementName: "ENGLISH_NAME")]
        public string EnglishName { get; set; }

        [BsonElement(elementName: "SUBSCRIPTION_START_DATE")]
        public DateTime? SubscriptionStartDate { get; set; }

        [BsonElement(elementName: "SUBSCRIPTION_END_DATE")]
        public DateTime? SubscriptionEndDate { get; set; }

        [BsonElement(elementName: "STATUS")]
        public bool Status { get; set; }

        [BsonElement(elementName: "API_KEY")]
        public string ApiKey { get; set; }

        [BsonElement(elementName: "SECRET_KEY")]
        public string SecretKey { get; set; }

        [BsonElement(elementName: "USER_TOKEN")]
        public string UserToken { get; set; }

        [BsonElement(elementName: "USER_PHOTO")]
        public byte[] UserPhoto { get; set; }

        [BsonElement(elementName: "PASSWORD")]
        public string Password { get; set; }

        [BsonElement(elementName: "USER_NAME")]
        public string UserName { get; set; }

        [BsonElement(elementName: "USER_GUID")]
        public string UserGuid { get; set; }

        [BsonElement(elementName: "EMAIL_ADDRESS")]
        public string EmailAddress { get; set; }

        [BsonElement(elementName: "REFRESH_TOKEN")]
        public string RefreshToken { get; set; }

        [BsonElement(elementName: "DEFAULT_URL")]
        public string DefaultUrl { get; set; }

        [BsonElement(elementName: "LAST_LOGIN")]
        public DateTime? LastLogin { get; set; }

        [BsonElement(elementName: "MOBILE_NUMBER")]
        public string MobileNumber { get; set; }

        [BsonElement(elementName: "RECAPTCHA_RESPONSE")]
        public string recaptchaResponse { get; set; }

        [BsonElement(elementName: "ACCESS_TOKEN_STAMP")]
        public string AccessTokenStamp { get; set; }
    }
}
