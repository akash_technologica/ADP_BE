﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class SubEventTypeMaster : IdentifiableEntityBase
    {

        public int Code { get; set; }

        public int EventTypeCode { get; set; }

        public string Description { get; set; }


    }
}
