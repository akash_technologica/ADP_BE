﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    public class Reclassification
    {
        public long Code { get; set; }
        public int EventTypeCode { get; set; }
        public int SubEventTypeCode { get; set; }
        public Boolean Status { get; set; }
        public string UserName { get; set; }
        public int UpdateStatusCode { get; set; }
        public string Comments { get; set; }
    }
}
