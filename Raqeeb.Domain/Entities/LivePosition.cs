﻿using Raqeeb.Domain.Enums;
using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    public class LivePosition : IdentifiableEntityBase
    {
        public string Code { get; set; }
        public long OriginalCode { get; set; }

        public int? VendorCode { get; set; }

        public int ContractorCode { get; set; }

        public int? DriverCode { get; set; }

        public int? VehicleCode { get; set; }

        public DateTime LocationTIme { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? Altitude { get; set; }
        public int? Heading { get; set; }
        public int? Speed { get; set; }
        public int? Prm { get; set; }

        public double? OdoMeter { get; set; }

        public PositionType Type { set; get; }


        public int? TripCode { set; get; }
        public int? SubTripCode { set; get; }
        public int? DriverSeatBelt { set; get; }
        public int? PassengerSeatBelt { set; get; }
        public int? FwdEngaged { set; get; }
        public string CurrentEvents { get; set; }
        public bool EngineOn { get; set; }
        public bool Idle { get; set; }
        public int? EngineTemperature { get; set; }
        public int? GreenDrivingType { get; set; }
        public int? GSM_Signal { get; set; }
        public int? Sleep_Mode { get; set; }
        public int? GNSS_PDOP { get; set; }
        public int? GNSS_HDOP { get; set; }
        public int? ExternalVoltage { get; set; }
        public int? Battery_Voltage { get; set; }
        public int? Digital_Input_1 { get; set; }
        public int? RFID { get; set; }
        public int? AcceleratorPedalPosition { get; set; }
        public int? Fuel_Consumed { get; set; }
        public int? FuelLevelLiters { get; set; }
        public int? TotalMileage { get; set; }
        public int? FuelLevelPercent { get; set; }
        public int? DoorStatus { get; set; }
        public int? FuelRate { get; set; }
        public int? EngineLoad { get; set; }
        public int? CNGStatus { get; set; }
        public long ReferenceNumber { get; set; }
    }
}
