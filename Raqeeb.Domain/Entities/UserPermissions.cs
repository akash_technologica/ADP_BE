﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class UserPermissions:IdentifiableEntityBase
    {
        public string UserId { get; set; }
        public bool Usersview { get; set; }
        public bool Usersall { get; set; }

        public bool UsersMenuActive { get; set; }
        public bool Rolesview { get; set; }
        public bool Rolesall { get; set; }
        public bool RolesMenuActive { get; set; }
        public bool Deviceview { get; set; }
        public bool Deviceall { get; set; }
        public bool DeviceMenuActive { get; set; }
        public bool Driverview { get; set; }
        public bool Driverall { get; set; }
        public bool DriverMenuActive { get; set; }
        public bool Vehiclesview { get; set; }
        public bool Vehiclesall { get; set; }
        public bool VehiclesMenuActive { get; set; }
        public bool Dispatcherview { get; set; }
        public bool Dispatcherall { get; set; }
        public bool DispatcherMenuActive { get; set; }
        public bool Report_violation { get; set; }
        public bool Report_violationMenuActive { get; set; }
        public bool Report_eventlog { get; set; }
        public bool Report_eventlogMenuActive { get; set; }
        public bool Report_trip { get; set; }
        public bool Report_tripMenuActive { get; set; }



    }
   
}
