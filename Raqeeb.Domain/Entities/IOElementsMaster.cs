﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class IOElementsMaster : IdentifiableEntityBase
    {
        public string Name { get; set; }
        public string IO_Code { get; set; }
        public bool IsActive { get; set; }
    }
}
