﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class AllocatedVehicleGeofencing: IdentifiableEntityBase
    {
        public string VehicleId { get; set; }
        public List<string> GeofenceId { get; set; }
        public bool IsViolationEnabled { get; set; }

    }
}
