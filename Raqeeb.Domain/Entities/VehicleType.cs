﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    public class VehicleType: IdentifiableEntityBase
    {
        public string VehicleTypeName { get; set; }
    }
}
