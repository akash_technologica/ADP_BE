﻿using MongoDB.Bson.Serialization.Attributes;
using Raqeeb.Framework.Core;
using System;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Template : AuditableEntityBase
    {
        [BsonElement(elementName: "NAME")]
        public string Name { get; set; }

        [BsonElement(elementName: "TITLE")]
        public string Title { get; set; }

        [BsonElement(elementName: "BODY")]
        public string Body { get; set; }

        [BsonElement(elementName: "PARAMETERS")]
        public string Parameters { get; set; }

        [BsonElement(elementName: "NOTIFICATION_TYPE")]
        public int NotificationType { get; set; }

        [BsonElement(elementName: "LANGUAGE")]
        public string LanguageIsoCode { get; set; }

        [BsonElement(elementName: "APPLICATION_ID")]
        public string ApplicationId { get; set; }

        [BsonElement(elementName: "IS_PUBLISHED")]
        public bool IsPublished { get; set; }

    }
}
