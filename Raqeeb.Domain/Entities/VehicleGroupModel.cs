﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    public class VehicleGroupModel
    {
        public int GroupId { get; set; }
        public string VehicleId { get; set; }
        public string GroupName { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool IsActive { get; set; }

    }
}
