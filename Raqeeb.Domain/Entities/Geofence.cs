﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Geofence: IdentifiableEntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Margin { get; set; }
        public bool Notification { get; set; }
        public string ShapeType { get; set; }
        public double North { get; set; }
        public double East { get; set; }
        public double South { get; set; }
        public double West { get; set; }
        public double CenterLat { get; set; }
        public double CenterLng { get; set; }
        public List<CoOrdinates> Points { get; set; }
        public double Radius { get; set; }
        public List<int> VehicleCodes { get; set; }

        public Timers StartTime { get; set; }
        public Timers EndTime { get; set; }

    }
    public class CoOrdinates
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
    public class Timers
    {
        public double Hour { get; set; }
        public double Minute { get; set; }
    }
}
