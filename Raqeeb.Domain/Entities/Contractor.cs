﻿using MongoDB.Bson.Serialization.Attributes;
using Raqeeb.Framework.Core;
using System;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Contractor : IdentifiableEntityBase
    {
        public int? Code { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string FaxNumber { get; set; }
        public int? VendorCode { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
