﻿using Raqeeb.Domain.Enums;
using Raqeeb.Framework.Core;
using System;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class WebServiceSetup : IdentifiableEntityBase
    {
        public string Name { get; set; }
        public DateTime LastSynchTime { get; set; }
        public long LasItemId { get; set; }
        public WebServiceType Type { set; get; }
        public bool IsFinished { set; get; }

        public long? NumberOfExecution { set; get; }
    }
}
