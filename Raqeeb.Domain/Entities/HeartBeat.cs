﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class HeartBeat : IdentifiableEntityBase 
    {
        public string VehicleRegistration;

        public string AuthenticationFailure { get; set; }

        public string BootloaderFirmwareFailure { get; set; }

        public string BuzzerFault { get; set; }

        public string CameraDetected { get; set; }

        public string CameraFault { get; set; }

        public int ClockDrift { get; set; }

        public string CmosBatteryFault { get; set; }

        public string ConfigurationFilesOk { get; set; }

        public string CpuLoadOk { get; set; }

        public double CpuPercent { get; set; }

        public string ErrorFlags { get; set; }

        public string EventFlags { get; set; }

        public string ExceptionInProgress { get; set; }

        public string FfcFault { get; set; }

        public string FfcNoData { get; set; }

        public string FfcNotDetected { get; set; }

        public string GpsDetected { get; set; }

        public string GpsDetectedButNoSignal { get; set; }

        public int GpsPercent { get; set; }

        public string IpAddress { get; set; }

        public string IrPodsInactive { get; set; }

        public int LogDisk { get; set; }
        public string LogDiskDetected { get; set; }
        public string MacAddress { get; set; }
        public string MaxRebootsExceeded { get; set; }
        public string OverTemperature { get; set; }
        public string PsuFault { get; set; }
        public string SimError { get; set; }
        public string SpeakerFault { get; set; }
        public int SystemDisk { get; set; }
        public string SystemDiskDetected { get; set; }
        public string SystemTimeUtc { get; set; }
        public int Temperature { get; set; }
        public int Tracking { get; set; }
        public string UnverifiedSystemTime { get; set; }
        public string UibrationMotorFault { get; set; }
        public string VibrationMotorOverheat { get; set; }
        public string Warnings { get; set; }
        public int Altitude { get; set; }
        public double Course { get; set; }
        public DateTime GpsTimeUtc { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double SpeedMps { get; set; }
        public string Suppressed { get; set; }
        public string Valid { get; set; }
        public DateTime LastCommand { get; set; }
        public DateTime ModifiedTime { get; set; }
        public string OperationalState { get; set; }
        public string ProductVersion { get; set; }
        public string ProvisioningState { get; set; }
        public string SerialNumber { get; set; }
        public string SoftwareVersion { get; set; }
        public DateTime InstallationDate { get; set; }
        public string CarrierBoardVersion { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
