﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class DriverScore : IdentifiableEntityBase
    {

        public int DriverCode { get; set; }
        public string Name { get; set; }
        public int MonthName { get; set; }
        public int YearName { get; set; }
        public double TotalScore { get; set; }
        public int TotalFatigue { get; set; }
        public int TotalDistraction { get; set; }
        public int TotalFOV { get; set; }
        public int TotalOverspeeding { get; set; }
        public int TotalHarshAcceleration { get; set; }
        public int TotalForwardCollisionWarning { get; set; }
        public int TotalHarshBraking { get; set; }
        public int TotalLeftLaneDeparture { get; set; }
        public int TotalMobileeyeTamperAlert { get; set; }
        public int TotalNearmiss { get; set; }
        public int TotalOverRevving { get; set; }
        public int TotalPanicButton { get; set; }
        public int TotalPedestrianInDangerZone { get; set; }
        public int TotalPedestrianInForwardCollisionWarning { get; set; }
        public int TotalRightLaneDeparture { get; set; }

        public int TotalTrafficSignRecognition { get; set; }

        public int TotalTrafficSignRecognitionWarning { get; set; }

        public int TotalPowerFailure { get; set; }

        public double TotalDistanceDriven { get; set; }

        public int TotalTrips { get; set; }
        public string TotalDrivingTime { get; set; }
        public string TotalStandingTime { get; set; }
        public string TotalDurationTime { get; set; }
        public int MaxSpeed { get; set; }
        public int MaxRPM { get; set; }
        public int MediaData { get; set; }
        public DateTime LastUpdate { get; set; }
        public int Status { get; set; }
        public List<VehicleHistoryDetail> VehicleHistory { get; set; }
    }
    public class VehicleHistoryDetail: IdentifiableEntityBase
    {
        public string Registraion { get; set; }
        public int VehicleCode { get; set; }
        public int Trips { get; set; }
        public double DistanceDriven { get; set; }
        

    }

}
