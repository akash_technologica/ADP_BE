﻿using MongoDB.Bson.Serialization.Attributes;
using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class MenuMaster : IdentifiableEntityBase
    {
        
        [BsonElement(elementName: "MENU_NAME")]
        public string MenuName { get; set; }

        [BsonElement(elementName: "IS_ACTIVE")]
        public bool IsActive { get; set; }

        [BsonElement(elementName: "MENU_URL")]
        public string RoutingUrl { get; set; }
    }
}
