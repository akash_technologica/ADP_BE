﻿using Raqeeb.Domain.Enums;
using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class EventMedia : IdentifiableEntityBase
    {
        public string ContentType { get; set; }
        public EventMediaType Type { get; set; }
        public string Url { get; set; }
        public string Context { get; set; }
        public int FrameNumber { get; set; }
        public ImageCrop ImageCrop { get; set; }
        public int SizeBytes { get; set; }
        public PayloadType PayloadType { get; set; }
        public string EventCode { get; set; }
        public long GuardianId { get; set; }
    }
}
