﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class ScoreSetUp : AuditableEntityBase
    {
     //   public string _id { get; set; }
        public int EventTypeId { get; set; }
        public int Min_occurrence { get; set; }
        public int Max_occurrence { get; set; }
        public int Score { get; set; }
        public string EventType { get; set; }

    }
}
