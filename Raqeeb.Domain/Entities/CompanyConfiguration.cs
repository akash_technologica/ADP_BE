﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class CompanyConfiguration : AuditableEntityBase
    {
        public string CompanyName { get; set; }
        public string CountryId { get; set; }
        public string CityId { get; set; }
        public string Address { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public Boolean IsActive { get; set; }
        public DateTime LicenseStart { get; set; }
        public DateTime LicenseEnd { get; set; }
        public string Notes { get; set; }
        public string AccountId { get; set; }
        public int Validity { get; set; }
    }
}
