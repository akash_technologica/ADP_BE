﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    public class VehicleModel:IdentifiableEntityBase
    {
        public string ModelNumber { get; set; }
        public string ModelName { get; set; }
        public string VehicleBrandId { get; set; }
    }
}
