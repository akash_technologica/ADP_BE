﻿using Raqeeb.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
   public class EventSetup
    {
        public EventType EventTypeCode{ get; set; }
        public string EventType { get; set; }
        public int MinEventsNumber { get; set; }

    }
}
