﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class CityMaster : IdentifiableEntityBase
    {
        public string EnglishName { get; set; }
        public bool IsActive { get; set; }
        public int CountryCode { get; set; } 
        public string CountryId { get; set; }
    }
}
