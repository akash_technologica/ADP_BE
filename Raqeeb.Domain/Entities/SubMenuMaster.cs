﻿using MongoDB.Bson.Serialization.Attributes;
using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class SubMenuMaster: IdentifiableEntityBase
    {
        [BsonElement(elementName: "MENU_ID")]
        public string MenuID { get; set; }
        [BsonElement(elementName: "SUB_MENU_NAME")]
        public string SubMenuName { get; set; }
        [BsonElement(elementName: "SUB_MENU_URL")]
        public string SubMenuUrl { get; set; }
        [BsonElement(elementName: "IS_ACTIVE")]
        public bool IsActive { get; set; }
        [BsonElement(elementName: "USER_ID")]
        public string UserId { get; set; }
    }
}
