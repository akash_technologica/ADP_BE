﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class TripCoOridinate: IdentifiableEntityBase
    {
        public long TripCode { get; set; }
        public List<LatLongList> TripLatLongDetails { get; set; }
    }

    public class LatLongList
    {
        public double Latitide { get; set; }
        public double Longitude { get; set; }
        public int Speed { get; set; }
        public double RPM { get; set; }
        public double Odometer { get; set; }
        public DateTime LocationDateTime { get; set; }
    }
}
