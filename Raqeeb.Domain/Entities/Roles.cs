﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Roles : IdentifiableEntityBase
    {
        [BsonElement(elementName: "ROLE")]
        public string Role { get; set; }

        [BsonElement(elementName: "IS_ACTIVE")]
        public bool IsActive { get; set; }
        [BsonElement(elementName: "MENU_ID")]
        public List<string> MenuId { get; set; }

    }
}
