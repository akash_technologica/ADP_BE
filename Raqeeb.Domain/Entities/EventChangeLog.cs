﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class EventChangeLog: IdentifiableEntityBase
    {
        public string EventCode { get; set; }
        public int SubEventCode { get; set; }
        public int ClassificationType { get; set; }
        public string UpdatedBy { get; set; }
        public string Comments { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
