﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class SubTrip : IdentifiableEntityBase
    {
        public long? Code { get; set; }

        public long? TripCode { get; set; }

        public int? VendorCode { get; set; }

        public int ContractorCode { get; set; }

        public int? DriverCode { get; set; }

        public int? VehicleCode { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? Duration { get; set; }

        public double? DistanceDriven { get; set; }

        public int? MaxSpeed { get; set; }

        public int? MaxPrm { get; set; }

        public string StartPositionCode { get; set; }

        public string EndPositionCode { get; set; }
        public long? StartPosition { get; set; }
        public long? EndPosition { get; set; }

    }
}
