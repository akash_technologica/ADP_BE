﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Driver : IdentifiableEntityBase
    {
        public int Code { get; set; }

        public int? VendorCode { get; set; }

        public int ContractorCode { get; set; }

        public string Name { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string Mobile { get; set; }

        public string ADSDNumber { get; set; }

        public DateTime? LastUpdated { get; set; }

        public DateTime? LicenseIssueDate { get; set; }

        public DateTime? LicenseExpiryDate { get; set; }

        public string Email { get; set; }
    }
}
