﻿using Raqeeb.Domain.Enums;
using Raqeeb.Framework.Core;
using System;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Log : IdentifiableEntityBase
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
        public string ExtraInformation { get; set; }
        public DateTime CreationDate { get; set; }
        public string Tags { get; set; }
        public LogType LogType { get; set; }
    }
}
