﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Event : IdentifiableEntityBase
    {
        public string Code { get; set; }
        public long? OriginalCode { get; set; }
        public string VendorCode { get; set; }

        public int ContractorCode { get; set; }
        public int? DriverCode { get; set; }

        public int? VehicleCode { get; set; }

        public int? EventTypeCode { get; set; }

        public long? TripCode { get; set; }

        public int? SubTripCode { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string StartPositionCode { get; set; }

        public string EndPositionCode { get; set; }

        //GUARDIAN
        public double Duration { get; set; } // (S)
        public string SerialNumber { get; set; }
        public string ContractorName { get; set; }
        public string ConfirmationState { get; set; } //CONFIRMATION
        public DateTime DetectedDateTime { get; set; } //createTimeUtc - TIME
        public string Alarms { get; set; }
        public string DetectedPositionCode { get; set; } // Location GPS
        public string Shift { get; set; } // SHIFT
        public int Tracking { get; set; } //TRACKING PERFORM
        public double TravelDistance { get; set; } // TRAVEL DISTANCE (M)
        public long StationaryTime { get; set; } //STATIONARY TIME (H:M:S)\
        public string EventSource { get; set; }
        public string Bearing { get; set; } // BEARING
        public string TripTime { get; set; } //TRIP TIME(H:M:S)
        public string Travel { get; set; } // TRAVEL
        public long Age { get; set; } // AGE
        public double? Speed { get; set; } // SPEED
        public string TimeIntoShift { get; set; } //TIME INTO SHIFT
        public float? GPSCoverage { get; set; } //GPS COVERAGE

        public long? ExternalId { get; set; }

        public string Url { get; set; }
        public string ContentType { get; set; }
        public string EventTypeDescription { get; set; }
        public bool EventAccepted { get; set; }
        public bool EventRejected { get; set; }
        public List<ChangesLog> EventVersion { get; set; }
        
        public DateTime UpdatedTime { set; get; }
        public int UpdatedBy { set; get; }
        public List<Media> MediaData { get; set; }

        public string UnknownDriverCode { get; set; }
        public int? EventTypeValue { get; set; }
        public long? ReferenceNumber { get; set; }
    }
    public class ChangesLog
    {
        public int EventCode { get; set; }
        public int SubEventCode { get; set; }
        public string ClassificationType { get; set; }
        public string UpdatedBy { get; set; }
        public string Comments { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
