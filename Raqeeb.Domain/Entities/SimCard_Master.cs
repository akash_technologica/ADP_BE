﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class SimCard_Master : IdentifiableEntityBase
    {
        public string Gsm_Code { get; set; }
        public string Sim_Code { get; set; }
        public string Number { get; set; }
        public string ICCID { get; set; }
        public string MSISDN { get; set; }
        public string IMSI { get; set; }
        public string Description { get; set; }
        public string EntityId { get; set; }
        public bool IsActive { get; set; }
    }
}
