﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class VehicleBrandMaster : IdentifiableEntityBase
    {
        public string Name { get; set; }
    }
}
