﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Vehicle : IdentifiableEntityBase
    {
        // basic details
        public int? Code { get; set; }
        public int? VendorCode { get; set; }
        public int? ContractorCode { get; set; }
        public DateTime UpdateDate { get; set; }
        public double? LastOdoMeter { get; set; }
        public string Registration { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime IssueDate { get; set; }
        public string Gps_UnitiId { get; set; }
        public string Vin { get; set; }
        public string VehicleType { get; set; }
        public string BodyType { get; set; }
        public string FuelType { get; set; }
        public string EngineCapacity { get; set; }
        public string EnginePower { get; set; }
        public string Engine { get; set; }
        public string BodyColor { get; set; }
        public string SteeringSystem { get; set; }
        public string GearBoxCode { get; set; }
        public string GearboxType { get; set; }
        public string GearBox { get; set; }
        public string Axle { get; set; }
        public string FuelConsumption { get; set; }
        public string ModelCode { get; set; }

        // tracking details
        public Boolean? EngineOn { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? Speed { get; set; }
        public int? RPM { get; set; }
        public DateTime LastUpdateLocationDate { get; set; }
        public string CurrentEvents { get; set; }
        public Boolean? Critical { get; set; }
        public Boolean? Elevated { get; set; }
        public Boolean? Normal { get; set; }
        public Boolean? Idle { get; set; }
        public Boolean? Reporting { get; set; }
        public int? Counter { get; set; }
        public string SM_Serial_Number { get; set; }
        public string FMS_Serial_Number { get; set; }
        public string VehicleStatus { get; set; }
        public int EventCount { get; set; }
        public int? DriverCode { get; set; }
        public bool IsGeofenceViolation { get; set; }
        public int? EngineTemperature { get; set; }
        public int? GreenDrivingType { get; set; }
        public int? GSM_Signal { get; set; }
        public int? Sleep_Mode { get; set; }
        public int? GNSS_PDOP { get; set; }
        public int? GNSS_HDOP { get; set; }
        public int? ExternalVoltage { get; set; }
        public int? Battery_Voltage { get; set; }
        public int? Battery_Current { get; set; }
        public int? Digital_Input_1 { get; set; }
        public int? Analog_Input_1 { get; set; }
        public int? Axis_X { get; set; }
        public int? Digital_Output_1 { get; set; }
        public int? Axis_Y { get; set; }
        public int? Axis_Z { get; set; }
        public int? SD_Status { get; set; }
        public int? RFID { get; set; }
        public int? Vehicle_CAN_Speed { get; set; }
        public int? AcceleratorPedalPosition { get; set; }
        public int? Fuel_Consumed { get; set; }
        public int? CNGStatus { get; set; }
        public int? EngineLoad { get; set; }
        public int? FuelRate { get; set; }
        public int? DoorStatus { get; set; }
        public int? FuelLevelPercent { get; set; }
        public int? TotalMileage { get; set; }
        public int? FuelLevelLiters { get; set; }
    }
}
