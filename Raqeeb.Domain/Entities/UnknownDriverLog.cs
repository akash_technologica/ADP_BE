﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class UnknownDriverLog : IdentifiableEntityBase
    {
        public DateTime LogDate { get; set; }
        public string DriverCode { get; set; }
        public int VehicleId { get; set; }
        public Double Lat { get; set; }
        public Double Long { get; set; }
    }
}
