﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class CountryMaster : IdentifiableEntityBase
    {
        public string EnglishName { get; set; }
        public string CountryCode { get; set; }
        public bool IsActive { get; set; }
        public string ArabicName { get; set; }
        public List<CityMaster> Cities { get; set; }
    }
}