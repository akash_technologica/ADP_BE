﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Trip : IdentifiableEntityBase
    {
        public long? Code { get; set; }

        public int? VendorCode { get; set; }

        public int ContractorCode { get; set; }
        public int? DriverCode { get; set; }

        public int? VehicleCode { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public int? Duration { get; set; }

        public int? DrivingTime { get; set; }
        public int? StandingTime { get; set; }
        public double? DistanceDriven { get; set; }
        public double? StartOdoMeter { get; set; }
        public double? EndOdoMeter { get; set; }

        public int? MaxSpeed { get; set; }

        public int? MaxPrm { get; set; }

        public string StartPositionCode { get; set; }
        public string EndPositionCode { get; set; }
        public long? StartPosition { get; set; }
        public long? EndPosition { get; set; }
        public List<TripCoOridinates> TripCoOridinatesList { get; set; }

    }
    public class TripCoOridinates
    {
        public double Latitide { get; set; }
        public double Longitude { get; set; }
        public int Speed { get; set; }
        public double RPM { get; set; }
        public double Odometer { get; set; }
        public DateTime LocationDateTime { get; set; }

    }
}
