﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Seeing_Machine_Data : IdentifiableEntityBase
    {
        /// <summary>
        /// 
        /// </summary>
        public GuardianEvent guardianEvent { get; set; }
        public long Code { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public class GuardianEvent
        {

            /// <summary>
            /// 
            /// </summary>
            public Acceleration acceleration { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int audioLevel { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Classification classification { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string confirmationState { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string createTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string crew { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public DetectedEventType detectedEventType { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public DetectionFrame detectionFrame { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double durationSeconds { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public EndFrame endFrame { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string eventTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public EventType eventType { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int gpioLevel { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public List<MediaItem> media { get; set; }


            public string rosetta { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Shift shift { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public ShiftStatistics shiftStatistics { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public StartFrame startFrame { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Trip trip { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public TripStatistics tripStatistics { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int utcOffsetMinutes { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string version { get; set; }


        }

        public class Classification
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string falsePositive { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public List<string> labels { get; set; }
        }
        public class Acceleration
        {
            public float? linearX { get; set; }
            public float? linearY { get; set; }
            public float? linearZ { get; set; }

        }

        public class DetectedEventType
        {

            /// <summary>
            /// 
            /// </summary>
            public string value { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public List<string> labels { get; set; }
        }

        public class DetectionFrame
        {

            /// <summary>
            /// 
            /// </summary>
            public int altitudeMetres { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public float? courseRadians { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public long frameNumber { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string frameTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string gpsTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Location location { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public float? speedMps { get; set; }
        }

        public class EndFrame
        {

            /// <summary>
            /// 
            /// </summary>
            public int altitudeMetres { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double courseRadians { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public long frameNumber { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string frameTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string gpsTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Location location { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double speedMps { get; set; }
        }
        public class EventType
        {

            /// <summary>
            /// 
            /// </summary>
            public string value { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public List<string> labels { get; set; }
        }

        public class MediaItem
        {

            /// <summary>
            /// 
            /// </summary>
            public string contentType { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string imageCrop { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int sizeBytes { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string payloadType { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string url { get; set; }
        }

        public class Shift
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int durationMinutes { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string startTime { get; set; }
        }

        public class ShiftStatistics
        {

            /// <summary>
            /// 
            /// </summary>
            public double averageSpeedMps { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int distanceTravelledMetres { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string endTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double gpsPercent { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double maximumSpeedMps { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int operatingTimeSeconds { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int startFrame { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string startTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int stationaryTimeSeconds { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double trackingPerformance { get; set; }
        }

        public class StartFrame
        {

            /// <summary>
            /// 
            /// </summary>
            public int? altitudeMetres { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double courseRadians { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public long frameNumber { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string frameTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string gpsTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Location location { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double speedMps { get; set; }
        }

        public class TripStatistics
        {

            /// <summary>
            /// 
            /// </summary>
            public double averageSpeedMps { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int distanceTravelledMetres { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string endTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public float? gpsPercent { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double maximumSpeedMps { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int operatingTimeSeconds { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int startFrame { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string startTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int stationaryTimeSeconds { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double trackingPerformance { get; set; }
        }

        public class Location
        {

            /// <summary>
            /// 
            /// </summary>
            public double latitude { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double longitude { get; set; }
        }
        public class Trip
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public EndFrame endFrame { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Sensor sensor { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public StartLocation startLocation { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string startTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int utcOffsetMinutes { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Vehicle vehicle { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string createTimeUtc { get; set; }
        }
        public class Sensor
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string serialNumber { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string softwareVersion { get; set; }
        }
        public class StartLocation
        {

            /// <summary>
            /// 
            /// </summary>
            public double latitude { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double longitude { get; set; }
        }
        public class Vehicle
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            // public string class { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Fleet fleet { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string group { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string rosetta { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string type { get; set; }
        }
        public class Fleet
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Account account { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public I18n i18n { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Location location { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string rosetta { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Timezone timezone { get; set; }
        }
        public class Account
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string name { get; set; }
        }

        public class I18n
        {

            /// <summary>
            /// 
            /// </summary>
            public string cultureCode { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string distanceUnits { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string languageCode { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string temperatureUnits { get; set; }
        }
        public class Timezone
        {

            /// <summary>
            /// 
            /// </summary>
            public int baseUtcOffsetMinutes { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string tzdataName { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string windowsName { get; set; }
        }
    }
}
