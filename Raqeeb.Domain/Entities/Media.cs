﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Media: IdentifiableEntityBase
    {
        public string ContentType { get; set; }
        public string Context { get; set; }
        public string FrameNumber { get; set; }
        public string ImageCrop { get; set; }
        public long SizeBytes { get; set; }
        public string PayloadType { get; set; }
        public string Url { get; set; }
        public byte[] MediaBit { get; set; }
        public long? EventCode { get; set; }
        public string content { get; set; }
        public long GuardianId { get; set; }
        public int? EventTypeCode { get; set; }
    }
}
