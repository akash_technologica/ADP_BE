﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class GSM_Master : IdentifiableEntityBase
    {
        public string OperatorName { get; set; }
        public string GSM_Code { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string EntityId { get; set; }
    }
}
