﻿using Raqeeb.Domain.Enums;
using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Setup  : IdentifiableEntityBase
    {
        public EventClassification ClassificationType { get; set; }
        public List<EventSetup> EventSetups { get; set; } 
    }
}
