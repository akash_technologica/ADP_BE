﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Device : IdentifiableEntityBase
    {
        public string ModelCode { get; set; }
        public string Name { get; set; }
        public string IMEI { get; set; }
        public string FirmwareVersion { get; set; }
        public DateTime LastCommandReceived { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime InstallationDate { get; set; }
        public DateTime LastServiceDate { get; set; }
        public bool IsActive { get; set; }
        public List<string> AccessoriesCode { get; set; }
        public string EntityId { get; set; }
        public string VehicleCode { get; set; }
        public string SimCardCode { get; set; }
    }
}
