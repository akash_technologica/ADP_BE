﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class DeviceModel : IdentifiableEntityBase
    {
        public string ManufacturerCode { get; set; }
        public string  Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public List<string> IOElementsId { get; set; }

    }
}
