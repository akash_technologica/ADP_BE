﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class DriverVehicleHistory : IdentifiableEntityBase
    {
        public string Registraion { get; set; }
        public int VehicleCode { get; set; }
        public int Trips { get; set; }
        public double DistanceDriven { get; set; }
        public string ScoreId { get; set; }
    }
}
