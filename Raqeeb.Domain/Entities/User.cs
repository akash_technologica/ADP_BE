﻿using MongoDB.Bson.Serialization.Attributes;
using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class User : AuditableEntityBase
    {
        [BsonElement(elementName: "USER_NAME")]
        public string UserName { get; set; }

        [BsonElement(elementName: "EMAIL")]
        public string Email { get; set; }

        [BsonElement(elementName: "MOBILE_NUMBER")]
        public string MobileNumber { get; set; }

        [BsonElement(elementName: "PASSWORD")]
        public string Password { get; set; }

        [BsonElement(elementName: "ENGLISH_NAME")]
        public string EnglishName { get; set; }

        [BsonElement(elementName: "ARABIC_NAME")]
        public string ArabicName { get; set; }

        [BsonElement(elementName: "IS_ACTIVE")]
        public bool IsActive { get; set; }

        [BsonElement(elementName: "ROLES")]
        public List<string> Roles { get; set; }

        [BsonElement(elementName: "LAST_LOGIN_TIME")]
        public DateTime LastLoginTime { get; set; }

        [BsonElement(elementName: "IS_LOGGED_IN")]
        public bool IsLoggedIn { get; set; }

        [BsonElement(elementName: "ACCESS_TOKEN_STAMP")]
        public string AccessTokenStamp { get; set; }

        [BsonElement(elementName: "COMPANY_ID")]
        public string CompanyId { get; set; }
        public List<MenuItem> MenuStatus { get; set; }

        public int GroupId { get; set; }
        public string UserId { get; set; }
    }

    public class MenuItem
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public List<Access> Tests { get; set; }
    }

    public class Access
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Checked { get; set; }
    }
}
