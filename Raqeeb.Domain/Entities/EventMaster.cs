﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
  [Serializable]
  public class EventMaster : IdentifiableEntityBase
  {
    public int Code { get; set; }

    public int? VendorCode { get; set; }

    public int? ContractorCode { get; set; }
   
    public DateTime? ValidForm { get; set; }

    public DateTime? ValidTo { get; set; }

    public string Description { get; set; }

    public int? IsViolation { get; set; }

  }
}
