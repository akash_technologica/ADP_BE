﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class Dashboard : IdentifiableEntityBase 
    {       
        public int MonthName { get; set; }
        public int YearName { get; set; }
        public long TotalTrips { get; set; }
        public long TotalEvents { get; set; }
        public double TotalDistanceDriven { get; set; }
        public string TotalStandingTime { get; set; }
        public string TotalDuration { get; set; }
        public string TotalDrivenTime { get; set; }
        public int TotalFatigue { get; set; }
        public int TotalDistraction { get; set; }
        public int TotalFOV { get; set; }
        public int TotalOverspeeding { get; set; }
        public int TotalHarshAcceleration { get; set; }
        public int TotalForwardCollisionWarning { get; set; }
        public int TotalHarshBraking { get; set; }
        public int TotalLeftLaneDeparture { get; set; }
        public int TotalMobileeyeTamperAlert { get; set; }
        public int TotalNearmiss { get; set; }
        public int TotalOverRevving { get; set; }
        public int TotalPanicButton { get; set; }
        public int TotalPedestrianInDangerZone { get; set; }
        public int TotalPedestrianInForwardCollisionWarning { get; set; }
        public int TotalRightLaneDeparture { get; set; }
        public int TotalTrafficSignRecognition { get; set; }
        public int TotalTrafficSignRecognitionWarning { get; set; }
        public int TotalPowerFailure { get; set; }
    }
}
