﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Entities
{
    [Serializable]
    public class ScoreSetUpMaster : AuditableEntityBase
    {
        public string EventsId { get; set; }
        public bool IsIncludedForScoring { get; set; }
    }
}
