﻿using System;

namespace Raqeeb.Domain
{
    [Serializable]
    public class ApiKeysDto
    {
        public string ApiKey { get; set; }
        public string Secretkey { get; set; }
    }
}
