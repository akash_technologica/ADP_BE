﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class AlarmDto
    {
        public int Id { get; set; }
        public string DeviceId { get; set; }
        public Nullable<System.DateTime> GpsTime { get; set; }
        public Nullable<int> Altitude { get; set; }
        public Nullable<int> Direction { get; set; }
        public Nullable<decimal> Latitude { get; set; }
        public Nullable<decimal> Longitude { get; set; }
        public Nullable<int> Speed { get; set; }
        public Nullable<int> RecordSpeed { get; set; }
        public Nullable<int> State { get; set; }
        public Nullable<System.DateTime> Time { get; set; }
        public Nullable<int> Type { get; set; }
        public string AlarmContent { get; set; }
        public string AlarmType { get; set; }
        public string CmdType { get; set; }
        public string Car_License { get; set; }

    }
    public class AlarmDetails
    {
        public List<AlarmDto> AlarmList { get; set; }
        public int TotalAlarms { get; set; }
        public int Video_Loss { get; set; }
        public int Motion_Detection { get; set; }
        public int Cover { get; set; }
        public int Storage_Exception { get; set; }
        public int Panic_Alarm { get; set; }
        public int Low_Speed { get; set; }
        public int High_Speed { get; set; }
        public int Low_Voltage { get; set; }
        public int ACC { get; set; }
        public int Fence { get; set; }
        public int Illegal_Ignition { get; set; }
        public int Illrgal_Shutdown { get; set; }
        public int Temperature { get; set; }
        public int Collision { get; set; }
        public int Lane_Departure { get; set; }
        public int Abnormal_Temperature_Changes { get; set; }
        public int DSM_Fatigue_Driving { get; set; }
        public int DSM_No_Driver { get; set; }
        public int DSM_Driver_Call_Up { get; set; }
        public int DSM_Driver_Smoking { get; set; }
        public int DSM_Driver_Distraction { get; set; }
        public int DSM_Lane_Departure { get; set; }
        public int DSM_Previous_Car_Collision { get; set; }
        public int Abnormal_Boot_Up { get; set; }
        public int Idle_Switch_Door { get; set; }
        public int DSM_OverSpeed { get; set; }
        public int DSM_License_Plate_Recognition { get; set; }
        public int DSM_Distance_Too_Close { get; set; }
    }
}
