﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class TripOrdinatesDto:List<LatLong>
    {

    }
    public class LatLong : List<LatLongDet> { }
 
    public class LatLongDet
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
