﻿using Raqeeb.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class EventLogDto
    {     
        public string SubEventCode { get; set; }
        public string UpdatedBy { get; set; }
        public string Comments { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ClassificationType { get; set; }
    }
}
