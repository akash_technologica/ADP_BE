﻿using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class EventDto
    {
        public string EventCode { get; set; }

        public DateTime? EventStartDate { get; set; }

        public DateTime? EventEndDate { get; set; }

        public string EventDescription { get; set; }

        public string VehicleRegistration { get; set; }

        public long? EventTypeCode { get; set; }

        public DateTime? TripStartDate { get; set; }

        public DateTime? TripEndDate { get; set; }
        public int? TripDuration { get; set; }

        public int? TripDrivingTime { get; set; }
        public int? TripStandingTime { get; set; }
        public double? TripDistanceDriven { get; set; }
        public double? TripStartOdoMeter { get; set; }
        public double? TripEndOdoMeter { get; set; }

        public int? TripMaxSpeed { get; set; }

        public int? TripMaxPrm { get; set; }

        public string TripStartPositionCode { get; set; }
        public string TripEndPositionCode { get; set; }

        public List<EventMedia> EventMedias { get; set; }

        public LivePosition StartPositionDetails { get; set; }
        public LivePosition EndPositionDetails { get; set; }

        //GUARDIAN
        public double? Duration { get; set; } // (S)
        public string SerialNumber { get; set; }
        public string ContractorName { get; set; }
        public string ConfirmationState { get; set; } //CONFIRMATION
        public DateTime? DetectedDateTime { get; set; } //createTimeUtc - TIME
        public string Alarms { get; set; }
        public string DetectedPositionCode { get; set; } // Location GPS
        public string Shift { get; set; } // SHIFT
        public int? Tracking { get; set; } //TRACKING PERFORM
        public double? TravelDistance { get; set; } // TRAVEL DISTANCE (M)
        public string StationaryTime { get; set; } //STATIONARY TIME (H:M:S)\
        public string EventSource { get; set; }
        public string Bearing { get; set; } // BEARING
        public string TripTime { get; set; } //TRIP TIME(H:M:S)
        public string Travel { get; set; } // TRAVEL
        public long? Age { get; set; } // AGE
        public double? Speed { get; set; } // SPEED
        public string TimeIntoShift { get; set; } //TIME INTO SHIFT
        public float? GPSCoverage { get; set; } //GPS COVERAGE
        public long? originalCode { get; set; }
        //Driver Info
        public string DriverName { get; set; }
        public string DriverMobile { get; set; }
        public List<EventSummaryList> EventSummaryLists { get; set; }
        public bool ShowExamine { get; set; }

        public bool IsClassificationAllowed { get; set; }
        public bool IsReclassificationAllowed { get; set; }
        public bool IsAcknowledged { get; set; }
        public int SuggestedEventTypeCode { get; set; }
        public string SuggestedEventTypeDescription { get; set; }
        public List<Media> Medias { get; set; }
        public List<SubEventTypeMaster> subEventTypes { get; set; }
        public List<SubEventTypeMaster> suggestedSubEventTypes { get; set; }
        public int duration_in_hours { get; set; }
        public int duration_in_minutes { get; set; }
        public int duration_in_seconds { get; set; }
        public string ageAgo { get; set; }
        public string EventStatus { get; set; }
        public string IdString { get; set; }
        public EventLogDto EventLog { get; set; }
        public EventSummary EventSummary { get; set; }
    }

    public class EventSummaryList
    {
        public int EventCount { get; set; }
        public string EventName { get; set; }
        public long? EventTypeCode { get; set; }
       
    }

    public class EventSummary
    {
        public int TotalEvents { get; set; }
        public int TotalDurations { get; set; }
        public int MaxSpeed { get; set; }
        public int MaxRPM { get; set; }

    }

}
