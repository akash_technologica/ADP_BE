﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class MenuMasterDto
    {
        public string MenuName { get; set; }      
        public bool IsActive { get; set; }
        public string RoutingUrl { get; set; }     
        public string MenuID { get; set; }    
        public List<SubMenuDto> SubMenusList { get; set; }
        public bool ViewOnly { get; set; }
        public bool ReadWrite { get; set; }

    }
    public class SubMenuDto
    {
        public string SubMenuName { get; set; }
        public string SubMenuUrl { get; set; }
        public bool ViewOnly { get; set; }
        public bool ReadWrite { get; set; }
        public bool IsActive { get; set; }
    }
}
