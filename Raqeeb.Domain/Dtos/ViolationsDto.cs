﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class ViolationsDto
    {
        public List<int> ViolationCount { get; set; }
        public List<string> MonthList { get; set; }
    }
}
