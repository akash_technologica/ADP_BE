﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class SearchCriteria<T>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; } = 10;
        public string OrderBy { get; set; }
        public bool Desc { get; set; }
        public T Filters { get; set; }
        public int Take { get; set; }
        public int Start { get; set; }
        public int Draw { get; set; }
        public int Group { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Dictionary<string, string> search { get; set; }
        public int First { get; set; }
        public int Rows { get; set; } = 10;
        public string GlobalFilter { get; set; }
        public int ItemsToSkip
        {
            get { return ((PageIndex - 1) * PageSize); }
        }
    }
}
