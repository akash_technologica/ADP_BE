﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class UserDto
    {
        public string UserName { get; set; }
        public string OldPassword { get; set; }
        public string Id { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string Password { get; set; }
        public string EnglishName { get; set; }
        public string ArabicName { get; set; }
        public bool IsActive { get; set; }
        public List<string> Roles { get; set; }
        public string RoleName { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public bool IsLoggedIn { get; set; }
        public string AccessTokenStamp { get; set; }
        public string GroupName { get; set; }
        public string CompanyId { get; set; }
        public List<MenuItem> MenuStatus { get; set; }
        public int GroupId { get; set; }
        public string UserId { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public DateTime CreationDate { get; set; }
        public int RoleId { get; set; }
        public string DeletedBy { get; set; }
    }
    public class MenuItem
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public List<Access> Tests { get; set; }
    }

    public class Access
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public bool Checked { get; set; }
    }

    public class UserModel
    {
        public List<UserDto> UserList { get; set; }
        public int ActiveUsers { get; set; }
        public int InactiveUsers { get; set; }
        public int ActiveContacts { get; set; }
        public int InactiveContacts { get; set; }
        public int ContactsLoggedInToday { get; set; }
    }
}
