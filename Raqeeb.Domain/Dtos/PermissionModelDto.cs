﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class PermissionModelDto
    {
        public string SubMenuId { get; set; }
        public string SubMenuName { get; set; }
        public bool ViewOnly { get; set; }
        public bool ReadWrite { get; set; }
    }
}
