﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class SM_Sensor_RAW
    {
        public class AdaptersItem
        {

            /// <summary>
            /// 
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string description { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string ip4Address { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string ip6Address { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string macAddress { get; set; }
        }

        public class Heartbeat
        {

            /// <summary>
            /// 
            /// </summary>
            public string authenticationFailure { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string bootloaderFirmwareFailure { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string buzzerFault { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string cameraDetected { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string cameraFault { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int clockDrift { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string cmosBatteryFault { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string configurationFilesOk { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string cpuLoadOk { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double cpuPercent { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string errorFlags { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string eventFlags { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string exceptionInProgress { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string ffcFault { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string ffcNoData { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string ffcNotDetected { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string gpsDetected { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string gpsDetectedButNoSignal { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int gpsPercent { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string ipAddress { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string irPodsInactive { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int logDisk { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string logDiskDetected { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string macAddress { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string maxRebootsExceeded { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string overTemperature { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string psuFault { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string simError { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string speakerFault { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int systemDisk { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string systemDiskDetected { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string systemTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int temperature { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int tracking { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string unverifiedSystemTime { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string vibrationMotorFault { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string vibrationMotorOverheat { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string warnings { get; set; }
        }

        public class Location
        {

            /// <summary>
            /// 
            /// </summary>
            public int altitude { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double course { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string gpsTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double latitude { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double longitude { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double speedMps { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string suppressed { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string valid { get; set; }
        }

        public class Fleet
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string account { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int accountId { get; set; }
        }

        public class Vehicle
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Fleet fleet { get; set; }
        }

        public class VehicleInstallation
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string installationUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Vehicle vehicle { get; set; }
        }

        public class SM_Data
        {

            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public List<AdaptersItem> adapters { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string carrierBoardVersion { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string contactTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string createTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Heartbeat heartbeat { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Location location { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string modifiedTimeUtc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string operationalState { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string productVersion { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string provisioningState { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string serialNumber { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string softwareVersion { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string uuid { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public VehicleInstallation vehicleInstallation { get; set; }
        }


    }
}
