﻿using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class VehicleDto
    {
        public int? Code { get; set; }
        public DateTime UpdateDate { get; set; }
        public double? LastOdoMeter { get; set; }
        public string Registration { get; set; }
        public string CarLicense { get; set; }
        public DateTime LocationTIme { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public List<Event> Events { get; set; }
        public long TotalVehicleEvents { get; set; }
        public int? Speed { get; set; }
        public int? Prm { get; set; }
        public string CurrentEvents { get; set; }
        public Boolean? Reporting { get; set; }
        public bool? EngineOn { get; set; }
        public Boolean? Idle { get; set; }
        public int? Counter { get; set; }
        public string FMS_Serial_Number { get; set; }
        public string SM_Serial_Number { get; set; }
        public string VehicleStatus { get; set; }
        public int EventCount { get; set; }
        public int VehicleState { get; set; }
        public string DriverName { get; set; }
        public bool IsGeofenceEnabled { get; set; }
        public string ChasisNumber { get; set; }
        public string DeviceId { get; set; }
        public bool? Online { get; set; }
        public string Sim_IP { get; set; }
        public string PlateCode { get; set; }
        public double? PlateNumber { get; set; }
        public DateTime? InstallationDate { get; set; }
        public string Callibration { get; set; }
        public string GroupName { get; set; }
    }
    public class VehicleSummary
    {
        public List<VehicleDto> Vehicles { get; set; }
        public int Moving { get; set; }
        public int Stopped { get; set; }
        public int Idle { get; set; }
        public int TotalVehicles { get; set; }
        public int NotReporting { get; set; }
    }
   
}
