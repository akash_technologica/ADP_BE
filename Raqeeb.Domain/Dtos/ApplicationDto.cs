﻿using MongoDB.Bson.Serialization.Attributes;
using Raqeeb.Framework.Core;
using System;

namespace Raqeeb.Domain
{
    [Serializable]
    public class ApplicationDto : IdentifiableEntityBase
    {
        [BsonElement(elementName: "ARABIC_NAME")]
        public string ArabicName { get; set; }

        [BsonElement(elementName: "ENGLISH_NAME")]
        public string EnglishName { get; set; }

        [BsonElement(elementName: "SUBSCRIPTION_START_DATE")]
        public DateTime? SubscriptionStartDate { get; set; }

        [BsonElement(elementName: "SUBSCRIPTION_END_DATE")]
        public DateTime? SubscriptionEndDate { get; set; }

        [BsonElement(elementName: "STATUS")]
        public bool Status { get; set; }

        [BsonElement(elementName: "USER_PHOTO")]
        public byte[] UserPhoto { get; set; }

        [BsonElement(elementName: "USER_NAME")]
        public string UserName { get; set; }

        [BsonElement(elementName: "EMAIL_ADDRESS")]
        public string EmailAddress { get; set; }

        [BsonElement(elementName: "DEFAULT_URL")]
        public string DefaultUrl { get; set; }

        [BsonElement(elementName: "MOBILE_NUMBER")]
        public string MobileNumber { get; set; }
      
    }
}
