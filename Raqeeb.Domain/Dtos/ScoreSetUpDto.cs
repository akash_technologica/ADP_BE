﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class ScoreSetUpDto
    {
        public string IdString { get; set; }
        public string EventsId { get; set; }
        public string EventTypeName { get; set; }
        public bool IsIncludedForScoring { get; set; }
    }
}
