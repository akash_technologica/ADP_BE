﻿using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class TripDto
    {
        public DateTime? StartDate { get; set; }

        public long? TripCode { get; set; }

        public DateTime? EndDate { get; set; }

        public string StartPositionCode { get; set; }
        public string EndPositionCode { get; set; }

        public string VehicleRegistration { get; set; }
        public long? EventTripCode { get; set; }
        public int? VehicleCode { get; set; }

        public long TotalVehicleEventsForTrip { get; set; }

        public int? TripDuration { get; set; }

        public int? TripDrivingTime { get; set; }
        public int? TripStandingTime { get; set; }
        public double? TripDistanceDriven { get; set; }
        public double? TripStartOdoMeter { get; set; }
        public double? TripEndOdoMeter { get; set; }

        public int? TripMaxSpeed { get; set; }

        public int? TripMaxPrm { get; set; }

        public List<EventDto> Events { get; set; }

        public LivePosition StartPositionDetails { get; set; }
        public LivePosition EndPositionDetails { get; set; }

        public List<SubTrip> SubTrips { get; set; }

        public int TripEventType { get; set; }
        public string DriverName { get; set; }
        public string durationForTrip { get; set; }
        public string durationForDriving { get; set; }
        public string TimeForStanding { get; set; }
        public string StartLocation { get; set; }
        public string EndLocation { get; set; }
    }
}
