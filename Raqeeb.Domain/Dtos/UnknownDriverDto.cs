﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class UnknownDriverDto
    {
        public DateTime LogDate { get; set; }
        public string DriverCode { get; set; }
        public int VehicleId { get; set; }
        public string Registration { get; set; }
        public Double Lat { get; set; }
        public Double Long { get; set; }
        public string _id { get; set; }
    }
}
