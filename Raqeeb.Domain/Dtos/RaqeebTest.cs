﻿using Raqeeb.Framework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    [Serializable]
    public class RaqeebTest : IdentifiableEntityBase
    {
        public DateTime ApplicationDate { get; set; }
    }
}
