﻿using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class DashboardDto
    {
        public List<string> MonthsList { get; set; }
        public List<long> ViolationsCountList { get; set; }
        public List<string> ViolationList { get; set; }
        public List<long> ViolationsCategoryCountList { get; set; }
        public List<Domain.DataModel.Dashboard> DashboardDataList { get; set; }     
        public List<Domain.DataModel.Vehicle> VehicleList { get; set; }
        public int VehicleCount { get; set; }
        public int OnlineCount { get; set; }
        public int OfflineCount { get; set; }
        public int ViolationCount { get; set; }
        public int ProcessedViolationCount { get; set; }
        public int UnProcessedViolationCount { get; set; }
        public int ValidatedViolationCount { get; set; }
        public int TotalDeviceCount { get; set; }
        public int OnlineDeviceCount { get; set; }
        public int OfflineDeviceCount { get; set; }
        public int NotReportingDeviceCount { get; set; }
        public int IdleVehicleCount { get; set; }
        public int MovingVehicles { get; set; }
        public int StoppedVehicles { get; set; }
        public List<int> Alarms { get; set; }
    }
}
