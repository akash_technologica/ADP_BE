﻿using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class DashboardEventDtos
    {
        public int TotalEvents { get; set; }
        public int Fatigue { get; set; }
        public int Distraction { get; set; }
        public int Fov { get; set; }
        public int OverSpeed { get; set; }
        public int HarshBrake { get; set; }
        public int HarshAcceleration { get; set; }
        public int OverRevving { get; set; }
        public int ForwardCollisionWarning { get; set; }
        public int PedestrianInDangerZone { get; set; }
        public int PedestrianInForwardCollisionWarning { get; set; }
        public int LeftLaneDeparture { get; set; }
        public int Mobileeye { get; set; }
        public int RightLaneDeparture { get; set; }
        public List<Event> EventList { get; set; }
    }
}
