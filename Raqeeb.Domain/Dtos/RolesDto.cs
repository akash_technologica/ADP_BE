﻿using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class RolesDto
    {       
        public string Role { get; set; }     
        public bool IsActive { get; set; }
        public string Id { get; set; }
        public string IdString { get; set; }
        public List<MenuMaster> Menus { get; set; }
    }
}
