﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class DeviceLogViewModel
    {
        public int Id { get; set; }
        public string CarLicense { get; set; }
        public string DeviceId { get; set; }
        public DateTime LogDate { get; set; }
        public bool Online { get; set; }
    }
}
