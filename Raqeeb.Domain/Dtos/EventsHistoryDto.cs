﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class EventsHistoryDto
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string EventDescription { get; set; }

    }
}
