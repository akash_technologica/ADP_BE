﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class MediaDownloadRequestDto
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Status { get; set; }
        public DateTime RequestDate { get; set; }
        public string FilePath { get; set; }
        public string UserId { get; set; }
        public string CarLicense { get; set; }
    }
    public class MediaRequestDownloadDetails
    {
        public List<MediaDownloadRequestDto> MediaDownloadRequestList { get; set; }
        public int TotalRequests { get; set; }
        public int TotalWaiting { get; set; }
        public int TotalFailed { get; set; }
        public int TotalSuccess { get; set; }

    }
}
