﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Dtos
{
    public class DriverProfileDto
    {
        public int Code { get; set; }

        public int? VendorCode { get; set; }

        public int ContractorCode { get; set; }

        public string Name { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string Mobile { get; set; }

        public string ADSDNumber { get; set; }

        public DateTime? LastUpdated { get; set; }

        public DateTime? LicenseIssueDate { get; set; }

        public DateTime? LicenseExpiryDate { get; set; }

        public string Email { get; set; }

        public int Score { get; set; }

        public int FatigueScore { get; set; }

        public int FOVScore { get; set; }

        public int DistractionScore { get; set; }

        public int ForwardCollisionWarningScore { get; set; }

        public int HarshAccelerationScore { get; set; }

        public int PowerFailureScore { get; set; }
        public int HarshBrakingScore { get; set; }

        public int OverSpeedingScore { get; set; }

        public decimal DrivingHours { get; set; }
        public decimal DrivingDistance { get; set; }
        public int TotalTrips { get; set; }

        public int TotalEvents { get; set; }

        public int FatigueCount { get; set; }

        public int FOVCount { get; set; }

        public int DistractionCount { get; set; }

        public int ForwardCollisionWarningCount { get; set; }

        public int HarshAccelerationCount { get; set; }
        public int HarshBrakingCount { get; set; }

        public int OverSpeedingCount { get; set; }

        public int PowerFailureCount { get; set; }


    }
}
