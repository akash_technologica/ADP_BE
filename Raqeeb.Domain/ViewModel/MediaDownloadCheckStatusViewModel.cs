﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
   public class MediaDownloadCheckStatusViewModel
    {
        public List<MediaDownloadCheckStatusData> data { get; set; }
        public int errorcode { get; set; }
    }

    public class MediaDownloadCheckStatusData
    {
        public int state { get; set; }
        public int percent { get; set; }
        public int taskid { get; set; }
    }
}
