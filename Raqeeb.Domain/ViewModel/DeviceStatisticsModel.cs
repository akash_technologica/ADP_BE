﻿using Raqeeb.Domain.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class DeviceStatisticsModel
    {
        public int TotalDevices { get; set; }
        public int TotalOnline { get; set; }
        public int TotalOffline { get; set; }
        public List<Device> DeviceList { get; set; }

    }
}
