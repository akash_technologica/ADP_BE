﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class LivePositionViewModel
    {
        public List<LivePositionModel> data { get; set; }
        public int errorcode { get; set; }
    }

    public class LivePositionModel
    {
        public string terid { get; set; }
        public string gpstime { get; set; }
        public int altitude { get; set; }
        public int direction { get; set; }
        public string gpslat { get; set; }
        public string gpslng { get; set; }
        public int speed { get; set; }
        public int recordspeed { get; set; }
        public int state { get; set; }
        public string time { get; set; }
        public int mileage { get; set; }
    }
}
