﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class DeviceViewModel
    {
        public List<DeviceModel> data { get; set; }
        public int errorcode { get; set; }
    }
    public class DeviceModel
    {      
        public string carlicense { get; set; }     
        public string deviceid { get; set; }
        public string terid { get; set; }
        public string sim { get; set; }
        public int channelcount { get; set; }
        public string cname { get; set; }
        public int platecolor { get; set; }
        public int groupid { get; set; }
        public string devicetype { get; set; }
        public string linktype { get; set; }
        public string deviceusername { get; set; }
        public string devicepassword { get; set; }
        public string registerip { get; set; }
        public int registerport { get; set; }
        public string transmitip { get; set; }
        public int transmitport { get; set; }
        public int en { get; set; }
    }
}
