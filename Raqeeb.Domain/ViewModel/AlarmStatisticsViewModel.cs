﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class AlarmStatisticsViewModel
    {
        public List<AlarmStatistics> data { get; set; }
        public int errorcode { get; set; }
    }
    public class AlarmStatistics
    {
        public string terid { get; set; }     
        public string date { get; set; }
        public int count { get; set; }
    }
}

