﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class MediaRequestDataViewModel
    {
        public MediaRequestDataViewModelData data { get; set; }

        public int errorcode { get; set; }
    }

    public class MediaRequestDataViewModelData
    {
        public int taskid { get; set; }
    }

}
