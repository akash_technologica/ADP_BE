﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class VehicleGroupModel
    {
        public List<VehicleGroup> data { get; set; }
        public int errorcode { get; set; }
    }

    public class VehicleGroup
    {
        public int groupid { get; set; }
        public string groupname { get; set; }
        public int groupfatherid { get; set; }
    }

}
