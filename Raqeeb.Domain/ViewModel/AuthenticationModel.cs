﻿using Raqeeb.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class AuthenticationModel
    {
        public Data data { get; set; }
        public int errorcode { get; set; }
        public string token { get; set; }
    }
    public class Data
    {
        public string key { get; set; }
    }
}
