﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class Item
    {
        public string AlarmID { get; set; }
        public string AlarmName { get; set; }
        public string AlarmTime { get; set; }
        public string C27Spic { get; set; }
        public string C27V { get; set; }
        public string C27Wpic { get; set; }
        public string C28Back { get; set; }
        public string C28Overview { get; set; }
        public string C28OverviewPic { get; set; }
        public string CarLicense { get; set; }
        public string EvidenceID { get; set; }
        public string EvidenceName { get; set; }
        public string EvidenceType { get; set; }
        public ExtraInfo ExtraInfo { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string LegalCode { get; set; }
        public string RadarKind { get; set; }
        public string RoadID { get; set; }
        public string RoadRegion { get; set; }
        public string ViolationDirection { get; set; }
        public string ViolationLane { get; set; }
        public string ViolationTypeID { get; set; }
        public string TG_Distance { get; set; }
        public string TG_Duration { get; set; }
        public string TG_Speed { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
    }
    public class ExtraInfo
    {
        public string City { get; set; }
        public string Type { get; set; }
        public string VN { get; set; }
        public string Category { get; set; }
    }
    public class Debug
    {
        public string Time { get; set; }
        public string debugCode { get; set; }
        public string Message { get; set; }
    }

    public class ValidViolationCSV
    {
        public string UID { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
    }
    public class InValidViolationCSV:ValidViolationCSV
    {
        public string InValidCode { get; set; }
        public string InvalidReason { get; set; }
    }
}
