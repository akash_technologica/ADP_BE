﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class DeviceOnlineOfflineLogViewModel
    {
        public List<DeviceOnlineOfflineLog> data { get; set; }
        public int errorcode { get; set; }
    }
    public class DeviceStateNow
    {
        public string terid { get; set; }
    }

    public class DeviceOnlineOfflineLog: DeviceStateNow
    {       
        public int type { get; set; }
        public string time { get; set; }
    }
}
