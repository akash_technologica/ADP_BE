﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class EventDetailViewModel
    {
        public int EventsId { get; set; }  
        public Nullable<System.DateTime> AlarmTime { get; set; }
        public string C27Spic { get; set; }
        public string C27V { get; set; }
        public string C27Wpic { get; set; }
        public string C28Back { get; set; }
        public string C28Overview { get; set; }
        public string BusId { get; set; }
        public string EvidenceID { get; set; }
        public string EvidenceType { get; set; }
        public Nullable<decimal> Lat { get; set; }
        public Nullable<decimal> Long { get; set; }      
        public string ViolationTypeID { get; set; }
        public string TG_Distance { get; set; }
        public string TG_Duration { get; set; }
        public string TG_Speed { get; set; }
        public string C28OverviewPic { get; set; }
        public Nullable<bool> Processed { get; set; }
        public Nullable<bool> NotProcessed { get; set; }
        public Nullable<bool> UnderProcess { get; set; }
        public string City { get; set; }
        public string Type { get; set; }
        public string VehicleNumber { get; set; }
        public string Category { get; set; }
        public string FilePath { get; set; }
        public string Comments { get; set; }
        public string Debug_Code { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string Street { get; set; }
        public string District { get; set; }
        public Nullable<bool> ProcessedStatus { get; set; }
        public Nullable<bool> ValidationStatus { get; set; }
        public string InValidViolationComments { get; set; }
        public string InValidViolationCode { get; set; }
        public string LegalCode { get; set; }
        public string RoadId { get; set; }
        public string RoadRegion { get; set; }
        public string ViolationDirection { get; set; }
        public string ViolationLane { get; set; }
    }
    public class ViolationSummary
    {
        public List<EventDetailViewModel> ViolationList { get; set; }
        public int TotalViolation { get; set; }
        public int StopArmViolation { get; set; }
        public int SafeZoneAlarm { get; set; }
        public int ValidationStatus { get; set; }
        public int ProcessedStatus { get; set; }
    }
}
