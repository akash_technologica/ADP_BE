﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class BusTreeViewModel
    {
        public string title { get; set; }
        public bool leaf { get; set; }
        public List<ChildrenItem> children { get; set; }

    }
    public class ChildrenItem
    {

        public string title { get; set; }
        public bool leaf { get; set; }
        public List<ChildrenItems> children { get; set; }
        public int groupId { get; set; }
    }
    public class ChildrenItems
    {
        public string title { get; set; }
        public bool leaf { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public bool isActive { get; set; }
        public double checking { get; set; }
        public List<SubChildrenItems> children { get; set; }
    }
    public class SubChildrenItems
    {
        public string title { get; set; }
        public bool leaf { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public bool isActive { get; set; }
        public double checking { get; set; }

    }


    public class TreeRoot
    {
        public List<DataItem> data { get; set; }
    }

    public class DataItem
    {
        public string label { get; set; }
        public string data { get; set; }
        public string expandedIcon { get; set; }
        public string collapsedIcon { get; set; }
        public List<ChildrenTreeItem> children { get; set; }
        public string key { get; set; }
    }

    public class ChildrenTreeLastItem
    {
        public string label { get; set; }
        public string icon { get; set; }
        public string data { get; set; }
        public bool leaf { get; set; }
        public int id { get; set; }
        public bool isActive { get; set; }
        public string FrontCameraUrl { get; set; }
        public string RearCameraUrl { get; set; }
        public string SideCameraUrl { get; set; }
        public string BackCameraUrl { get; set; }
        public bool? Online { get; set; }
        public string key { get; set; }
    }

    public class ChildrenTreeItem
    {
        public string label { get; set; }
        public string data { get; set; }
        public string expandedIcon { get; set; }
        public string collapsedIcon { get; set; }
        public List<ChildrenTreeLastItem> children { get; set; }
        public int groupId { get; set; }
        public bool leaf { get; set; }
        public string key { get; set; }
    }

}
