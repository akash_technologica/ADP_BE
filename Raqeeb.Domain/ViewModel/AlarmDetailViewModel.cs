﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.ViewModel
{
    public class AlarmDetailViewModel
    {
        public List<AlarmDetailModel> data { get; set; }
        public int errorcode { get; set; }
    }
    public class AlarmDetailModel
    {
        public string terid { get; set; }
        public string gpstime { get; set; }
        public int altitude { get; set; }
        public int direction { get; set; }
        public string gpslat { get; set; }
        public string gpslng { get; set; }
        public int speed { get; set; }
        public int recordspeed { get; set; }
        public int state { get; set; }
        public string time { get; set; }
        public int type { get; set; }
        public string content { get; set; } 
        public int cmdtype { get; set; }
    }
}
