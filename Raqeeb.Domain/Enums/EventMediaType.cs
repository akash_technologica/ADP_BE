﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Enums
{
    public enum EventMediaType
    {
        Video = 1,
        Image = 2
    }
}
