﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Enums
{
    public enum DownloadRequestStatus
    {
        Pause = -6,
        ConnectionNumberIsLimited = -5,
        Analysisng = -4,
        UnDone = -3,
        InsufficientSpaceInDrive = -2,
        Waiting = -1,
        ResulationIsComplete = 0,
        Downloading = 1,
        NoneRecordingFile = 2,
        TaskComplete = 3,
        TaskFailed = 4,
        Delete = 5,
        DownloadFailed = 6
    }
}
