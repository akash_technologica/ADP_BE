﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Enums
{
    public enum AlarmCommandType
    {
        alarm = 1,
        release = 2
    }
}
