﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Enums
{
    public enum WebServiceType
    {
        GetContractorsMasterData = 1,
        GetDriversMasterData = 2,
        GetDriversMasterDataExtended = 3,
        GetEventsData = 4,
        GetEventsMasterData = 5,
        GetLiveEventsData = 6,
        GetLiveGPSPositionData = 7,
        GetLivePositionbyDriver = 8,
        GetLivePositionbyVehicle = 9,
        GetSubTripSummaryData = 10,
        GetTripSummaryData = 11,
        GetVehiclesMasterData = 12,
        GetSeeingMachineData = 13,
        GetTripCoOrdinates = 14,
        GetUnknownDriverLog = 15,
        GetSeeingMachineMedia = 16


    }


    public enum WebSrvMode
    {
        Insert = 1,
        Update = 2
    }
}
