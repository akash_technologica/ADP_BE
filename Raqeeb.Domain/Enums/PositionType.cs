﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Enums
{
  public enum PositionType  
  {
    Vehicle = 1,
    Driver = 2,
    GPS=3
  }
}
