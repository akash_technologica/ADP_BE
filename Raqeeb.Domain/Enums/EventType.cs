﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Enums
{
    public enum EventType
    {
        Distraction = 25,
        Driver_Fatigue = 22,
        Forward_Collision_Warning = 14,
        FOV_Exception = 24,
        Harsh_Acceleration = 3,
        Harsh_Braking = 2,
        Left_Lane_Departure = 16,
        Mobileeye_Tamper_Alert = 18,
        Near_Missc = 23,
        Over_Revving = 4,
        Over_Speeding = 1,
        Panic_Button = 13,
        Pedestrian_In_Danger_Zone = 19,
        Pedestrian_In_Forward_Collision_Warning = 20,
        Right_Lane_Departure = 15,
        Traffic_Sign_Recognition = 17,
        Traffic_Sign_Recognition_Warning = 21,
        PowerFailure = 11,
        Geofence_Violation = 5,
        SEEING_MACHINE_OTHER = 98701,
        Harsh_Cornering = 6
    }
}
