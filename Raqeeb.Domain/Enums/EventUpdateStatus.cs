﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Enums
{
    public enum EventUpdateStatus
    {
        Acknowledge = 1,
        Classify = 2,
        Reclassify = 3,
        Rejected = 4

    }
}
