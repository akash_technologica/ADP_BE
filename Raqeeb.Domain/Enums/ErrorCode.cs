﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Enums
{
    public enum ErrorCode
    {
        Success = 200,
        Illegal_request = 201,
        Server_error = 202,
        Permission_denied = 203,
        Authorization_is_invalid = 204,
        Account_has_expired = 205,
        Username_and_password_error = 206,
        The_number_of_exceptions_for_requested_parameter = 207,
        Request_format_is_wrong = 208,
        No_authorization_key_detected = 209,
        Authorize_key_error = 210,
        Database_connection_error = 300,
        Database_operation_exception = 301,
        Interface_parameter_error = 302,
        The_terminal_retrieves_the_video_monthly_calendar_failed = 400,
        Device_is_not_online = 401,
        Device_retrieving_service_busy = 402,
        Device_fail_to_execute = 403,
        Device_fail_to_obtain = 1001
    }
}
