﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.Domain.Enums
{
    public enum ImageCrop
    {
        cropped = 1,
        full_frame = 2
    }
}
