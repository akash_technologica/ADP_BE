﻿using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Net;
using System.Security;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Raqeeb.Apis
{
    public class GlobalExceptionMiddleware : OwinMiddleware
    {
        public GlobalExceptionMiddleware(OwinMiddleware next) : base(next)
        { }

        public override async Task Invoke(IOwinContext context)
        {
            try
            {
                await Next.Invoke(context);
            }
            catch (Exception exception)
            {

                if (exception is SecurityException)
                {
                    //GenerateErrorResult(context, HttpStatusCode.Unauthorized, new FaultException<AmwalServiceError>(new AmwalServiceError((int)AmwalErrorCode.UnhandledException, exception.Message)));
                    return;
                }

                //if (exception is FaultException<AuthorizationValidationException>)
                //{
                //    GenerateErrorResult(context, HttpStatusCode.Unauthorized, new FaultException<AmwalServiceError>(new AmwalServiceError((int)AmwalErrorCode.UnhandledException, exception.Message)));
                //    return;
                //}
                //if (exception is FaultException<AmwalServiceError>)
                //{
                //    GenerateErrorResult(context, HttpStatusCode.InternalServerError, ((FaultException<AmwalServiceError>)exception));
                //    return;
                //}
              
                if (exception is FaultException)
                {
                    //GenerateErrorResult(context, HttpStatusCode.InternalServerError, new FaultException<AmwalServiceError>(new AmwalServiceError((int)AmwalErrorCode.UnhandledException, exception.Message)));
                    return;
                }


                if (exception is UnauthorizedAccessException)
                {
                   // GenerateErrorResult(context, HttpStatusCode.Forbidden, new FaultException<AmwalServiceError>(new AmwalServiceError((int)AmwalErrorCode.UnhandledException, exception.Message)));
                    return;
                }

                if (exception is UnauthorizedAccessException)
                {
                   // GenerateErrorResult(context, HttpStatusCode.Forbidden, new FaultException<AmwalServiceError>(new AmwalServiceError((int)AmwalErrorCode.UnhandledException, exception.Message)));
                    return;
                }

               // GenerateErrorResult(context, HttpStatusCode.InternalServerError, new FaultException<AmwalServiceError>(new AmwalServiceError((int)AmwalErrorCode.UnhandledException, exception.Message)));
            }


        }

        private void GenerateErrorResult(IOwinContext context, HttpStatusCode code, Object obj)
        {
            context.Response.StatusCode = (int)code;
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }));
        }
    }
}