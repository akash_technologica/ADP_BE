﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Raqeeb.Apis.Handlers
{
    public class CustomDateTimeConverter : DateTimeConverterBase
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {

            return reader.Value != null ? DateTime.Parse(reader.Value.ToString()) : reader.Value;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((DateTime)value).ToString("dd/MM/yyyy hh:mm:ss tt"));
        }
    }
}