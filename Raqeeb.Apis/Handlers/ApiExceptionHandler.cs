﻿using Raqeeb.Framework.Core;
using System;
using System.Net;
using System.Net.Http;
using System.Security;
using System.ServiceModel;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;


namespace Raqeeb.Apis.Handlers
{
    public class ApiExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            try
            {
                if (context.Exception is SecurityException)
                {
                    context.Result = new ResponseMessageResult(context.Request.CreateResponse(HttpStatusCode.Unauthorized, new ErrorData(context.Exception.Message)));
                    return;
                }
                if (context.Exception is FaultException<AuthorizationValidationException>)
                {
                    context.Result = new ResponseMessageResult(context.Request.CreateResponse(HttpStatusCode.Unauthorized, new ErrorData(context.Exception.Message)));
                    return;
                }
                if (context.Exception is FaultException)
                {
                    context.Result = new ResponseMessageResult(context.Request.CreateResponse(HttpStatusCode.InternalServerError, new ErrorData(context.Exception.Message)));
                    return;
                }
                if (context.Exception is BussinessException)
                {
                    context.Result = new ResponseMessageResult(context.Request.CreateResponse(HttpStatusCode.InternalServerError, ((BussinessException)context.Exception).Errors));
                    return;
                }
                if (context.Exception is UnauthorizedAccessException)
                {
                    context.Result = new ResponseMessageResult(context.Request.CreateResponse(HttpStatusCode.Forbidden));
                    return;
                }
                if (context.Exception is Exception)
                {
                    context.Result = new ResponseMessageResult(context.Request.CreateResponse(HttpStatusCode.InternalServerError, new ErrorData(context.Exception.Message)));
                    return;
                }
                context.Result = new ResponseMessageResult(context.Request.CreateResponse(HttpStatusCode.InternalServerError, new ErrorData(context.Exception.Message)));

            }
            catch (Exception ex)
            {
                context.Result = new InternalServerErrorResult(context.Request);
            }
        }


        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return true;
        }

    }
}