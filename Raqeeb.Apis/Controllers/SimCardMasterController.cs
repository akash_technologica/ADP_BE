﻿using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Entities;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/simcard")]
    public class SimCardMasterController : ApiController
    {
        #region Register Services

        SimCardManager simManager = new SimCardManager();

        #endregion


        [HttpGet, Route("getall/{code}"), AuthorizeUser(Roles = "Admin")]
        public async Task<IHttpActionResult> GetSimByGSMId(HttpRequestMessage request, [FromUri] string code)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, simManager.GetAll(code)));
        }
        [HttpGet, Route("getsim/{code}"), AuthorizeUser(Roles = "Admin")]
        public async Task<IHttpActionResult> GetSimByEntity(HttpRequestMessage request, [FromUri] string code)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, simManager.GetSim(code)));
        }

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] SimCard_Master sim)
        {
            if (sim.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, simManager.Add(sim)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, simManager.Update(sim)));
            }
        }
    }
}
