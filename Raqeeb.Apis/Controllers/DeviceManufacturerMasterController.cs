﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raqeeb.Manager.Manager;
using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Framework.Core;
using Raqeeb.Domain.Entities;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/deviceManufacturer")]
    public class DeviceManufacturerMasterController : ApiControllerBase
    {
        #region Register Services

        readonly DeviceManufacturerManager manufacturerManager = new DeviceManufacturerManager();

        #endregion

        [HttpGet, Route("getall"), AuthorizeUser(Roles = "Admin,User")]

        public async Task<IHttpActionResult> GetAll(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, manufacturerManager.GetManufacturer()));
        }

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] DeviceManufacturerMaster manufacturer)
        {
            if (manufacturer.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, manufacturerManager.Add(manufacturer)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, manufacturerManager.Update(manufacturer)));
            }
        }
       
    }
}
