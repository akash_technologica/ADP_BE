﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/scoresetup")]
    public class ScoreSetUpController : ApiControllerBase
    {
       
        ScoreSetUpManager scoresetupManager = new ScoreSetUpManager();

        #region Register Services

        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(scoresetupManager);
        }


        [HttpGet, Route("getall"), AuthorizeUser(Roles = "Admin")]

        public IHttpActionResult GetAll(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, scoresetupManager.GetAll());
        }

       
        [HttpGet, Route("add/{scoresetup}")]
        public IHttpActionResult Add(HttpRequestMessage request, [FromUri]ScoreSetUp setup)
        {
            var isExist = scoresetupManager.CheckExisting(setup.EventTypeId);
            if (isExist)
            {
                scoresetupManager.Add(setup);
                return Content(HttpStatusCode.OK, "");
            }
            else
            {
                return Content(HttpStatusCode.NotFound, "Event Type already exits");
            }
        }
        //[HttpPost, Route("addScoreSetup"), Authorize(Roles = "Admin")]
        //public IHttpActionResult AddSetUp(HttpRequestMessage request, [FromBody] ScoreSetUp scoresetup)
        //{
        //    var isExist = scoresetupManager.CheckExisting(scoresetup.EventTypeId);

        //    if (isExist)
        //    {
        //        scoresetupManager.Update(scoresetup);
        //        return Content(HttpStatusCode.OK, "");
              
        //    }
        //    else
        //    {
        //        scoresetupManager.Add(scoresetup);
        //        return Content(HttpStatusCode.OK, "");
        //    }
        //}
        [HttpPost, Route("addScoreSetup"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> AddScoreSetUp(HttpRequestMessage request, [FromBody] List<string> scoresetup)
        {
                    
             return await Task.Run(()=>Ok(scoresetupManager.AddBulk(scoresetup)));
                
        }
        [HttpGet, Route("getScoreSetUp"), AuthorizeUser(Roles = "Admin")]

        public IHttpActionResult GetScoreSetUp(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, scoresetupManager.GetScoreSetUp());
        }
        #endregion
    }
}
