﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/gsm")]
    public class GsmMasterController : ApiControllerBase
    {
        #region Register Services

        GsmMasterManager gsmManager = new GsmMasterManager();

        #endregion

        [HttpGet, Route("getall"), AuthorizeUser(Roles = "Admin")]

        public async Task<IHttpActionResult> GetAll(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, gsmManager.GetAll()));
        }

       
        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] GSM_Master gsm)
        {
            if (gsm.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, gsmManager.Add(gsm)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, gsmManager.Update(gsm)));
            }
        }
    }
}
