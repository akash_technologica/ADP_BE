﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.DataModel;
using Raqeeb.Domain.Entities;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/group")]
    public class GroupMasterController  : ApiControllerBase
    {
        #region Register Services

        readonly GroupManager groupManager = new GroupManager();
        int group;

        #endregion

        public GroupMasterController()
        {
            ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            group = Convert.ToInt32(principal.Claims.FirstOrDefault(c => c.Type == "Group").Value);
        }

        [HttpGet, Route("group-List"), AuthorizeUser(Roles = "Admin,User")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetGroupList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(groupManager.GetGroupList()));
            //if (group == 0)
            //{
            //    return await Task.Run(() => Ok(groupManager.GetGroupList()));
            //}
            //else
            //{
            //    return await Task.Run(() => Ok(groupManager.GetGroupList(group)));
            //}
            
        }

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> AddGroup(HttpRequestMessage request, [FromBody] GroupMaster group)
        {
            if (group.GroupId == 0)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, groupManager.Add(group)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, groupManager.Update(group)));
            }
        }
    }
}
