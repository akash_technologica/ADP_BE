﻿using Raqeeb.Framework.Core;
using Raqeeb.Framework.Core.Data.Entites;
using Raqeeb.Apis.Core;
using Raqeeb.Domain;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Manager;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Raqeeb.Apis.FilterAttributes;

namespace Raqeeb.Apis
{
    [RoutePrefix("api/template")]
    public class TemplateController : ApiControllerBase
    {
        #region Register Managers

        TemplateManager templateManager = new TemplateManager();
        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(templateManager);
        }
        #endregion

        #region Method
        [HttpPost, Route("add"), AuthorizeUser]
        public IHttpActionResult RegisterApplication(HttpRequestMessage request, [FromBody] Contractor template)
        {
            return Ok(templateManager.Add(template));
        }


        [HttpPost, Route("search"), AuthorizeUser]
        public IHttpActionResult Search(HttpRequestMessage request, [FromBody] Paging<TemplateFilter> paging)
        {
            return Ok(templateManager.Search(paging));
        }
        #endregion
    }
}