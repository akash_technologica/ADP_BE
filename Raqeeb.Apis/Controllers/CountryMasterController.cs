﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raqeeb.Manager.Manager;
using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Framework.Core;
using Raqeeb.Domain.Entities;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/country")]
    public class CountryMasterController : ApiControllerBase
    {
        #region Register Services

        readonly CountryMasterManager countryManager = new CountryMasterManager();

        #endregion
        [HttpGet, Route("country-List"), AuthorizeUser(Roles = "Admin")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetCountryList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(countryManager.GetCountryList()));
        }
        

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] CountryMaster country)
        {
            if (country.IdString != null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, countryManager.Add(country)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, countryManager.Update(country)));
            }
        }
       

        [HttpPost, Route("addCity"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> AddCity(HttpRequestMessage request, [FromBody] CityMaster city)
        {
            if (city.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, countryManager.AddCity(city)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, countryManager.UpdateCity(city)));
            }
        }
        [HttpPost, Route("cityList"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> GetCity(HttpRequestMessage request, [FromBody] CountryMaster country)
        {
            return await Task.Run(() => Ok(countryManager.GetCityByCountry(country)));
        }

        [HttpPost, Route("delete"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Delete(HttpRequestMessage request, [FromBody] CountryMaster country)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, countryManager.Delete(country)));
        }
       
    }
}
