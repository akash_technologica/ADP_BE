﻿using Raqeeb.Framework.Core;
using Raqeeb.Apis.Core;
using Raqeeb.Domain;
using Raqeeb.Domain.Entities;
using Raqeeb.Manager;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{

    [RoutePrefix("api/test")]
    public class TestingController : ApiControllerBase
    {
        #region Register Services

        UserManager userManager = new UserManager();
        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(userManager);
        }
        #endregion

        #region Method
        [HttpGet, Route("add-user")]
        public IHttpActionResult RegisterApplication(HttpRequestMessage request)
        {
            Domain.Dtos.UserDto user = new Domain.Dtos.UserDto();
            user.CreationDate = DateTime.Now;
            user.Email = "super.admin@notification.com";
            user.EnglishName = "Notification Super Admin";
            user.ArabicName = "مدير نظام الإشعارات";
            user.MobileNumber = "00971562123212";
            user.UserName= "super.admin@notification.com";
            user.Password =  new Crypto().Encrypt("Test@1234", "o6806642kbM7c5");
            user.IsActive = true;
            user.Roles = new List<string>();
            user.Roles.Add("SuperAdmin");
            return Ok(userManager.Add(user));
        }


        #endregion

    }
}