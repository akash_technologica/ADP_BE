﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Entities;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/geofence")]
    public class GeofenceController :  ApiControllerBase
    {
        #region Register Services

            GeofenceManager geofenceManager = new GeofenceManager();
                      
            protected override void RegisterServices(List<IServiceContract> disposableServices)
            {
                disposableServices.Add(geofenceManager);
            }
        #endregion

        #region Method

        [HttpGet, Route("geofence-List"), AuthorizeUser(Roles = "Admin")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetGeofenceList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(geofenceManager.GetGeofenceList()));
        }

        [HttpPost, Route("add-Geofence")]
        public async Task<IHttpActionResult> AddGeofence(HttpRequestMessage request, [FromBody] Geofence geofence)
        {
            if(geofence.IdString != null)
            {
                return await Task.Run(() => Ok(geofenceManager.UpdateGeofencing(geofence)));
            }
            else
            {
                return await Task.Run(() => Ok(geofenceManager.Insert(geofence)));
            }
            
        }

        [HttpPost, Route("delete-Geofence")]
        public async Task<IHttpActionResult> DeleteGeofence(HttpRequestMessage request, [FromBody] Geofence geofence)
        {
            if (geofence.IdString != null)
            {
                return await Task.Run(() => Ok(geofenceManager.Delete(geofence)));
            }
            else
            {
                return await Task.Run(() => Ok("Error"));
            }

        }
        [HttpPost, Route("assign-vehicles")]
        public async Task<IHttpActionResult> AssignVehicles(HttpRequestMessage request, [FromBody] Geofence geofence)
        {
            if (geofence.IdString != null)
            {
                return await Task.Run(() => Ok(geofenceManager.Update(geofence)));
            }
            else
            {
                return await Task.Run(() => Ok("Error"));
            }

        }
        #endregion
    }

} 
