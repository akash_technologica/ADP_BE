﻿using Raqeeb.Framework.Core;
using Raqeeb.Apis.Core;
using Raqeeb.Domain;
using Raqeeb.Manager;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Raqeeb.Domain.Entities;
using Raqeeb.Apis.FilterAttributes;
using System.Net;
using System.Threading.Tasks;
using Raqeeb.Domain.Dtos;

namespace Raqeeb.Apis
{
    [RoutePrefix("api/user")]
    public class UserController : ApiControllerBase
    {
        #region Register Services

        UserManager userManager = new UserManager();
        RolesManager rolesManager = new RolesManager();
        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(userManager);
        }

        #endregion
        [HttpPost]
        [Route("update"), AuthorizeUser]
        public async Task<IHttpActionResult> Update(HttpRequestMessage request, [FromBody] User user)
        {
            return await Task.Run(() => Ok(userManager.Update(user)));  // done
        }

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] UserDto user)
        {
            if (user.Id != null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, userManager.UpdateUserData(user)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, userManager.Add(user)));
            }
        }

        [HttpPost, Route("delete"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Delete(HttpRequestMessage request, [FromBody] UserDto user)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, userManager.Delete(user)));
        }

        [HttpPost, Route("inactive"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Inactive(HttpRequestMessage request, [FromBody] User user)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, userManager.Inactive(user)));
        }

        [HttpGet, Route("getall"), AuthorizeUser(Roles = "Admin")]
        public async Task<IHttpActionResult> GetAll(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, userManager.GetAll()));
        }

        [HttpPost]
        [Route("checkUserExist")]
        public async Task<IHttpActionResult> CheckUserExist(HttpRequestMessage request, [FromBody] User user)
        {
            return await Task.Run(() => Ok(userManager.CheckUserExist(user)));
        }

        [HttpPost]
        [Route("send-confirmation-email")]
        public async Task<IHttpActionResult> SendConfirmationEmail(HttpRequestMessage request, [FromBody] User user)
        {
            return await Task.Run(() => Ok(userManager.SendConfirmationEmail(user)));
        }

        [HttpGet, Route("validate-confirmation-token/{token}")]
        public async Task<IHttpActionResult> ValidateConfirmationToken(HttpRequestMessage request, string token)
        {
            return await Task.Run(() => Ok(userManager.ValidateConfirmationToken(token)));
        }

        [HttpGet, Route("update-password/{newPassword}/{token}")]
        public async Task<IHttpActionResult> updatePassword(HttpRequestMessage request, string newPassword, string token)
        {
            return await Task.Run(() => Ok(userManager.updatePassword(newPassword, token)));
        }
        [HttpPost, Route("changePassword")]
        public async Task<IHttpActionResult> ChangePassword(HttpRequestMessage request,UserDto user)
        {
            return await Task.Run(() => Ok(userManager.ChangePassword(user)));
        }
        [HttpPost, Route("get-menus")]
        public async Task<IHttpActionResult> GetMenuByUser_Role(HttpRequestMessage request, [FromBody] User user)
        {
            return await Task.Run(() => Ok(rolesManager.GetMenuByUser_Role(user.UserName)));
        }
        [HttpGet, Route("get-submenu-by-user/{id}"), AuthorizeUser(Roles = "Admin")]
        public async Task<IHttpActionResult> GetSubMenuByCode(HttpRequestMessage request, [FromUri] string id)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, userManager.GetPermissionById(id)));
        }
        [HttpPost, Route("add-permission"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> AddPermissions(HttpRequestMessage request, [FromBody] UserPermissions userPermissions)
        {
            if (userPermissions.IdString != null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, userManager.UpdateUserPermissions(userPermissions)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, userManager.AddPermissions(userPermissions)));
            }
        }
    }
}