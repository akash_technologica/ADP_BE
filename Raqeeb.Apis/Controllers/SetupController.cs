﻿using Raqeeb.Apis.Core;
using Raqeeb.Domain.Entities;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/setup")]
    public class SetupController : ApiControllerBase
    {
        #region Register Services

        SetupManager setupManager = new SetupManager();

        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(setupManager);
        }


        [HttpGet, Route("get-all"), Authorize(Roles = "Admin")]
        // first part of Dashboard
        public IHttpActionResult GetAll(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, setupManager.GetAll());
        }

        [HttpGet, Route("add/{classificationType}")]
        public IHttpActionResult Add(HttpRequestMessage request , [FromUri]int classificationType)
        {
            setupManager.Add( classificationType);
            return Content(HttpStatusCode.OK, "");
        }


        [HttpPost, Route("update"), Authorize(Roles = "Admin")]
        public IHttpActionResult Update(HttpRequestMessage request, [FromBody] Setup setup)
        {
            setupManager.Update(setup);
            return Content(HttpStatusCode.OK, "");
        }

        [HttpGet, Route("get-by-classification-type/{classificationType}"), Authorize(Roles = "Admin")]
        // first part of Dashboard
        public IHttpActionResult GetByClassificationEventTyp(HttpRequestMessage request , int classificationType)
        {
            return Content(HttpStatusCode.OK, setupManager.GetByClassificationEventTyp(classificationType));
        }
        

        #endregion
    }
}