﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Entities;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/vehicleBrand")]
    public class VehicleBrandMasterController : ApiControllerBase
    {
        #region Register Services

        readonly VehicleBrandMasterManager vehicleBrandMasterManager = new VehicleBrandMasterManager();

        #endregion

        [HttpGet, Route("getall"), AuthorizeUser(Roles = "SuperUser,Admin,User")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetCountryList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(vehicleBrandMasterManager.GetAll()));
        }


        [HttpPost, Route("add"), Authorize(Roles = "SuperUser,User")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] VehicleBrandMaster brand)
        {
            if (brand.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, vehicleBrandMasterManager.Add(brand)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, vehicleBrandMasterManager.Update(brand)));
            }
        }


        [HttpGet, Route("brand-List"), AuthorizeUser(Roles = "SuperUser,Admin,User")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetGroupList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(vehicleBrandMasterManager.GetBrandList()));
        }


    }
}
