﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Common;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;



namespace Raqeeb.Apis.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/event")]
    public class EventController : ApiControllerBase
    {
        #region Register Services

        EventManager eventManager = new EventManager();
        EventMasterManager eventMasterManager = new EventMasterManager();
        DebugCodeMasterManager debugCodeMasterManager = new DebugCodeMasterManager();
        int group;
        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(eventManager);
            disposableServices.Add(eventMasterManager);


        }
        #endregion

        public EventController()
        {
            ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            group = Convert.ToInt32(principal.Claims.FirstOrDefault(c => c.Type == "Group").Value);
        }

        [HttpPost, Route("get-events-dashboard"), AuthorizeUser(Roles = "Admin,User")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetEventsDashboard(HttpRequestMessage request, [FromBody] SearchCriteria<EventFilter> searchCriteria)
        {
            searchCriteria.Filters.Shift = Utility.ShiftFromDateTime();
            if (searchCriteria.Filters.Shift == "Night")
            {
                searchCriteria.Filters.StartDate = DateTime.Now.Date.AddHours(18);
                searchCriteria.Filters.EndDate = DateTime.Now.AddDays(1).Date.AddHours(6);
            }
            else
            {
                searchCriteria.Filters.StartDate = DateTime.Now.Date.AddHours(6);
                searchCriteria.Filters.EndDate = DateTime.Now.Date.AddHours(18);
            }
            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.GetEventsDashboard(searchCriteria)));
        }


        [HttpPost, Route("search"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> Search(HttpRequestMessage request, [FromBody] SearchCriteria<EventFilter> searchCriteria)
        {



            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.Search(searchCriteria)));
        }
        [HttpPost, Route("searchEventPage"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> SearchEventPage(HttpRequestMessage request, [FromBody] SearchCriteria<EventFilter> searchCriteria)
        {

            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.SearchEventPage(searchCriteria)));
        }


        [HttpGet, Route("get-event-details-by-code/{code}"), AuthorizeUser(Roles = "Admin,User")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetEventDetailsByCode(HttpRequestMessage request, [FromUri] string code)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.GetEventDetailsByCode(code)));
        }

        [HttpPost, Route("get-event-details-by-vehicle"), AuthorizeUser(Roles = "Admin,User")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetEventDetailsByVehicle(HttpRequestMessage request, [FromBody] SearchCriteria<EventFilter> searchCriteria)
        {
            // searchCriteria.Filters.StartDate = DateTime.Now.Date;post-Accept
            searchCriteria.Filters.Shift = Utility.ShiftFromDateTime();
            if (searchCriteria.Filters.Shift == "Night")
            {
                searchCriteria.Filters.StartDate = DateTime.Now.Date.AddHours(18);
                searchCriteria.Filters.EndDate = DateTime.Now.AddDays(1).Date.AddHours(6);
            }
            else
            {
                searchCriteria.Filters.StartDate = DateTime.Now.Date.AddHours(6);
                searchCriteria.Filters.EndDate = DateTime.Now.Date.AddHours(18);
            }

            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.GetEventDetailsByVehicle(searchCriteria)));
        }

        [HttpGet, Route("get-event-types"), AuthorizeUser(Roles = "Admin,User")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetEventTypes(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, eventMasterManager.GetAll()));
        }

        [HttpPost, Route("post-Accept"), AuthorizeUser(Roles = "Admin,User")]
        // first part of Dashboardpost-Reclassification
        public async Task<IHttpActionResult> PostAcceptEvent(HttpRequestMessage request, Reclassification data)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.PostAcceptEvent(data)));
        }

        [HttpPost, Route("post-Classification-Reclassification"), AuthorizeUser(Roles = "Admin,Monitoring,User")]
        // first part of Dashboardpost-Reclassification
        public IHttpActionResult PostReclassificationEvent(HttpRequestMessage request, Reclassification data)
        {
            return Content(HttpStatusCode.OK, eventManager.PostReclassificationEvent(data));
        }

        [HttpPost, Route("post-Rejection"), AuthorizeUser(Roles = "Admin,Monitoring,User")]
        // first part of Dashboardpost-Reclassification
        public async Task<IHttpActionResult> PostRejectEvent(HttpRequestMessage request, Reclassification data)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.PostRejectEvent(data)));
        }

        [HttpGet, Route("get-event-types-by-code/{eventTypeCode}"), AuthorizeUser(Roles = "Admin,Monitoring,User")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetEventTypes(HttpRequestMessage request, [FromUri] int eventTypeCode)
        {
            var eventList = eventMasterManager.GetAll();


            List<EventMaster> eventListWithoutCurrentEvent = new List<EventMaster>();
            foreach (var item in eventList)
            {
                if (item.Code != eventTypeCode)
                {
                    var eventListNew = new EventMaster()
                    {
                        Code = item.Code,
                        ContractorCode = item.ContractorCode,
                        Description = item.Description,

                    };
                    eventListWithoutCurrentEvent.Add(eventListNew);

                }
            }


            return await Task.Run(() => Content(HttpStatusCode.OK, eventListWithoutCurrentEvent));
        }

        [HttpGet, Route("get-sub-event-types-by-code/{eventTypeCode}"), AuthorizeUser(Roles = "Admin,Monitoring,User")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetSubEventTypes(HttpRequestMessage request, [FromUri] int eventTypeCode)
        {
            var eventList = eventMasterManager.GetSubEventByEventCode(eventTypeCode);


            //List<EventMaster> eventListWithoutCurrentEvent = new List<EventMaster>();
            //foreach (var item in eventList)
            //{
            //    if (item.Code != eventTypeCode)
            //    {
            //        var eventListNew = new EventMaster()
            //        {
            //            Code = item.Code,
            //            ContractorCode = item.ContractorCode,
            //            Description = item.Description,

            //        };
            //        eventListWithoutCurrentEvent.Add(eventListNew);

            //    }
            //}


            return await Task.Run(() => Content(HttpStatusCode.OK, eventList));
        }
        [HttpGet, Route("get-debug-types"), AuthorizeUser(Roles = "Admin,User")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetDebugTypes(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, debugCodeMasterManager.GetAll()));
        }

        [HttpGet, Route("download-violation/{evidenceID}"), AuthorizeUser(Roles = "Admin,User")]
        // first part of Dashboard
        public async Task<IHttpActionResult> DownloadFiles(HttpRequestMessage request, [FromUri] string evidenceID)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            //using (ZipFile zip = new ZipFile())
            //{
            //    //this code takes an array of documents' paths and Zip them
            //    zip.AddFiles(docs, false, "");
            //    return ZipContentResult(zip);
            //}
          //  MemoryStream memoryStream = eventManager.DownloadFilesAsync(evidenceID);
          //  return new eBookResult(memoryStream, Request, evidenceID);
            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.DownloadFilesAsync(evidenceID)));           

        }
        


    }

    public class eBookResult : IHttpActionResult
    {
        MemoryStream bookStuff;
        string PdfFileName;
        HttpRequestMessage httpRequestMessage;
        HttpResponseMessage httpResponseMessage;
        public eBookResult(MemoryStream data, HttpRequestMessage request, string filename)
        {
            bookStuff = data;
            httpRequestMessage = request;
            PdfFileName = filename +".zip";
        }
        public System.Threading.Tasks.Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            httpResponseMessage = httpRequestMessage.CreateResponse(HttpStatusCode.OK);
            httpResponseMessage.Content = new StreamContent(bookStuff);
            //httpResponseMessage.Content = new ByteArrayContent(bookStuff.ToArray());  
            httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            httpResponseMessage.Content.Headers.ContentDisposition.FileName = PdfFileName;
            httpResponseMessage.Content.Headers.ContentLength = bookStuff.ToArray().LongLength;
            httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/zip");
            
            return System.Threading.Tasks.Task.FromResult(httpResponseMessage);
        }
    }
}