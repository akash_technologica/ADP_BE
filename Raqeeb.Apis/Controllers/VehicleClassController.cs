﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Entities;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/vehicleClass")]
    public class VehicleClassController : ApiControllerBase
    {
        #region Register Services

        VehicleClassManager vehicleClassManager = new VehicleClassManager();

        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(vehicleClassManager);
        }
        #endregion

        #region Method

        [HttpGet, Route("vehicleClass-List"), AuthorizeUser(Roles = "Admin,User")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetVehicleClassList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(vehicleClassManager.GetVehicleClassList()));
        }

        [HttpPost, Route("add-vehicleClass")]
        public async Task<IHttpActionResult> AddVehicleClass(HttpRequestMessage request, [FromBody] VehicleClass VehicleClass)
        {
            if (VehicleClass.IdString != null)
            {
                return await Task.Run(() => Ok(vehicleClassManager.UpdateVehicleClass(VehicleClass)));
            }
            else
            {
                return await Task.Run(() => Ok(vehicleClassManager.Insert(VehicleClass)));
            }

        }

        [HttpPost, Route("delete-vehicleClass")]
        public async Task<IHttpActionResult> DeleteVehicleClass(HttpRequestMessage request, [FromBody] VehicleClass vehicleClass)
        {
            if (vehicleClass.IdString != null)
            {
                return await Task.Run(() => Ok(vehicleClassManager.Delete(vehicleClass)));
            }
            else
            {
                return await Task.Run(() => Ok("Error"));
            }

        }
        #endregion

    }
}
