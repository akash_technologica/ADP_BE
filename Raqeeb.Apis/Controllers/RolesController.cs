﻿using Raqeeb.Framework.Core;
using Raqeeb.Apis.Core;
using Raqeeb.Domain;
using Raqeeb.Manager;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Raqeeb.Domain.Entities;
using Raqeeb.Apis.FilterAttributes;
using System;
using System.Threading.Tasks;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/roles")]
    public class RolesController : ApiController
    {
        #region Register Services

        RolesManager rolesManager = new RolesManager();

        //protected override void RegisterServices(List<IServiceContract> disposableServices)
        //{
        //    disposableServices.Add(rolesManager);
        //}
        #endregion

        [HttpGet, Route("getall"), AuthorizeUser(Roles = "Admin")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetAll(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, rolesManager.GetAll()));
        }

        [HttpGet, Route("get-roles-type"), AuthorizeUser(Roles = "Admin")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetRolesType(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, rolesManager.GetRolesType()));
        }

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] Roles role)
        {
            if (role.IdString != null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, rolesManager.Update(role)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, rolesManager.Add(role)));
            }
        }

        [HttpPost, Route("delete"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Delete(HttpRequestMessage request, [FromBody] Roles role)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, rolesManager.Delete(role)));
        }

        [HttpPost, Route("inactive"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Inactive(HttpRequestMessage request, [FromBody] Roles role)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, rolesManager.Inactive(role)));
        }
    }
}
