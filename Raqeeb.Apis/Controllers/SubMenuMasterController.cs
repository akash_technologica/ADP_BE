﻿using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Entities;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/submenu")]
    public class SubMenuMasterController : ApiController
    {
        #region Register Services

        SubMenuMasterManager subMenuManager = new SubMenuMasterManager();

        //protected override void RegisterServices(List<IServiceContract> disposableServices)
        //{
        //    disposableServices.Add(rolesManager);
        //}
        #endregion

        [HttpGet, Route("getall"), AuthorizeUser(Roles = "Admin")]
        // first part of Dashboard
        public IHttpActionResult GetAll(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, subMenuManager.GetAll());
        }

        [HttpGet, Route("get-sub-menu-type"), AuthorizeUser(Roles = "Admin")]
        // first part of Dashboard
        public IHttpActionResult GetRolesType(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, subMenuManager.GetMenuType());
        }

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public IHttpActionResult Add(HttpRequestMessage request, [FromBody] SubMenuMaster submenu)
        {
            if (submenu.IdString != null)
            {
                return Content(HttpStatusCode.OK, subMenuManager.Update(submenu));
            }
            else
            {
                return Content(HttpStatusCode.OK, subMenuManager.Add(submenu));
            }
        }

        [HttpPost, Route("delete"), Authorize(Roles = "Admin")]
        public IHttpActionResult Delete(HttpRequestMessage request, [FromBody] SubMenuMaster submenu)
        {
            return Content(HttpStatusCode.OK, subMenuManager.Delete(submenu));
        }

        [HttpPost, Route("inactive"), Authorize(Roles = "Admin")]
        public IHttpActionResult Inactive(HttpRequestMessage request, [FromBody] SubMenuMaster submenu)
        {
            return Content(HttpStatusCode.OK, subMenuManager.Inactive(submenu));
        }

        [HttpGet, Route("get-submenu-by-code/{id}"), AuthorizeUser(Roles = "Admin")]
        public IHttpActionResult GetByCode(HttpRequestMessage request, [FromUri] string id)
        {
            return Content(HttpStatusCode.OK, subMenuManager.GetById(id));
        }
       
    }
}
