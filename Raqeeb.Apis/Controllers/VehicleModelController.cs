﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Entities;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/vehicleModel")]
    public class VehicleModelController : ApiControllerBase
    {
        #region Register Services

        VehicleModelManager vehicleModelManager = new VehicleModelManager();

        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(vehicleModelManager);
        }
        #endregion

        #region Method

        [HttpGet, Route("vehicleModel-List"), AuthorizeUser(Roles = "Admin,User")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetVehicleModelList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(vehicleModelManager.GetvehicleModelList()));
        }
        [HttpGet, Route("get-model-by-brand/{code}"), AuthorizeUser(Roles = "SuperUser,Admin,User")]
        public async Task<IHttpActionResult> GetModelByManufacturerId(HttpRequestMessage request, [FromUri] string code)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, vehicleModelManager.GetModelByBrand(code)));
        }

        [HttpPost, Route("add-vehicleModel")]
        public async Task<IHttpActionResult> AddVehicleModel(HttpRequestMessage request, [FromBody] VehicleModel vehicleModel)
        {
            if (vehicleModel.IdString != null)
            {
                return await Task.Run(() => Ok(vehicleModelManager.UpdateVehicleModel(vehicleModel)));
            }
            else
            {
                return await Task.Run(() => Ok(vehicleModelManager.Insert(vehicleModel)));
            }

        }

        [HttpPost, Route("delete-vehicleModel")]
        public async Task<IHttpActionResult> DeleteVehicleModel(HttpRequestMessage request, [FromBody] VehicleModel vehicleModel)
        {
            if (vehicleModel.IdString != null)
            {
                return await Task.Run(() => Ok(vehicleModelManager.Delete(vehicleModel)));
            }
            else
            {
                return await Task.Run(() => Ok("Error"));
            }

        }
        #endregion
    }
}
