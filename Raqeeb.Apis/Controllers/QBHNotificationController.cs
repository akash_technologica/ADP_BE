﻿using Raqeeb.Framework.Core;
using Raqeeb.Apis.Core;
using Raqeeb.Domain;
using Raqeeb.Manager;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace Raqeeb.Apis
{
    [RoutePrefix("api/test/notification")]
    public class QBHNotificationController : ApiControllerBase
    {
        #region Register Services
        QbhNotificationManager qbhNotificationService = new QbhNotificationManager();
        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(qbhNotificationService);
        }
        #endregion

        #region Method
        [HttpGet]
        [Route("add")]
        public IHttpActionResult Add(HttpRequestMessage request)
        {
            Application application = new Application();
            return Ok(qbhNotificationService.Add(application));
        }


        [HttpGet]
        [Route("update")]
        public IHttpActionResult Update(HttpRequestMessage request)
        {
            Application application = new Application();
            return Ok(qbhNotificationService.Update(application));
        }


        [HttpGet]
        [Route("get")]
        [Authorize]
        public IHttpActionResult GetApplicationById(HttpRequestMessage request)
        {
            Application application = new Application();
            return Ok(qbhNotificationService.GetApplicationById("5a8ddb3eaaa8283fa095806b"));
        }


        [HttpGet]
        [Route("lookup")]
        [Authorize]
        public IHttpActionResult GetLookup(HttpRequestMessage request)
        {

            return Ok(qbhNotificationService.GetLookup<NotificationType>());
        }
        #endregion
    }
}