﻿using Raqeeb.Framework.Core;
using Raqeeb.Apis.Core;
using Raqeeb.Domain;
using Raqeeb.Manager;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Net;

namespace Raqeeb.Apis
{
    [RoutePrefix("api/host")]
    public class HostController : ApiControllerBase
    {
        [HttpGet]
        [Route("ip")]
        public string IP()
        {
            IPHostEntry host;
            string localIP = string.Empty;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP += ip.ToString();
                    break;
                }
            return localIP;
        }
    }
}