﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{

    [RoutePrefix("api/vehicle")]
    public class VehicleController : ApiControllerBase
    {
        #region Register Services

        VehicleManager vehicleManager = new VehicleManager();
        LivePositionManager livePositionManager = new LivePositionManager();
        int group;
        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(vehicleManager);
            disposableServices.Add(livePositionManager);


        }
        #endregion
        public VehicleController()
        {
            ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            group = Convert.ToInt32(principal.Claims.FirstOrDefault(c => c.Type == "Group").Value);
        }

        #region Method

        [HttpGet, Route("get-vehicle-by-code/{code}"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetByCode(HttpRequestMessage request, [FromUri] int code)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, vehicleManager.GetByCode(code)));
        }

        [HttpPost, Route("search"), AuthorizeUser(Roles = "Admin,,User")]
        // second part of dashboard
        public async Task<IHttpActionResult> Search(HttpRequestMessage request, [FromBody] SearchCriteria<VehicleFilter> searchCriteria)
        {
            searchCriteria.Filters.UserGroup = group;

            return await Task.Run(() => Ok(vehicleManager.Search(searchCriteria)));
        }

        [HttpGet, Route("get-live-position-by-vehiclecode/{code}"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetLivePositionByVehicleCode(HttpRequestMessage request, [FromUri] int code)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, livePositionManager.GetByVehicleCode(code)));
        }

        [HttpGet, Route("get-all-vehicles-locations"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetALlVehiclesLocations(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, vehicleManager.GetAllLVehiclesLocations()));
        }


        [HttpGet, Route("get-all-vehicles-locations_v2"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetALlVehiclesLocations_v2(HttpRequestMessage request)
        {
            var result = vehicleManager.GetALlVehiclesLocations_v2();
            var finalResult = result.ToJson(new JsonWriterSettings { OutputMode = JsonOutputMode.Strict });
            return await Task.Run(() => Content(HttpStatusCode.OK, finalResult));
        }

        [HttpPost, Route("get-vehicles-events-by-classificationId"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetVehiclesEventsByClassificationId(HttpRequestMessage request, [FromBody] VehicleFilter vehicleFilter)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, vehicleManager.GetVehiclesEventsByClassificationId(vehicleFilter)));
        }


        [HttpGet, Route("get-all-vehicles"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetAllVehicles(HttpRequestMessage request)
        {
            if (group == 0)
                return await Task.Run(() => Content(HttpStatusCode.OK, vehicleManager.GetALL()));
            else return await Task.Run(() => Content(HttpStatusCode.OK, vehicleManager.GetALL(group)));
        }
        [HttpPost, Route("getVehicleStatistics"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetStatistics(HttpRequestMessage request, [FromBody] SearchCriteria<VehicleFilter> searchCriteria)
        {
            return await Task.Run(() => Ok(vehicleManager.GetStatistics(searchCriteria)));
        }

        [HttpGet, Route("get-vehicles-geofencing/{code}"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetGeofencingVehicles(HttpRequestMessage request, [FromUri] string code)
        {
            return await Task.Run(() => Ok(vehicleManager.GetGeofencing(code)));
        }
        [HttpPost, Route("add-vehicle"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] Vehicle vehicle)
        {
            if (vehicle.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, vehicleManager.Add(vehicle)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, vehicleManager.UpdateVehicle(vehicle)));
            }
        }

        [HttpGet, Route("get-vehicles-by-groupId/{groupId}"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetVechilesFromGroupId(int groupId)
        {
            return await Task.Run(() => Ok(vehicleManager.GetVehiclesByGroupId(groupId)));
        }


        [HttpGet, Route("get-vehicle-by-tree"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetByCode(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, vehicleManager.GetVehicleTreeModel()));
        }

        #endregion
    }
}