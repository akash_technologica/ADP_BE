﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Common;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Raqeeb.Apis.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/eventlogs")]
    public class EventLogsController : ApiControllerBase
    {
        #region Register Services

        EventManager eventManager = new EventManager();
        EventMasterManager eventMasterManager = new EventMasterManager();

        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(eventManager);
            disposableServices.Add(eventMasterManager);
        }

        #endregion

        [HttpPost, Route("get-events-logs"), AuthorizeUser(Roles = "Admin")]
        public async Task<IHttpActionResult> GetEventLogs(HttpRequestMessage request, [FromBody] SearchCriteria<EventFilter> searchCriteria)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.GetEventsLog(searchCriteria)));
        }

    }
}
