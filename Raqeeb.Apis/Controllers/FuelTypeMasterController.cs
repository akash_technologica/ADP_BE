﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Entities;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/fuelType")]
    public class FuelTypeMasterController : ApiControllerBase
    {
        #region Register Services

        VehicleTypeManager vehicleTypeManager = new VehicleTypeManager();

        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(vehicleTypeManager);
        }
        #endregion
        #region Method

        [HttpGet, Route("fuelType-List"), AuthorizeUser(Roles = "Admin,SuperUser,User")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetVehicleTypeList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(vehicleTypeManager.GetVehicleTypeList()));
        }

        [HttpPost, Route("add-fuelType"), AuthorizeUser(Roles = "SuperUser,User")]
        public async Task<IHttpActionResult> AddVehicleType(HttpRequestMessage request, [FromBody] VehicleType vehicleType)
        {
            if (vehicleType.IdString != null)
            {
                return await Task.Run(() => Ok(vehicleTypeManager.UpdateVehicleType(vehicleType)));
            }
            else
            {
                return await Task.Run(() => Ok(vehicleTypeManager.Insert(vehicleType)));
            }

        }

        [HttpPost, Route("delete-fuelType"), AuthorizeUser(Roles = "SuperUser")]
        public async Task<IHttpActionResult> DeleteVehicleType(HttpRequestMessage request, [FromBody] VehicleType vehicleType)
        {
            if (vehicleType.IdString != null)
            {
                return await Task.Run(() => Ok(vehicleTypeManager.Delete(vehicleType)));
            }
            else
            {
                return await Task.Run(() => Ok("Error"));
            }

        }
        #endregion
    }
}
