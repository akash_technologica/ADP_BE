﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/alarm")]
    public class DeviceAlarmController : ApiControllerBase
    {
        #region Register Services

        DeviceAlarmManager alarmManager = new DeviceAlarmManager();
        int group;

        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(alarmManager);

        }
        #endregion
        public DeviceAlarmController()
        {
            ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            group = Convert.ToInt32(principal.Claims.FirstOrDefault(c => c.Type == "Group").Value);
        }

        #region Method
        [HttpPost, Route("search"), AuthorizeUser(Roles = "Admin,User")]
        // second part of dashboard
        public async Task<IHttpActionResult> Search(HttpRequestMessage request, [FromBody] SearchCriteria<AlarmFilter> searchCriteria)
        {
            searchCriteria.Group = group;
            return await Task.Run(() => Ok(alarmManager.Search(searchCriteria)));
        }


        [HttpGet, Route("export"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> exportAll()
        {
            return await Task.Run(() => Ok(alarmManager.exportData(group)));
        }
        #endregion
    }
}