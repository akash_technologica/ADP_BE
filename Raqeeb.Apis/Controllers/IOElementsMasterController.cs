﻿using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Entities;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/ioElements")]
    public class IOElementsMasterController : ApiController
    {
        #region Register Services

        readonly IOElementsMasterManager ioElementsManager = new IOElementsMasterManager();

        #endregion
        [HttpGet, Route("ioElements-List"), AuthorizeUser(Roles = "Admin")]
       
        public async Task<IHttpActionResult> GetIOElementsList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(ioElementsManager.GetList()));
        }


        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] IOElementsMaster ioElements)
        {
            if (ioElements.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, ioElementsManager.Add(ioElements)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, ioElementsManager.Update(ioElements)));
            }
        }


    }
}
