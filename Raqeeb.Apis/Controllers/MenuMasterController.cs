﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Raqeeb.Manager;
using System.Net.Http;
using System.Web.Http;
using Raqeeb.Manager.Manager;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Entities;
using System.Threading.Tasks;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/menus")]
    public class MenuMasterController : ApiController
    {
        #region Register Services

        MenuMasterManager menuManager = new MenuMasterManager();

        //protected override void RegisterServices(List<IServiceContract> disposableServices)
        //{
        //    disposableServices.Add(rolesManager);
        //}
        #endregion

        [HttpGet, Route("getall"), AuthorizeUser(Roles = "Admin")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetAll(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, menuManager.GetAll()));
        }

        [HttpGet, Route("get-menu-type"), AuthorizeUser(Roles = "Admin")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetRolesType(HttpRequestMessage request)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, menuManager.GetMenuType()));
        }

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] MenuMaster menu)
        {
            if (menu.IdString != null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, menuManager.Update(menu)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, menuManager.Add(menu)));
            }
        }

        [HttpPost, Route("delete"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Delete(HttpRequestMessage request, [FromBody] MenuMaster menu)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, menuManager.Delete(menu)));
        }

        [HttpPost, Route("inactive"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Inactive(HttpRequestMessage request, [FromBody] MenuMaster menu)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, menuManager.Inactive(menu)));
        }
    }
}
