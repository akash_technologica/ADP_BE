﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raqeeb.Manager.Manager;
using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Framework.Core;
using Raqeeb.Domain.Entities;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/city")]
    public class CityMasterController : ApiControllerBase
    {
        #region Register Services

        readonly CityMasterManager cityManager = new CityMasterManager();

        #endregion
      
        [HttpGet, Route("getcitybycountry/{code}"), AuthorizeUser(Roles = "Admin")]
        public async Task<IHttpActionResult> GetCityByCountry(HttpRequestMessage request, [FromUri] string code)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, cityManager.GetCityByCountry(code)));
        }

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] CityMaster city)
        {
            if (city.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, cityManager.Add(city)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, cityManager.Update(city)));
            }
        }

        [HttpPost, Route("delete"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Delete(HttpRequestMessage request, [FromBody] CityMaster city)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, cityManager.Delete(city)));
        }





    }
}
