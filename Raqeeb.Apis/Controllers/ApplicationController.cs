﻿using Raqeeb.Framework.Core;
using Raqeeb.Apis.Core;
using Raqeeb.Domain;
using Raqeeb.Manager;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace Raqeeb.Apis
{
    [RoutePrefix("api/application")]
    public class ApplicationController : ApiControllerBase
    {
        #region Register Services

        ApplicationManager applicationService = new ApplicationManager();
        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(applicationService);
        }
        #endregion

        #region Method
        [HttpPost, Route("register")]
        public IHttpActionResult RegisterApplication(HttpRequestMessage request, [FromBody] Application application)
        {
            return Ok(applicationService.Add(application));
        }

        [HttpGet, Route("check-email-exist")]
        public IHttpActionResult CheckEmailExist(HttpRequestMessage request, [FromUri] string email)
        {
            return Ok(applicationService.CheckIfApplicationExist(email));
        }

        #endregion

    }
}