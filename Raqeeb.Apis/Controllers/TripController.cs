﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{

    [RoutePrefix("api/trip")]
    public class TripController : ApiControllerBase
    {
        #region Register Services

        TripManager tripManager = new TripManager();
        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(tripManager);
        }
        #endregion

        #region Method

        [HttpPost, Route("search"), AuthorizeUser(Roles = "Admin")]
        // second part of dashboard
        public async Task<IHttpActionResult> Search(HttpRequestMessage request, [FromBody] SearchCriteria<TripFilter> searchCriteria)
        {
            return await Task.Run(() => Ok(tripManager.Search(searchCriteria)));
        }

        [HttpGet, Route("get-trip-details-by-code/{code}/{isVehicleDisplayed}"), AuthorizeUser(Roles = "Admin")]
        public async Task<IHttpActionResult> GetTripDetailsByCode(HttpRequestMessage request, [FromUri] long code, [FromUri] bool isVehicleDisplayed)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, tripManager.GetTripDetailsByCode(code, isVehicleDisplayed)));
        }
        [HttpGet, Route("get-trip-coordinates-by-code/{code}"), AuthorizeUser(Roles = "Admin")]
        public async Task<IHttpActionResult> GetTripOrdinatesByCode(HttpRequestMessage request, [FromUri] long code)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, tripManager.GetTripOrdinatesByCode(code)));
        }

        #region Dashboard
        [HttpPost, Route("get-total-vehicles-by-trip-date"), AuthorizeUser(Roles = "Admin")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetTotalVehiclesByTripDateDashboard(HttpRequestMessage request, [FromBody] SearchCriteria<TripFilter> searchCriteria)
        {
            return await Task.Run(() => Ok(tripManager.GetTotalVehiclesByTripDateDashboard(searchCriteria)));
        }


        [HttpPost, Route("get-events-vehicles-by-trip-date"), AuthorizeUser(Roles = "Admin")]
        // third part of dashboard
        public async Task<IHttpActionResult> GetEventsWithVehiclesDashboard(HttpRequestMessage request, [FromBody] SearchCriteria<TripFilter> searchCriteria)
        {
            return await Task.Run(() => Ok(tripManager.GetEventsWithVehiclesDashboard(searchCriteria)));
        }



        [HttpPost, Route("qasem")]
        // third part of dashboard
        public async Task<IHttpActionResult> GetTripsAndEventCategories(HttpRequestMessage request , SearchCriteria<TripFilter> searchCriteria)
        {
            var result = await Task.Run(() => tripManager.GetTripsAndEventCategories(searchCriteria));
            var finalResult = result.ToJson(new JsonWriterSettings { OutputMode = JsonOutputMode.Strict });
            return await Task.Run(() => Ok(finalResult));
        }

        [HttpGet, Route("qasem2")]
        // third part of dashboard
        public async Task<IHttpActionResult> GetEventsByTripVehicles(HttpRequestMessage request)
        {
            var result = await Task.Run(() => tripManager.GetEventsByTripVehicles());
            var finalResult = result.ToJson(new JsonWriterSettings { OutputMode = JsonOutputMode.Strict });
            return await Task.Run(() => Ok(finalResult));
        }
        #endregion

        #endregion

    }
}