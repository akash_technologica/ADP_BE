﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/driver")]
    public class DriverProfileController : ApiControllerBase
    {
        #region Register Services

        DriverManager driveManager = new DriverManager();
        List<DriverProfileDto> driverDto = new List<DriverProfileDto>();
        EventManager eventManager = new EventManager();
        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(driveManager);
        }
        #endregion

        #region Method

        [HttpGet, Route("driverList"), AuthorizeUser(Roles = "Admin")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetDriverList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(driveManager.GetAllDrivers()));
        }

        [HttpPost, Route("search"), AuthorizeUser(Roles = "Admin")]
         public async Task<IHttpActionResult> Search(HttpRequestMessage request, [FromBody] SearchCriteria<DriverFilter> searchCriteria)
        {
            return await Task.Run(() => Ok(driveManager.Search(searchCriteria)));
        }


        [HttpGet, Route("get-driver-by-code/{code}"), AuthorizeUser(Roles = "Admin")]
        public async Task<IHttpActionResult> GetByCode(HttpRequestMessage request, [FromUri] int code)
        {
            var driverDet = await Task.Run(() => driveManager.GetByCode(code));
            var driBehData = await Task.Run(() => eventManager.GetEventDetailsByDriverId(code));

            driBehData.Name = driverDet.Name;
            driBehData.ADSDNumber = driverDet.ADSDNumber;
            driBehData.Mobile = driverDet.Mobile;
            driBehData.Code = driverDet.Code;

            return await Task.Run(() => Ok(driBehData));
        }
        [HttpPost, Route("get-statistics-by-code"), AuthorizeUser(Roles = "Admin")]
        public async Task<IHttpActionResult> GetDriverStatisticsByCode(HttpRequestMessage request, [FromBody] SearchCriteria<DriverFilter> searchCriteria)
        {
           
            return await Task.Run(() => Ok(driveManager.GetDriverStatisticsByCode(searchCriteria)));
        }


        #endregion
    }
}
