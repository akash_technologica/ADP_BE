﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.DataModel;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/Media")]
    public class MediaDownloadRequestController : ApiControllerBase
    {
        #region Register Services


        MediaRequestDownloadManager downloadRequestManager = new MediaRequestDownloadManager();
        #endregion

        [HttpPost, Route("search-download-request"), AuthorizeUser(Roles = "Admin,User")]

        public async Task<IHttpActionResult> SearchDownloadMediaRequest(HttpRequestMessage request, [FromBody] SearchCriteria<MediaDownloadRequestFilter> searchCriteria)
        {
            return await Task.Run(() => Ok(downloadRequestManager.GetMediaDownloadRequests(searchCriteria)));
        }


        [HttpPost, Route("add"), AuthorizeUser(Roles = "Admin,User")]

        public async Task<IHttpActionResult> AddNew(MediaDownloadRequest mediaDownloadRequest)
        {
            downloadRequestManager.addnewDownloadRequest(mediaDownloadRequest);
            return await Task.Run(() => Ok());
        }

        [HttpGet, Route("export-all"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> exportAll()
        {
            int group = 0;
            return await Task.Run(() => Ok(downloadRequestManager.exportData()));
        }
    }
}
