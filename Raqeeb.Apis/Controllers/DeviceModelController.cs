﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/deviceModel")]
    public class DeviceModelController : ApiControllerBase
    {
        #region Register Services

        readonly DeviceModelManager modelManager = new DeviceModelManager();

        #endregion

        [HttpGet, Route("getmodelbymanufacturer/{code}"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> GetModelByManufacturerId(HttpRequestMessage request, [FromUri] string code)
        {
            return await Task.Run(() => Content(HttpStatusCode.OK, modelManager.GetDeviceModel(code)));
        }

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] DeviceModel model)
        {
            if (model.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, modelManager.Add(model)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, modelManager.Update(model)));
            }
        }

       
    }
}
