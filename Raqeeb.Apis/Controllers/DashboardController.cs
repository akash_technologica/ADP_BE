﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Common;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Security.Claims;
using System.Security.Principal;

namespace Raqeeb.Apis.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/dashboard")]
    public class DashboardController : ApiControllerBase
    {
        #region Register Services

        DashboardManager dashboardManager = new DashboardManager();

        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(dashboardManager);
        }
        #endregion



        [HttpPost, Route("get-vehicles-dashboard"), AuthorizeUser(Roles = "Admin,User")]
       
        public async Task<IHttpActionResult> GetDashboardData(HttpRequestMessage request,[FromBody] VehicleFilter vehicleFilter )
        {
            ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            var email = principal.Claims.FirstOrDefault().Value;

            if (vehicleFilter.Shift == "Night")
            {
                vehicleFilter.StartDate = DateTime.Now.Date.AddHours(18);
                vehicleFilter.EndDate = DateTime.Now.AddDays(1).Date.AddHours(6);
            }
            else
            {
                vehicleFilter.StartDate = DateTime.Now.Date.AddHours(6);
                vehicleFilter.EndDate = DateTime.Now.Date.AddHours(18);
            }


            return await Task.Run(() => Content(HttpStatusCode.OK, dashboardManager.GetDashboardData(vehicleFilter)));
        }

        [HttpPost, Route("get-Elivated-vehicles-dashboard"), AuthorizeUser(Roles = "Admin,User")]

        public async Task<IHttpActionResult> GetElivatedDashboardData(HttpRequestMessage request, [FromBody] VehicleFilter vehicleFilter)
        {
            //if (vehicleFilter.Shift == "Night")
            //{
            //    vehicleFilter.StartDate = DateTime.Now.Date.AddHours(18);
            //    vehicleFilter.EndDate = DateTime.Now.AddDays(1).Date.AddHours(6);
            //}
            //else
            //{
            //    vehicleFilter.StartDate = DateTime.Now.Date.AddHours(6);
            //    vehicleFilter.EndDate = DateTime.Now.Date.AddHours(18);
            //}
            return  await Task.Run(()=> Content(HttpStatusCode.OK, dashboardManager.GetDashboardData(vehicleFilter)));
        }
        [HttpPost, Route("get-Normal-vehicles-dashboard"), AuthorizeUser(Roles = "Admin,User")]

        public async Task<IHttpActionResult> GetNormalDashboardData(HttpRequestMessage request, [FromBody] VehicleFilter vehicleFilter)
        {
            //if (vehicleFilter.Shift == "Night")
            //{
            //    vehicleFilter.StartDate = DateTime.Now.Date.AddHours(18);
            //    vehicleFilter.EndDate = DateTime.Now.AddDays(1).Date.AddHours(6);
            //}
            //else
            //{
            //    vehicleFilter.StartDate = DateTime.Now.Date.AddHours(6);
            //    vehicleFilter.EndDate = DateTime.Now.Date.AddHours(18);
            //}
            return await Task.Run(() => Content(HttpStatusCode.OK, dashboardManager.GetDashboardData(vehicleFilter)));
        }
        [HttpPost, Route("get-Idle-vehicles-dashboard"), AuthorizeUser(Roles = "Admin,User")]

        public async Task<IHttpActionResult> GetIdleDashboardData(HttpRequestMessage request, [FromBody] VehicleFilter vehicleFilter)
        {
            //if (vehicleFilter.Shift == "Night")
            //{
            //    vehicleFilter.StartDate = DateTime.Now.Date.AddHours(18);
            //    vehicleFilter.EndDate = DateTime.Now.AddDays(1).Date.AddHours(6);
            //}
            //else
            //{
            //    vehicleFilter.StartDate = DateTime.Now.Date.AddHours(6);
            //    vehicleFilter.EndDate = DateTime.Now.Date.AddHours(18);
            //}
            return await Task.Run(() => Content(HttpStatusCode.OK, dashboardManager.GetDashboardData(vehicleFilter)));
        }


        //[HttpGet, Route("get-All-ViolationData"), AuthorizeUser(Roles = "Admin")]

        //public IHttpActionResult GetViolationData(HttpRequestMessage request)
        //{
        //    return Content(HttpStatusCode.OK, dashboardManager.GetViolationData());
        //}

        [HttpGet, Route("get-All-TripData"), AuthorizeUser(Roles = "Admin,User")]

        public IHttpActionResult GetTripData(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, dashboardManager.GetTripData());
        }

        [HttpGet, Route("get-All-DriverData"), AuthorizeUser(Roles = "Admin,User")]

        public IHttpActionResult GetDriverData(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, dashboardManager.GetDriverData());
        }

        //[HttpGet, Route("get-All-ScoreSetUpData"), AuthorizeUser(Roles = "Admin")]

        //public IHttpActionResult GetScoreSetUpData(HttpRequestMessage request)
        //{
        //    return Content(HttpStatusCode.OK, dashboardManager.GetScoreSetUpData());
        //}

        [HttpGet, Route("get-All-LivePositionData"), AuthorizeUser(Roles = "Admin,User")]

        public IHttpActionResult LivePositionData(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, dashboardManager.GetLivePositionData());
        }
        [HttpGet, Route("get-All-ViolationData"), AuthorizeUser(Roles = "Admin,User")]

        public IHttpActionResult GetViolationMonthWise(HttpRequestMessage request)
        {
            ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            string group = principal.Claims.FirstOrDefault(c=>c.Type == "Group").Value;
            return Content(HttpStatusCode.OK, dashboardManager.GetViolationDashboardData(Convert.ToInt32(group)));
        }
        [HttpGet, Route("get-Critical-Drivers"), AuthorizeUser(Roles = "Admin,User")]

        public IHttpActionResult GetCriticalDrivers(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, dashboardManager.GetCriticalDrivers());
        }
        [HttpGet, Route("get-Critical-Vehicles"), AuthorizeUser(Roles = "Admin,User")]

        public IHttpActionResult GetCriticalVehicles(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, dashboardManager.GetCriticalVehicles());
        }
        [HttpPost, Route("get-events-dashboard"), AuthorizeUser(Roles = "Admin,User")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetEventsDashboard(HttpRequestMessage request, [FromBody] SearchCriteria<EventFilter> searchCriteria)
        {
            EventManager eventManager = new EventManager();
            
            searchCriteria.Filters.Shift = Utility.ShiftFromDateTime();
            if (searchCriteria.Filters.Shift == "Night")
            {
                searchCriteria.Filters.StartDate = DateTime.Now.Date.AddHours(18);
                searchCriteria.Filters.EndDate = DateTime.Now.AddDays(1).Date.AddHours(6);
            }
            else
            {
                searchCriteria.Filters.StartDate = DateTime.Now.Date.AddHours(6);
                searchCriteria.Filters.EndDate = DateTime.Now.Date.AddHours(18);
            }
            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.GetEventsForDashboard(searchCriteria)));
        }
        [HttpPost, Route("get-events-history"), AuthorizeUser(Roles = "Admin,User")]
        // first part of Dashboard
        public async Task<IHttpActionResult> GetEventsHistory(HttpRequestMessage request, [FromBody] SearchCriteria<EventFilter> searchCriteria)
        {
            ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            var email = principal.Claims.FirstOrDefault().Value;
            EventManager eventManager = new EventManager();
            DateTime today = DateTime.Today;

            string startDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
           

            searchCriteria.Filters.StartDate = Convert.ToDateTime(startDate);
            searchCriteria.Filters.EndDate = DateTime.Now.AddHours(4);
          
            return await Task.Run(() => Content(HttpStatusCode.OK, eventManager.GetEventsHistory(searchCriteria)));
        }
    }
}