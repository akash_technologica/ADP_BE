﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Raqeeb.Apis.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/manage-web-services")]
    public class ManageWebServiceController : ApiControllerBase
    {
        #region Register Managers

        WebServiceSetupManager webServiceSetupManager = new WebServiceSetupManager();
        LoggingManager loggingManager = new LoggingManager();
        protected override void RegisterServices(List<IServiceContract> disposableServices)
        {
            disposableServices.Add(webServiceSetupManager);
            disposableServices.Add(loggingManager);
        }
        #endregion

        [HttpPost, Route("search"), AuthorizeUser(Roles = "Admin")]
        public IHttpActionResult Search(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, webServiceSetupManager.Search());
        }



        [HttpPost, Route("log/search"), Authorize(Roles = "Admin")]
        public IHttpActionResult SearchLog(HttpRequestMessage request)
        {
            return Content(HttpStatusCode.OK, loggingManager.Search());
        }
    }
}