﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raqeeb.Manager.Manager;
using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Framework.Core;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Filters;
using System.Security.Claims;
using System.Web;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/devices")]
    public class DeviceController : ApiControllerBase
    {
        #region Register Services

        DeviceManager deviceManager = new DeviceManager();
        int group;

        #endregion
        public DeviceController()
        {
            ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            group = Convert.ToInt32(principal.Claims.FirstOrDefault(c => c.Type == "Group").Value);
        }

        [HttpGet, Route("getall"), AuthorizeUser(Roles = "Admin,User")]

        public async Task<IHttpActionResult> GetAll(HttpRequestMessage request)
        {
            if(group == 0)
            return await Task.Run(() => Content(HttpStatusCode.OK, deviceManager.GetAll()));
            else return await Task.Run(() => Content(HttpStatusCode.OK, deviceManager.GetAll(group)));
        }

        [HttpGet, Route("device-List/{code}"), AuthorizeUser(Roles = "Admin,User")]

        public async Task<IHttpActionResult> GetDeviceList(HttpRequestMessage request, [FromUri] string code)
        {
            return await Task.Run(() => Ok(deviceManager.GetList(code)));
        }


        [HttpPost, Route("add"), Authorize(Roles = "Admin,User")]
        public async Task<IHttpActionResult> Add(HttpRequestMessage request, [FromBody] Device device)
        {
            if (device.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, deviceManager.Add(device)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, deviceManager.Update(device)));
            }
        }
        [HttpPost, Route("search"), AuthorizeUser(Roles = "Admin,User")]
        public async Task<IHttpActionResult> Search(HttpRequestMessage request, [FromBody] SearchCriteria<DeviceFilter> searchCriteria)
        {
            searchCriteria.Filters.UserGroup = group;
            return await Task.Run(() => Content(HttpStatusCode.OK, deviceManager.Search(searchCriteria)));
        }

    }
}
