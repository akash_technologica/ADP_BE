﻿using Raqeeb.Apis.Core;
using Raqeeb.Apis.FilterAttributes;
using Raqeeb.Domain.Entities;
using Raqeeb.Manager.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Raqeeb.Apis.Controllers
{
    [RoutePrefix("api/account")]
    public class CompanyConfigurationController : ApiControllerBase
    {
        #region Register Services

        readonly CompanyConfigurationManager companyConfigurationManager = new CompanyConfigurationManager();

        #endregion
        [HttpGet, Route("account-List"), AuthorizeUser(Roles = "Admin")]
        // second part of dashboard
        public async Task<IHttpActionResult> GetEntityList(HttpRequestMessage request)
        {
            return await Task.Run(() => Ok(companyConfigurationManager.GetAccountList()));
        }

        [HttpPost, Route("add"), Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> AddEntity(HttpRequestMessage request, [FromBody] CompanyConfiguration profile)
        {
            if (profile.IdString == null)
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, companyConfigurationManager.Add(profile)));
            }
            else
            {
                return await Task.Run(() => Content(HttpStatusCode.OK, companyConfigurationManager.Update(profile)));
            }
        }
    }
}
