using Raqeeb.Framework.Core;
using System.Collections.Generic;


namespace Raqeeb.Apis.Core
{
    public interface IServiceAwareController
    {
        void RegisterDisposableServices(List<IServiceContract> disposableServices);

        List<IServiceContract> DisposableServices { get; }
    }
}
