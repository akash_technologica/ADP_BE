﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace Raqeeb.Apis
{
    public class RefreshTokenActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Response == null)
            {
                return;
            }
            var request = actionExecutedContext.Request;
            var response = actionExecutedContext.Response;
            if (response == null)
            {
                return;
            }
            try
            {
                if (request.Headers.Contains("lastActiveTime"))
                {
                    IEnumerable<string> refreshTokenHeaderValues = request.Headers.GetValues("lastActiveTime");
                    if (string.IsNullOrEmpty(refreshTokenHeaderValues.FirstOrDefault()) || refreshTokenHeaderValues.FirstOrDefault() == "null")
                    {
                        response.Headers.Add("lastActiveTime", DateTime.Now.ToString());
                    }
                    else
                    {
                        DateTime clientTime;  // = DateTime.Parse(refreshTokenHeaderValues.FirstOrDefault());
                        bool canBeConverted = DateTime.TryParse(refreshTokenHeaderValues.FirstOrDefault(), out clientTime);

                       // DateTime dt = DateTime.ParseExact(refreshTokenHeaderValues.FirstOrDefault(), "DDD, dd MMM yyyy H:mm:ss tt zzz", null);
                      //  clientTime = DateTime.ParseExact(refreshTokenHeaderValues.FirstOrDefault(), "yyyy-MM-dd HH:mm:ss \"GMT\"zzz", null); //DateTime.Parse(refreshTokenHeaderValues.FirstOrDefault()).ToUniversalTime();
                        if (canBeConverted)
                        {
                            clientTime = clientTime.AddMinutes(int.Parse(ConfigurationManager.AppSettings["sessionTimeOut"]));
                        if (clientTime > DateTime.Now)
                            response.Headers.Add("lastActiveTime", DateTime.Now.ToString());
                        else
                        {
                            response.Headers.Add("lastActiveTime", "");
                            throw new UnauthorizedAccessException();
                        }
                        }
                        else
                        {
                            response.Headers.Add("lastActiveTime", "");
                            throw new UnauthorizedAccessException();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UnauthorizedAccessException();
            }
            base.OnActionExecuted(actionExecutedContext);
        }


    }

}