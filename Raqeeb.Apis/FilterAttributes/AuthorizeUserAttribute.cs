﻿using Raqeeb.Domain.Entities;
using Raqeeb.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Raqeeb.Apis.FilterAttributes
{
    public class AuthorizeUserAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {


            #region Validate access token stamp

            var claimsIdentity = actionContext.RequestContext.Principal.Identity as ClaimsIdentity;
            if (claimsIdentity == null)
            {
                this.HandleUnauthorizedRequest(actionContext);
            }


            var accessTokenStamp = claimsIdentity.Claims.FirstOrDefault(c => c.Type == "AccessTokenStamp");

            if (accessTokenStamp == null)
            {
                // There was no stamp in the token.
                this.HandleUnauthorizedRequest(actionContext);
            }
            else
            {
                var userId = claimsIdentity.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;

                if (!ValidateAccessTokenStamp(userId.ToString(), accessTokenStamp.Value))
                {
                    throw new UnauthorizedAccessException();
                }
            }

            #endregion


            base.OnAuthorization(actionContext);
        }


        public bool ValidateAccessTokenStamp(string userId, string accessTokenStamp)
        {
            string accessTokenStampClaimCacheKey =  "AccessTokenStamp_" + userId;
            //string savedAccessTokenStamp = GlobalCachingProvider.Instance.GetItem<string>(accessTokenStampClaimCacheKey, false);
            string savedAccessTokenStamp = null;

            if (savedAccessTokenStamp == null)
            {
                //load it from db because may be removed from cache
                UserManager userManager = new UserManager();

                User user = userManager.GetById(userId);

                if (user != null && user.AccessTokenStamp == accessTokenStamp)
                {
                    //GlobalCachingProvider.Instance.RemoveItem(accessTokenStampClaimCacheKey);
                    //GlobalCachingProvider.Instance.AddItem(accessTokenStampClaimCacheKey, user.AccessTokenStamp, 20);

                    return true;
                }

                return false;
            }

            if (savedAccessTokenStamp == accessTokenStamp)
                return true;
            return false;

        }


    }
}