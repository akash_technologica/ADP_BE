﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Raqeeb.Framework.Core;
using Raqeeb.Domain;
using Raqeeb.Domain.Entities;
using Raqeeb.Manager;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System;

namespace Raqeeb.Apis
{
    public class AuthorizationWebServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                List<string> rolesStr = new List<string>();
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                var data = await context.Request.ReadFormAsync();
                UserManager userManager = new UserManager();

                User user = userManager.Login(data["userName"], data["password"]);

                identity.AddClaim(new Claim(ClaimTypes.Name, data["userName"]));
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.IdString));
                identity.AddClaim(new Claim("EnglishName", user.EnglishName));
                identity.AddClaim(new Claim("ArabicName", user.ArabicName));
                identity.AddClaim(new Claim("MobileNumber", user.MobileNumber));
                identity.AddClaim(new Claim("EntityCode", user.CompanyId));
                identity.AddClaim(new Claim("Group", user.GroupId.ToString()));
                identity.AddClaim(new Claim("AccessTokenStamp", user.AccessTokenStamp.ToString()));
                
                // context.Response.Headers.Set("lastActiveTime" ,  DateTime.Now.ToString());

                if (user.Roles != null && user.Roles.Count > 0)
                    foreach (string role in user.Roles)
                    {
                        identity.AddClaim(new Claim(ClaimTypes.Role, role));
                    }

                var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                     { "user" , JsonConvert.SerializeObject(user)},
                });
                var loggedUser = new AuthenticationTicket(identity, props);
                context.Validated(loggedUser);
            }
            catch (BussinessException ex)
            {
                context.SetError(ex.MessageCode);
            }
            catch (System.Exception ex)
            {
                context.SetError(ex.Message);
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

    }


    public class AuthorizationAppServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                List<string> rolesStr = new List<string>();
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                var data = await context.Request.ReadFormAsync();

                ApplicationManager applicationService = new ApplicationManager();
                ApplicationDto app = applicationService.GetAccessByApiKeys(data["clientApiKey"], data["clientSecretKey"]);

                identity.AddClaim(new Claim(ClaimTypes.Name, data["clientApiKey"]));
                identity.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, app.IdString));

                var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                     { "user" , JsonConvert.SerializeObject(app)},
                });
                var ticket = new AuthenticationTicket(identity, props);
                context.Validated(ticket);
            }
            catch (BussinessException ex)
            {
                context.SetError(ex.MessageCode);
            }
            catch (System.Exception ex)
            {
                context.SetError(ex.Message);
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

    }
}


