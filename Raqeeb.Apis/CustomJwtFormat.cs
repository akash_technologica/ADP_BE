﻿
using Microsoft.Owin.Security;
using Raqeeb.Common;
using System;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace Raqeeb.Apis
{

    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly string _issuer = string.Empty;

        public CustomJwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            string path = @"E:\ADP\API\private.localhost.pfx";
            X509Certificate2 cert = new X509Certificate2(path, "localhost", X509KeyStorageFlags.MachineKeySet);
            //X509Certificate2 cert = new X509Certificate2(Path.Combine(Utility.AssemblyDirectory, ConfigurationManager.AppSettings["PrivateCertificate"]), ConfigurationManager.AppSettings["CertificatePassword"].ToString(), X509KeyStorageFlags.MachineKeySet);
            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityToken(_issuer, ConfigurationManager.AppSettings["OAuthAccessTokenAudience"].ToString(), data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, new X509SigningCredentials(cert));


            var handler = new JwtSecurityTokenHandler();

            var jwt = handler.WriteToken(token);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }


}