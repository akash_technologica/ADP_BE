﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using Raqeeb.Common;
using System.Configuration;

[assembly: OwinStartup(typeof(Raqeeb.Apis.Startup))]

namespace Raqeeb.Apis
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.Use<GlobalExceptionMiddleware>();
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);

            ConfigureWebOAuth(app);
            ConfigureAppOAuth(app);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        public void ConfigureWebOAuth(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/authorization/login"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new AuthorizationWebServerProvider(),
                //RefreshTokenProvider = new RefreshTokenProvider()
                AccessTokenFormat = new CustomJwtFormat(ConfigurationManager.AppSettings["OAuthAccessTokenIssuer"].ToString())

            };


            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            //app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            //get private key
            string path = @"E:\ADP\API\public.localhost.cer";
            X509Certificate2 cert = new X509Certificate2(path, "localhost");
           // X509Certificate2 cert = new X509Certificate2(Path.Combine(Utility.AssemblyDirectory, ConfigurationManager.AppSettings["PublicCertificate"]), ConfigurationManager.AppSettings["CertificatePassword"]);
            // ConfigurationManager.AppSettings["PublicCertificate"].ToString()
            // Api controllers with an[Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { "http://localhost" },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new X509CertificateSecurityTokenProvider("http://localhost", cert)
                    },

                });
        }

        public void ConfigureAppOAuth(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/authorization/get-access"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new AuthorizationAppServerProvider(),
                //RefreshTokenProvider = new RefreshTokenProvider()
                AccessTokenFormat = new CustomJwtFormat(ConfigurationManager.AppSettings["OAuthAccessTokenIssuer"].ToString())

            };


            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            //app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            //get private key
            string path = @"E:\ADP\API\private.localhost.pfx";
            X509Certificate2 cert = new X509Certificate2(path,"localhost");
            // ConfigurationManager.AppSettings["PublicCertificate"].ToString()
            // Api controllers with an[Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { "http://localhost" },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new X509CertificateSecurityTokenProvider("http://localhost", cert)
                    },

                });
        }
    }
}
