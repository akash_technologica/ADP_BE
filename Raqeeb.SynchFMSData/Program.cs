﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.SynchFMSData
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG
            SynchFMSData SynchFMSData = new SynchFMSData();
            SynchFMSData.OnDebug();
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
#else
          ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new SynchFMSData()
            };
            ServiceBase.Run(ServicesToRun);
#endif

        }
    }
}
