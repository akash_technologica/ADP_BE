﻿using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using Raqeeb.Manager.Manager;
using Raqeeb.SynchFMSData.RaqeebSoapService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Raqeeb.SynchFMSData
{
    public partial class SynchFMSData : ServiceBase
    {
        // Check The minId and ItemId to make sure that ItemId will not go to Zero Anytime !
        #region VARIABLES
        WebServiceSetupManager webServiceSetupManager = new WebServiceSetupManager();
        private Timer Schedular;
        private readonly bool _useEventLogger = false;
        int runEverySeconds = 120;
        Log eventLog = null;
        #endregion
        #region WIN SRV METHOD
        public SynchFMSData()
        {
            InitializeComponent();

            int.TryParse(ConfigurationManager.AppSettings["RunEverySeconds"], out runEverySeconds);
            _useEventLogger = string.Equals(ConfigurationManager.AppSettings["UseEventLogger"],
               true.ToString(), StringComparison.InvariantCultureIgnoreCase);

            eventLog = new Log() { Code = "FMS_SERVICE_INITIALIZED", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "FMS SERVICE INITIALIZED", Tags = "FMS_INITIALIZED,FMS" };
            LogEvent(eventLog);
        }

        protected override void OnStart(string[] args)
        {
            eventLog = new Log() { Code = "FMS_SERVICE_STARTED", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "FMS SERVICE STARTED", Tags = "FMS_STARTED,FMS" };
            LogEvent(eventLog);

            ScheduleService();
        }

        protected override void OnStop()
        {
            eventLog = new Log() { Code = "FMS_SERVICE_STOPPED", CreationDate = DateTime.Now, LogType = LogType.Warning, Message = "FMS SERVICE STOPPED", Tags = "FMS_STOPPED,FMS" };
            LogEvent(eventLog);
        }

        public void OnDebug()
        {
            eventLog = new Log() { Code = "FMS_SERVICE_DEBUG_MODE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "FMS SERVICE ON DEBUG MODE STARTED", Tags = "FMS_ONDEBUG,FMS" };
            LogEvent(eventLog);
            OnStart(null);
        }


        private void ScheduleService()
        {
            try
            {


                Schedular = new Timer(new TimerCallback(SchedularCallback));
                DateTime scheduledTime = DateTime.Now;


                StartSynchFMSData(); // HERE THE CODE



                TimeSpan timeSpan = DateTime.Now.Subtract(DateTime.Now.AddSeconds(-runEverySeconds));
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);
                Schedular.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                eventLog = new Log() { Code = "FMS_SERVICE_EXCEPTION", CreationDate = DateTime.Now, LogType = LogType.Error, Message = ex.Message, Tags = "FMS_EXCEPTION,FMS", Details = ex.StackTrace };
                LogEvent(eventLog);
            }
            finally
            {
                TimeSpan timeSpan = DateTime.Now.Subtract(DateTime.Now.AddSeconds(-runEverySeconds));
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);
                Schedular.Change(dueTime, Timeout.Infinite);
            }
        }

        private void SchedularCallback(object e)
        {
            ScheduleService();
        }
        #endregion
        #region SYNCH METHOD
        private void StartSynchFMSData()
        {
            GetContractorDataDelegate getContractorDataDelegate = new GetContractorDataDelegate(GetContractorData);
            ExecuteGetContractorDataAsyncOperationWithCallback(getContractorDataDelegate, GetContractorDataCompleted);

            GetDriversMasterDataDelegate getDriversMasterDataDelegate = new GetDriversMasterDataDelegate(GetDriversMasterData);
            ExecuteGetDriversMasterDataAsyncOperationWithCallback(getDriversMasterDataDelegate, GetDriversMasterDataCompleted);

            GetEventDataDelegate getEventDataDelegate = new GetEventDataDelegate(GetEventData);
            ExecuteGetEventDataAsyncOperationWithCallback(getEventDataDelegate, GetEventDataCompleted);


            GetEventsMasterDataDelegate getEventsMasterDataDelegate = new GetEventsMasterDataDelegate(GetEventsMasterData);
            ExecuteGetEventsMasterDataAsyncOperationWithCallback(getEventsMasterDataDelegate, GetEventsMasterDataCompleted);

            //GetLiveEventDataDelegate getLiveEventDataDelegate = new GetLiveEventDataDelegate(GetLiveEventData);
            //ExecuteGetLiveEventDataAsyncOperationWithCallback(getLiveEventDataDelegate, GetLiveEventDataCompleted);


            GetLiveGPSPositionDataDelegate getLiveGPSPositionDataDelegate = new GetLiveGPSPositionDataDelegate(GetLiveGPSPositionData);
            ExecuteGetLiveGPSPositionDataAsyncOperationWithCallback(getLiveGPSPositionDataDelegate, GetLiveGPSPositionDataCompleted);


            GetLivePositionbyDriverDataDelegate getLivePositionbyDriverDataDelegate = new GetLivePositionbyDriverDataDelegate(GetLivePositionbyDriverData);
            ExecuteGetLivePositionbyDriverDataAsyncOperationWithCallback(getLivePositionbyDriverDataDelegate, GetLivePositionbyDriverDataCompleted);


            GetLivePositionbyVehicleDataDelegate getLivePositionbyVehicleDataDelegate = new GetLivePositionbyVehicleDataDelegate(GetLivePositionbyVehicleData);
            ExecuteGetLivePositionbyVehicleDataAsyncOperationWithCallback(getLivePositionbyVehicleDataDelegate, GetLivePositionbyVehicleDataCompleted);

            GetSubTripSummaryDataDelegate getSubTripSummaryDataDelegate = new GetSubTripSummaryDataDelegate(GetSubTripSummaryData);
            ExecuteGetSubTripSummaryDataAsyncOperationWithCallback(getSubTripSummaryDataDelegate, GetSubTripSummaryDataCompleted);


            GetTripSummaryDataDelegate getTripSummaryDataDelegate = new GetTripSummaryDataDelegate(GetTripSummaryData);
            ExecuteGetTripSummaryDataAsyncOperationWithCallback(getTripSummaryDataDelegate, GetTripSummaryDataCompleted);


            GetVehiclesMasterDataDelegate getVehiclesMasterDataDelegate = new GetVehiclesMasterDataDelegate(GetVehiclesMasterData);
            ExecuteGetVehiclesMasterDataAsyncOperationWithCallback(getVehiclesMasterDataDelegate, GetVehiclesMasterDataCompleted);



        }
        #endregion
        #region Private Helper Methods
        public string ShiftFromDateTime(DateTime currentDateTime)
        {
            currentDateTime = currentDateTime.AddHours(-4);
            string shiftType = "Night";
            TimeSpan start = TimeSpan.Parse("06:00"); // 6 AM
            TimeSpan end = TimeSpan.Parse("18:00");   // 2 AM
            TimeSpan now = currentDateTime.TimeOfDay;

            if (now >= start && now <= end)
            {
                shiftType = "Day";
            }

            return shiftType;

        }

        private void LogEvent(Log log)
        {
            //if (_useEventLogger)
            //{
            //    LoggingManager loggingManager = new LoggingManager();
            //    loggingManager.Insert(log);
            //}
        }
        public DateTime convertIntToDate(int value)
        {
            string year = string.Empty;
            string month = string.Empty;
            string day = string.Empty;

            year = value.ToString().Substring(0, 4);
            month = value.ToString().Substring(4, 2);
            day = value.ToString().Substring(6, 2);
            //DateTime current = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day), 0, 0, 0, DateTimeKind.Local);

            DateTime current = new DateTime(Convert.ToInt32(DateTime.Now.Year), Convert.ToInt32(DateTime.Now.Month), Convert.ToInt32(DateTime.Now.Day), 0, 0, 0, DateTimeKind.Local);

            return current;
        }


        public DateTime convertIntToDateTime(decimal value)
        {
            string year = string.Empty;
            string month = string.Empty;
            string day = string.Empty;
            string hour = string.Empty;
            string min = string.Empty;
            string sec = string.Empty;

            year = value.ToString().Substring(0, 4);
            month = value.ToString().Substring(4, 2);
            day = value.ToString().Substring(6, 2);

            hour = value.ToString().Substring(8, 2);
            min = value.ToString().Substring(10, 2);
            sec = value.ToString().Substring(12, 2);
            //DateTime current = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day), Convert.ToInt32(hour), Convert.ToInt32(min), Convert.ToInt32(sec), DateTimeKind.Local);


            DateTime current = new DateTime(Convert.ToInt32(DateTime.Now.Year), Convert.ToInt32(DateTime.Now.Month), Convert.ToInt32(DateTime.Now.Day), Convert.ToInt32(hour), Convert.ToInt32(min), Convert.ToInt32(sec), DateTimeKind.Local);



            return current.AddHours(4);
        }

        private void AddUpdateWebSRVSetup(WebServiceSetup serviceSetup, WebSrvMode mode, WebServiceType type, long ItemId)
        {
            int numberOfExecution = 0;
            if (mode == WebSrvMode.Insert)
            {
                WebServiceSetup webServiceSetup = new WebServiceSetup()
                {
                    IsFinished = true,
                    LasItemId = ItemId,
                    LastSynchTime = DateTime.Now.AddHours(4),
                    Name = type.ToString(),
                    NumberOfExecution = 1,
                    Type = type
                };

                InsertWebSrvSetup(webServiceSetup);
            }
            else if (mode == WebSrvMode.Update)
            {
                if (serviceSetup.NumberOfExecution.HasValue)
                    serviceSetup.NumberOfExecution = serviceSetup.NumberOfExecution.Value + 1;
                else
                    serviceSetup.NumberOfExecution = 1;
                serviceSetup.IsFinished = true;
                serviceSetup.LasItemId = ItemId;
                serviceSetup.LastSynchTime = DateTime.Now.AddHours(4);
                UpdateWebSrvSetup(serviceSetup);
            }
        }
        #endregion
        #region WEB SRV BUSINESS
        #region WebService GET_CONTRACTOR_DATA_DELEGATE
        delegate Task GetContractorDataDelegate(System.Action callbackAction);
        private void ExecuteGetContractorDataAsyncOperationWithCallback(
            GetContractorDataDelegate contractorDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetContractorData(callbackAction);
            });
            task.Start();
        }

        public async Task GetContractorData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_CONTRACTOR_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_CONTRACTOR_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetContractorsMasterData);
                long minId = 0;
                bool isFinished = false;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    // START GET WEB SERVICE DATA
                    Add_Contractors(serviceSetup, mode, "PTARaqeeb", "PTA123Raqeeb", minId);
                    // END GET WEB SERVICE DATA
                }
            });

        }
        private void GetContractorDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_CONTRACTOR_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_CONTRACTOR_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #region WebService GET_DRIVER_MASTER_DATA_DELEGATE
        delegate Task GetDriversMasterDataDelegate(System.Action callbackAction);
        private void ExecuteGetDriversMasterDataAsyncOperationWithCallback(
            GetDriversMasterDataDelegate DriversMasterDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetDriversMasterData(callbackAction);
            });
            task.Start();
        }

        public async Task GetDriversMasterData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_DRIVER_MASTER_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_DRIVER_MASTER_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetDriversMasterData);
                long minId = 0;
                bool isFinished = true;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    // START GET WEB SERVICE DATA
                    Add_Drivers(serviceSetup, mode, "PTARaqeeb", "PTA123Raqeeb", minId);
                    // END GET WEB SERVICE DATA
                }
            });

        }
        private void GetDriversMasterDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_DRIVER_MASTER_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_DRIVER_MASTER_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #region WebService GET_EVENT_DATA_DELEGATE
        delegate Task GetEventDataDelegate(System.Action callbackAction);
        private void ExecuteGetEventDataAsyncOperationWithCallback(
            GetEventDataDelegate EventDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetEventData(callbackAction);
            });
            task.Start();
        }

        public async Task GetEventData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_EVENT_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_EVENT_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetEventsData);
                long minId = 0;
                bool isFinished = true;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    // START GET WEB SERVICE DATA
                    Add_Event(serviceSetup, mode, "PTARaqeeb", "PTA123Raqeeb", minId);
                    // END GET WEB SERVICE DATA
                }
            });

        }
        private void GetEventDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_EVENT_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_EVENT_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #region WebService GET_EVENT_MASTER_DATA_DELEGATE
        delegate Task GetEventsMasterDataDelegate(System.Action callbackAction);
        private void ExecuteGetEventsMasterDataAsyncOperationWithCallback(
            GetEventsMasterDataDelegate EventsMasterDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetEventsMasterData(callbackAction);
            });
            task.Start();
        }

        public async Task GetEventsMasterData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_EVENT_MASTER_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_EVENT_MASTER_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetEventsMasterData);
                long minId = 0;
                bool isFinished = true;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    Add_EventMaster(serviceSetup, mode, "PTARaqeeb", "PTA123Raqeeb", minId);
                }
            });

        }
        private void GetEventsMasterDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_EVENT_MASTER_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_EVENT_MASTER_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #region (NOT USED) WebService GET_LIVE_EVENT_DATA_DELEGATE
        delegate Task GetLiveEventDataDelegate(System.Action callbackAction);
        private void ExecuteGetLiveEventDataAsyncOperationWithCallback(
            GetLiveEventDataDelegate LiveEventDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetLiveEventData(callbackAction);
            });
            task.Start();
        }

        public async Task GetLiveEventData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_LIVE_EVENT_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_LIVE_EVENT_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetLiveEventsData);
                long minId = 0;
                bool isFinished = true;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    // START GET WEB SERVICE DATA

                    // END GET WEB SERVICE DATA

                    if (mode == WebSrvMode.Insert)
                    {
                        WebServiceSetup webServiceSetup = new WebServiceSetup()
                        {
                            IsFinished = true,
                            LasItemId = 10,
                            LastSynchTime = DateTime.Now.AddHours(4),
                            Name = "GetLiveEventsData",
                            Type = WebServiceType.GetLiveEventsData
                        };

                        InsertWebSrvSetup(webServiceSetup);
                    }
                    else if (mode == WebSrvMode.Update)
                    {
                        serviceSetup.IsFinished = true;
                        serviceSetup.LasItemId = 10;
                        serviceSetup.LastSynchTime = DateTime.Now.AddHours(4);
                        UpdateWebSrvSetup(serviceSetup);
                    }
                }
            });

        }
        private void GetLiveEventDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_LIVE_EVENT_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_LIVE_EVENT_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #region WebService GET_LIVE_GPS_POSITION_DATA_DELEGATE
        delegate Task GetLiveGPSPositionDataDelegate(System.Action callbackAction);
        private void ExecuteGetLiveGPSPositionDataAsyncOperationWithCallback(
            GetLiveGPSPositionDataDelegate LiveGPSPositionDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetLiveGPSPositionData(callbackAction);
            });
            task.Start();
        }

        public async Task GetLiveGPSPositionData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_LIVE_GPS_POSITION_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_LIVE_GPS_POSITION_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetLiveGPSPositionData);
                long minId = 0;
                bool isFinished = true;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    Add_GPSPosition(serviceSetup, mode, "PTARaqeeb", "PTA123Raqeeb", minId);
                }
            });

        }
        private void GetLiveGPSPositionDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_LIVE_GPS_POSITION_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_LIVE_GPS_POSITION_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #region WebService GET_LIVE_POSITION_BY_DRIVER_DATA_DELEGATE
        delegate Task GetLivePositionbyDriverDataDelegate(System.Action callbackAction);
        private void ExecuteGetLivePositionbyDriverDataAsyncOperationWithCallback(
            GetLivePositionbyDriverDataDelegate LivePositionbyDriverDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetLivePositionbyDriverData(callbackAction);
            });
            task.Start();
        }

        public async Task GetLivePositionbyDriverData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_LIVE_POSITION_BY_DRIVER_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_LIVE_POSITION_BY_DRIVER_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetLivePositionbyDriver);
                long minId = 0;
                bool isFinished = true;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    Add_PositionByDriver(serviceSetup, mode, "PTARaqeeb", "PTA123Raqeeb", minId);
                }
            });

        }
        private void GetLivePositionbyDriverDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_LIVE_POSITION_BY_DRIVER_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_LIVE_POSITION_BY_DRIVER_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #region WebService GET_LIVE_POSITION_BY_VEHICLE_DATA_DELEGATE
        delegate Task GetLivePositionbyVehicleDataDelegate(System.Action callbackAction);
        private void ExecuteGetLivePositionbyVehicleDataAsyncOperationWithCallback(
            GetLivePositionbyVehicleDataDelegate LivePositionbyVehicleDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetLivePositionbyVehicleData(callbackAction);
            });
            task.Start();
        }

        public async Task GetLivePositionbyVehicleData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_LIVE_POSITION_BY_VEHICLE_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_LIVE_POSITION_BY_VEHICLE_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetLivePositionbyVehicle);
                long minId = 0;
                bool isFinished = true;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    Add_PositionVehicle(serviceSetup, mode, "PTARaqeeb", "PTA123Raqeeb", minId);
                }
            });

        }
        private void GetLivePositionbyVehicleDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_LIVE_POSITION_BY_VEHICLE_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_LIVE_POSITION_BY_VEHICLE_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #region WebService GET_SUB_TRIP_SUMMARY_DATA_DELEGATE
        delegate Task GetSubTripSummaryDataDelegate(System.Action callbackAction);
        private void ExecuteGetSubTripSummaryDataAsyncOperationWithCallback(
            GetSubTripSummaryDataDelegate SubTripSummaryDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetSubTripSummaryData(callbackAction);
            });
            task.Start();
        }

        public async Task GetSubTripSummaryData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_SUB_TRIP_SUMMARY_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_SUB_TRIP_SUMMARY_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetSubTripSummaryData);
                long minId = 0;
                bool isFinished = true;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    Add_SubTrip(serviceSetup, mode, "PTARaqeeb", "PTA123Raqeeb", minId);
                }
            });

        }
        private void GetSubTripSummaryDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_SUB_TRIP_SUMMARY_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_SUB_TRIP_SUMMARY_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #region WebService GET_TRIP_SUMMARY_DATA_DELEGATE
        delegate Task GetTripSummaryDataDelegate(System.Action callbackAction);
        private void ExecuteGetTripSummaryDataAsyncOperationWithCallback(
            GetTripSummaryDataDelegate TripSummaryDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetTripSummaryData(callbackAction);
            });
            task.Start();
        }

        public async Task GetTripSummaryData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_TRIP_SUMMARY_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_TRIP_SUMMARY_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetTripSummaryData);
                long minId = 0;
                bool isFinished = true;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    Add_Trip(serviceSetup, mode, "PTARaqeeb", "PTA123Raqeeb", minId);
                }
            });

        }
        private void GetTripSummaryDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_TRIP_SUMMARY_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_TRIP_SUMMARY_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #region  WebService GET_VEHICLES_MASTER_DATA_DELEGATE
        delegate Task GetVehiclesMasterDataDelegate(System.Action callbackAction);
        private void ExecuteGetVehiclesMasterDataAsyncOperationWithCallback(
            GetVehiclesMasterDataDelegate VehiclesMasterDataDelegate, System.Action callbackAction)
        {
            Task task = new Task(async () =>
            {
                await GetVehiclesMasterData(callbackAction);
            });
            task.Start();
        }

        public async Task GetVehiclesMasterData(System.Action callbackAction)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                eventLog = new Log() { Code = "START_GET_VEHICLES_MASTER_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "START GET CONTRACTOR DATA DELEGATE", Tags = "START_GET_VEHICLES_MASTER_DATA_DELEGATE,FMS,OPERATION_END" };
                LogEvent(eventLog);
                //System.Threading.Thread.Sleep(10000);

                WebServiceSetup serviceSetup = GetWebSrvByType(WebServiceType.GetVehiclesMasterData);
                long minId = 0;
                bool isFinished = true;
                WebSrvMode mode = WebSrvMode.Insert;
                if (serviceSetup != null)
                {
                    minId = serviceSetup.LasItemId;
                    isFinished = serviceSetup.IsFinished;
                    mode = WebSrvMode.Update;
                }

                if (isFinished)
                {
                    Add_Vehciles(serviceSetup, mode, "PTARaqeeb", "PTA123Raqeeb", minId);
                }
            });

        }
        private void GetVehiclesMasterDataCompleted()
        {
            eventLog = new Log() { Code = "END_GET_VEHICLES_MASTER_DATA_DELEGATE", CreationDate = DateTime.Now, LogType = LogType.Information, Message = "END GET CONTRACTOR DATA DELEGATE", Tags = "END_GET_VEHICLES_MASTER_DATA_DELEGATE,FMS,OPERATION_START" };
            LogEvent(eventLog);
        }
        #endregion
        #endregion
        #region MANAGE SETUP TABLE
        public WebServiceSetup GetWebSrvByType(WebServiceType webServiceType)
        {
            var result = webServiceSetupManager.GetByType(webServiceType);
            return result;
        }
        public WebServiceSetup InsertWebSrvSetup(WebServiceSetup webServiceSetup)
        {
            var result = webServiceSetupManager.Insert(webServiceSetup);
            return result;
        }
        public WebServiceSetup UpdateWebSrvSetup(WebServiceSetup webServiceSetup)
        {
            var result = webServiceSetupManager.Update(webServiceSetup);
            return result;
        }
        #endregion
        #region WEB SRV METHOD
        public void Add_Contractors(WebServiceSetup serviceSetup, WebSrvMode mode, string username, string password, long? minId)
        {
            if (minId.HasValue && minId.Value == 0)
            {
                long itemId = 0;
                ContractorManager contractorManager = new ContractorManager();
                RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
                using (new OperationContextScope(obj.InnerChannel))
                {
                    var contractorMasterDataResult = obj.GetContractorsMasterData(username, password);

                    Contractor contractor = null;
                    List<Contractor> contractorList = new List<Contractor>();

                    foreach (ContractorMasterDataResult oContractor in contractorMasterDataResult)
                    {
                        contractor = new Contractor();
                        contractor.Code = oContractor.CONTRACTOR_CODE;
                        contractor.EndDate = oContractor.CONTRACT_END_DATE.HasValue ? Convert.ToDateTime(oContractor.CONTRACT_END_DATE.Value) : (DateTime?)null;
                        contractor.StartDate = oContractor.CONTRACT_START_DATE.HasValue ? Convert.ToDateTime(oContractor.CONTRACT_START_DATE.Value) : (DateTime?)null;
                        contractor.VendorCode = oContractor.VENDOR_CODE;
                        contractor.Name = oContractor.COTRACTOR_NAME;
                        contractorList.Add(contractor);
                    }
                    contractorManager.InsertBulk(contractorList);
                    if (contractorList.Count > 0)
                        itemId = Convert.ToInt64(contractorList[contractorList.Count - 1].Code + 1);
                }

                AddUpdateWebSRVSetup(serviceSetup, mode, WebServiceType.GetContractorsMasterData, itemId);
            }
        }

        public void Add_Drivers(WebServiceSetup serviceSetup, WebSrvMode mode, string username, string password, long? minId)
        {
            if (minId.HasValue && minId.Value == 0)
            {
                long itemId = 0;
                DriverManager driverManager = new DriverManager();
                RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
                using (new OperationContextScope(obj.InnerChannel))
                {
                    var DriversMasterDataList = obj.GetDriversMasterData(username, password);

                    Driver driver = null;
                    List<Driver> driverList = new List<Driver>();

                    foreach (DriverMasterDataResult odriver in DriversMasterDataList)
                    {
                        driver = new Driver();
                        driver.ADSDNumber = odriver.ADSD_NUMBER;
                        driver.Code = odriver.DRIVER_CODE;
                        driver.ContractorCode = odriver.CONTRACTOR_CODE;
                        driver.DateOfBirth = odriver.DATE_OF_BIRTH.HasValue ? Convert.ToDateTime(odriver.DATE_OF_BIRTH.Value) : (DateTime?)null;

                        if (odriver.LAST_UPDATED.HasValue)
                        {
                            driver.LastUpdated = convertIntToDate(odriver.LAST_UPDATED.Value);
                        }

                        if (odriver.LICENTE_EXPIRY_DATE.HasValue)
                        {
                            driver.LicenseExpiryDate = convertIntToDate(odriver.LICENTE_EXPIRY_DATE.Value);
                        }

                        if (odriver.LICENSE_ISSUE_DATE.HasValue)
                        {
                            driver.LicenseIssueDate = convertIntToDate(odriver.LICENSE_ISSUE_DATE.Value);
                        }

                        driver.Mobile = odriver.MOBILE;
                        driver.Name = odriver.DRIVER_NAME;
                        driver.VendorCode = odriver.VENDOR_CODE;
                        driver.Email = odriver.EMAIL;

                        driverList.Add(driver);
                    }
                    driverManager.InsertBulk(driverList);
                    if (driverList.Count > 0)
                        itemId = Convert.ToInt64(driverList[driverList.Count - 1].Code + 1);
                }
                AddUpdateWebSRVSetup(serviceSetup, mode, WebServiceType.GetDriversMasterData, itemId);
            }
        }

        public void Add_Vehciles(WebServiceSetup serviceSetup, WebSrvMode mode, string username, string password, long? minId)
        {
            if (minId.HasValue && minId.Value == 0)
            {
                long itemId = 0;

                VehicleManager vehicleManager = new VehicleManager();
                RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
                using (new OperationContextScope(obj.InnerChannel))
                {

                    var GetVehiclesMasterDataList = obj.GetVehiclesMasterData(username, password);

                    Vehicle oVehicle = null;
                    List<Vehicle> oVehicleList = new List<Vehicle>();

                    foreach (VehicleMasterDataResult _Vehicle in GetVehiclesMasterDataList)
                    {
                        oVehicle = new Vehicle();
                        oVehicle.Code = _Vehicle.VEHICLE_CODE;
                        oVehicle.ContractorCode = _Vehicle.CONTRACTOR_CODE;
                        oVehicle.LastOdoMeter = _Vehicle.LAST_ODOMETER;
                        oVehicle.Registration = _Vehicle.REGISTRATION;
                        if (_Vehicle.UPDATE_DATE.HasValue)
                            oVehicle.UpdateDate = convertIntToDate(_Vehicle.UPDATE_DATE.Value);
                        oVehicle.VendorCode = _Vehicle.VENDOR_CODE;


                        oVehicleList.Add(oVehicle);
                    }

                    vehicleManager.InsertBulk(oVehicleList);

                    if (oVehicleList.Count > 0)
                        itemId = Convert.ToInt64(oVehicleList[oVehicleList.Count - 1].Code + 1);
                }
                AddUpdateWebSRVSetup(serviceSetup, mode, WebServiceType.GetVehiclesMasterData, itemId);
            }
        }

        public void Add_EventMaster(WebServiceSetup serviceSetup, WebSrvMode mode, string username, string password, long? minId)
        {
            if (minId.HasValue && minId.Value == 0)
            {
                long itemId = 0;

                EventMasterManager eventMasterManager = new EventMasterManager();
                RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
                using (new OperationContextScope(obj.InnerChannel))
                {
                    var GetEventsMasterData = obj.GetEventsMasterData(username, password);

                    EventMaster oEventMaster = null;
                    List<EventMaster> EventMasterList = new List<EventMaster>();

                    foreach (EventMasterDataResult _event in GetEventsMasterData)
                    {
                        oEventMaster = new EventMaster();

                        oEventMaster.Code = _event.EVENT_CODE;
                        oEventMaster.ContractorCode = _event.CONTRACTOR_CODE;
                        oEventMaster.Description = _event.EVENT_DESC;
                        oEventMaster.IsViolation = _event.IS_VIOLATION;

                        if (_event.VALID_FROM.HasValue)
                            oEventMaster.ValidForm = convertIntToDate(_event.VALID_FROM.Value);

                        if (_event.VALID_TO.HasValue)
                            oEventMaster.ValidTo = convertIntToDate(_event.VALID_TO.Value);

                        oEventMaster.VendorCode = _event.VENDOR_CODE;

                        EventMasterList.Add(oEventMaster);
                    }
                    eventMasterManager.InsertBulk(EventMasterList);
                    if (EventMasterList.Count > 0)
                        itemId = Convert.ToInt64(EventMasterList[EventMasterList.Count - 1].Code + 1);
                }
                AddUpdateWebSRVSetup(serviceSetup, mode, WebServiceType.GetEventsMasterData, itemId);
            }
        }

        public void Add_Event(WebServiceSetup serviceSetup, WebSrvMode mode, string username, string password, long minId)
        {
            long itemId = 0;

            EventManager eventManager = new EventManager();
            RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
            using (new OperationContextScope(obj.InnerChannel))
            {
                var GetEventsDataList = obj.GetEventsData(username, password, minId);

                Event oEvent = null;
                List<Event> EventList = new List<Event>();

                foreach (EventDataResult _event in GetEventsDataList)
                {
                    oEvent = new Event();
                    oEvent.Code = "FMS-" + _event.EVENT_CODE.ToString();
                    oEvent.OriginalCode = _event.EVENT_CODE;
                    oEvent.ContractorCode = _event.CONTRACTOR_CODE;
                    oEvent.DriverCode = _event.DRIVER_CODE;
                    if (_event.EVENT_END_DATE.HasValue)
                        oEvent.EndDate = convertIntToDateTime(_event.EVENT_END_DATE.Value);

                    oEvent.EndPositionCode = _event.EVENT_END_POSITION_CODE.HasValue ? "FMS-" + _event.EVENT_END_POSITION_CODE.Value.ToString() : null;

                    if (_event.EVENT_START_DATE.HasValue)
                        oEvent.StartDate = convertIntToDateTime(_event.EVENT_START_DATE.Value);

                    oEvent.StartPositionCode = _event.EVENT_START_POSITION_CODE.HasValue ? "FMS-" + _event.EVENT_START_POSITION_CODE.Value.ToString() : null;
                    oEvent.SubTripCode = _event.SUBTRIP_CODE;
                    oEvent.TripCode = _event.TRIP_CODE;
                    oEvent.VehicleCode = _event.VEHICLE_CODE;
                    oEvent.VendorCode = _event.VendorCode;
                    oEvent.EventTypeCode = _event.EVENT_TYPE_CODE;

                    oEvent.EventSource = "FMS";
                    if (oEvent.StartDate.HasValue)
                        oEvent.Shift = ShiftFromDateTime(oEvent.StartDate.Value);
                    else
                        oEvent.Shift = "NA";

                    EventList.Add(oEvent);
                }

                eventManager.InsertBulk(EventList);
                if (EventList.Count > 0)
                    itemId = Convert.ToInt64(EventList[EventList.Count - 1].OriginalCode + 1);
            }
            AddUpdateWebSRVSetup(serviceSetup, mode, WebServiceType.GetEventsData, itemId);
        }

        public void Add_Trip(WebServiceSetup serviceSetup, WebSrvMode mode, string username, string password, long minId)
        {
            long itemId = 0;
            TripManager tripManager = new TripManager();
            RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
            using (new OperationContextScope(obj.InnerChannel))
            {
                TripSummaryDataResult[] GetTripSummaryDataList = obj.GetTripSummaryData(username, password, minId);

                Trip oTrip = null;
                List<Trip> oTripList = new List<Trip>();

                foreach (TripSummaryDataResult _Trip in GetTripSummaryDataList)
                {
                    oTrip = new Trip();
                    oTrip.Code = _Trip.TRIP_CODE;
                    oTrip.ContractorCode = _Trip.CONTRACTOR_CODE;
                    oTrip.DistanceDriven = _Trip.TRIP_DISTANCE_DRIVEN;
                    oTrip.DriverCode = _Trip.DRIVER_CODE;
                    oTrip.DrivingTime = _Trip.TRIP_DRIVING_TIME;
                    oTrip.Duration = _Trip.TRIP_DURATION;

                    oTrip.EndOdoMeter = _Trip.TRIP_END_ODOMETER;
                    oTrip.EndPositionCode = _Trip.TRIP_END_POSITION_CODE.HasValue ? "FMS-" + _Trip.TRIP_END_POSITION_CODE.Value.ToString() : null;
                    oTrip.MaxPrm = _Trip.TRIP_MAX_RPM;
                    oTrip.MaxSpeed = _Trip.TRIP_MAX_SPEED;
                    oTrip.StandingTime = _Trip.TRIP_STANDING_TIME;

                    oTrip.StartOdoMeter = _Trip.TRIP_START_ODOMETER;
                    oTrip.StartPositionCode = _Trip.TRIP_START_POSITION_CODE.HasValue ? "FMS-" + _Trip.TRIP_START_POSITION_CODE.Value.ToString() : null;
                    oTrip.VehicleCode = _Trip.VEHICLE_CODE;
                    oTrip.VendorCode = _Trip.VENDOR_CODE;

                    if (_Trip.TRIP_END_DATE.HasValue)
                        oTrip.EndDate = convertIntToDateTime(_Trip.TRIP_END_DATE.Value);

                    if (_Trip.TRIP_START_DATE.HasValue)
                        oTrip.StartDate = convertIntToDateTime(_Trip.TRIP_START_DATE.Value);

                    oTripList.Add(oTrip);
                }

                tripManager.InsertBulk(oTripList);

                if (oTripList.Count > 0)
                    itemId = Convert.ToInt64(oTripList[oTripList.Count - 1].Code + 1);
            }
            AddUpdateWebSRVSetup(serviceSetup, mode, WebServiceType.GetTripSummaryData, itemId);
        }

        public void Add_SubTrip(WebServiceSetup serviceSetup, WebSrvMode mode, string username, string password, long minId)
        {
            long itemId = 0;
            SubTripManager subTripManager = new SubTripManager();
            RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
            using (new OperationContextScope(obj.InnerChannel))
            {
                SubTripSummaryDataResult[] GetSubTripSummaryDataList = obj.GetSubTripSummaryData(username, password, minId);

                SubTrip oSubTrip = null;
                List<SubTrip> oSubTripList = new List<SubTrip>();

                foreach (SubTripSummaryDataResult _SubTrip in GetSubTripSummaryDataList)
                {
                    oSubTrip = new SubTrip();

                    oSubTrip.Code = _SubTrip.SUBTRIP_CODE;
                    oSubTrip.ContractorCode = _SubTrip.CONTRACTOR_CODE;
                    oSubTrip.DistanceDriven = _SubTrip.SUBTRIP_DISTANCE_DRIVEN;
                    oSubTrip.DriverCode = _SubTrip.DRIVER_CODE;
                    oSubTrip.Duration = _SubTrip.SUBTRIP_DURATION;

                    oSubTrip.EndPositionCode = _SubTrip.SUBTRIP_END_POSITION_CODE.HasValue ? "FMS-" + _SubTrip.SUBTRIP_END_POSITION_CODE.Value.ToString() : null;
                    oSubTrip.MaxPrm = _SubTrip.SUBTRIP_MAX_RPM;
                    oSubTrip.MaxSpeed = _SubTrip.SUBTRIP_MAX_SPEED;

                    oSubTrip.StartPositionCode = _SubTrip.SUBTRIP_START_POSITION_CODE.HasValue ? "FMS-" + _SubTrip.SUBTRIP_START_POSITION_CODE.Value.ToString() : null;
                    oSubTrip.TripCode = _SubTrip.TRIP_CODE;
                    oSubTrip.VehicleCode = _SubTrip.VEHICLE_CODE;
                    oSubTrip.VendorCode = _SubTrip.VENDOR_CODE;

                    if (_SubTrip.SUBTRIP_END_DATE.HasValue)
                        oSubTrip.EndDate = convertIntToDateTime(_SubTrip.SUBTRIP_END_DATE.Value);

                    if (_SubTrip.SUBTRIP_START_DATE.HasValue)
                        oSubTrip.StartDate = convertIntToDateTime(_SubTrip.SUBTRIP_START_DATE.Value);

                    oSubTripList.Add(oSubTrip);
                }

                subTripManager.InsertBulk(oSubTripList);

                if (oSubTripList.Count > 0)
                    itemId = Convert.ToInt64(oSubTripList[oSubTripList.Count - 1].Code + 1);
            }
            AddUpdateWebSRVSetup(serviceSetup, mode, WebServiceType.GetSubTripSummaryData, itemId);
        }

        public void Add_GPSPosition(WebServiceSetup serviceSetup, WebSrvMode mode, string username, string password, long minId)
        {
            long itemId = 0;

            LivePositionManager livePositionManager = new LivePositionManager();
            RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
            using (new OperationContextScope(obj.InnerChannel))
            {
                var GetLiveGPSPositionDataList = obj.GetLiveGPSPositionData(username, password, minId);

                LivePosition oLivePosition = null;
                List<LivePosition> oLivePositionList = new List<LivePosition>();

                foreach (GPSPositionDataResult _LivePosition in GetLiveGPSPositionDataList)
                {
                    oLivePosition = new LivePosition();
                    oLivePosition.Altitude = _LivePosition.ALTITUDE;
                    oLivePosition.Code = "FMS-" + _LivePosition.POSITION_CODE.ToString();
                    oLivePosition.OriginalCode = _LivePosition.POSITION_CODE;
                    oLivePosition.ContractorCode = _LivePosition.CONTRACTOR_CODE;
                    oLivePosition.DriverCode = _LivePosition.DRIVER_CODE;
                    oLivePosition.DriverSeatBelt = _LivePosition.DRIVER_SEAT_BELT;
                    oLivePosition.FwdEngaged = _LivePosition.FWD_ENGAGED;
                    oLivePosition.Heading = _LivePosition.HEADING;
                    oLivePosition.Latitude = _LivePosition.LATITUDE;
                    if (_LivePosition.LOCATION_TIMESTAMP.HasValue)
                        oLivePosition.LocationTIme = convertIntToDateTime(_LivePosition.LOCATION_TIMESTAMP.Value);

                    oLivePosition.Longitude = _LivePosition.LONGITUDE;
                    oLivePosition.OdoMeter = _LivePosition.ODOMETER;
                    oLivePosition.PassengerSeatBelt = _LivePosition.PASSENGER_SEAT_BELT;
                    oLivePosition.Prm = _LivePosition.RPM;
                    oLivePosition.Speed = _LivePosition.SPEED;
                    oLivePosition.SubTripCode = _LivePosition.SUBTRIP_CODE;
                    oLivePosition.TripCode = _LivePosition.TRIP_CODE;
                    oLivePosition.VehicleCode = _LivePosition.VEHICLE_CODE;
                    oLivePosition.VendorCode = _LivePosition.VENDOR_CODE;
                    oLivePosition.Type = Domain.Enums.PositionType.GPS;

                    oLivePositionList.Add(oLivePosition);
                }

                livePositionManager.InsertBulk(oLivePositionList);
                if (oLivePositionList.Count > 0)
                    itemId = Convert.ToInt64(oLivePositionList[oLivePositionList.Count - 1].OriginalCode + 1);
            }
            AddUpdateWebSRVSetup(serviceSetup, mode, WebServiceType.GetLiveGPSPositionData, itemId);
        }

        public void Add_PositionByDriver(WebServiceSetup serviceSetup, WebSrvMode mode, string username, string password, long? minId)
        {
            if (minId.HasValue && minId.Value == 0)
            {
                long itemId = 0;

                LivePositionManager livePositionManager = new LivePositionManager();
                RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
                using (new OperationContextScope(obj.InnerChannel))
                {
                    var GetLiveGPSPositionDataList = obj.GetLivePositionbyDriver(username, password);

                    LivePosition oLivePosition = null;
                    List<LivePosition> oLivePositionList = new List<LivePosition>();

                    foreach (LivePositionbyDriverResult _LivePosition in GetLiveGPSPositionDataList)
                    {
                        oLivePosition = new LivePosition();
                        oLivePosition.Altitude = _LivePosition.ALTITUDE;
                        oLivePosition.Code = "FMS-" + _LivePosition.LIVE_LOCATION_CODE.ToString();
                        oLivePosition.OriginalCode = _LivePosition.LIVE_LOCATION_CODE;
                        oLivePosition.ContractorCode = _LivePosition.CONTRACTOR_CODE;
                        oLivePosition.DriverCode = _LivePosition.DRIVER_CODE;

                        oLivePosition.Heading = _LivePosition.HEADING;
                        oLivePosition.Latitude = _LivePosition.LATITUDE;
                        if (_LivePosition.LOCATION_TIMESTAMP.HasValue)
                            oLivePosition.LocationTIme = convertIntToDateTime(_LivePosition.LOCATION_TIMESTAMP.Value);

                        oLivePosition.Longitude = _LivePosition.LONGITUDE;
                        oLivePosition.OdoMeter = _LivePosition.ODOMETER;

                        oLivePosition.Prm = _LivePosition.RPM;
                        oLivePosition.Speed = _LivePosition.SPEED;


                        oLivePosition.VehicleCode = _LivePosition.VEHICLE_CODE;
                        oLivePosition.VendorCode = Convert.ToInt32(_LivePosition.VENDOR_CODE);
                        oLivePosition.Type = Domain.Enums.PositionType.Driver;

                        oLivePositionList.Add(oLivePosition);
                    }
                    livePositionManager.InsertBulk(oLivePositionList);
                    if (oLivePositionList.Count > 0)
                        itemId = Convert.ToInt64(oLivePositionList[oLivePositionList.Count - 1].OriginalCode + 1);
                }
                AddUpdateWebSRVSetup(serviceSetup, mode, WebServiceType.GetLivePositionbyDriver, itemId);
            }
        }

        public void Add_PositionVehicle(WebServiceSetup serviceSetup, WebSrvMode mode, string username, string password, long? minId)
        {
            if (minId.HasValue && minId.Value == 0)
            {
                long itemId = 0;
                LivePositionManager livePositionManager = new LivePositionManager();
                RaqeebSoapService.Service1SoapClient obj = new RaqeebSoapService.Service1SoapClient();
                using (new OperationContextScope(obj.InnerChannel))
                {
                    var GetLivePositionbyVehicleList = obj.GetLivePositionbyVehicle(username, password);

                    LivePosition oLivePosition = null;
                    List<LivePosition> oLivePositionList = new List<LivePosition>();

                    foreach (LivePositionbyVehicleResult _LivePosition in GetLivePositionbyVehicleList)
                    {
                        oLivePosition = new LivePosition();
                        oLivePosition.Altitude = _LivePosition.ALTITUDE;
                        oLivePosition.Code = "FMS-" + _LivePosition.LIVE_LOCATION_CODE.ToString();
                        oLivePosition.OriginalCode = _LivePosition.LIVE_LOCATION_CODE;
                        oLivePosition.ContractorCode = _LivePosition.CONTRACTOR_CODE;
                        oLivePosition.DriverCode = _LivePosition.DRIVER_CODE;

                        oLivePosition.Heading = _LivePosition.HEADING;
                        oLivePosition.Latitude = _LivePosition.LATITUDE;
                        if (_LivePosition.LOCATION_TIMESTAMP.HasValue)
                            oLivePosition.LocationTIme = convertIntToDateTime(_LivePosition.LOCATION_TIMESTAMP.Value);

                        oLivePosition.Longitude = _LivePosition.LONGITUDE;
                        oLivePosition.OdoMeter = _LivePosition.ODOMETER;

                        oLivePosition.Prm = _LivePosition.RPM;
                        oLivePosition.Speed = _LivePosition.SPEED;


                        oLivePosition.VehicleCode = _LivePosition.VEHICLE_CODE;
                        oLivePosition.VendorCode = Convert.ToInt32(_LivePosition.VENDOR_CODE);
                        oLivePosition.Type = Domain.Enums.PositionType.Vehicle;

                        oLivePositionList.Add(oLivePosition);
                    }
                    livePositionManager.InsertBulk(oLivePositionList);
                    if (oLivePositionList.Count > 0)
                        itemId = Convert.ToInt64(oLivePositionList[oLivePositionList.Count - 1].OriginalCode + 1);
                }
                AddUpdateWebSRVSetup(serviceSetup, mode, WebServiceType.GetLivePositionbyVehicle, itemId);
            }
        }
        #endregion
    }
}
