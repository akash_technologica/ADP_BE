﻿using System;
using System.Configuration;

namespace Raqeeb.DataAccess
{
    public abstract class DataAccessBase
    {
        #region Constants
        #region Collection
        protected const string COLLECTION_APPLICATION = "APPLICATION";
        protected const string COLLECTION_LOOKUP_NOTIFICATION_TYPE = "LOOKUP_NOTIFICATION_TYPE";
        protected const string COLLECTION_TEMPLATE= "TEMPLATE";
        protected const string COLLECTION_USER = "USER";
        protected const string COLLECTION_USER_PERMISSIONS = "USER_PERMISSIONS";
        #endregion

        #region Fields
        protected const string ID_FIELD = "_id";
        protected const string DELETED_BY_FIELD = "DELETED_BY";
    #endregion
    protected int IncreasedTimeFromUI = Convert.ToInt32(ConfigurationManager.AppSettings["IncreasedTimeFromUI"]);
    protected  int IncreasedTimeFromBackEnd = Convert.ToInt32(ConfigurationManager.AppSettings["IncreasedTimeFromBackEnd"]);
    #endregion
  }
}
