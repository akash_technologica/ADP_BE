﻿using MongoDB.Driver;
using Raqeeb.Framework.Core;
using System.Configuration;

namespace Raqeeb.DataAccess
{
    public sealed class MongoDbManager
    {
        static MongoClient _client;
        static IMongoDatabase _db;
        private MongoDbManager() { }

        public static IMongoDatabase GetMongoDataBase()
        {

            if (_client == null)
            {

                _client = new MongoClient("mongodb://localhost:27017");
                return _client.GetDatabase("UnifiedTracking");

            }
            else
            {
                if (_db != null)
                {
                    return _db;
                }
                else
                {
                    return _client.GetDatabase("UnifiedTracking");
                    // return _client.GetDatabase(new Crypto().Decrypt(ConfigurationManager.AppSettings["NotificationDBName"].ToString(), "o6806642kbM7c5"));
                }
            }

        }

    }
}
