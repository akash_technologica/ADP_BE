﻿using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
  public class ContractorDAL : DataAccessBase
  {
    #region Methods
    public List<Contractor> InsertBulk(List<Contractor> contractors)
    {
      IMongoDatabase db = MongoDbManager.GetMongoDataBase();
      db.GetCollection<Contractor>("CONTRACTOR").InsertMany(contractors);
      return contractors;
    }
    
    public Contractor GetByCode(string code)
    {
      Contractor result = new Contractor();
      IMongoDatabase db = MongoDbManager.GetMongoDataBase();
      var filter = Builders<Contractor>.Filter.Eq("Code", code);
      result = db.GetCollection<Contractor>("CONTRACTOR").Find<Contractor>(filter).FirstOrDefault();
      return result;
    }
    #endregion
  }
}
