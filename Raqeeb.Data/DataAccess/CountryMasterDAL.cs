﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;

namespace Raqeeb.DataAccess.DataAccess
{
    public class CountryMasterDAL : DataAccessBase
    {
        #region Methods

        public List<CountryMaster> GetAll()
        {
            List<CountryMaster> result = new List<CountryMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<CountryMaster>.Filter.Empty;

            result = db.GetCollection<CountryMaster>("COUNTRY_MASTER").Find<CountryMaster>(filter).ToList();
            return result;
        }

        public CountryMaster Add(CountryMaster country)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<CountryMaster>("COUNTRY_MASTER").InsertOne(country);
            return country;
        }


        public CountryMaster Update(CountryMaster country)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(country.IdString);
            var filter = Builders<CountryMaster>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<CountryMaster>.Update
                .Set("CountryCode", country.CountryCode)
                .Set("EnglishName", country.EnglishName);
                                
            db.GetCollection<CountryMaster>("COUNTRY_MASTER").UpdateOne(filter, updateCommand);

            return country;
        }

        public CountryMaster AddCity(CityMaster city)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            CountryMaster result = new CountryMaster();

            var filter = Builders<CountryMaster>.Filter.Empty;
            filter = Builders<CountryMaster>.Filter.Eq("CountryCode", city.CountryCode);
            result = db.GetCollection<CountryMaster>("COUNTRY_MASTER").Find<CountryMaster>(filter).FirstOrDefault();

            city.CountryId = result.IdString;

            db.GetCollection<CityMaster>("CITY_MASTER").InsertOne(city);
                     
            return db.GetCollection<CountryMaster>("COUNTRY_MASTER").Find<CountryMaster>(filter).FirstOrDefault();

        }
        public List<CityMaster> GetCityByCountryId(CountryMaster countryMaster)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            List<CityMaster> result = new List<CityMaster>();

            var filter = Builders<CityMaster>.Filter.Empty;
            filter = Builders<CityMaster>.Filter.Eq("CountryId", countryMaster.IdString);
            return db.GetCollection<CityMaster>("CITY_MASTER").Find<CityMaster>(filter).ToList();
        }
        public CityMaster UpdateCity(CityMaster city)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(city.IdString);
            var filter = Builders<CityMaster>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<CityMaster>.Update
                .Set("CountryCode", city.CountryCode)
                .Set("EnglishName", city.EnglishName);

            db.GetCollection<CityMaster>("CITY_MASTER").UpdateOne(filter, updateCommand);

            return city;
        }

        public CountryMaster Delete(CountryMaster country)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(country.IdString);
            var filter = Builders<CountryMaster>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<CountryMaster>("COUNTRY_MASTER").DeleteOne(filter);
            return country;
        }


        #endregion
    }
}
