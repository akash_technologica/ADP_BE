﻿using MongoDB.Driver;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class LoggingDAL : DataAccessBase
    {
        public Log Insert(Log log)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<Log>("LOG").InsertOne(log);
            return log;
        }



        public PagedSearchResultMongo<Log> Search()
        {
            try
            {
                SearchCriteria<LoggingFilter> searchCriteria = new SearchCriteria<LoggingFilter>();
                PagedSearchResultMongo<Log> result = new PagedSearchResultMongo<Log>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<Log>.Empty;

                result.NumberOfRecords = db.GetCollection<Log>("LOG").Count(query);
                result.Collection = db.GetCollection<Log>("LOG").Find<Log>(query)
                     .SortByDescending(p => p.CreationDate).Skip(searchCriteria.ItemsToSkip).Limit(searchCriteria.PageSize).ToList();
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
