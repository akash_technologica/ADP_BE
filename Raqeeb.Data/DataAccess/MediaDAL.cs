﻿using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class MediaDAL:  DataAccessBase
    {
        #region Methods
        public List<EventMedia> InsertBulk(List<EventMedia> medias)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            if(medias.Count>0)
            db.GetCollection<EventMedia>("MEDIA").InsertMany(medias);
            return medias;
        }

        public List<Media> InsertBulkMedia(List<Media> medias)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            if (medias.Count > 0)
                db.GetCollection<Media>("MEDIA").InsertMany(medias);
            return medias;
        }
        public Driver GetByCode(string code)
        {
            Driver result = new Driver();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Driver>.Filter.Eq("Code", code);
            result = db.GetCollection<Driver>("DRIVER").Find<Driver>(filter).FirstOrDefault();
            return result;
        }

        public List<Driver> GetAllDrivers()
        {
            Driver result = new Driver();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Driver>.Filter.Empty;
            return db.GetCollection<Driver>("DRIVER").Find<Driver>(filter).ToList();
        }
        #endregion
    }
}
