﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain;
using System;
using System.Collections.Generic;

namespace Raqeeb.DataAccess
{
    public class QBHNotificationDAL : DataAccessBase
    {
        #region Constants
        #region Fields
        protected const string ARABIC_NAME_FIELD = "ARABIC_NAME";
        protected const string ENGLISH_NAME_FIELD = "ENGLISH_NAME";
        protected const string MODIFIED_BY_FIELD = "MODIFIED_BY";
        protected const string MODIFICATION_DATE_FIELD = "MODIFICATION_DATE";


        #endregion
        #endregion

        #region Methods
        public Application Add(Application application)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<Application>(COLLECTION_APPLICATION).InsertOne(application);
            return application;
        }
        public Application Update(Application application)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Application>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(application.IdString)));
            var updateCommand = Builders<Application>.Update
                .Set(ARABIC_NAME_FIELD, application.ArabicName)
                .Set(ENGLISH_NAME_FIELD, application.EnglishName)
                .Set(MODIFICATION_DATE_FIELD, application.ModificationDate)
            .Set(MODIFIED_BY_FIELD, application.ModifiedBy);
            db.GetCollection<Application>(COLLECTION_APPLICATION).UpdateOne(filter, updateCommand);
            return application;
        }

        public Application GetApplicationById(string id)
        {
                Application result = new Application();
                IMongoDatabase db = MongoDbManager.GetMongoDataBase();
                var filter = Builders<Application>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(id)));
                result = db.GetCollection<Application>(COLLECTION_APPLICATION).Find<Application>(filter).FirstOrDefault();
                return result;
        }


        public List<T> GetAll<T>()
        {
            try
            {
                List<T> result = new List<T>();
                IMongoDatabase db = MongoDbManager.GetMongoDataBase();
                result = db.GetCollection<T>(COLLECTION_LOOKUP_NOTIFICATION_TYPE).Find<T>(FilterDefinition<T>.Empty).ToList();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
