﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain;
using Raqeeb.Domain.Entities;
using Raqeeb.Framework.Core;
using System.Web;
using System.Configuration;
using System;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;
using System.Linq;

namespace Raqeeb.DataAccess
{
    public class RolesDAL : DataAccessBase
    {
        #region Constants
        #region Fields
        protected const string ROLE_FIELD = "ROLE";
        protected const string IS_ACTIVE_FIELD = "IS_ACTIVE";
        protected const string ID_FIELD = "_id";
        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();
        #endregion
        #endregion

        #region Methods

        public List<Domain.DataModel.Role_Master> GetAll()
        {
            //List<Roles> result = new List<Roles>();
            //IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            //var filter = Builders<Roles>.Filter.Empty;

            //result = db.GetCollection<Roles>("ROLES").Find<Roles>(filter).ToList();

            List<Domain.DataModel.Role_Master> roleList = aDPEntities.Role_Master.ToList();
            return roleList;
        }

        public List<Domain.DataModel.Role_Master> GetRolesType()
        {
            //List<Roles> result = new List<Roles>();
            //IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            //var filter = Builders<Roles>.Filter.Empty;
            //filter = filter & Builders<Roles>.Filter.Eq(IS_ACTIVE_FIELD, true);
            //result = db.GetCollection<Roles>("ROLES").Find<Roles>(filter).ToList();
            //return result;
           return aDPEntities.Role_Master.ToList();
        }

        public Roles Add(Roles role)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<Roles>("ROLES").InsertOne(role);
            return role;
        }

        public Roles Inactive(Roles role)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(role.IdString);
            bool active = role.IsActive ? false : true;
            var filter = Builders<Roles>.Filter.Eq(ID_FIELD, id);

            var updateCommand = Builders<Roles>.Update.Set("IS_ACTIVE", active);

            db.GetCollection<Roles>("ROLES").UpdateOne(filter, updateCommand);
            return role;
        }

        public Roles Update(Roles role)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(role.IdString);
            var filter = Builders<Roles>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<Roles>.Update
                .Set("ROLE", role.Role)
                .Set("MENU_ID",role.MenuId)
                .Set("IS_ACTIVE", role.IsActive);

            db.GetCollection<Roles>("ROLES").UpdateOne(filter, updateCommand);

            return role;
        }

        public Roles GetMenuByUser_Role(string roles)
        {
            Roles result = new Roles();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Roles>.Filter.Empty;
            filter = filter & Builders<Roles>.Filter.Eq(ROLE_FIELD, roles);
            result = db.GetCollection<Roles>("ROLES").Find<Roles>(filter).FirstOrDefault();
            return result;
        }

        public Roles Delete(Roles role)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(role.IdString);
            var filter = Builders<Roles>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<Roles>("ROLES").DeleteOne(filter);
            return role;
        }


        #endregion
    }
}
