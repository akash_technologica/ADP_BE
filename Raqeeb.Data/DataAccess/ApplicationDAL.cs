﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain;

namespace Raqeeb.DataAccess
{
    public class ApplicationDAL : DataAccessBase
    {
        #region Constants
        #region Fields
        protected const string ARABIC_NAME_FIELD = "ARABIC_NAME";
        protected const string ENGLISH_NAME_FIELD = "ENGLISH_NAME";
        protected const string MODIFIED_BY_FIELD = "MODIFIED_BY";
        protected const string MODIFICATION_DATE_FIELD = "MODIFICATION_DATE";
        protected const string USER_NAME_FIELD = "USER_NAME";
        protected const string PASSWORD_FIELD = "PASSWORD";
        protected const string SECRET_KEY_FIELD = "SECRET_KEY";
        protected const string API_KEY_FIELD = "API_KEY";
        #endregion
        #endregion

        #region Methods
        public Application Add(Application application)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<Application>(COLLECTION_APPLICATION).InsertOne(application);
            return application;
        }
        public Application Update(Application application)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Application>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(application.IdString)));
            var updateCommand = Builders<Application>.Update
                .Set(ARABIC_NAME_FIELD, application.ArabicName)
                .Set(ENGLISH_NAME_FIELD, application.EnglishName)
                .Set(MODIFICATION_DATE_FIELD, application.ModificationDate)
            .Set(MODIFIED_BY_FIELD, application.ModifiedBy);
            db.GetCollection<Application>(COLLECTION_APPLICATION).UpdateOne(filter, updateCommand);
            return application;
        }

        public Application GetApplicationById(string id)
        {
            Application result = new Application();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Application>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(id)));
            result = db.GetCollection<Application>(COLLECTION_APPLICATION).Find<Application>(filter).FirstOrDefault();
            return result;
        }


        public Application GetAccess(string userName, string password)
        {
            Application result = new Application();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<Application>.Empty;
            query = query & Builders<Application>.Filter.Eq(USER_NAME_FIELD, userName);
            query = query & Builders<Application>.Filter.Eq(PASSWORD_FIELD, password);

            result = db.GetCollection<Application>(COLLECTION_APPLICATION).Find<Application>(query).FirstOrDefault();
            return result;
        }

        public Application GetAccessByApiKeys(string apiKey, string secretKey)
        {
            Application result = new Application();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<Application>.Empty;
            query = query & Builders<Application>.Filter.Eq(API_KEY_FIELD, apiKey);
            query = query & Builders<Application>.Filter.Eq(SECRET_KEY_FIELD, secretKey);

            result = db.GetCollection<Application>(COLLECTION_APPLICATION).Find<Application>(query).FirstOrDefault();
            return result;
        }

        public Application GetApplicationByEmail(string email)
        {
            Application result = new Application();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Application>.Filter.Eq(USER_NAME_FIELD,email);
            result = db.GetCollection<Application>(COLLECTION_APPLICATION).Find<Application>(filter).FirstOrDefault();
            return result;
        }
        #endregion
    }
}
