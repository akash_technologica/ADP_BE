﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;

namespace Raqeeb.DataAccess.DataAccess
{
    public class CompanyConfigurationDAL : DataAccessBase
    {
        #region Methods

        public List<CompanyConfiguration> GetAll()
        {
            List<CompanyConfiguration> result = new List<CompanyConfiguration>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<CompanyConfiguration>.Filter.Empty;

            result = db.GetCollection<CompanyConfiguration>("COMPANY_CONFIGURATION").Find<CompanyConfiguration>(filter).ToList();
            return result;
        }

        public List<CompanyConfiguration> GetActiveCompany()
        {
            List<CompanyConfiguration> result = new List<CompanyConfiguration>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<CompanyConfiguration>.Filter.Empty;
            filter = filter & Builders<CompanyConfiguration>.Filter.Eq("IsActive", true);
            result = db.GetCollection<CompanyConfiguration>("COMPANY_CONFIGURATION").Find<CompanyConfiguration>(filter).ToList();
            return result;
        }

        public CompanyConfiguration Add(CompanyConfiguration company)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<CompanyConfiguration>("COMPANY_CONFIGURATION").InsertOne(company);
            return company;
        }

        public CompanyConfiguration Inactive(CompanyConfiguration company)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(company.IdString);
            bool active = company.IsActive ? false : true;
            var filter = Builders<CompanyConfiguration>.Filter.Eq(ID_FIELD, id);

            var updateCommand = Builders<CompanyConfiguration>.Update.Set("IsActive", active);

            db.GetCollection<CompanyConfiguration>("COMPANY_CONFIGURATION").UpdateOne(filter, updateCommand);
            return company;
        }

        public CompanyConfiguration Update(CompanyConfiguration company)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(company.IdString);
            var filter = Builders<CompanyConfiguration>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<CompanyConfiguration>.Update
                .Set("CompanyName", company.CompanyName)
                .Set("Address", company.Address)
                .Set("CountryId", company.CountryId)
                .Set("Email", company.Email)
                .Set("LicenseEnd", company.LicenseEnd)
                .Set("LicenseStart", company.LicenseStart)
                .Set("Mobile", company.Mobile)
                .Set("Notes", company.Notes)
                .Set("IsActive", company.IsActive);


            db.GetCollection<CompanyConfiguration>("COMPANY_CONFIGURATION").UpdateOne(filter, updateCommand);

            return company;
        }

        public CompanyConfiguration Delete(CompanyConfiguration company)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(company.IdString);
            var filter = Builders<CompanyConfiguration>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<CompanyConfiguration>("COMPANY_CONFIGURATION").DeleteOne(filter);
            return company;
        }


        #endregion
    }
}
