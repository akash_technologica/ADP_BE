﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class GsmMasterDAL : DataAccessBase
    {
        #region Methods

        public List<GSM_Master> GetAll()
        {
            List<GSM_Master> result = new List<GSM_Master>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<GSM_Master>.Filter.Empty;

            result = db.GetCollection<GSM_Master>("GSM_MASTER").Find<GSM_Master>(filter).ToList();
            return result;
        }

        public GSM_Master Add(GSM_Master gsm)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<GSM_Master>("GSM_MASTER").InsertOne(gsm);
            return gsm;
        }


        public GSM_Master Update(GSM_Master gsm)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(gsm.IdString);
            var filter = Builders<GSM_Master>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<GSM_Master>.Update
                .Set("Description", gsm.Description)
                .Set("IsActive", gsm.IsActive)
                .Set("GSM_Code", gsm.GSM_Code)
                .Set("OperatorName", gsm.OperatorName);

            db.GetCollection<GSM_Master>("GSM_MASTER").UpdateOne(filter, updateCommand);

            return gsm;
        }

       

        #endregion
    }
}
