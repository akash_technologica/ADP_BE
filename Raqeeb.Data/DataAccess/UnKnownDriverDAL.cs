﻿using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class UnKnownDriverDAL : DataAccessBase
    {
        #region Methods
        public List<UnknownDriverLog> InsertBulk(List<UnknownDriverLog> logs)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<UnknownDriverLog>("UNKNOWN_DRIVER_LOGS").InsertMany(logs);
            return logs;
        }

        public string GetdriverCode(int vehicleCode, DateTime startDate)
        {
            List<UnknownDriverLog> result = new List<UnknownDriverLog>();
           // int? driverCode;

            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            // DateTime thisDate = new DateTime(2019, 10, 01);
            var filter = Builders<UnknownDriverLog>.Filter.Empty;
            var query = FilterDefinition<UnknownDriverLog>.Empty;
            query = query & Builders<UnknownDriverLog>.Filter.Lte("LogDate", startDate);
            query = query & Builders<UnknownDriverLog>.Filter.Eq("VehicleId", vehicleCode);

            result = db.GetCollection<UnknownDriverLog>("UNKNOWN_DRIVER_LOGS").Find<UnknownDriverLog>(query).ToList();
            foreach (var item in result)
            {
                if (item.DriverCode != null)
                {
                    return item.DriverCode;
                }

            }

            return "";
        }
        #endregion

    }
}
