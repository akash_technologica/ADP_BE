﻿using Raqeeb.Domain.DataModel;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Enums;
using Raqeeb.Domain.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class MediaDownloadRequestDAL : DataAccessBase
    {
        ADPEntities aDPEntities = new ADPEntities();

        public MediaRequestDownloadDetails GetAllDownloadRequestData(SearchCriteria<MediaDownloadRequestFilter> searchCriteria)
        {
            List<MediaDownloadRequestDto> downloadRequestDtos = new List<MediaDownloadRequestDto>();
            MediaRequestDownloadDetails mediaRequestDownloadDetails = new MediaRequestDownloadDetails();

            List<Raqeeb.Domain.DataModel.MediaDownloadRequest> mediaDownloadRequests = new List<Domain.DataModel.MediaDownloadRequest>();

            if (searchCriteria.Filters.StartDate == null && searchCriteria.Filters.EndDate == null)
            {
                DateTime today = DateTime.Today.AddDays(-30);

                string startDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
                searchCriteria.Filters.StartDate = Convert.ToDateTime(startDate);
                searchCriteria.Filters.EndDate = DateTime.Now;
            }
            if (searchCriteria.Group == 0)
            {
                if (searchCriteria.GlobalFilter != null && searchCriteria.GlobalFilter.Length > 0)
                {
                    mediaDownloadRequests = aDPEntities.MediaDownloadRequests
                                          .Where(c => c.RequestDate >= searchCriteria.Filters.StartDate && c.RequestDate <= searchCriteria.Filters.EndDate &&
                                          c.CarLicense.ToLower().Contains(searchCriteria.GlobalFilter.ToLower()
                                          ))
                                          .OrderBy(x => x.Id)
                                          .Skip(searchCriteria.First)
                                          .Take(searchCriteria.Rows)
                                          .ToList();
                    mediaRequestDownloadDetails.TotalRequests = aDPEntities.MediaDownloadRequests
                        .Where(c => c.RequestDate >= searchCriteria.Filters.StartDate && c.RequestDate <= searchCriteria.Filters.EndDate && c.CarLicense.ToLower().Contains(searchCriteria.GlobalFilter.ToLower()))
                        .Count();
                }
                else
                {
                    mediaDownloadRequests = aDPEntities.MediaDownloadRequests
          .Where(c => c.RequestDate >= searchCriteria.Filters.StartDate && c.RequestDate <= searchCriteria.Filters.EndDate)
          .OrderBy(x => x.Id)
          .Skip(searchCriteria.First)
          .Take(searchCriteria.Rows)
          .ToList();
                    mediaRequestDownloadDetails.TotalRequests = aDPEntities.MediaDownloadRequests
                        .Where(c => c.RequestDate >= searchCriteria.Filters.StartDate && c.RequestDate <= searchCriteria.Filters.EndDate).Count();
                }
            }
            else
            {
                if (searchCriteria.GlobalFilter != null && searchCriteria.GlobalFilter.Length > 0)
                {
                    mediaDownloadRequests = aDPEntities.MediaDownloadRequests.Where(c => c.RequestDate >= searchCriteria.Filters.StartDate && c.RequestDate <= searchCriteria.Filters.EndDate && c.CarLicense.ToLower().Contains(searchCriteria.GlobalFilter.ToLower()))
                        .OrderBy(x => x.Id)
                    .Skip(searchCriteria.First)
                    .Take(searchCriteria.Rows)
                    .ToList();
                    mediaRequestDownloadDetails.TotalRequests = aDPEntities.MediaDownloadRequests.Where(c => c.RequestDate >= searchCriteria.Filters.StartDate &&
                    c.RequestDate <= searchCriteria.Filters.EndDate && c.CarLicense.ToLower().Contains(searchCriteria.GlobalFilter.ToLower())).Count();
                }
                else
                {
                    mediaDownloadRequests = aDPEntities.MediaDownloadRequests.Where(c => c.RequestDate >= searchCriteria.Filters.StartDate && c.RequestDate < searchCriteria.Filters.EndDate).OrderBy(x => x.Id)
                .Skip(searchCriteria.First)
                .Take(searchCriteria.Rows)
                .ToList();
                    mediaRequestDownloadDetails.TotalRequests = aDPEntities.MediaDownloadRequests
                    .Where(c => c.RequestDate >= searchCriteria.Filters.StartDate && c.RequestDate < searchCriteria.Filters.EndDate).Count();
                }

            }
            foreach (var item in mediaDownloadRequests)
            {
                MediaDownloadRequestDto mediaDownloadRequestDto = new MediaDownloadRequestDto()
                {
                    Id = item.Id,
                    RequestDate = item.RequestDate,
                    CarLicense = item.CarLicense,
                    StartTime = item.StartTime,
                    EndTime = item.EndTime,
                    FilePath = item.FilePath,
                    TaskId = item.TaskId,
                    Status = item.Status,
                    UserId = item.UserId
                };
                downloadRequestDtos.Add(mediaDownloadRequestDto);
                if (item.Status == DownloadRequestStatus.Pause.ToString() || item.Status == DownloadRequestStatus.Analysisng.ToString() || item.Status == DownloadRequestStatus.Waiting.ToString())
                {
                    mediaRequestDownloadDetails.TotalWaiting += 1;
                }
                else if (item.Status == DownloadRequestStatus.ConnectionNumberIsLimited.ToString()
                    || item.Status == DownloadRequestStatus.UnDone.ToString()
                    || item.Status == DownloadRequestStatus.NoneRecordingFile.ToString()
                    || item.Status == DownloadRequestStatus.DownloadFailed.ToString()
                    || item.Status == DownloadRequestStatus.TaskFailed.ToString())
                {
                    mediaRequestDownloadDetails.TotalFailed += 1;
                }
                else if (item.Status == DownloadRequestStatus.TaskComplete.ToString())
                {
                    mediaRequestDownloadDetails.TotalSuccess += 1;
                }

                mediaRequestDownloadDetails.MediaDownloadRequestList = downloadRequestDtos;
            }
            return mediaRequestDownloadDetails;
        }
        public void AddNewDownloadRequest(MediaDownloadRequest mediaDownloadRequest)
        {

            aDPEntities.MediaDownloadRequests.Add(mediaDownloadRequest);
            aDPEntities.SaveChanges();
        }
        public List<Raqeeb.Domain.DataModel.MediaDownloadRequest> exportAll()
        {
            return aDPEntities.MediaDownloadRequests.ToList();
        }
    }
}
