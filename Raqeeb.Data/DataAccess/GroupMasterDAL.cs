﻿using Raqeeb.Domain.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class GroupMasterDAL
    {
        #region Constants
        #region Fields

        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();

        #endregion
        #endregion
        #region Methods
        public GroupMaster Add(GroupMaster group)
        {
            GroupMaster groupDet = new GroupMaster()
            {
               GroupName = group.GroupName,
               CreatedBy = group.CreatedBy,
               CreatedDate = DateTime.Now


            };
            aDPEntities.GroupMasters.Add(groupDet);
            aDPEntities.SaveChanges();
            
            return groupDet;
        }

        public GroupMaster Delete(GroupMaster group)
        {
            GroupMaster grpDet = aDPEntities.GroupMasters.Where(c => c.GroupId == group.GroupId).FirstOrDefault();
           // grpDet.IsActive = group.IsActive;
           // grpDet.UpdatedBy = group.UpdatedBy;
          //  grpDet.UpdatedDate = DateTime.Now;

            aDPEntities.Entry(grpDet).State = System.Data.Entity.EntityState.Modified;
            aDPEntities.SaveChanges();
            return group;
        }

       
        public List<GroupMaster> GetAll()
        {
            List<GroupMaster> grpDetList = new List<GroupMaster>();
            try
            {
                 grpDetList = aDPEntities.GroupMasters.Select(c=>c).ToList();

                return grpDetList;

            }
            catch (Exception ex)
            {
                Exception tt = ex;
                return grpDetList;
            }
            
        }


        public GroupMaster UpdateGroup(GroupMaster group)
        {
            Raqeeb.Domain.DataModel.GroupMaster grpDet = aDPEntities.GroupMasters.Where(c => c.GroupId == group.GroupId).FirstOrDefault();
            grpDet.CreatedBy = group.CreatedBy;
            grpDet.GroupName = group.GroupName;
            grpDet.CreatedDate = DateTime.Now;

            try
            {
                aDPEntities.Entry(group).State = System.Data.Entity.EntityState.Modified;
                aDPEntities.SaveChanges();
            }
            catch (Exception ex)
            {

                
            }

           
            return group;
        }

        public string GetGroupName(int? gruoupId)
        {
            string grp_name = "";
            if(gruoupId != null)
            {
                return aDPEntities.GroupMasters.Where(c => c.GroupId == gruoupId.Value).Select(c => c.GroupName).FirstOrDefault();
                 
            }
            else
            {
                return grp_name;
            }
           
        }

        #endregion
    }
}
