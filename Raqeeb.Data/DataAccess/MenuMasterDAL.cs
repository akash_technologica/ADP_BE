﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class MenuMasterDAL: DataAccessBase
    {
        #region Constants
        #region Fields
        protected const string MENU_FIELD = "MENU_NAME";
        protected const string IS_ACTIVE_FIELD = "IS_ACTIVE";
        protected const string ID_FIELD = "_id";
        protected const string MENU_URL_FIELD = "MENU_URL";
        #endregion
        #endregion
        #region Methods

        public List<MenuMaster> GetAll()
        {
            List<MenuMaster> result = new List<MenuMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<MenuMaster>.Filter.Empty;

            result = db.GetCollection<MenuMaster>("MENU_MASTER").Find<MenuMaster>(filter).ToList();
            return result;
        }

        public MenuMaster GetMenuById(string item)
        {
           
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(item);
            var filter = Builders<MenuMaster>.Filter.Empty;
            filter = filter & Builders<MenuMaster>.Filter.Eq(ID_FIELD, id);
           return db.GetCollection<MenuMaster>("MENU_MASTER").Find<MenuMaster>(filter).FirstOrDefault();
           
        }

        public List<MenuMaster> GetMenuType()
        {
            List<MenuMaster> result = new List<MenuMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<MenuMaster>.Filter.Empty;
            filter = filter & Builders<MenuMaster>.Filter.Eq(IS_ACTIVE_FIELD, true);
            result = db.GetCollection<MenuMaster>("MENU_MASTER").Find<MenuMaster>(filter).ToList();
            return result;
        }

        public MenuMaster Add(MenuMaster menu)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<MenuMaster>("MENU_MASTER").InsertOne(menu);
            return menu;
        }

        public MenuMaster Inactive(MenuMaster menu)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(menu.IdString);
            bool active = menu.IsActive ? false : true;
            var filter = Builders<MenuMaster>.Filter.Eq(ID_FIELD, id);

            var updateCommand = Builders<MenuMaster>.Update.Set("IS_ACTIVE", active);

            db.GetCollection<MenuMaster>("MENU_MASTER").UpdateOne(filter, updateCommand);
            return menu;
        }

        public MenuMaster Update(MenuMaster menu)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(menu.IdString);
            var filter = Builders<MenuMaster>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<MenuMaster>.Update
                .Set("MENU_NAME", menu.MenuName)
                .Set("MENU_URL", menu.RoutingUrl)
                .Set("IS_ACTIVE", menu.IsActive);

            db.GetCollection<MenuMaster>("MENU_MASTER").UpdateOne(filter, updateCommand);

            return menu;
        }

        public MenuMaster Delete(MenuMaster menu)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(menu.IdString);
            var filter = Builders<MenuMaster>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<MenuMaster>("MENU_MASTER").DeleteOne(filter);
            return menu;
        }


        #endregion

    }
}
