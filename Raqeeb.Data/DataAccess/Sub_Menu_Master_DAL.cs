﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class Sub_Menu_Master_DAL:DataAccessBase
    {
        #region Constants
        #region Fields
        protected const string SUB_MENU_FIELD = "SUB_MENU_NAME";
        protected const string IS_ACTIVE_FIELD = "IS_ACTIVE";
        protected const string ID_FIELD = "_id";
        protected const string MENU_URL_FIELD = "SUB_MENU_URL";
        protected const string MENU_ID = "MENU_ID";
        #endregion
        #endregion
        #region Methods

        public List<SubMenuMaster> GetAll()
        {
            List<SubMenuMaster> result = new List<SubMenuMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<SubMenuMaster>.Filter.Empty;

            result = db.GetCollection<SubMenuMaster>("SUB_MENU_MASTER").Find<SubMenuMaster>(filter).ToList();
            return result;
        }

        public SubMenuMaster GetMenuById(string item)
        {

            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(item);
            var filter = Builders<SubMenuMaster>.Filter.Empty;
            filter = filter & Builders<SubMenuMaster>.Filter.Eq(ID_FIELD, id);
            return db.GetCollection<SubMenuMaster>("SUB_MENU_MASTER").Find<SubMenuMaster>(filter).FirstOrDefault();

        }

        public List<SubMenuMaster> GetMenuType()
        {
            List<SubMenuMaster> result = new List<SubMenuMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<SubMenuMaster>.Filter.Empty;
            filter = filter & Builders<SubMenuMaster>.Filter.Eq(IS_ACTIVE_FIELD, true);
            result = db.GetCollection<SubMenuMaster>("SUB_MENU_MASTER").Find<SubMenuMaster>(filter).ToList();
            return result;
        }

        public List<SubMenuMaster> GetSubMenuById(string id)
        {

            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            
            var filter = Builders<SubMenuMaster>.Filter.Empty;
            filter = filter & Builders<SubMenuMaster>.Filter.Eq(MENU_ID, id);
            return db.GetCollection<SubMenuMaster>("SUB_MENU_MASTER").Find<SubMenuMaster>(filter).ToList();

        }

        public SubMenuMaster Add(SubMenuMaster menu)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<SubMenuMaster>("SUB_MENU_MASTER").InsertOne(menu);
            return menu;
        }

        public SubMenuMaster Inactive(SubMenuMaster menu)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(menu.IdString);
            bool active = menu.IsActive ? false : true;
            var filter = Builders<SubMenuMaster>.Filter.Eq(ID_FIELD, id);

            var updateCommand = Builders<SubMenuMaster>.Update.Set("IS_ACTIVE", active);

            db.GetCollection<SubMenuMaster>("SUB_MENU_MASTER").UpdateOne(filter, updateCommand);
            return menu;
        }

        public SubMenuMaster Update(SubMenuMaster menu)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(menu.IdString);
            var filter = Builders<SubMenuMaster>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<SubMenuMaster>.Update
                .Set("MENU_NAME", menu.SubMenuName)
                .Set("MENU_URL", menu.SubMenuUrl)
                .Set("IS_ACTIVE", menu.IsActive);

            db.GetCollection<SubMenuMaster>("SUB_MENU_MASTER").UpdateOne(filter, updateCommand);

            return menu;
        }

        public List<SubMenuMaster> GetSubMenuByMenuId(string menuId)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
           
            var filter = Builders<SubMenuMaster>.Filter.Eq(MENU_ID, menuId);
            return db.GetCollection<SubMenuMaster>("SUB_MENU_MASTER").Find<SubMenuMaster>(filter).ToList();
            
        }

        public SubMenuMaster Delete(SubMenuMaster menu)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(menu.IdString);
            var filter = Builders<MenuMaster>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<MenuMaster>("SUB_MENU_MASTER").DeleteOne(filter);
            return menu;
        }


        #endregion
    }
}
