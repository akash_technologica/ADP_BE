﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class ScoreSetUpDAL : DataAccessBase
    {
        #region Methods

        EventMasterDAL eventMasterDAL = new EventMasterDAL();
        public List<ScoreSetUp> GetAll()
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<ScoreSetUp>.Filter.Empty;
            var list =  db.GetCollection<ScoreSetUp>("SCORE-SETUP").Find<ScoreSetUp>(filter).ToList();
            var eventList = eventMasterDAL.GetAll();
            foreach (var item in list)
            {
                foreach(var masteritem in eventList)
                {
                    if(item.EventTypeId == masteritem.Code)
                    {
                        item.EventType = masteritem.Description;
                    }
                }
            }
            return list;
        }



        public void Update(ScoreSetUp setup)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<ScoreSetUp>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(setup.IdString)));
            var updateCommand = Builders<ScoreSetUp>.Update
                .Set("Max_occurrence", setup.Max_occurrence)
                .Set("Min_occurrence", setup.Min_occurrence)
                .Set("Score", setup.Score);

            db.GetCollection<ScoreSetUp>("SCORE-SETUP").UpdateOne(filter, updateCommand);
        }

        public void UpdateScoreSetUP(ScoreSetUpMaster setup)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<ScoreSetUpMaster>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(setup.IdString)));
            var updateCommand = Builders<ScoreSetUpMaster>.Update
                .Set("IsIncludedForScoring", setup.IsIncludedForScoring);
                

            db.GetCollection<ScoreSetUpMaster>("SCORE_SETUP_MASTER").UpdateOne(filter, updateCommand);
        }

        //public Setup GetByClassificationEventTyp(int classificationType)
        //{
        //    Setup result = new Setup();
        //    IMongoDatabase db = MongoDbManager.GetMongoDataBase();
        //    var filter = Builders<Setup>.Filter.Eq("ClassificationType", classificationType);
        //    result = db.GetCollection<Setup>("SETUP").Find<Setup>(filter).FirstOrDefault();
        //    return result;
        //}


        public void Add(ScoreSetUp setup)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            
            db.GetCollection<ScoreSetUp>("SCORE-SETUP").InsertOne(setup);
        }

        #endregion
        //public List<ScoreSetUp> GetScoreSetUpData()
        //{
        //    IMongoDatabase db = MongoDbManager.GetMongoDataBase();
        //    var filter = Builders<ScoreSetUp>.Filter.Empty;
        //    var collection = db.GetCollection<ScoreSetUp>("SCORE-SETUP");
        //    var scoresetup = collection.Find(filter).ToList();
        //    return scoresetup;
        //}

        public List<ScoreSetUpMaster> GetScoreSetUpData()
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<ScoreSetUpMaster>.Filter.Empty;
            var collection = db.GetCollection<ScoreSetUpMaster>("SCORE_SETUP_MASTER");
            var scoresetup = collection.Find(filter).ToList();
            return scoresetup;
        }
        public ScoreSetUpMaster InsertBulk(ScoreSetUpMaster setup)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
           
            db.GetCollection<ScoreSetUpMaster>("SCORE_SETUP_MASTER").InsertOne(setup);
            
            return setup;
        }

    }
}
