﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class CityMasterDAL : DataAccessBase
    {
        #region Methods

        public List<CityMaster> GetAll()
        {
            List<CityMaster> result = new List<CityMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<CityMaster>.Filter.Empty;

            result = db.GetCollection<CityMaster>("CITY_MASTER").Find<CityMaster>(filter).ToList();
            return result;
        }

        public List<CityMaster> GetByCountryId(string countryId)
        {
            List<CityMaster> result = new List<CityMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<CityMaster>.Empty;
            query = query & Builders<CityMaster>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(countryId)));
            result = db.GetCollection<CityMaster>("CITY_MASTER").Find<CityMaster>(query).ToList();
            return result;
        }

        public CityMaster Add(CityMaster city)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<CityMaster>("CITY_MASTER").InsertOne(city);
            return city;
        }


        public CityMaster Update(CityMaster city)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(city.IdString);
            var filter = Builders<CityMaster>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<CityMaster>.Update
               
                .Set("EnglishName", city.EnglishName);

            db.GetCollection<CityMaster>("CITY_MASTER").UpdateOne(filter, updateCommand);

            return city;
        }

        public CityMaster Delete(CityMaster city)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(city.IdString);
            var filter = Builders<CityMaster>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<CityMaster>("CITY_MASTER").DeleteOne(filter);
            return city;
        }


        #endregion
    }
}
