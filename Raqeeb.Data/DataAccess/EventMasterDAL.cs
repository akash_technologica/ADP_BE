﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class EventMasterDAL : DataAccessBase
    {
        #region Constants
        #region Fields

        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();

        #endregion
        #endregion

        #region Methods
        public List<EventMaster> InsertBulk(List<EventMaster> events)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<EventMaster>("EVENT_MASTER").InsertMany(events);
            return events;
        }
        public EventMaster GetByCode(long? code)
        {
            EventMaster result = new EventMaster();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<EventMaster>.Filter.Eq("Code", code);
            result = db.GetCollection<EventMaster>("EVENT_MASTER").Find<EventMaster>(filter).FirstOrDefault();
            return result;
        }

        //public List<EventMaster> GetAll()
        //{
        //    List<EventMaster> result = new List<EventMaster>();
        //    IMongoDatabase db = MongoDbManager.GetMongoDataBase();
        //    var filter = Builders<EventMaster>.Filter.Empty;

        //    result = db.GetCollection<EventMaster>("EVENT_MASTER").Find<EventMaster>(filter).ToList();
        //    return result;
        //}

        public List<SubEventTypeMaster> InsertSubEventBulk(List<SubEventTypeMaster> subEventMasters)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<SubEventTypeMaster>("SUB_EVENT_MASTER").InsertMany(subEventMasters);
            return subEventMasters;
        }

        public List<SubEventTypeMaster> GetSubEventByEventCode(int code)
        {
            List<SubEventTypeMaster> result = new List<SubEventTypeMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<SubEventTypeMaster>.Filter.Eq("EventTypeCode", code);
            result = db.GetCollection<SubEventTypeMaster>("SUB_EVENT_MASTER").Find<SubEventTypeMaster>(filter).ToList();
            return result;
        }
        public SubEventTypeMaster GetSubEventCode(int code)
        {
            SubEventTypeMaster result = new SubEventTypeMaster();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<SubEventTypeMaster>.Filter.Eq("Code", code);
            result = db.GetCollection<SubEventTypeMaster>("SUB_EVENT_MASTER").Find<SubEventTypeMaster>(filter).FirstOrDefault();
            return result;
        }

        public string GetEventByIdString(string eventsId)
        {
            var id = BsonObjectId.Create(eventsId);

            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<EventMaster>.Filter.Eq("_id", id);
            return db.GetCollection<EventMaster>("EVENT_MASTER").Find<EventMaster>(filter).FirstOrDefault().Description;
            
        }

        public List<Raqeeb.Domain.DataModel.Event_Master> GetAll()
        {
            try
            {
                return aDPEntities.Event_Master.Select(c => c).ToList();            

            }
            catch (Exception ex)
            {
                Exception tt = ex;
                throw;
            }

        }

        #endregion
    }
}
