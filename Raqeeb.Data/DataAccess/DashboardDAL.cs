﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class DashboardDAL : DataAccessBase
    {
        #region Constants
        #region Fields

        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();

        #endregion
        #endregion
        #region Methods
        public Dashboard InsertOne(Dashboard dashboard)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<Dashboard>("DASHBOARD").InsertOne(dashboard);
            return dashboard;

        }

        public Dashboard CheckVehicleStatisticsExists(int month, int year)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Dashboard>.Filter.Empty;
            var query = FilterDefinition<Dashboard>.Empty;
            query = query & Builders<Dashboard>.Filter.Eq("MonthName", month);
            query = query & Builders<Dashboard>.Filter.Eq("YearName", year);


            return db.GetCollection<Dashboard>("DASHBOARD").Find<Dashboard>(query).FirstOrDefault(); 

        }

        public void Insert_Update_One(Dashboard dashboard, bool v)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            if (v)
            {
                db.GetCollection<Dashboard>("DASHBOARD").InsertOne(dashboard);
            }
            else
            {
                var id = BsonObjectId.Create(dashboard.Id);
                var filter = Builders<Dashboard>.Filter.Eq(ID_FIELD, id);
                var updateCommand = Builders<Dashboard>.Update
                    .Set("TotalDistanceDriven", dashboard.TotalDistanceDriven)
                    .Set("TotalDistraction", dashboard.TotalDistraction)
                    .Set("TotalDrivenTime", dashboard.TotalDrivenTime)
                    .Set("TotalDuration", dashboard.TotalDuration)
                    .Set("TotalEvents", dashboard.TotalEvents)
                    .Set("TotalFatigue", dashboard.TotalFatigue)
                    .Set("TotalForwardCollisionWarning", dashboard.TotalForwardCollisionWarning)
                    .Set("TotalFOV", dashboard.TotalFOV)
                    .Set("TotalHarshAcceleration", dashboard.TotalHarshAcceleration)
                    .Set("TotalHarshBraking", dashboard.TotalHarshBraking)
                    .Set("TotalLeftLaneDeparture", dashboard.TotalLeftLaneDeparture)
                    .Set("TotalMobileeyeTamperAlert", dashboard.TotalMobileeyeTamperAlert)
                    .Set("TotalNearmiss", dashboard.TotalNearmiss)
                    .Set("TotalOverRevving", dashboard.TotalOverRevving)
                    .Set("TotalOverspeeding", dashboard.TotalOverspeeding)
                    .Set("TotalPanicButton", dashboard.TotalPanicButton)
                    .Set("TotalPedestrianInDangerZone", dashboard.TotalPedestrianInDangerZone)
                    .Set("TotalPedestrianInForwardCollisionWarning", dashboard.TotalPedestrianInForwardCollisionWarning)
                    .Set("TotalRightLaneDeparture", dashboard.TotalRightLaneDeparture)
                    .Set("TotalStandingTime", dashboard.TotalStandingTime)
                    .Set("TotalTrafficSignRecognition", dashboard.TotalTrafficSignRecognition)
                    .Set("TotalTrafficSignRecognitionWarning", dashboard.TotalTrafficSignRecognitionWarning)
                    .Set("TotalTrips", dashboard.TotalTrips)
                    .Set("YearName", dashboard.YearName);
                  

                db.GetCollection<Dashboard>("DASHBOARD").UpdateOne(filter, updateCommand);
            }
        }

        public List<Domain.DataModel.Dashboard> GetAllDashboardData()
        {
            List<Domain.DataModel.Dashboard> dashboard = aDPEntities.Dashboards.ToList();
            return dashboard;
        }
        #endregion
    }
}
