﻿using MongoDB.Driver;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class WebServiceSetupDAL : DataAccessBase
    {
        public WebServiceSetup Insert(WebServiceSetup webServiceSetup)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<WebServiceSetup>("WEB_SERVICE_SETUP").InsertOne(webServiceSetup);
            return webServiceSetup;
        }

        public WebServiceSetup GetByType(WebServiceType type)
        {
            WebServiceSetup result = new WebServiceSetup();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<WebServiceSetup>.Filter.Eq("Type", type);
            result = db.GetCollection<WebServiceSetup>("WEB_SERVICE_SETUP").Find<WebServiceSetup>(filter).FirstOrDefault();
            return result;
        }


        public WebServiceSetup Update(WebServiceSetup webServiceSetup)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<WebServiceSetup>.Filter.Eq("Type", webServiceSetup.Type);
            var updateCommand = Builders<WebServiceSetup>.Update
                .Set("LasItemId", webServiceSetup.LasItemId)
                .Set("LastSynchTime", webServiceSetup.LastSynchTime)
            .Set("IsFinished", webServiceSetup.IsFinished)
            .Set("NumberOfExecution", webServiceSetup.NumberOfExecution);
            db.GetCollection<WebServiceSetup>("WEB_SERVICE_SETUP").UpdateOne(filter, updateCommand);
            return webServiceSetup;
        }


        public PagedSearchResultMongo<WebServiceSetup> Search()
        {
            try
            {
                SearchCriteria<WebServiceFilter> searchCriteria = new SearchCriteria<WebServiceFilter>();
                PagedSearchResultMongo<WebServiceSetup> result = new PagedSearchResultMongo<WebServiceSetup>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<WebServiceSetup>.Empty;

                result.NumberOfRecords = db.GetCollection<WebServiceSetup>("WEB_SERVICE_SETUP").Count(query);
                result.Collection = db.GetCollection<WebServiceSetup>("WEB_SERVICE_SETUP").Find<WebServiceSetup>(query)
                     .SortByDescending(p => p.LastSynchTime).Skip(searchCriteria.ItemsToSkip).Limit(searchCriteria.PageSize).ToList();
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
