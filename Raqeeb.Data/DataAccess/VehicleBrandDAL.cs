﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class VehicleBrandDAL : DataAccessBase
    {
        #region Constants
        #region Fields

        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();

        #endregion
        #endregion
        #region Methods

        public List<VehicleBrandMaster> GetAll()
        {
            List<VehicleBrandMaster> result = new List<VehicleBrandMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<VehicleBrandMaster>.Filter.Empty;

            result = db.GetCollection<VehicleBrandMaster>("VEHICLE_BRAND_MASTER").Find<VehicleBrandMaster>(filter).ToList();
            return result;
        }

        

        public VehicleBrandMaster Add(VehicleBrandMaster brand)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<VehicleBrandMaster>("VEHICLE_BRAND_MASTER").InsertOne(brand);
            return brand;
        }


        public VehicleBrandMaster Update(VehicleBrandMaster brand)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(brand.IdString);
            var filter = Builders<VehicleBrandMaster>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<VehicleBrandMaster>.Update

                .Set("Name", brand.Name);

            db.GetCollection<VehicleBrandMaster>("VEHICLE_BRAND_MASTER").UpdateOne(filter, updateCommand);

            return brand;
        }

        public VehicleBrandMaster Delete(VehicleBrandMaster brand)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(brand.IdString);
            var filter = Builders<VehicleBrandMaster>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<VehicleBrandMaster>("VEHICLE_BRAND_MASTER").DeleteOne(filter);
            return brand;
        }

        public List<Raqeeb.Domain.DataModel.VehicleBrandMaster> GetBrandList()
        {
            try
            {
                List<Domain.DataModel.VehicleBrandMaster> grpDetList = aDPEntities.VehicleBrandMasters.Select(c => c).ToList();

                return grpDetList;

            }
            catch (Exception ex)
            {
                Exception tt = ex;
                throw;
            }

        }
        


        #endregion
    }
}
