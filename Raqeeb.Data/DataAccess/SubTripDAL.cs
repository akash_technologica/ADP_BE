﻿using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class SubTripDAL : DataAccessBase
    {
        #region Methods
        public List<SubTrip> InsertBulk(List<SubTrip> subTrips)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            if (subTrips.Count > 0) 
            db.GetCollection<SubTrip>("SUB_TRIP").InsertMany(subTrips);
            return subTrips;
        }


        public SubTrip GetByCode(long code)
        {
            SubTrip result = new SubTrip();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<SubTrip>.Filter.Eq("Code", code);
            result = db.GetCollection<SubTrip>("SUB_TRIP").Find<SubTrip>(filter).FirstOrDefault();
            return result;
        }

        public List<SubTrip> GetByTripCode(long code)
        {
            List<SubTrip> result = new List<SubTrip>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<SubTrip>.Filter.Eq("TripCode", code);
            result = db.GetCollection<SubTrip>("SUB_TRIP").Find<SubTrip>(filter).ToList();
            return result;
        }
        #endregion
    }
}
