﻿using MongoDB.Driver;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class LivePositionDAL : DataAccessBase
    {
        #region Methods
        #region Constants
        #region Fields

        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();

        #endregion
        #endregion
        public List<LivePosition> InsertBulk(List<LivePosition> livePositions)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            if(livePositions.Count>0)
            db.GetCollection<LivePosition>("LIVE_POSITION").InsertMany(livePositions);
            return livePositions;
        }

        public Domain.DataModel.LivePosition GetByCode(string code)
        {

           Domain.DataModel.LivePosition lastLiveData = aDPEntities.LivePositions.Where(c => c.VehicleCode == code).OrderByDescending(c=>c.Id).FirstOrDefault() ;

            return lastLiveData;
            //IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            //var filter = Builders<LivePosition>.Filter.Eq("Code", code);
            //result = db.GetCollection<LivePosition>("LIVE_POSITION").Find<LivePosition>(filter).FirstOrDefault();
            //return result;
        }

        public int? GetDriverId(int vehhicleCode, DateTime startDate)
        {
            List<LivePosition> result = new List<LivePosition>();
            int? driverCode;
            LivePosition livePosition = new LivePosition();

            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
           
           // DateTime thisDate = new DateTime(2019, 10, 01);
            var filter = Builders<LivePosition>.Filter.Empty;
            var query = FilterDefinition<LivePosition>.Empty;
            query = query & Builders<LivePosition>.Filter.Gte("LocationTIme", startDate.Date);
            query = query & Builders<LivePosition>.Filter.Eq("VehicleCode", vehhicleCode);

            result = db.GetCollection<LivePosition>("LIVE_POSITION").Find<LivePosition>(query).ToList();
            if (result.Count > 0)
            {
                livePosition = result[result.Count - 1];



                if (livePosition.LocationTIme.Date == startDate.Date)
                {
                    if (livePosition.LocationTIme.Hour == startDate.Hour)
                    {
                        return driverCode = livePosition.DriverCode;
                    }
                    else
                    {
                        return driverCode = 0;
                    }
                }
                else
                {
                    return driverCode = 0;
                }

            }
            else
            {
                return driverCode = 0;
            }

            //return driverCode = 0;
        }

        public List<LivePosition> GetDataBetweenStartAndEnd(long startPositionCode, long endPositionCode, int vehicleCode)
        {
            try
            {
                PagedSearchResultMongo<LivePosition> result = new PagedSearchResultMongo<LivePosition>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<LivePosition>.Empty;

                query = query & Builders<LivePosition>.Filter.Gte("OriginalCode", startPositionCode);
                query = query & Builders<LivePosition>.Filter.Lte("OriginalCode", endPositionCode);
                query = query & Builders<LivePosition>.Filter.Eq("VehicleCode", vehicleCode);

                result.NumberOfRecords = db.GetCollection<LivePosition>("LIVE_POSITION").Count(query);
                result.Collection = db.GetCollection<LivePosition>("LIVE_POSITION").Find<LivePosition>(query)
                   .ToList();
                return result.Collection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<LivePosition> GetBetweenStartAndEnd(string startPositionCode, string endPositionCode, int vehicleCode)
        {
            try
            {
                PagedSearchResultMongo<LivePosition> result = new PagedSearchResultMongo<LivePosition>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<LivePosition>.Empty;

                query = query & Builders<LivePosition>.Filter.Gte("Code", startPositionCode);
                query = query & Builders<LivePosition>.Filter.Lte("Code", endPositionCode);
                query = query & Builders<LivePosition>.Filter.Eq("VehicleCode", vehicleCode);

                result.NumberOfRecords = db.GetCollection<LivePosition>("LIVE_POSITION").Count(query);
                result.Collection = db.GetCollection<LivePosition>("LIVE_POSITION").Find<LivePosition>(query)
                   .ToList();
                return result.Collection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public LivePosition GetByVehicleCode(long code)
        {
            LivePosition result = new LivePosition();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<LivePosition>.Filter.Eq("VehicleCode", code);
            filter = filter & Builders<LivePosition>.Filter.Eq("Type", PositionType.Vehicle);
            result = db.GetCollection<LivePosition>("LIVE_POSITION").Find<LivePosition>(filter).FirstOrDefault();
            return result;
        }

        public List<LivePosition> GetAllByType(int positiontype)
        {
            List<LivePosition> result = new List<LivePosition>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
           var filter = Builders<LivePosition>.Filter.Eq("Type", positiontype);
            result = db.GetCollection<LivePosition>("LIVE_POSITION").Find<LivePosition>(filter).ToList();
            return result;
        }

        public PagedSearchResultMongo<LivePosition> Search(SearchCriteria<LivePositionFilter> searchCriteria, int IncludedTime = 0)
        {
            try
            {
                PagedSearchResultMongo<LivePosition> result = new PagedSearchResultMongo<LivePosition>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<LivePosition>.Empty;
                if (searchCriteria.Filters != null)
                {
              
                    if (searchCriteria.Filters.Type != null)
                    {
                        query = query & Builders<LivePosition>.Filter.Eq("Type", searchCriteria.Filters.Type);
                    }
                  
                    if (searchCriteria.Filters.VehicleCode != null)
                    {
                        query = query & Builders<LivePosition>.Filter.Eq("VehicleCode", searchCriteria.Filters.VehicleCode);
                    }
                    if (searchCriteria.Filters.StartDate != null)
                    {
                        // add 4 Hours
                        searchCriteria.Filters.StartDate = new DateTime(searchCriteria.Filters.StartDate.Value.Year, searchCriteria.Filters.StartDate.Value.Month,
                                                  searchCriteria.Filters.StartDate.Value.Day, searchCriteria.Filters.StartDate.Value.Hour,
                                                  searchCriteria.Filters.StartDate.Value.Minute, searchCriteria.Filters.StartDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                        query = query & Builders<LivePosition>.Filter.Gte("LocationTIme", searchCriteria.Filters.StartDate);
                    }
                    if (searchCriteria.Filters.EndDate != null)
                    {
                        DateTime date = (DateTime)searchCriteria.Filters.EndDate;
                        searchCriteria.Filters.EndDate = new DateTime(date.Year, date.Month,
                                                 date.Day, searchCriteria.Filters.EndDate.Value.Hour,
                                                 searchCriteria.Filters.EndDate.Value.Minute, searchCriteria.Filters.EndDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                        query = query & Builders<LivePosition>.Filter.Lte("LocationTIme", searchCriteria.Filters.EndDate);
                    }
                }

                result.NumberOfRecords = db.GetCollection<LivePosition>("LIVE_POSITION").Count(query);
                result.Collection = db.GetCollection<LivePosition>("LIVE_POSITION").Find<LivePosition>(query)
                   .Skip(searchCriteria.ItemsToSkip).Limit(searchCriteria.PageSize).ToList();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<LivePosition> GetLivePositionData()
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<LivePosition>.Filter.Empty;
            var collection = db.GetCollection<LivePosition>("LIVE_POSITION");
            var liveposition = collection.Find(filter).ToList();
            return liveposition;
        }
        #endregion
    }
}
