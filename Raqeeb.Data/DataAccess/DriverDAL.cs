﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class DriverDAL : DataAccessBase
    {
        #region Methods
        public List<Driver> InsertBulk(List<Driver> drivers)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<Driver>("DRIVER").InsertMany(drivers);
            return drivers;
        }


        public Driver GetByCode(int code)
        {
            Driver result = new Driver();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Driver>.Filter.Eq("Code", code);
            result = db.GetCollection<Driver>("DRIVER").Find<Driver>(filter).FirstOrDefault();
            return result;
        }

        public List<Driver> GetAllDrivers()
        {
            Driver result = new Driver();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Driver>.Filter.Empty;
            return db.GetCollection<Driver>("DRIVER").Find<Driver>(filter).ToList();
        }

        public void UpdateDriverScore(DriverScore scoreitem)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter2 = Builders<DriverScore>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(scoreitem.IdString)));

            //var id = BsonObjectId.Create(scoreitem.IdString);
            //var filter = Builders<DriverScore>.Filter.Eq(ID_FIELD, id);


            var updateCommand = Builders<DriverScore>.Update
                     .Set("TotalDistraction", scoreitem.TotalDistraction)
                     .Set("TotalFatigue", scoreitem.TotalFatigue)
                     .Set("TotalForwardCollisionWarning", scoreitem.TotalForwardCollisionWarning)
                      .Set("TotalFOV", scoreitem.TotalFOV)
                      .Set("TotalHarshAcceleration", scoreitem.TotalHarshAcceleration)
                      .Set("TotalHarshBraking", scoreitem.TotalHarshBraking)
                      .Set("TotalLeftLaneDeparture", scoreitem.TotalLeftLaneDeparture)
                      .Set("TotalMobileeyeTamperAlert", scoreitem.TotalMobileeyeTamperAlert)
                      .Set("TotalNearmiss", scoreitem.TotalNearmiss)
                      .Set("TotalOverRevving", scoreitem.TotalOverRevving)
                      .Set("TotalOverspeeding", scoreitem.TotalOverspeeding)
                      .Set("TotalPanicButton", scoreitem.TotalPanicButton)
                      .Set("TotalPedestrianInDangerZone", scoreitem.TotalPedestrianInDangerZone)
                      .Set("TotalPedestrianInForwardCollisionWarning", scoreitem.TotalPedestrianInForwardCollisionWarning)
                      .Set("TotalRightLaneDeparture", scoreitem.TotalRightLaneDeparture)
                      .Set("TotalScore", scoreitem.TotalScore)
                      .Set("TotalTrafficSignRecognition", scoreitem.TotalTrafficSignRecognition)
                      .Set("TotalDistanceDriven", scoreitem.TotalDistanceDriven)
                      .Set("TotalDrivingTime", scoreitem.TotalDrivingTime)
                      .Set("TotalDurationTime", scoreitem.TotalDurationTime)
                      .Set("TotalStandingTime", scoreitem.TotalStandingTime)
                      .Set("TotalTrips", scoreitem.TotalTrips)
                      .Set("MaxSpeed", scoreitem.MaxSpeed)
                      .Set("LastUpdate", scoreitem.LastUpdate)
                      .Set("Status", scoreitem.Status)
                      .Set("VehicleHistory", scoreitem.VehicleHistory)
                      .Set("MaxRPM", scoreitem.MaxRPM);

                      db.GetCollection<DriverScore>("DRIVER_SCORE").UpdateMany(filter2, updateCommand);


        }

        public List<DriverScore> CriticalDriverByMonth(int month, int year)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<DriverScore>.Filter.Empty;
            var query = FilterDefinition<DriverScore>.Empty;          
            query &= Builders<DriverScore>.Filter.Eq("MonthName", month);
            query &= Builders<DriverScore>.Filter.Eq("YearName", year);
            query &= Builders<DriverScore>.Filter.Eq("Status", 3);

            DriverScore driverScore = new DriverScore();

            return db.GetCollection<DriverScore>("DRIVER_SCORE").Find<DriverScore>(query).ToList(); ;
        }

        public Driver GetByRegistrationNumber(int item)
        {
            Driver result = new Driver();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Driver>.Filter.Eq("Code", item);
            result = db.GetCollection<Driver>("DRIVER").Find<Driver>(filter).FirstOrDefault();
            return result;
        }

        public List<DriverVehicleHistory> GetDriverScoreVehicleHistory(string idString)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(idString);
            var filter = Builders<DriverVehicleHistory>.Filter.Eq(ID_FIELD, id);
            return db.GetCollection<DriverVehicleHistory>("DRIVER_SCORE_VEHICLE_HISTORY").Find<DriverVehicleHistory>(filter).ToList();
        }

        public DriverScore GetDriverStatisticsByCode(SearchCriteria<DriverFilter> searchCriteria,int code)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<DriverScore>.Filter.Empty;
            var query = FilterDefinition<DriverScore>.Empty;
            query &= Builders<DriverScore>.Filter.Eq("DriverCode", code);
            query &= Builders<DriverScore>.Filter.Eq("MonthName", searchCriteria.Filters.Month);
            query &= Builders<DriverScore>.Filter.Eq("YearName", searchCriteria.Filters.Year);

            DriverScore driverScore = new DriverScore();

            return db.GetCollection<DriverScore>("DRIVER_SCORE").Find<DriverScore>(query).FirstOrDefault(); ;

        }

        public void InsertDriverScore(DriverScore driverScore)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<DriverScore>("DRIVER_SCORE").InsertOne(driverScore);

        }

        public DriverScore CheckDriverScoreExists(int code, int month, int year)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<DriverScore>.Filter.Empty;
            var query = FilterDefinition<DriverScore>.Empty;
            query = query & Builders<DriverScore>.Filter.Eq("DriverCode", code);
            query = query & Builders<DriverScore>.Filter.Eq("MonthName", month);
            query = query & Builders<DriverScore>.Filter.Eq("YearName", year);

            DriverScore driverScore = new DriverScore();

            return db.GetCollection<DriverScore>("DRIVER_SCORE").Find<DriverScore>(query).FirstOrDefault(); ;


        }

        public PagedSearchResultMongo<Driver> Search(SearchCriteria<DriverFilter> searchCriteria)
        {
            try
            {
                PagedSearchResultMongo<Driver> result = new PagedSearchResultMongo<Driver>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<Driver>.Empty;
                if (searchCriteria.Filters != null)
                {
                    if (!string.IsNullOrEmpty(searchCriteria.Filters.ADSDNumber))
                    {
                        var filter = new BsonDocument() {
                    {"Registration" , new BsonDocument(){
                        {"$regex", ".*" + searchCriteria.Filters.ADSDNumber + ".*" },
                        {"$options" ,"i" }
                        }}};
                        query = query & filter;
                    }



                }
                result.NumberOfRecords = db.GetCollection<Driver>("DRIVER").Count(query);
                result.Collection = db.GetCollection<Driver>("DRIVER").Find<Driver>(query)
                     .SortByDescending(p => p.LastUpdated).Skip(searchCriteria.ItemsToSkip).Limit(searchCriteria.PageSize).ToList();
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Driver> GetDriverData()
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Driver>.Filter.Empty;
            var collection = db.GetCollection<Driver>("DRIVER");
            var driver = collection.Find(filter).ToList();
            return driver;
        }
        #endregion
    }
}
