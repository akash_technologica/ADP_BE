﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class DeviceDAL : DataAccessBase
    {
        #region Methods
        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();
        public List<HeartBeat> GetAll()
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<HeartBeat>.Filter.Empty;

            return (List<HeartBeat>)db.GetCollection<HeartBeat>("DEVICE_HEART_BEAT").Find<HeartBeat>(filter).SortByDescending(c => c.LastCommand).ToList().Distinct();

        }
        public HeartBeat InsertBulk(HeartBeat heartBeat)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<HeartBeat>("DEVICE_HEART_BEAT").InsertOne(heartBeat);
            return heartBeat;
        }

        public void UpdateHeartBeat(HeartBeat heartBeat)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var filter = Builders<HeartBeat>.Filter.Eq(ID_FIELD, heartBeat.Id);


            var updateCommand = Builders<HeartBeat>.Update
                     .Set("Altitude", heartBeat.Altitude)
                     .Set("AuthenticationFailure", heartBeat.AuthenticationFailure)
                     .Set("BootloaderFirmwareFailure", heartBeat.BootloaderFirmwareFailure)
                      .Set("BuzzerFault", heartBeat.BuzzerFault)
                      .Set("CameraDetected", heartBeat.CameraDetected)
                      .Set("CameraFault", heartBeat.CameraFault)
                      .Set("CarrierBoardVersion", heartBeat.CarrierBoardVersion)
                      .Set("ClockDrift", heartBeat.ClockDrift)
                      .Set("CmosBatteryFault", heartBeat.CmosBatteryFault)
                      .Set("ConfigurationFilesOk", heartBeat.ConfigurationFilesOk)
                      .Set("Course", heartBeat.Course)
                      .Set("CpuLoadOk", heartBeat.CpuLoadOk)
                      .Set("CpuPercent", heartBeat.CpuPercent)
                      .Set("ErrorFlags", heartBeat.ErrorFlags)
                      .Set("EventFlags", heartBeat.EventFlags)
                      .Set("ExceptionInProgress", heartBeat.ExceptionInProgress)
                      .Set("FfcFault", heartBeat.FfcFault)
                      .Set("FfcNoData", heartBeat.FfcNoData)
                      .Set("FfcNotDetected", heartBeat.FfcNotDetected)
                      .Set("GpsDetected", heartBeat.GpsDetected)
                      .Set("GpsDetectedButNoSignal", heartBeat.GpsDetectedButNoSignal)
                      .Set("GpsPercent", heartBeat.GpsPercent)
                      .Set("GpsTimeUtc", heartBeat.GpsTimeUtc)
                      .Set("IpAddress", heartBeat.IpAddress)
                      .Set("IrPodsInactive", heartBeat.IrPodsInactive)
                      .Set("LastCommand", heartBeat.LastCommand)
                      .Set("Latitude", heartBeat.Latitude)
                      .Set("Longitude", heartBeat.Longitude)
                      .Set("MacAddress", heartBeat.MacAddress)
                      .Set("MaxRebootsExceeded", heartBeat.MaxRebootsExceeded)
                      .Set("ModifiedTime", heartBeat.ModifiedTime)
                      .Set("OperationalState", heartBeat.OperationalState)
                      .Set("OverTemperature", heartBeat.OverTemperature)
                      .Set("ProductVersion", heartBeat.ProductVersion)
                      .Set("ProvisioningState", heartBeat.ProvisioningState)
                      .Set("PsuFault", heartBeat.PsuFault)
                      .Set("SimError", heartBeat.SimError)
                      .Set("SoftwareVersion", heartBeat.SoftwareVersion)
                      .Set("SpeakerFault", heartBeat.SpeakerFault)
                      .Set("SpeedMps", heartBeat.SpeedMps)
                      .Set("Suppressed", heartBeat.Suppressed)
                      .Set("SystemDisk", heartBeat.SystemDisk)
                      .Set("SystemDiskDetected", heartBeat.SystemDiskDetected)
                      .Set("SystemTimeUtc", heartBeat.SystemTimeUtc)
                      .Set("Temperature", heartBeat.Temperature)
                      .Set("Tracking", heartBeat.Tracking)
                      .Set("UibrationMotorFault", heartBeat.UibrationMotorFault)
                      .Set("UnverifiedSystemTime", heartBeat.UnverifiedSystemTime)
                      .Set("Valid", heartBeat.Valid)
                      .Set("VibrationMotorOverheat", heartBeat.VibrationMotorOverheat)
                      .Set("Warnings", heartBeat.Warnings);
                   

            db.GetCollection<HeartBeat>("DEVICE_HEART_BEAT").UpdateMany(filter, updateCommand);
        }

        public List<Domain.DataModel.DeviveOnlineOfflineLog> Search(SearchCriteria<DeviceFilter> searchCriteria)
        {
            List<Domain.DataModel.DeviveOnlineOfflineLog> logDetList = new List<Domain.DataModel.DeviveOnlineOfflineLog>();
            try
            {             
                if (searchCriteria.Filters.StartDate == null && searchCriteria.Filters.StartDate == null)
                {
                    DateTime today = DateTime.Today.AddDays(-2);

                    string startDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
                    searchCriteria.Filters.StartDate = Convert.ToDateTime(startDate);
                    searchCriteria.Filters.EndDate = DateTime.Now;
                }
                if(searchCriteria.Filters.UserGroup == 0)
                {
                    if (searchCriteria.Filters.DeviceNumbers.Length > 0)
                    {

                        logDetList = aDPEntities.DeviveOnlineOfflineLogs.Where(x => searchCriteria.Filters.DeviceNumbers.Contains(x.DeviceId)
                        && x.Date >= searchCriteria.Filters.StartDate && x.Date <= searchCriteria.Filters.EndDate).ToList();
                    }
                    else
                    {
                        logDetList = aDPEntities.DeviveOnlineOfflineLogs.Where(c => c.Date >= searchCriteria.Filters.StartDate &&
                                                                                                              c.Date <= searchCriteria.Filters.EndDate).Select(c => c).ToList();
                    }
                }
                else
                {
                    if (searchCriteria.Filters.DeviceNumbers.Length > 0)
                    {

                        logDetList = aDPEntities.DeviveOnlineOfflineLogs.Where(x => searchCriteria.Filters.DeviceNumbers.Contains(x.DeviceId)
                        && x.Date >= searchCriteria.Filters.StartDate && x.Date <= searchCriteria.Filters.EndDate && x.GroupId == searchCriteria.Filters.UserGroup).ToList();
                    }
                    else
                    {
                        logDetList = aDPEntities.DeviveOnlineOfflineLogs.Where(c => c.Date >= searchCriteria.Filters.StartDate && c.GroupId == searchCriteria.Filters.UserGroup
                                                                                                            &&  c.Date <= searchCriteria.Filters.EndDate).Select(c => c).ToList();
                    }
                }

                



                return logDetList;



            }
            catch (Exception ex)
            {
                Raqeeb.Common.Utility.WriteLog(ex);
                return logDetList;
            }
        }

        public List<Device> GetLists(string entityId)
        {
            List<Device> result = new List<Device>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<Device>.Empty;
            query = query & Builders<Device>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(entityId)));
            result = db.GetCollection<Device>("DEVICE").Find<Device>(query).ToList();
            return result;
        }
        public List<Domain.DataModel.Device> GetList()
        {
            return aDPEntities.Devices.ToList();
           
        }
        public List<Domain.DataModel.Device> GetList(int group)
        {
            return aDPEntities.Devices.Where(c=>c.GroupId == group).ToList();

        }
        public List<Domain.DataModel.Device> GetList(List<VehicleDto> vehicleDto)
        {
            List<String> car_licenses = new List<string>();
            foreach (var item in vehicleDto)
            {
                car_licenses.Add(item.Registration);
            }
           
            return aDPEntities.Devices.Where(x=> car_licenses.Contains(x.CarLicense)).ToList();

        }
        public Device Add(Device device)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<Device>("DEVICE").InsertOne(device);
            return device;
        }


        public Device Update(Device device)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(device.IdString);
            var filter = Builders<Device>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<Device>.Update
                .Set("ModelCode", device.ModelCode)
                .Set("Name", device.Name)
                .Set("IMEI", device.IMEI)
                .Set("FirmwareVersion", device.FirmwareVersion)
                .Set("InstallationDate", device.InstallationDate)
                .Set("IsActive", device.IsActive)
                .Set("LastServiceDate", device.LastServiceDate)
                .Set("EntityId", device.EntityId)
                .Set("VehicleCode", device.VehicleCode);
                

            db.GetCollection<Device>("DEVICE").UpdateOne(filter, updateCommand);

            return device;
        }

        #endregion
    }
}
