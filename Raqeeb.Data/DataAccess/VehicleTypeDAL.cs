﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class VehicleTypeDAL : DataAccessBase
    {
        #region Methods
        public VehicleType Insert(VehicleType vehicleType)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<VehicleType>("VehicleType").InsertOne(vehicleType);
            return vehicleType;
        }


        public List<VehicleType> GetAllVehicleType()
        {
            VehicleType result = new VehicleType();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<VehicleType>.Filter.Empty;
            return db.GetCollection<VehicleType>("VehicleType").Find<VehicleType>(filter).ToList();
        }

        public VehicleType DeleteVehicleType(VehicleType vehicleType)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(vehicleType.IdString);
            var filter = Builders<VehicleType>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<VehicleType>("VehicleType").DeleteOne(filter);
            return vehicleType;
        }

        public VehicleType UpdateAsync(VehicleType vehicleType)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var filter = Builders<VehicleType>
             .Filter.Eq(e => e.IdString, vehicleType.IdString);




            return vehicleType;
        }
        public VehicleType UpdateVehicleType(VehicleType vehicleType)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var filter = Builders<VehicleType>
             .Filter.Eq(e => e.IdString, vehicleType.IdString);


            var updateCommand = Builders<VehicleType>.Update
                     .Set("VehicleTypeName", vehicleType.VehicleTypeName);
                    

            db.GetCollection<VehicleType>("VehicleType").UpdateMany(filter, updateCommand);

            return vehicleType;
        }
        #endregion
    }
}
