﻿using Raqeeb.Domain.DataModel;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class DeviceAlarmDAL : DataAccessBase
    {
        #region Constants
        #region Fields

        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();
        VehicleDAL vehicleDAL = new VehicleDAL();

        public AlarmDetails Search(SearchCriteria<AlarmFilter> searchCriteria)
        {
            List<AlarmDto> alarmDto = new List<AlarmDto>();
            AlarmDetails alarmDetails = new AlarmDetails();
            try
            {
                List<Raqeeb.Domain.DataModel.DeviceAlarmDetail> deviceAlarmDetails = new List<Domain.DataModel.DeviceAlarmDetail>();
                if (searchCriteria.Filters.StartDate == null && searchCriteria.Filters.EndDate == null)
                {
                    DateTime today = DateTime.Today.AddDays(-30);

                    string startDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
                    searchCriteria.Filters.StartDate = Convert.ToDateTime(startDate);
                    searchCriteria.Filters.EndDate = DateTime.Now;
                }
                if (searchCriteria.Group == 0)
                {
                    if (searchCriteria.GlobalFilter != null && searchCriteria.GlobalFilter.Length > 0)
                    {
                        deviceAlarmDetails = aDPEntities.DeviceAlarmDetails
                                              .Where(c => c.Time >= searchCriteria.Filters.StartDate && c.Time <= searchCriteria.Filters.EndDate &&
                                              (c.DeviceId.ToLower().Contains(searchCriteria.GlobalFilter.ToLower()) ||
                                              c.AlarmContent.ToLower().Contains(searchCriteria.GlobalFilter.ToLower())
                                              ))
                                              .OrderBy(x => x.Id)
                                              .Skip(searchCriteria.First)
                                              .Take(searchCriteria.Rows)
                                              .ToList();
                        alarmDetails.TotalAlarms = aDPEntities.DeviceAlarmDetails
                            .Where(c => c.Time >= searchCriteria.Filters.StartDate && c.Time <= searchCriteria.Filters.EndDate && (c.DeviceId.ToLower().Contains(searchCriteria.GlobalFilter.ToLower()) ||
                                              c.AlarmContent.ToLower().Contains(searchCriteria.GlobalFilter.ToLower())))
                            .Count();
                    }
                    else
                    {
                        deviceAlarmDetails = aDPEntities.DeviceAlarmDetails
              .Where(c => c.Time >= searchCriteria.Filters.StartDate && c.Time <= searchCriteria.Filters.EndDate)
              .OrderBy(x => x.Id)
              .Skip(searchCriteria.First)
              .Take(searchCriteria.Rows)
              .ToList();
                        alarmDetails.TotalAlarms = aDPEntities.DeviceAlarmDetails
                            .Where(c => c.Time >= searchCriteria.Filters.StartDate && c.Time <= searchCriteria.Filters.EndDate).Count();
                    }
                }
                else
                {
                    if (searchCriteria.GlobalFilter != null && searchCriteria.GlobalFilter.Length > 0)
                    {
                        deviceAlarmDetails = aDPEntities.DeviceAlarmDetails.Where(c => c.Time >= searchCriteria.Filters.StartDate && c.Time <= searchCriteria.Filters.EndDate && (c.DeviceId.ToLower().Contains(searchCriteria.GlobalFilter.ToLower()) ||
                                              c.AlarmContent.ToLower().Contains(searchCriteria.GlobalFilter.ToLower()))).OrderBy(x => x.Id)
                        .Skip(searchCriteria.First)
                        .Take(searchCriteria.Rows)
                        .ToList();
                        alarmDetails.TotalAlarms = aDPEntities.DeviceAlarmDetails.Where(c => c.Time >= searchCriteria.Filters.StartDate &&
                        c.Time <= searchCriteria.Filters.EndDate && (c.DeviceId.ToLower().Contains(searchCriteria.GlobalFilter.ToLower()) ||
                        c.AlarmContent.ToLower().Contains(searchCriteria.GlobalFilter.ToLower()))).Count();
                    }
                    else
                    {
                        deviceAlarmDetails = aDPEntities.DeviceAlarmDetails.Where(c => c.Time >= searchCriteria.Filters.StartDate && c.GroupId == searchCriteria.Filters.Group && c.Time < searchCriteria.Filters.EndDate).OrderBy(x => x.Id)
                    .Skip(searchCriteria.First)
                    .Take(searchCriteria.Rows)
                    .ToList();
                        alarmDetails.TotalAlarms = aDPEntities.DeviceAlarmDetails
                        .Where(c => c.Time >= searchCriteria.Filters.StartDate && c.GroupId == searchCriteria.Filters.Group && c.Time < searchCriteria.Filters.EndDate).Count();
                    }

                }
                foreach (var item in deviceAlarmDetails)
                {
                    AlarmDto alarmDtos = new AlarmDto()
                    {
                        AlarmContent = item.AlarmContent,
                        AlarmType = GetAlarmName(item.Type.Value),
                        Altitude = item.Altitude,
                        DeviceId = item.DeviceId,
                        Car_License = vehicleDAL.GetByCode(item.DeviceId),
                        Time = item.Time,
                        Direction = item.Direction,
                        Id = item.Id,
                        Longitude = item.Longitude,
                        Speed = item.Speed,
                        State = item.State,
                        Latitude = item.Latitude,
                        Type = item.Type

                    };
                    alarmDto.Add(alarmDtos);

                    switch (item.Type)
                    {
                        case 1:
                            alarmDetails.Video_Loss += 1; break;
                        case 2:
                            alarmDetails.Motion_Detection += 1; break;
                        case 3:
                            alarmDetails.Cover += 1; break;
                        case 4:
                            alarmDetails.Storage_Exception += 1; break;
                        case 13:
                            alarmDetails.Panic_Alarm += 1; break;
                        case 14:
                            alarmDetails.Motion_Detection += 1; break;
                        case 15:
                            alarmDetails.Motion_Detection += 1; break;
                        case 16:
                            alarmDetails.Motion_Detection += 1; break;
                        case 17:
                            alarmDetails.Motion_Detection += 1; break;
                        case 18:
                            alarmDetails.Motion_Detection += 1; break;
                        case 19:
                            alarmDetails.Motion_Detection += 1; break;
                        case 20:
                            alarmDetails.Motion_Detection += 1; break;
                        case 29:
                            alarmDetails.Motion_Detection += 1; break;
                        case 36:
                            alarmDetails.Motion_Detection += 1; break;
                        case 38:
                            alarmDetails.Motion_Detection += 1; break;
                        case 47:
                            alarmDetails.Motion_Detection += 1; break;
                        case 58:
                            alarmDetails.Motion_Detection += 1; break;
                        case 59:
                            alarmDetails.Motion_Detection += 1; break;
                        case 60:
                            alarmDetails.Motion_Detection += 1; break;
                        case 61:
                            alarmDetails.Motion_Detection += 1; break;
                        case 62:
                            alarmDetails.Motion_Detection += 1; break;
                        case 63:
                            alarmDetails.Motion_Detection += 1; break;
                        case 64:
                            alarmDetails.Motion_Detection += 1; break;
                        case 74:
                            alarmDetails.Motion_Detection += 1; break;
                        case 81:
                            alarmDetails.Motion_Detection += 1; break;
                        case 160:
                            alarmDetails.Motion_Detection += 1; break;
                        case 161:
                            alarmDetails.Motion_Detection += 1; break;
                        case 162:
                            alarmDetails.Motion_Detection += 1; break;

                    }
                }
                alarmDetails.AlarmList = alarmDto;
                return alarmDetails;

            }
            catch (Exception ex)
            {

                return alarmDetails;
            }
        }
        private string GetAlarmName(int alarmCode)
        {
            return aDPEntities.AlarmTypeMasters.Where(c => c.AlarmId == alarmCode).Select(c => c.AlarmName).FirstOrDefault();
        }

        public List<DeviceAlarmDetail> exportAll(int group)
        {
            return group == 0 ? aDPEntities.DeviceAlarmDetails.ToList() : aDPEntities.DeviceAlarmDetails.Where(x => x.GroupId == group).ToList();
        }

        #endregion
        #endregion
        #region Methods





        #endregion
    }
}
