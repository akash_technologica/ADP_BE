﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class IOElementsMasterDAL: DataAccessBase
    {
        #region Methods

        public List<IOElementsMaster> GetAll()
        {
            List<IOElementsMaster> result = new List<IOElementsMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<IOElementsMaster>.Filter.Empty;

            result = db.GetCollection<IOElementsMaster>("IO_ELEMENTS_MASTER").Find<IOElementsMaster>(filter).ToList();
            return result;
        }

        public IOElementsMaster Add(IOElementsMaster ioElements)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<IOElementsMaster>("IO_ELEMENTS_MASTER").InsertOne(ioElements);
            return ioElements;
        }


        public IOElementsMaster Update(IOElementsMaster ioElements)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(ioElements.IdString);
            var filter = Builders<IOElementsMaster>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<IOElementsMaster>.Update
                .Set("Name", ioElements.Name)
                .Set("IO_Code", ioElements.IO_Code);

            db.GetCollection<IOElementsMaster>("IO_ELEMENTS_MASTER").UpdateOne(filter, updateCommand);

            return ioElements;
        }



        #endregion
    }
}
