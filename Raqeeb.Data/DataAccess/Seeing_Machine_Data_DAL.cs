﻿using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class Seeing_Machine_Data_DAL
    {
        #region Methods
        public List<Seeing_Machine_Data> InsertBulk(List<Seeing_Machine_Data> sm_data)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            if (sm_data.Count > 0)
                db.GetCollection<Seeing_Machine_Data>("SEEING_MACHINE_DATA").InsertMany(sm_data);
            return sm_data;
        }

        public List<Seeing_Machine_Data> GetAll()
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Seeing_Machine_Data>.Filter.Empty;
            var list = db.GetCollection<Seeing_Machine_Data>("SEEING_MACHINE_DATA").Find<Seeing_Machine_Data>(filter).ToList();

            return list;

        }

        public Seeing_Machine_Data GetSM_DataByCodeAndTime(string registration, DateTime? startDate, DateTime? endDate)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            List<Seeing_Machine_Data> seeing_Machine_DataList = new List<Seeing_Machine_Data>();
            Seeing_Machine_Data seeing_Machine_Data = new Seeing_Machine_Data();
            var query = FilterDefinition<Seeing_Machine_Data>.Empty;

            if (startDate != null && endDate != null && registration != null)
            {
                //startDate = new DateTime(startDate.Value.Year, startDate.Value.Month,
                //                                   startDate.Value.Day, startDate.Value.Hour, startDate.Value.Minute, startDate.Value.Second);

                //endDate = new DateTime(endDate.Value.Year, endDate.Value.Month,
                //                                endDate.Value.Day, endDate.Value.Hour, endDate.Value.Minute, endDate.Value.Second);

                query &= Builders<Seeing_Machine_Data>.Filter.Gte("StartDate", startDate.Value);
                query &= Builders<Seeing_Machine_Data>.Filter.Lte("EndDate", endDate.Value);
                query = query & Builders<Seeing_Machine_Data>.Filter.Eq("guardianEvent.trip.vehicle.name", registration);


                seeing_Machine_DataList = db.GetCollection<Seeing_Machine_Data>("SEEING_MACHINE_DATA").Find<Seeing_Machine_Data>(c=>c.guardianEvent.trip.vehicle.name == registration).ToList();
                foreach(var item in seeing_Machine_DataList)
                {
                    

                    if(startDate.Value.TimeOfDay.Seconds==item.StartDate.TimeOfDay.Seconds && endDate.Value.TimeOfDay.Seconds == item.EndDate.TimeOfDay.Seconds)
                    {
                        seeing_Machine_Data = item;
                    }
                   
                }

            }

            return seeing_Machine_Data;


        }


        #endregion
    }
}
