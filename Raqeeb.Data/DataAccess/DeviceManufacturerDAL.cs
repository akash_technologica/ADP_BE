﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class DeviceManufacturerDAL : DataAccessBase
    {
        #region Methods

        public List<DeviceManufacturerMaster> GetAll()
        {
            List<DeviceManufacturerMaster> result = new List<DeviceManufacturerMaster>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<DeviceManufacturerMaster>.Filter.Empty;

            result = db.GetCollection<DeviceManufacturerMaster>("DEVICE_MANUFACTURER_MASTER").Find<DeviceManufacturerMaster>(filter).ToList();
            return result;
        }

        public DeviceManufacturerMaster Add(DeviceManufacturerMaster manufacturer)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<DeviceManufacturerMaster>("DEVICE_MANUFACTURER_MASTER").InsertOne(manufacturer);
            return manufacturer;
        }


        public DeviceManufacturerMaster Update(DeviceManufacturerMaster manufacturer)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(manufacturer.IdString);
            var filter = Builders<DeviceManufacturerMaster>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<DeviceManufacturerMaster>.Update
                .Set("CountryCode", manufacturer.Name)
                .Set("Description", manufacturer.Description)
                .Set("IsActive", manufacturer.IsActive);
                

            db.GetCollection<DeviceManufacturerMaster>("DEVICE_MANUFACTURER_MASTER").UpdateOne(filter, updateCommand);

            return manufacturer;
        }

        public CountryMaster Delete(CountryMaster country)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(country.IdString);
            var filter = Builders<CountryMaster>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<CountryMaster>("COUNTRY_MASTER").DeleteOne(filter);
            return country;
        }


        #endregion
    }
}
