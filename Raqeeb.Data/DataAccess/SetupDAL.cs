﻿using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
   public class SetupDAL : DataAccessBase
    {
        #region Methods

        public List<Setup> GetAll()
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Setup>.Filter.Empty;
            return db.GetCollection<Setup>("SETUP").Find<Setup>(filter).ToList();
        }

        public void Update(Setup setup)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Setup>.Filter.Eq("ClassificationType",(int)setup.ClassificationType );
            var updateCommand = Builders<Setup>.Update
                .Set("EventSetups", setup.EventSetups);
            db.GetCollection<Setup>("SETUP").UpdateOne(filter, updateCommand);
        }

        public Setup GetByClassificationEventTyp(int classificationType)
        {
            Setup result = new Setup();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Setup>.Filter.Eq("ClassificationType", classificationType);
            result = db.GetCollection<Setup>("SETUP").Find<Setup>(filter).FirstOrDefault();
            return result;
        }


        public void Add(Setup setup)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<Setup>("SETUP").InsertOne(setup);
        }
        
        #endregion
    }
}
