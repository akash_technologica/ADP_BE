﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Framework.Core.Data.Entites;
using Raqeeb.Domain;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;

namespace Raqeeb.DataAccess
{
    public class TemplateDAL : DataAccessBase
    {
        #region Constant
        private const string NAME_FIELD = "NAME";
        private const string NOTIFICATION_TYPE_FIELD = "NOTIFICATION_TYPE";
        private const string IS_PUBLISHED_FIELD = "IS_PUBLISHED";


        #endregion

        public Contractor Add(Contractor template)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<Contractor>(COLLECTION_TEMPLATE).InsertOne(template);
            return template;
        }


        public PagedSearchResultMongo<Contractor> Search(Paging<TemplateFilter> paging)
        {
            PagedSearchResultMongo<Contractor> result = new PagedSearchResultMongo<Contractor>();
            //IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            //var query = FilterDefinition<Contractor>.Empty;
            //query = query & Builders<Contractor>.Filter.Eq(DELETED_BY_FIELD, BsonNull.Value);

            //if (!string.IsNullOrEmpty(paging.SearchFilters.Name))
            //    query = query & Builders<Contractor>.Filter.Eq(NAME_FIELD, paging.SearchFilters.Name);

            //if (paging.SearchFilters.NotificationTypeId.HasValue)
            //    query = query & Builders<Contractor>.Filter.Eq(NOTIFICATION_TYPE_FIELD, paging.SearchFilters.NotificationTypeId.Value);

            //if (paging.SearchFilters.StatusId.HasValue)
            //    query = query & Builders<Contractor>.Filter.Eq(IS_PUBLISHED_FIELD, paging.SearchFilters.StatusId.Value);


            //result.NumberOfRecords = db.GetCollection<Contractor>(COLLECTION_TEMPLATE).Count(query);
            //result.Collection = db.GetCollection<Contractor>(COLLECTION_TEMPLATE).Find<Contractor>(query)
            //     .SortByDescending(p => p.CreationDate).Skip(paging.ItemsToSkip).Limit(paging.PageSize).ToList();

            return result;
        }
    }
}
