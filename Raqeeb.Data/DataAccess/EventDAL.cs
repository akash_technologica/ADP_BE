﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Enums;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class EventDAL : DataAccessBase
    {
        #region Constants
        #region Fields

        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();

        #endregion
        #endregion
        #region Methods
        public List<Event> InsertBulk(List<Event> events)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            if (events.Count > 0)
                db.GetCollection<Event>("EVENT").InsertMany(events);
            return events;
        }
        public Event InsertOne(Event events)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<Event>("EVENT").InsertOne(events);
            return events;
        }


        public List<Event> GetAllEvents()
        {
            List<Event> result = new List<Event>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Event>.Filter.Empty;
            result = db.GetCollection<Event>("EVENT").Find<Event>(filter).ToList();
            return result;
        }

        public List<Event> GetAllEventsByDate()
        {
            List<Event> result = new List<Event>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            DateTime thisDate = DateTime.Now.Date;
            var filter = Builders<Event>.Filter.Empty;
            var query = FilterDefinition<Event>.Empty;
            query = query & Builders<Event>.Filter.Gte("StartDate", thisDate);
            query = query & Builders<Event>.Filter.Eq("EventSource", "SM");
            result = db.GetCollection<Event>("EVENT").Find<Event>(query).ToList();
            return result;
        }

        public Event GetByCode(string code)
        {
            Event result = new Event();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Event>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(code)));
            // var filter = Builders<Event>.Filter.Eq("Code", code);
            result = db.GetCollection<Event>("EVENT").Find<Event>(filter).FirstOrDefault();
            return result;
        }



        public long GetEventsCount(EventFilter eventFilter)
        {
            long totlaEvents = 0;
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var query = FilterDefinition<Event>.Empty;
            if (eventFilter.StartDate != null)
            {
                // add 4 Hours
                eventFilter.StartDate = new DateTime(eventFilter.StartDate.Value.Year, eventFilter.StartDate.Value.Month,
                                          eventFilter.StartDate.Value.Day, eventFilter.StartDate.Value.Hour,
                                          eventFilter.StartDate.Value.Minute, eventFilter.StartDate.Value.Second).AddHours(IncreasedTimeFromBackEnd);
                query = query & Builders<Event>.Filter.Gte("StartDate", eventFilter.StartDate);
            }
            if (eventFilter.EndDate != null)
            {
                DateTime date = (DateTime)eventFilter.EndDate;
                eventFilter.EndDate = new DateTime(date.Year, date.Month,
                                         date.Day, eventFilter.EndDate.Value.Hour,
                                         eventFilter.EndDate.Value.Minute, eventFilter.EndDate.Value.Second).AddHours(IncreasedTimeFromBackEnd);
                query = query & Builders<Event>.Filter.Lte("EndDate", eventFilter.EndDate);
            }
            var filter1 = FilterDefinition<Event>.Empty;
            var filter2 = FilterDefinition<Event>.Empty;

            if (eventFilter.TripCode != null)
            {
                filter1 = Builders<Event>.Filter.Where(x => x.TripCode == eventFilter.TripCode && x.VehicleCode == null);
                //query = query & filter1;
            }
            if (eventFilter.VehicleCode != null)
            {
                filter2 = filter1 | Builders<Event>.Filter.Where(x => x.VehicleCode == eventFilter.VehicleCode && x.TripCode == null);
                // query = query & filter1;
            }
            query = query & (filter1 | filter2);


            totlaEvents = db.GetCollection<Event>("EVENT").Find<Event>(query).Count();
            return totlaEvents;
        }

        public EventChangeLog GetEventLogs(string eventCode)
        {

            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var query = FilterDefinition<EventChangeLog>.Empty;
            query &= Builders<EventChangeLog>.Filter.Eq("EventCode", eventCode);

            //var filter = Builders<ChangesLog>.Filter.Eq("EventCode", eventCode);
            return db.GetCollection<EventChangeLog>("EVENT_CHANGE_LOG").Find<EventChangeLog>(c => c.EventCode == eventCode).FirstOrDefault();

        }

        public long GetEventsTripByVehicleCode(EventFilter eventFilter)
        {

            long totlaEvents = 0;
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var query = FilterDefinition<Event>.Empty;
            if (eventFilter.StartDate != null)
            {
                // add 4 Hours
                eventFilter.StartDate = new DateTime(eventFilter.StartDate.Value.Year, eventFilter.StartDate.Value.Month,
                                          eventFilter.StartDate.Value.Day, eventFilter.StartDate.Value.Hour,
                                          eventFilter.StartDate.Value.Minute, eventFilter.StartDate.Value.Second).AddHours(IncreasedTimeFromBackEnd);
                query = query & Builders<Event>.Filter.Gte("StartDate", eventFilter.StartDate);
            }
            if (eventFilter.EndDate != null)
            {
                DateTime date = (DateTime)eventFilter.EndDate;
                eventFilter.EndDate = new DateTime(date.Year, date.Month,
                                         date.Day, eventFilter.EndDate.Value.Hour,
                                         eventFilter.EndDate.Value.Minute, eventFilter.EndDate.Value.Second).AddHours(IncreasedTimeFromBackEnd);
                query = query & Builders<Event>.Filter.Lte("EndDate", eventFilter.EndDate);
            }

            if (eventFilter.VehicleCode != null)
            {
                query = query & Builders<Event>.Filter.Eq("VehicleCode", eventFilter.VehicleCode);
            }
            if (eventFilter.EventTypeCode != null)
            {
                query = query & Builders<Event>.Filter.Eq("EventTypeCode", eventFilter.EventTypeCode);
            }

            totlaEvents = db.GetCollection<Event>("EVENT").Find<Event>(query).Count();
            return totlaEvents;
        }



        public List<Event> GetByDateAndCodeAndVehicle(EventFilter filter, bool isVehicleDisplayed, int IncludedTime = 0)
        {
            try
            {
                List<Event> result = new List<Event>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<Event>.Empty;

                if (filter.StartDate != null)
                {
                    filter.StartDate = new DateTime(filter.StartDate.Value.Year, filter.StartDate.Value.Month,
                                               filter.StartDate.Value.Day, filter.StartDate.Value.Hour, filter.StartDate.Value.Minute, filter.StartDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                    query = query & Builders<Event>.Filter.Gte("StartDate", filter.StartDate);
                }
                if (filter.EndDate != null)
                {
                    DateTime date = (DateTime)filter.EndDate;
                    filter.EndDate = new DateTime(date.Year, date.Month,
                                             date.Day, filter.EndDate.Value.Hour,
                                             filter.EndDate.Value.Minute, filter.EndDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                    query = query & Builders<Event>.Filter.Lte("EndDate", filter.EndDate);
                }
                if (filter.EventTypeCode != null)
                {
                    query = query & Builders<Event>.Filter.Eq("EventTypeCode", filter.EventTypeCode);
                }

                if (filter.TripCode != null)
                {
                    query = query & Builders<Event>.Filter.Where(x => x.TripCode == filter.TripCode && x.VehicleCode == null);
                }
                if (filter.VehicleCode != null)
                {
                    query = query & Builders<Event>.Filter.Where(x => x.VehicleCode == filter.VehicleCode && x.TripCode == null);
                }

                result = db.GetCollection<Event>("EVENT").Find<Event>(query)
                     .SortByDescending(p => p.EndDate).ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Reclassification PostAcceptEvent(Reclassification data)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            Event result = new Event();

            var filter = Builders<Event>.Filter.Eq("OriginalCode", data.Code);
            result = db.GetCollection<Event>("EVENT").Find<Event>(filter).FirstOrDefault();


            var filter2 = Builders<Event>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(result.IdString)));
            var updateCommand = Builders<Event>.Update
                .Set("EventAccepted", true)
                .Set("UpdatedTime", DateTime.Now);

            //.Set("UpdatedBy", DateTime.Now);

            db.GetCollection<Event>("EVENT").UpdateOne(filter2, updateCommand);
            if (data.UpdateStatusCode == (int)EventUpdateStatus.Classify || data.UpdateStatusCode == (int)EventUpdateStatus.Reclassify
                || data.UpdateStatusCode == (int)EventUpdateStatus.Acknowledge)
            {
                EventChangeLog eventChangeLog = new EventChangeLog()
                {
                    ClassificationType = data.UpdateStatusCode,
                    Comments = data.Comments,
                    EventCode = result.Code,
                    SubEventCode = data.SubEventTypeCode,
                    UpdatedBy = data.UserName,
                    UpdatedDate = DateTime.Now
                };
                db.GetCollection<EventChangeLog>("EVENT_CHANGE_LOG").InsertOne(eventChangeLog);
            }


            return data;
        }

        public Reclassification PostRejectEvent(Reclassification data)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            Event result = new Event();

            var filter = Builders<Event>.Filter.Eq("OriginalCode", data.Code);
            result = db.GetCollection<Event>("EVENT").Find<Event>(filter).FirstOrDefault();


            var filter2 = Builders<Event>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(result.IdString)));
            var updateCommand = Builders<Event>.Update
                .Set("EventRejected", true)
                .Set("UpdatedTime", DateTime.Now);

            db.GetCollection<Event>("EVENT").UpdateOne(filter2, updateCommand);

            EventChangeLog eventChangeLog = new EventChangeLog()
            {
                Comments = data.Comments,
                EventCode = result.Code,
                UpdatedBy = data.UserName,
                UpdatedDate = DateTime.Now
            };
            db.GetCollection<EventChangeLog>("EVENT_CHANGE_LOG").InsertOne(eventChangeLog);

            return data;
        }
        public Reclassification PostReclassificationEvent(Reclassification data)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            Event result = new Event();

            var filter = Builders<Event>.Filter.Eq("OriginalCode", data.Code);
            result = db.GetCollection<Event>("EVENT").Find<Event>(filter).FirstOrDefault();


            var filter2 = Builders<Event>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(result.IdString)));


            if (data.Status == true)
            {
                // classify
                var updateCommand = Builders<Event>.Update

              .Set("ReclassifiedSubEventTypeCode", data.EventTypeCode)
              .Set("EventAcknowledged", true)
              // .Set("Eventclassified", true)
              .Set("UpdatedTime", DateTime.Now)
              .Set("EventVersion.$.Version", 1)
              .Set("EventVersion.$.EventCode", data.EventTypeCode)
              .Set("EventVersion.$.UpdatedBy", 1)
              .Set("EventVersion.$.Comments", "abc")
              .Set("EventVersion.$.UpdatedDate", DateTime.Now);

                db.GetCollection<Event>("EVENT").UpdateOne(filter2, updateCommand);
            }
            else
            {
                // Re-classify
                var updateCommand = Builders<Event>.Update
               .Set("ReclassifiedEventTypeCode", data.EventTypeCode)
               .Set("ReclassifiedSubEventTypeCode", data.SubEventTypeCode)
               .Set("EventAcknowledged", true)
               .Set("EventReclassified", true)
               .Set("UpdatedTime", DateTime.Now);

                db.GetCollection<Event>("EVENT").UpdateOne(filter2, updateCommand);
            }


            return data;
        }

        public PagedSearchResultMongo<Event> Search(SearchCriteria<EventFilter> searchCriteria, int IncludedTime = 0)
        {
            try
            {
                PagedSearchResultMongo<Event> result = new PagedSearchResultMongo<Event>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<Event>.Empty;
                if (searchCriteria.Filters != null)
                {
                    if (searchCriteria.Filters.StartDate != null)
                    {
                        //    searchCriteria.Filters.StartDate = new DateTime(searchCriteria.Filters.StartDate.Value.Year, searchCriteria.Filters.StartDate.Value.Month,
                        //                              searchCriteria.Filters.StartDate.Value.Day, searchCriteria.Filters.StartDate.Value.Hour, searchCriteria.Filters.StartDate.Value.Minute, searchCriteria.Filters.StartDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                        query = query & Builders<Event>.Filter.Gte("StartDate", searchCriteria.Filters.StartDate);
                    }
                    if (searchCriteria.Filters.EndDate != null)
                    {
                        DateTime date = (DateTime)searchCriteria.Filters.EndDate;
                        searchCriteria.Filters.EndDate = new DateTime(date.Year, date.Month,
                                                 date.Day, searchCriteria.Filters.EndDate.Value.Hour,
                                                 searchCriteria.Filters.EndDate.Value.Minute, searchCriteria.Filters.EndDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                        query = query & Builders<Event>.Filter.Lte("EndDate", searchCriteria.Filters.EndDate);
                    }
                    if (searchCriteria.Filters.EventTypeCode != null)
                    {
                        query = query & Builders<Event>.Filter.Eq("EventTypeCode", searchCriteria.Filters.EventTypeCode);
                    }
                    if (searchCriteria.Filters.TripCode != null)
                    {
                        query = query & Builders<Event>.Filter.Eq("TripCode", searchCriteria.Filters.TripCode);
                    }

                    if (searchCriteria.Filters.VehicleCode != null)
                    {
                        query = query & Builders<Event>.Filter.Eq("VehicleCode", searchCriteria.Filters.VehicleCode);
                    }
                    if (searchCriteria.Filters.Shift != null)
                    {
                        query = query & Builders<Event>.Filter.Eq("Shift", searchCriteria.Filters.Shift);
                    }

                    query = query & Builders<Event>.Filter.Eq("EventAccepted", false);
                    query = query & Builders<Event>.Filter.Eq("EventRejected", false);

                }

                result.NumberOfRecords = db.GetCollection<Event>("EVENT").Count(query);
                result.Collection = db.GetCollection<Event>("EVENT").Find<Event>(query)
                     .SortByDescending(p => p.EndDate).Skip(searchCriteria.ItemsToSkip).Limit(100).ToList();


                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Domain.DataModel.EventEvidence> GetViolationDataByEventType(int eventTypeCode)
        {
            
            List<Domain.DataModel.EventEvidence> vehicleDetList = aDPEntities.EventEvidences.Where(c=>c.ViolationTypeID == eventTypeCode.ToString()).ToList();

            return vehicleDetList;
            //try
            //{
            //    PagedSearchResultMongo<Event> result = new PagedSearchResultMongo<Event>();

            //    IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            //    var query = FilterDefinition<Event>.Empty;
            //    DateTime today = DateTime.Today;
            //    string startDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();




            //    query = query & Builders<Event>.Filter.Eq("EventTypeCode", eventTypeCode);
            //    query = query & Builders<Event>.Filter.Gte("StartDate", startDate);
            //    query = query & Builders<Event>.Filter.Lte("StartDate", DateTime.Now.AddHours(4));

            //    result.NumberOfRecords = db.GetCollection<Event>("EVENT").Count(query);
            //    result.Collection = db.GetCollection<Event>("EVENT").Find<Event>(query)
            //         .ToList();


            //    return result.Collection;

            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }

        public Event UpdateEventsWithMedia(Event item)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();


            var filter2 = Builders<Event>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(item.IdString)));
            var updateCommand = Builders<Event>.Update
                .Set("MediaData", item.MediaData);
            if (item.MediaData != null)
            {
                db.GetCollection<Event>("EVENT").UpdateMany(filter2, updateCommand);
            }

            //db.GetCollection<Event>("EVENT").UpdateOne(filter2, updateCommand);


            return item;
        }

        public long GetEventCountByEventTypeCode(int eventTypeCode, string shift, int vehicleCode)
        {
            try
            {
                PagedSearchResultMongo<Event> result = new PagedSearchResultMongo<Event>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();
                if (shift == "Night")
                {
                    startDate = DateTime.Now.Date.AddHours(18);
                    endDate = DateTime.Now.AddDays(1).Date.AddHours(6);
                }
                else
                {
                    startDate = DateTime.Now.Date.AddHours(6);
                    endDate = DateTime.Now.Date.AddHours(18);
                }

                var query = FilterDefinition<Event>.Empty;


                query = query & Builders<Event>.Filter.Gte("StartDate", startDate);
                query = query & Builders<Event>.Filter.Lte("EndDate", endDate);
                query = query & Builders<Event>.Filter.Eq("EventTypeCode", eventTypeCode);
                query = query & Builders<Event>.Filter.Eq("Shift", shift);





                result.NumberOfRecords = db.GetCollection<Event>("EVENT").Count(query);
                //result.Collection = db.GetCollection<Event>("EVENT").Find<Event>(query)
                //     .SortByDescending(p => p.EndDate).Skip(searchCriteria.ItemsToSkip).Limit(100).ToList();


                return result.NumberOfRecords;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Event> GetAllSM_EventsFromFMS(long? minId)
        {
            List<Event> result = new List<Event>();

            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<Event>.Empty;
            var filter = Builders<Event>.Filter.Empty;
            result = db.GetCollection<Event>("EVENT").Find(c => (c.EventTypeCode.Value == (int)EventType.Driver_Fatigue || c.EventTypeCode.Value == (int)EventType.Distraction || c.EventTypeCode.Value == (int)EventType.FOV_Exception) && c.OriginalCode > minId).ToList();
            return result;
        }

        public List<Event> GetEventsByVehcileCodeForDashboard(int? VehicleCode, DateTime? StartDate = null, DateTime? EndDate = null, string Shift = null, int IncludedTime = 0)
        {
            try
            {
                List<Event> result = new List<Event>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<Event>.Empty;

                if (StartDate != null)
                {
                    StartDate = new DateTime(StartDate.Value.Year, StartDate.Value.Month,
                                              StartDate.Value.Day, StartDate.Value.Hour, StartDate.Value.Minute, StartDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                    query = query & Builders<Event>.Filter.Gte("StartDate", StartDate);
                }
                if (EndDate != null)
                {
                    DateTime date = (DateTime)EndDate;
                    EndDate = new DateTime(date.Year, date.Month,
                                             date.Day, EndDate.Value.Hour,
                                             EndDate.Value.Minute, EndDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                    query = query & Builders<Event>.Filter.Lte("EndDate", EndDate);
                }
                if (VehicleCode != null)
                {
                    query = query & Builders<Event>.Filter.Eq("VehicleCode", VehicleCode);
                }

                if (Shift != null)
                {
                    query = query & Builders<Event>.Filter.Eq("Shift", Shift);
                }

                result = db.GetCollection<Event>("EVENT").Find<Event>(query).ToList();

                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Event> GetEventsByVehcileCode(int? VehicleCode, DateTime? StartDate = null, DateTime? EndDate = null, string Shift = null, int IncludedTime = 0)
        {
            try
            {
                List<Event> result = new List<Event>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<Event>.Empty;

                if (StartDate != null)
                {
                    StartDate = new DateTime(StartDate.Value.Year, StartDate.Value.Month,
                                              StartDate.Value.Day, StartDate.Value.Hour, StartDate.Value.Minute, StartDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);

                    query = query & Builders<Event>.Filter.Gte("StartDate", StartDate);
                }
                if (EndDate != null)
                {
                    DateTime date = (DateTime)EndDate;
                    EndDate = new DateTime(date.Year, date.Month,
                                             date.Day, EndDate.Value.Hour,
                                             EndDate.Value.Minute, EndDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                    query = query & Builders<Event>.Filter.Lte("EndDate", EndDate);
                }
                if (VehicleCode != null)
                {
                    query = query & Builders<Event>.Filter.Eq("VehicleCode", VehicleCode);
                }

                if (Shift != null)
                {
                    query = query & Builders<Event>.Filter.Eq("Shift", Shift);
                }

                query = query & Builders<Event>.Filter.Eq("EventSource", "SM");
                query = query & Builders<Event>.Filter.Eq("EventAccepted", "false");

                result = db.GetCollection<Event>("EVENT").Find<Event>(query).ToList();

                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DriverProfileDto GetEventDetailsByDriverId(int driver_code)
        {
            List<Event> result = new List<Event>();
            List<Event> new_result = new List<Event>();
            var driver_dto = new DriverProfileDto();

            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Event>.Filter.Eq("DriverCode", driver_code);
            result = db.GetCollection<Event>("EVENT").Find<Event>(filter).ToList();


            foreach (var item in result)
            {
                if (item.StartDate.Value.Year == 2018 || item.StartDate.Value.Month == DateTime.Now.Month || item.StartDate.Value.Month == 11 || item.StartDate.Value.Month == 10)
                {
                    new_result.Add(item);
                }
            }

            foreach (var dtoItem in new_result)
            {
                switch (dtoItem.EventTypeCode)
                {
                    case 25:
                        driver_dto.DistractionCount += driver_dto.DistractionCount; driver_dto.DistractionScore += GetScore(dtoItem.EventTypeCode.Value);
                        break;
                    case 22: driver_dto.FatigueCount += 1; driver_dto.FatigueScore += GetScore(dtoItem.EventTypeCode.Value); break;
                    case 14: driver_dto.ForwardCollisionWarningCount += 1; driver_dto.ForwardCollisionWarningScore += GetScore(dtoItem.EventTypeCode.Value); break;
                    case 24: driver_dto.FOVCount += 1; driver_dto.FOVScore += GetScore(dtoItem.EventTypeCode.Value); break;
                    case 11: driver_dto.PowerFailureCount += 1; driver_dto.PowerFailureScore += GetScore(dtoItem.EventTypeCode.Value); break;

                }


            }
            return driver_dto;
        }
        private int GetScore(int eventTypeId)
        {
            ScoreSetUpDAL scoreSetUpDAL = new ScoreSetUpDAL();
            var scoreMaster = scoreSetUpDAL.GetAll();

            foreach (var item in scoreMaster)
            {
                if (item.EventTypeId == eventTypeId)
                {
                    return item.Score;
                }

            }
            return 0;
        }

        public List<Event> GetViolationData()
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            DateTime currentDate = new DateTime(DateTime.Now.Ticks);

            int days = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);

            DateTime thisDate = new DateTime(currentDate.Year, currentDate.Month, 01);

            var filter = Builders<Event>.Filter.Empty;
            var query = FilterDefinition<Event>.Empty;
            query &= Builders<Event>.Filter.Gte("StartDate", thisDate.AddHours(-4));

            var collection = db.GetCollection<Event>("EVENT").Find<Event>(query).ToList();
           
            return collection;
        }

        public List<Event> GetSM_EventsByDateAndVehicle(DateTime startDate, DateTime endDate, int? vehicleCode, int eventType)
        {
            List<Event> result = new List<Event>();

            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<Event>.Empty;
            //if (startDate != null)
            //{

            //    query = query & Builders<Event>.Filter.Gte("StartDate", startDate);
            //}
            //if (endDate != null)
            //{

            //    query = query & Builders<Event>.Filter.Lte("EndDate", endDate);
            //}
            if (vehicleCode != null)
            {
                query = query & Builders<Event>.Filter.Eq("VehicleCode", vehicleCode);
            }

            query = query & Builders<Event>.Filter.Eq("EventTypeCode", eventType);

            query = query & Builders<Event>.Filter.Eq("EventSource", "SM");


            result = db.GetCollection<Event>("EVENT").Find<Event>(query).ToList();

            return result;

        }

        public void UpdateEventWithDriver(Event eventData)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter2 = Builders<Event>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(eventData.IdString)));
            var updateCommand = Builders<Event>.Update

              .Set("DriverCode", eventData.DriverCode);


            db.GetCollection<Event>("EVENT").UpdateOne(filter2, updateCommand);

        }

        public List<Event> GetAllEventsByVehicleCode(int vehicleCode)
        {
            List<Event> result = new List<Event>();

            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<Event>.Empty;
            query = query & Builders<Event>.Filter.Eq("VehicleCode", vehicleCode);

            result = db.GetCollection<Event>("EVENT").Find<Event>(query).ToList();

            return result;
        }

        public List<Event> GetAllEventsByMonth(int code, bool check_for_dri_veh)
        {
            List<Event> result = new List<Event>();


            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            DateTime currentDate = new DateTime(DateTime.Now.Ticks);

            int days = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);

            DateTime thisDate = new DateTime(currentDate.Year, currentDate.Month, 01);
            DateTime lessDate = new DateTime(currentDate.Year, currentDate.Month, days);



            var filter = Builders<Event>.Filter.Empty;
            var query = FilterDefinition<Event>.Empty;
            query &= Builders<Event>.Filter.Gte("StartDate", thisDate.AddHours(-4));
            query &= Builders<Event>.Filter.Lt("StartDate", lessDate.AddHours(-4));

            if (check_for_dri_veh)
            {
                query &= Builders<Event>.Filter.Eq("VehicleCode", code);
            }
            else
            {
                query &= Builders<Event>.Filter.Eq("DriverCode", code);

            }

            result = db.GetCollection<Event>("EVENT").Find<Event>(query).ToList();
            return result;
        }


        public List<Domain.DataModel.EventEvidence> SearchEventForPage(SearchCriteria<EventFilter> searchCriteria, int IncludedTime = 0)
        {
            List<Domain.DataModel.EventEvidence> eventDetList = new List<Domain.DataModel.EventEvidence>();
            try
            {
                if (searchCriteria.Filters.StartDate == null && searchCriteria.Filters.StartDate == null)
                {
                    DateTime today = DateTime.Today.AddDays(-2);

                    string startDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
                    searchCriteria.Filters.StartDate = Convert.ToDateTime(startDate);
                    searchCriteria.Filters.EndDate = DateTime.Now;
                }
                if(searchCriteria.Filters.EventTypeCodes.Length > 0 && searchCriteria.Filters.PlateNumbers.Length > 0)
                {
                    List<string> debugCodes = new List<string>();
                    foreach (var item in searchCriteria.Filters.EventTypeCodes)
                    {
                        debugCodes.Add(item.ToString());
                    }
                    eventDetList = aDPEntities.EventEvidences.Where(x => debugCodes.Contains(x.Debug_Code) 
                    && searchCriteria.Filters.PlateNumbers.Contains(x.BusId) && x.AlarmTime >= searchCriteria.Filters.StartDate &&
                                                                                                          x.AlarmTime <= searchCriteria.Filters.EndDate).ToList();
                }
                else
                {
                    eventDetList = aDPEntities.EventEvidences.Where(c => c.AlarmTime >= searchCriteria.Filters.StartDate &&
                                                                                                          c.AlarmTime <= searchCriteria.Filters.EndDate).Select(c => c).ToList();
                }



                return eventDetList;

                

            }
            catch (Exception ex)
            {
                Raqeeb.Common.Utility.WriteLog(ex);
                return eventDetList;
            }

        }

        public List<int> GetTotalAlarms()
        {
            List<int> alarm_count = new List<int>();

            alarm_count.Add(aDPEntities.DeviceAlarmDetails.Count());
            alarm_count.Add(aDPEntities.DeviceAlarmDetails.Where(c => c.Type == 1).Count());           
            alarm_count.Add(aDPEntities.DeviceAlarmDetails.Where(c => c.Type == 3).Count());           
            alarm_count.Add(aDPEntities.DeviceAlarmDetails.Where(c => c.Type == 4).Count());
            alarm_count.Add(aDPEntities.DeviceAlarmDetails.Where(c => c.Type == 74).Count());

            return alarm_count;
        }
        public List<int> GetTotalAlarms(List<Domain.DataModel.Device> devices)
        {
            List<int> alarm_count = new List<int>();
            List<string> deviceId = new List<string>();

            foreach (var item in devices)
            {
                deviceId.Add(item.DeviceId);
            }

            alarm_count.Add(aDPEntities.DeviceAlarmDetails.Where(x=>deviceId.Contains(x.DeviceId)).Count());
            alarm_count.Add(aDPEntities.DeviceAlarmDetails.Where(c => c.Type == 1 && deviceId.Contains(c.DeviceId)).Count());
            alarm_count.Add(aDPEntities.DeviceAlarmDetails.Where(c => c.Type == 3 && deviceId.Contains(c.DeviceId)).Count());
            alarm_count.Add(aDPEntities.DeviceAlarmDetails.Where(c => c.Type == 4 && deviceId.Contains(c.DeviceId)).Count());
            alarm_count.Add(aDPEntities.DeviceAlarmDetails.Where(c => c.Type == 74 && deviceId.Contains(c.DeviceId)).Count());

            return alarm_count;
        }


        public List<int> GetViolationCount()
        {
            List<int> count = new List<int>();
            try
            {
                var tt = aDPEntities.EventEvidences.Select(c => c).Count();             
                count.Add(aDPEntities.EventEvidences.Select(c => c).Count());
                count.Add(aDPEntities.EventEvidences.Where(c => c.ValidationStatus.HasValue).Count());
                count.Add(aDPEntities.EventEvidences.Where(c => c.ProcessedStatus.HasValue).Count());
                count.Add(aDPEntities.EventEvidences.Where(c => c.ProcessedStatus.HasValue == false).Count());
               
                return count;
            }
            catch (Exception ex)
            {

                return count;
            }
            
        }
        public List<int> GetViolationCount(string groupName)
        {
            List<int> count = new List<int>();
            try
            {

                var tt = aDPEntities.EventEvidences.Where(c=>c.RoadRegion == groupName).Select(c => c).Count();
                count.Add(aDPEntities.EventEvidences.Where(c => c.RoadRegion == groupName).Select(c => c).Count());
                count.Add(aDPEntities.EventEvidences.Where(c => c.ValidationStatus.HasValue && c.RoadRegion == groupName).Count());
                count.Add(aDPEntities.EventEvidences.Where(c => c.ProcessedStatus.HasValue && c.RoadRegion == groupName).Count());
                count.Add(aDPEntities.EventEvidences.Where(c => c.ProcessedStatus.HasValue == false && c.RoadRegion == groupName).Count());
                
                return count;
            }
            catch (Exception ex)
            {

                return count;
            }

        }

        public List<UnknownDriverLog> SearchLogs(SearchCriteria<EventFilter> searchCriteria)
        {
            try
            {
                List<UnknownDriverLog> result = new List<UnknownDriverLog>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<UnknownDriverLog>.Empty;
                if (searchCriteria.Filters != null)
                {
                    if (searchCriteria.Filters.StartDate != null)
                    {
                        searchCriteria.Filters.StartDate = new DateTime(searchCriteria.Filters.StartDate.Value.Year, searchCriteria.Filters.StartDate.Value.Month,
                                                  searchCriteria.Filters.StartDate.Value.Day, searchCriteria.Filters.StartDate.Value.Hour, searchCriteria.Filters.StartDate.Value.Minute, searchCriteria.Filters.StartDate.Value.Second).AddHours(IncreasedTimeFromUI);
                        query = query & Builders<UnknownDriverLog>.Filter.Gte("LogDate", searchCriteria.Filters.StartDate);
                    }
                    if (searchCriteria.Filters.EndDate != null)
                    {
                        DateTime date = (DateTime)searchCriteria.Filters.EndDate;
                        searchCriteria.Filters.EndDate = new DateTime(date.Year, date.Month,
                                                 date.Day, searchCriteria.Filters.EndDate.Value.Hour,
                                                 searchCriteria.Filters.EndDate.Value.Minute, searchCriteria.Filters.EndDate.Value.Second).AddHours(IncreasedTimeFromUI );
                        query = query & Builders<UnknownDriverLog>.Filter.Lte("LogDate", searchCriteria.Filters.EndDate);
                    }
               
                    if (searchCriteria.Filters.VehicleCode != null)
                    {
                        query = query & Builders<UnknownDriverLog>.Filter.Eq("VehicleId", searchCriteria.Filters.VehicleCode);
                       
                    }
                    
                }

                // result.NumberOfRecords = db.GetCollection<Event>("EVENT").Count(query);
                result = db.GetCollection<UnknownDriverLog>("UNKNOWN_DRIVER_LOGS").Find<UnknownDriverLog>(query).SortByDescending(c=>c.LogDate)
                     .ToList();

                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Event getExistingGeofenceEvent(int? code, int geofence_Violation)
        {
            
            try
            {
                List<Event> result = new List<Event>();
                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<Event>.Empty;

                query &= Builders<Event>.Filter.Eq("VehicleCode", code);
                query &= Builders<Event>.Filter.Eq("EventTypeCode", geofence_Violation);

                result = db.GetCollection<Event>("EVENT").Find<Event>(query)
                    .ToList();
                return result[result.Count - 1];

            }
            catch (Exception)
            {
                Event emptyEvent = new Event();
                return emptyEvent;
            }
        }

        public void UpdateGeofenceEvent(Event e_event)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
          
            var filter2 = Builders<Event>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(e_event.IdString)));
            var updateCommand = Builders<Event>.Update
                .Set("EndPositionCode", e_event.EndPositionCode)
                .Set("EndDate", e_event.EndDate)
                .Set("Shift", e_event.Shift)
                .Set("Speed", e_event.Speed);
                

            db.GetCollection<Event>("EVENT").UpdateOne(filter2, updateCommand);
        }

        public List<Domain.DataModel.EventEvidence> GetAllViolatins()
        {
            List<Event> result = new List<Event>();
            List<Domain.DataModel.EventEvidence> vehicleDetList = aDPEntities.EventEvidences.ToList();

            return vehicleDetList;
        }

        public Domain.DataModel.EventEvidence GetViolationById(string evidenceId)
        {
            return aDPEntities.EventEvidences.Where(c => c.EvidenceID == evidenceId).FirstOrDefault();
        }
        #endregion
    }
}
