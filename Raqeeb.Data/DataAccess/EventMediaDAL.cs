﻿using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class EventMediaDAL : DataAccessBase
    {
        #region Methods
        public List<EventMedia> InsertBulk(List<EventMedia> eventMedia)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            if(eventMedia.Count>0)
            db.GetCollection<EventMedia>("EVENT_MEDIA").InsertMany(eventMedia);
            return eventMedia;
        }
        public List<Media> GetByEventCode(long eventCode)
        {
            List<Media> result = new List<Media>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Media>.Filter.Eq("EventCode", eventCode);
            result = db.GetCollection<Media>("MEDIA").Find<Media>(filter).ToList();
            return result;
        }
        #endregion
    }
}
