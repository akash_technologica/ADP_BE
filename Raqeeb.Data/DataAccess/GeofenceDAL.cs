﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class GeofenceDAL : DataAccessBase
    {
        #region Methods
        public Geofence Insert(Geofence geofence)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<Geofence>("GEOFENCE").InsertOne(geofence);
            return geofence;
        }


        public List<Geofence> GetAllGeofence()
        {
            Geofence result = new Geofence();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Geofence>.Filter.Empty;
            return db.GetCollection<Geofence>("GEOFENCE").Find<Geofence>(filter).ToList();
        }

        public Geofence DeleteGeofence(Geofence geofence)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(geofence.IdString);
            var filter = Builders<Geofence>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<Geofence>("GEOFENCE").DeleteOne(filter);
            return geofence;
        }

        public Geofence UpdateAsync(Geofence geofence)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
          
            var filter = Builders<Geofence>
             .Filter.Eq(e => e.IdString, geofence.IdString);

            foreach (var item in geofence.VehicleCodes)
            {
                var update = Builders<Geofence>.Update.Push<int>(e => e.VehicleCodes, item);
                db.GetCollection<Geofence>("GEOFENCE").FindOneAndUpdate(filter, update);
            }
                      
           
            return geofence;
        }
        public Geofence UpdateGeofencing(Geofence geofence)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
           
            var filter = Builders<Geofence>
             .Filter.Eq(e => e.IdString, geofence.IdString);


            var updateCommand = Builders<Geofence>.Update
                     .Set("Name", geofence.Name)
                     .Set("Margin", geofence.Margin)
                     .Set("Notification", geofence.Notification)
                     .Set("Points", geofence.Points)
                     .Set("StartTime", geofence.StartTime)
                     .Set("EndTime", geofence.EndTime)
                      .Set("Description", geofence.Description);
                      

            db.GetCollection<Geofence>("GEOFENCE").UpdateMany(filter, updateCommand);

          

            return geofence;
        }
        #endregion
    }
}
