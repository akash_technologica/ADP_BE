﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class TripDAL : DataAccessBase
    {
        #region Methods
        public List<Trip> InsertBulk(List<Trip> trips)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            if (trips.Count > 0)
            {
                db.GetCollection<Trip>("TRIP").InsertMany(trips);
            }

            return trips;
        }


        public Trip GetByCode(long? code)
        {
            Trip result = new Trip();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Trip>.Filter.Eq("Code", code);
            result = db.GetCollection<Trip>("TRIP").Find<Trip>(filter).FirstOrDefault();
            return result;
        }



        public Trip GetTripByCode(long? code)
        {
            Trip result = new Trip();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Trip>.Filter.Eq("Code", code);
            result = db.GetCollection<Trip>("TRIP").Find<Trip>(filter).FirstOrDefault();
            return result;
        }


        public List<Trip> Search(SearchCriteria<TripFilter> searchCriteria, int IncludedTime = 0)
        {
            try
            {
                PagedSearchResultMongo<Trip> result = new PagedSearchResultMongo<Trip>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<Trip>.Empty;
                if (searchCriteria.Filters != null)
                {
                    if (searchCriteria.Filters.StartDate != null)
                    {
                        searchCriteria.Filters.StartDate = new DateTime(searchCriteria.Filters.StartDate.Value.Year, searchCriteria.Filters.StartDate.Value.Month,
                                                  searchCriteria.Filters.StartDate.Value.Day, searchCriteria.Filters.StartDate.Value.Hour,
                                                  searchCriteria.Filters.StartDate.Value.Minute, searchCriteria.Filters.StartDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                        query = query & Builders<Trip>.Filter.Gte("StartDate", searchCriteria.Filters.StartDate);
                    }
                    if (searchCriteria.Filters.EndDate != null)
                    {
                        DateTime date = (DateTime)searchCriteria.Filters.EndDate;
                        searchCriteria.Filters.EndDate = new DateTime(date.Year, date.Month,
                                                 date.Day, searchCriteria.Filters.EndDate.Value.Hour,
                                                 searchCriteria.Filters.EndDate.Value.Minute, searchCriteria.Filters.EndDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                        query = query & Builders<Trip>.Filter.Lte("EndDate", searchCriteria.Filters.EndDate);
                    }
                    if (searchCriteria.Filters.VehicleCode != null)
                    {
                        query = query & Builders<Trip>.Filter.Eq("VehicleCode", searchCriteria.Filters.VehicleCode);
                    }
                    if (searchCriteria.Filters.DriverCode != null)
                    {
                        query = query & Builders<Trip>.Filter.Eq("DriverCode", searchCriteria.Filters.DriverCode);
                    }
                }
                result.NumberOfRecords = db.GetCollection<Trip>("TRIP").Count(query);
                result.Collection = db.GetCollection<Trip>("TRIP").Find<Trip>(query)
                     .SortByDescending(p => p.EndDate).Skip(searchCriteria.ItemsToSkip).Limit(searchCriteria.PageSize).ToList();



                return result.Collection;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public PagedSearchResultMongo<Trip> GetTripsForDashboard(SearchCriteria<TripFilter> searchCriteria, int IncludedTime = 0)
        {
            try
            {
                PagedSearchResultMongo<Trip> result = new PagedSearchResultMongo<Trip>();

                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<Trip>.Empty;
                var filter1 = new BsonDocument("$project", new BsonDocument()
                        .Add("_id", 0)
                        .Add("TRIP", "$$ROOT"));
                // query = query & filter;
                var filter2 = new BsonDocument("$lookup", new BsonDocument()
                       .Add("localField", "TRIP.Code")
                       .Add("from", "EVENT")
                       .Add("foreignField", "TripCode")
                       .Add("as", "EVENT"));

                var filter3 = new BsonDocument("$unwind", new BsonDocument()
                        .Add("path", "$EVENT")
                        .Add("preserveNullAndEmptyArrays", new BsonBoolean(false)));

                var filter4 = new BsonDocument("$match", new BsonDocument()
                        .Add("$and", new BsonArray()
                                .Add(new BsonDocument()
                                        .Add("TRIP.StartDate", new BsonDocument()
                                                .Add("$gte", searchCriteria.Filters.StartDate)
                                        )
                                )
                                .Add(new BsonDocument()
                                        .Add("TRIP.EndDate", new BsonDocument()
                                                .Add("$lte", searchCriteria.Filters.EndDate)
                                        )
                                )
                        ));
                var filter5 = new BsonDocument("$group", new BsonDocument()
                          .Add("_id", new BsonDocument()
                                  .Add("TRIP\u1390Code", "$TRIP.Code")
                                  .Add("EVENT\u1390EventTypeCode", "$EVENT.EventTypeCode")
                          )
                          .Add("COUNT(*)", new BsonDocument()
                                  .Add("$sum", 1)
                          ));
                var filter6 = new BsonDocument("$project", new BsonDocument()
                        .Add("NumberOfRecords", "$COUNT(*)")
                        .Add("TripCode", "$_id.TRIP\u1390Code")
                        .Add("EventTypeCode", "$_id.EVENT\u1390EventTypeCode")
                        .Add("_id", 0));


                //if (searchCriteria.Filters != null)
                //{
                //    if (searchCriteria.Filters.StartDate != null)
                //    {
                //        searchCriteria.Filters.StartDate = new DateTime(searchCriteria.Filters.StartDate.Value.Year, searchCriteria.Filters.StartDate.Value.Month,
                //                                  searchCriteria.Filters.StartDate.Value.Day, searchCriteria.Filters.StartDate.Value.Hour,
                //                                  searchCriteria.Filters.StartDate.Value.Minute, searchCriteria.Filters.StartDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                //        query = query & Builders<Trip>.Filter.Gte("StartDate", searchCriteria.Filters.StartDate);
                //    }
                //    if (searchCriteria.Filters.EndDate != null)
                //    {
                //        DateTime date = (DateTime)searchCriteria.Filters.EndDate;
                //        searchCriteria.Filters.EndDate = new DateTime(date.Year, date.Month,
                //                                 date.Day, searchCriteria.Filters.EndDate.Value.Hour,
                //                                 searchCriteria.Filters.EndDate.Value.Minute, searchCriteria.Filters.EndDate.Value.Second).AddHours(IncreasedTimeFromUI + IncludedTime);
                //        query = query & Builders<Trip>.Filter.Lte("EndDate", searchCriteria.Filters.EndDate);
                //    }
                //    if (searchCriteria.Filters.VehicleCode != null)
                //    {
                //        query = query & Builders<Trip>.Filter.Eq("VehicleCode", searchCriteria.Filters.VehicleCode);
                //    }
                //}
                query = query & filter1 & filter2 & filter3 & filter4 & filter5 & filter6;
                result.NumberOfRecords = db.GetCollection<Trip>("TRIP").Count(query);
                result.Collection = db.GetCollection<Trip>("TRIP").Find<Trip>(query)
                     .SortByDescending(p => p.EndDate).Skip(searchCriteria.ItemsToSkip).Limit(searchCriteria.PageSize).ToList();
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public TripCoOridinate GetTripOrdinatesByCode(long code)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<TripCoOridinate>.Empty;

            query = query & Builders<TripCoOridinate>.Filter.Eq("TripCode", code);

            var tripList = db.GetCollection<TripCoOridinate>("TRIP_CO_ORDINATES").Find<TripCoOridinate>(query).FirstOrDefault();

            return tripList;
        }

        public object GetTotalVehiclesByTripDateDashboard(SearchCriteria<TripFilter> searchCriteria)
        {
            long totlaVehiclstrip = 0;
            long totalTrips = 0;
            try
            {
                IMongoDatabase db = MongoDbManager.GetMongoDataBase();

                var query = FilterDefinition<Trip>.Empty;
                if (searchCriteria.Filters != null)
                {
                    if (searchCriteria.Filters.StartDate != null)
                    {
                        // add 4 Hours
                        searchCriteria.Filters.StartDate = new DateTime(searchCriteria.Filters.StartDate.Value.Year, searchCriteria.Filters.StartDate.Value.Month,
                                                  searchCriteria.Filters.StartDate.Value.Day, searchCriteria.Filters.StartDate.Value.Hour,
                                                  searchCriteria.Filters.StartDate.Value.Minute, searchCriteria.Filters.StartDate.Value.Second).AddHours(8);
                        query = query & Builders<Trip>.Filter.Gte("StartDate", searchCriteria.Filters.StartDate);
                    }
                    if (searchCriteria.Filters.EndDate != null)
                    {
                        DateTime date = (DateTime)searchCriteria.Filters.EndDate;
                        searchCriteria.Filters.EndDate = new DateTime(date.Year, date.Month,
                                                 date.Day, searchCriteria.Filters.EndDate.Value.Hour,
                                                 searchCriteria.Filters.EndDate.Value.Minute, searchCriteria.Filters.EndDate.Value.Second).AddHours(8);
                        query = query & Builders<Trip>.Filter.Lte("EndDate", searchCriteria.Filters.EndDate);
                    }
                }
                totalTrips = db.GetCollection<Trip>("TRIP").Find<Trip>(query).Count();
                totlaVehiclstrip = db.GetCollection<Trip>("TRIP").Find<Trip>(query)
                     .SortByDescending(p => p.EndDate).ToList().GroupBy(tr => tr.VehicleCode).Distinct().Count();
                return new { totlaVehiclstrip = totlaVehiclstrip, totalTrips = totalTrips };

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<BsonDocument> GetTripsAndEventCategories(SearchCriteria<TripFilter> searchCriteria)
        {
            List<BsonDocument> result = new List<BsonDocument>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>("TRIP");
            var query = FilterDefinition<Vehicle>.Empty;
            // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

            var options = new AggregateOptions()
            {
                AllowDiskUse = true
            };

            PipelineDefinition<BsonDocument, BsonDocument> pipeline = new BsonDocument[]
           {
                new BsonDocument("$project", new BsonDocument()
                        .Add("_id", 0)
                        .Add("TRIP", "$$ROOT")),
                new BsonDocument("$lookup", new BsonDocument()
                        .Add("localField", "TRIP.Code")
                        .Add("from", "EVENT")
                        .Add("foreignField", "TripCode")
                        .Add("as", "EVENT")),
                new BsonDocument("$unwind", new BsonDocument()
                        .Add("path", "$EVENT")
                        .Add("preserveNullAndEmptyArrays", new BsonBoolean(false))),
                new BsonDocument("$match", new BsonDocument()
                        .Add("$and", new BsonArray()
                                .Add(new BsonDocument()
                                        .Add("TRIP.StartDate", new BsonDocument()
                                                .Add("$gte", searchCriteria.Filters.StartDate)
                                        )
                                )
                                .Add(new BsonDocument()
                                        .Add("TRIP.EndDate", new BsonDocument()
                                                .Add("$lte",searchCriteria.Filters.EndDate)
                                        )
                                )
                        )),
                new BsonDocument("$group", new BsonDocument()
                        .Add("_id", new BsonDocument()
                                .Add("TRIP\u1390Code", "$TRIP.Code")
                                .Add("EVENT\u1390EventTypeCode", "$EVENT.EventTypeCode")
                        )
                        .Add("COUNT(*)", new BsonDocument()
                                .Add("$sum", 1)
                        )),
                new BsonDocument("$project", new BsonDocument()
                        .Add("NumberOfRecords", "$COUNT(*)")
                        .Add("TripCode", "$_id.TRIP\u1390Code")
                        .Add("EventTypeCode", "$_id.EVENT\u1390EventTypeCode")
                        .Add("_id", 0))
           };

            using (var cursor = collection.Aggregate(pipeline, options))
            {
                while (cursor.MoveNext())
                {
                    var batch = cursor.Current;
                    result.AddRange(batch);
                    //foreach (BsonDocument document in batch)
                    //{
                    //    Console.WriteLine(document.ToJson());
                    //}
                }
            }

            return result;
        }

        public void UpdateTripCoOdinateList(TripCoOridinate item)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            Trip result = new Trip();

            if (item != null)
            {
               db.GetCollection<TripCoOridinate>("TRIP_CO_ORDINATES").InsertOne(item);

            }


        }

        public List<Trip> GetTripListByCode(long minId)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<Trip>.Empty;

            query = query & Builders<Trip>.Filter.Gte("Code", minId);

            var tripList = db.GetCollection<Trip>("TRIP").Find<Trip>(query).Limit(200).ToList();

            return tripList;
        }

        public List<BsonDocument> GetEventsByTripVehicles()
        {
            List<BsonDocument> result = new List<BsonDocument>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>("VEHICLE");

            // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

            var options = new AggregateOptions()
            {
                AllowDiskUse = true
            };

            PipelineDefinition<BsonDocument, BsonDocument> pipeline = new BsonDocument[]
            {
                new BsonDocument("$project", new BsonDocument()
                        .Add("_id", 0)
                        .Add("VEHICLE", "$$ROOT")),
                new BsonDocument("$lookup", new BsonDocument()
                        .Add("localField", "VEHICLE.Code")
                        .Add("from", "TRIP")
                        .Add("foreignField", "VehicleCode")
                        .Add("as", "TRIP")),
                new BsonDocument("$unwind", new BsonDocument()
                        .Add("path", "$TRIP")
                        .Add("preserveNullAndEmptyArrays", new BsonBoolean(false))),
                new BsonDocument("$lookup", new BsonDocument()
                        .Add("localField", "VEHICLE.Code")
                        .Add("from", "EVENT")
                        .Add("foreignField", "VehicleCode")
                        .Add("as", "EVENT")),
                new BsonDocument("$unwind", new BsonDocument()
                        .Add("path", "$EVENT")
                        .Add("preserveNullAndEmptyArrays", new BsonBoolean(false))),
                new BsonDocument("$match", new BsonDocument()
                        .Add("$and", new BsonArray()
                                .Add(new BsonDocument()
                                        .Add("TRIP.StartDate", new BsonDocument()
                                                .Add("$gte", new BsonDateTime(DateTime.ParseExact("2018-12-22 04:00:00.000+0400", "yyyy-MM-dd HH:mm:ss.fffzzz", CultureInfo.InvariantCulture)))
                                        )
                                )
                                .Add(new BsonDocument()
                                        .Add("TRIP.StartDate", new BsonDocument()
                                                .Add("$lte", new BsonDateTime(DateTime.ParseExact("2018-12-24 04:00:00.000+0400", "yyyy-MM-dd HH:mm:ss.fffzzz", CultureInfo.InvariantCulture)))
                                        )
                                )
                                .Add(new BsonDocument()
                                        .Add("EVENT.StartDate", new BsonDocument()
                                                .Add("$gte", new BsonDateTime(DateTime.ParseExact("2018-12-22 04:00:00.000+0400", "yyyy-MM-dd HH:mm:ss.fffzzz", CultureInfo.InvariantCulture)))
                                        )
                                )
                                .Add(new BsonDocument()
                                        .Add("EVENT.StartDate", new BsonDocument()
                                                .Add("$lte", new BsonDateTime(DateTime.ParseExact("2018-12-24 04:00:00.000+0400", "yyyy-MM-dd HH:mm:ss.fffzzz", CultureInfo.InvariantCulture)))
                                        )
                                )
                                .Add(new BsonDocument()
                                        .Add("EVENT.TripCode", BsonNull.Value)
                                )
                        )),
                new BsonDocument("$group", new BsonDocument()
                        .Add("_id", new BsonDocument()
                                .Add("VEHICLE\u1390Code", "$VEHICLE.Code")
                                .Add("EVENT\u1390EventTypeCode", "$EVENT.EventTypeCode")
                        )
                        .Add("COUNT(*)", new BsonDocument()
                                .Add("$sum", 1)
                        )),
                new BsonDocument("$project", new BsonDocument()
                        .Add("NumberOfRecords", "$COUNT(*)")
                        .Add("VehicleCode", "$_id.VEHICLE\u1390Code")
                        .Add("EventTypeCode", "$_id.EVENT\u1390EventTypeCode")
                        .Add("_id", 0))
            };

            using (var cursor = collection.Aggregate(pipeline, options))
            {
                while (cursor.MoveNext())
                {
                    var batch = cursor.Current;
                    //foreach (BsonDocument document in batch)
                    //{
                    //    Console.WriteLine(document.ToJson());
                    //}
                    result.AddRange(batch);
                }
            }

            return result;
        }

        public List<Trip> GetAllTripByMonth(int code, bool check_for_dri_veh)
        {
            List<Trip> result = new List<Trip>();


            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            DateTime currentDate = new DateTime(DateTime.Now.Ticks);

            int days = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);

            DateTime thisDate = new DateTime(currentDate.Year, currentDate.Month, 01);
            DateTime lessDate = new DateTime(currentDate.Year, currentDate.Month, days);



            var filter = Builders<Trip>.Filter.Empty;
            var query = FilterDefinition<Trip>.Empty;
            query &= Builders<Trip>.Filter.Gte("StartDate", thisDate.AddHours(-4));
            query &= Builders<Trip>.Filter.Lt("StartDate", lessDate.AddDays(-4));
            if (check_for_dri_veh)
            {
                query &= Builders<Trip>.Filter.Eq("VehicleCode", code);

            }
            else
            {
                query &= Builders<Trip>.Filter.Eq("DriverCode", code);

            }


            result = db.GetCollection<Trip>("TRIP").Find<Trip>(query).ToList();
            // db.

            //db.GetCollection.AsQueryable<Element>().Select(e => e.Word).Distinct();

            result = result.GroupBy(x => x.Code).Select(c => c.First()).ToList();
            return result;
        }

        public List<Trip> GetTripData()
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Trip>.Filter.Empty;
            var collection = db.GetCollection<Trip>("TRIP");
            var trip = collection.Find(filter).ToList();
            return trip;
        }

        #endregion
    }
}
