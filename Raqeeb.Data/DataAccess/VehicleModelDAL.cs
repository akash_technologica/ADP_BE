﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class VehicleModelDAL:  DataAccessBase
    {
        #region Methods
        public VehicleModel Insert(VehicleModel vehicleModel)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<VehicleModel>("vehicleModel").InsertOne(vehicleModel);
            return vehicleModel;
        }


        public List<VehicleModel> GetAllvehicleModel()
        {
            VehicleModel result = new VehicleModel();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<VehicleModel>.Filter.Empty;
            return db.GetCollection<VehicleModel>("vehicleModel").Find<VehicleModel>(filter).ToList();
        }
        public List<VehicleModel> GetAllvehicleModelByBrand(string code)
        {
            VehicleModel result = new VehicleModel();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<VehicleModel>.Filter.Empty;
            filter = Builders<VehicleModel>
             .Filter.Eq(e => e.VehicleBrandId, code);
            return db.GetCollection<VehicleModel>("VEHICLE_MODEL").Find<VehicleModel>(filter).ToList();
        }
        public VehicleModel DeletevehicleModel(VehicleModel vehicleModel)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(vehicleModel.IdString);
            var filter = Builders<VehicleModel>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<VehicleModel>("vehicleModel").DeleteOne(filter);
            return vehicleModel;
        }

        public VehicleModel UpdateAsync(VehicleModel vehicleModel)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var filter = Builders<VehicleModel>
             .Filter.Eq(e => e.IdString, vehicleModel.IdString);




            return vehicleModel;
        }
        public VehicleModel UpdateVehicleModel(VehicleModel vehicleModel)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var filter = Builders<VehicleModel>
             .Filter.Eq(e => e.IdString, vehicleModel.IdString);


            var updateCommand = Builders<VehicleModel>.Update
                     .Set("ModelName", vehicleModel.ModelName)
                     .Set("ModelNumber", vehicleModel.ModelNumber);
                    


            db.GetCollection<VehicleModel>("vehicleModel").UpdateMany(filter, updateCommand);



            return vehicleModel;
        }
        #endregion
    }
}
