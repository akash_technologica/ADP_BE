﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class SimCardMasterDAL : DataAccessBase
    {
        #region Methods

        public List<SimCard_Master> GetAll(string code)
        {
            List<SimCard_Master> result = new List<SimCard_Master>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<SimCard_Master>.Empty;
            query &= Builders<SimCard_Master>.Filter.Eq("Gsm_Code", new BsonObjectId(new ObjectId(code)));

            result = db.GetCollection<SimCard_Master>("SIM_MASTER").Find<SimCard_Master>(query).ToList();
            return result;
        }
        public List<SimCard_Master> GetSim(string code)
        {
            List<SimCard_Master> result = new List<SimCard_Master>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var query = FilterDefinition<SimCard_Master>.Empty;
            query &= Builders<SimCard_Master>.Filter.Eq("EntityId", new BsonObjectId(new ObjectId(code)));
            query &= Builders<SimCard_Master>.Filter.Eq("IsActive", "false");

            result = db.GetCollection<SimCard_Master>("SIM_MASTER").Find<SimCard_Master>(query).ToList();
            return result;
        }

        public void Active(string simCardCode)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(simCardCode);
            var filter = Builders<SimCard_Master>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<SimCard_Master>.Update

                .Set("IsActive", true);


            db.GetCollection<SimCard_Master>("SIM_MASTER").UpdateOne(filter, updateCommand);
        }

        public SimCard_Master Add(SimCard_Master sim)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            sim.IsActive = false;
            db.GetCollection<SimCard_Master>("SIM_MASTER").InsertOne(sim);
            return sim;
        }


        public SimCard_Master Update(SimCard_Master sim)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(sim.IdString);
            var filter = Builders<SimCard_Master>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<SimCard_Master>.Update
                .Set("Description", sim.Description)
                .Set("IsActive", sim.IsActive)
                .Set("GSM_Code", sim.MSISDN)
                .Set("GSM_Code", sim.Sim_Code)
                .Set("GSM_Code", sim.Gsm_Code)
                .Set("GSM_Code", sim.ICCID)
                .Set("GSM_Code", sim.IMSI)
                .Set("OperatorName", sim.Number);

            db.GetCollection<SimCard_Master>("SIM_MASTER").UpdateOne(filter, updateCommand);

            return sim;
        }



        #endregion
    }
}
