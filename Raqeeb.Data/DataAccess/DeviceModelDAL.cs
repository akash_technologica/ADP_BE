﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class DeviceModelDAL : DataAccessBase
    {
        #region Methods

        public List<DeviceModel> GetAll(string manufacturerId)
        {
            List<DeviceModel> result = new List<DeviceModel>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            
            var query = FilterDefinition<DeviceModel>.Empty;
            query &= Builders<DeviceModel>.Filter.Eq("ManufacturerCode", new BsonObjectId(new ObjectId(manufacturerId)));
            result = db.GetCollection<DeviceModel>("DEVICE_MODEL").Find<DeviceModel>(query).ToList();
            return result;
        }

        public DeviceModel Add(DeviceModel deviceModel)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<DeviceModel>("DEVICE_MODEL").InsertOne(deviceModel);
            return deviceModel;
        }


        public DeviceModel Update(DeviceModel deviceModel)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(deviceModel.IdString);
            var filter = Builders<DeviceModel>.Filter.Eq(ID_FIELD, id);
            var updateCommand = Builders<DeviceModel>.Update
                .Set("CountryCode", deviceModel.Name)
                .Set("ManufacturerCode", deviceModel.ManufacturerCode)
                .Set("Description", deviceModel.Description)
                .Set("IsActive", deviceModel.IsActive);


            db.GetCollection<DeviceModel>("DEVICE_MODEL").UpdateOne(filter, updateCommand);

            return deviceModel;
        }


        #endregion
    }
}
