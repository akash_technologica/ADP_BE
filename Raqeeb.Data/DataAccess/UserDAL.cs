﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain;
using Raqeeb.Domain.Entities;
using Raqeeb.Framework.Core;
using System.Web;
using System.Configuration;
using System;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;
using Raqeeb.Domain.Entities;
using System.Linq;
using Raqeeb.Domain.Dtos;

namespace Raqeeb.DataAccess
{
    public class UserDAL : DataAccessBase
    {
        #region Constants
        #region Fields
        protected const string USER_NAME_FIELD = "USER_NAME";
        protected const string Email_FIELD = "Email";
        protected const string ID_FIELD = "_id";
        protected const string PASSWORD_FIELD = "PASSWORD";
        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();
        #endregion
        #endregion

        #region Methods
        public UserDto Add(UserDto user)
        {
            Raqeeb.Domain.DataModel.User userDet = new Domain.DataModel.User()
            {
                ArabicName = user.UserName,
                CompanyId = "0",
                Created_By = user.CreatedBy,
                Creation_Date = DateTime.Now,
                C_id = Raqeeb.Common.Utility.RandomString(10),
                Email = user.Email,
                EnglishName = user.UserName,
                IsActive = true,
                MobileNumber = user.MobileNumber,
                Password = user.Password,
                Roles = user.Roles[0],
                UserName = user.Email,
                LastLoginTime = DateTime.Now,
                GroupId = user.GroupId,
                IsLoggedIn = false,
                               

            };

            aDPEntities.Users.Add(userDet);
            aDPEntities.SaveChanges();


            Domain.DataModel.UserMenuAccess userMenuAccess = new Domain.DataModel.UserMenuAccess();

            foreach (var item in user.MenuStatus)
            {
                userMenuAccess.UserId = userDet.C_id;
                userMenuAccess.MenuId = Convert.ToInt32(item.CategoryId);
                userMenuAccess.LastUpdate = DateTime.Now;

                foreach (var item2 in item.Tests)
                {
                    if (item2.Name == "View")
                        userMenuAccess.Is_View = item2.Checked;
                    else userMenuAccess.Is_Download = item2.Checked;
                }

                aDPEntities.UserMenuAccesses.Add(userMenuAccess);
                aDPEntities.SaveChanges();
            }
            
            //IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            //db.GetCollection<User>(COLLECTION_USER).InsertOne(user);

            user.Id = userDet.C_id;
            return user;
        }

        public UserDto Delete(UserDto user)
        {
            Raqeeb.Domain.DataModel.User userDet = aDPEntities.Users.Where(c => c.C_id == user.Id).FirstOrDefault();
            userDet.IsActive = user.IsActive;
            userDet.Deleted_By = user.DeletedBy;
            userDet.Deletion_Date = DateTime.Now;

            aDPEntities.Entry(userDet).State = System.Data.Entity.EntityState.Modified;
            aDPEntities.SaveChanges();
            //IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            //var id = BsonObjectId.Create(user.IdString);
            //var filter = Builders<Roles>.Filter.Eq(ID_FIELD, id);
            //db.GetCollection<Roles>(COLLECTION_USER).DeleteOne(filter);
            return user;
        }

        public User Inactive(User user)
        {
            bool active = user.IsActive ? false : true;

            Raqeeb.Domain.DataModel.User userDet = aDPEntities.Users.Where(c => c.C_id == user.IdString).FirstOrDefault();
            userDet.IsActive = user.IsActive;
            userDet.Deleted_By = user.DeletedBy;
            userDet.Deletion_Date = DateTime.Now;

            aDPEntities.Entry(userDet).State = System.Data.Entity.EntityState.Modified;
            aDPEntities.SaveChanges();

            return user;
        }

        public List<Domain.DataModel.User> GetAll()
        {
            return aDPEntities.Users.ToList();
           
        }

        public User UpdateUser(User user)
        {
            Raqeeb.Domain.DataModel.User userDet = aDPEntities.Users.Where(c => c.C_id == user.IdString).FirstOrDefault();
            userDet.LastLoginTime = user.LastLoginTime;
            userDet.AccessTokenStamp = user.AccessTokenStamp;

            try
            {
                aDPEntities.Entry(userDet).State = System.Data.Entity.EntityState.Modified;
                aDPEntities.SaveChanges();
            }
            catch (Exception ex)
            {

                throw;
            }

            //IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            //var filter = Builders<User>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(user.IdString)));
            //var updateCommand = Builders<User>.Update
            //    .Set("LAST_LOGIN_TIME", user.LastLoginTime)
            //    .Set("ACCESS_TOKEN_STAMP", user.AccessTokenStamp);

            //db.GetCollection<User>(COLLECTION_USER).UpdateOne(filter, updateCommand);
            return user;
        }

        public UserDto UpdateUserData(UserDto user)
        {
            Domain.DataModel.User userDet = aDPEntities.Users.Where(c => c.C_id == user.Id).FirstOrDefault();

            userDet.Modified_By = user.ModifiedBy;
            userDet.Modification_Date = user.ModificationDate;
            userDet.MobileNumber = user.MobileNumber;
            userDet.UserName = user.UserName;
            userDet.EnglishName = user.EnglishName;
            userDet.Email = user.Email;
            userDet.ArabicName = user.ArabicName;
            userDet.Roles = user.Roles[0];

            aDPEntities.Entry(userDet).State = System.Data.Entity.EntityState.Modified;
            aDPEntities.SaveChanges();

            return user;
        }

        public User GetByUserName(string userid)
        {
            Domain.DataModel.User item = aDPEntities.Users.Where(c => c.UserName == userid).FirstOrDefault();
            List<string> role = new List<string>();
            role.Add(item.Roles);
            User result = new User()
            {
                ArabicName = item.ArabicName,
                CompanyId = item.CompanyId,
                CreatedBy = item.Created_By,
                CreationDate = item.Creation_Date,
                Email = item.Email,
                EnglishName = item.EnglishName,
                IdString = item.C_id,
                IsActive = item.IsActive,
                IsLoggedIn = item.IsLoggedIn.Value,
                LastLoginTime = item.LastLoginTime.Value,
                MobileNumber = item.MobileNumber,
                Roles = role,
                UserName = item.UserName
            };

            return result;
        }

        public UserPermissions UpdateUserPermissions(UserPermissions userPermissions)
        {
            Domain.DataModel.User_Permission item = aDPEntities.User_Permission.Where(c => c.Id == userPermissions.IdString).FirstOrDefault();

            
            item.Report_eventlog = userPermissions.Report_eventlog;
            item.Report_eventlogMenuActive = userPermissions.Report_eventlogMenuActive;
            item.Report_trip = userPermissions.Report_trip;
            item.Report_tripMenuActive = userPermissions.Report_tripMenuActive;
            item.Report_violation = userPermissions.Report_violation;
            item.Report_violationMenuActive = userPermissions.Report_violationMenuActive;
            item.Rolesall = userPermissions.Rolesall;
            item.RolesMenuActive = userPermissions.RolesMenuActive;
            item.Rolesview = userPermissions.Rolesview;
            item.Usersall = userPermissions.Usersall;
            item.UsersMenuActive = userPermissions.UsersMenuActive;
            item.Usersview = userPermissions.Usersview;
            item.Vehiclesall = userPermissions.Vehiclesall;
            item.VehiclesMenuActive = userPermissions.VehiclesMenuActive;
            item.Vehiclesview = userPermissions.Vehiclesview;
            item.Deviceall = userPermissions.Deviceall;
            item.DeviceMenuActive = userPermissions.DeviceMenuActive;
            item.Deviceview = userPermissions.Deviceview;
            item.Dispatcherall = userPermissions.Dispatcherall;
            item.DispatcherMenuActive = userPermissions.DispatcherMenuActive;
            item.Dispatcherview = userPermissions.Dispatcherview;
            item.Driverall = userPermissions.Driverall;
            item.DriverMenuActive = userPermissions.DriverMenuActive;
            item.Driverview = userPermissions.Driverview;

            aDPEntities.Entry(item).State = System.Data.Entity.EntityState.Modified;
            aDPEntities.SaveChanges();

            return userPermissions;
        }

        public UserPermissions GetPermissionById(string id)
        {
            Domain.DataModel.User_Permission item = aDPEntities.User_Permission.Where(c => c.UserId == id).FirstOrDefault();
            UserPermissions userPermissions = new UserPermissions();

            if (item != null)
            {
                  userPermissions = new UserPermissions()
                {
                    Report_eventlog = item.Report_eventlog,
                    Report_eventlogMenuActive = item.Report_eventlog,
                    Report_trip = item.Report_trip,
                    Report_tripMenuActive = item.Report_tripMenuActive,
                    Report_violation = item.Report_violation,
                    Report_violationMenuActive = item.Report_violationMenuActive,
                    Rolesall = item.Rolesall,
                    RolesMenuActive = item.RolesMenuActive,
                    Rolesview = item.Rolesview,
                    Usersall = item.Usersall,
                    UsersMenuActive = item.UsersMenuActive,
                    Usersview = item.Usersview,
                    Vehiclesall = item.Vehiclesall,
                    VehiclesMenuActive = item.VehiclesMenuActive,
                    Vehiclesview = item.Vehiclesview,
                    Deviceall = item.Deviceall,
                    DeviceMenuActive = item.DeviceMenuActive,
                    Deviceview = item.Deviceview,
                    Dispatcherall = item.Dispatcherall,
                    DispatcherMenuActive = item.DispatcherMenuActive,
                    Dispatcherview = item.Dispatcherview,
                    Driverall = item.Driverall,
                    DriverMenuActive = item.DriverMenuActive,
                    Driverview = item.Driverview
                };
            }

         

            return userPermissions;


        }

        public UserPermissions AddUserPermissions(UserPermissions userPermissions)
        {
            Domain.DataModel.User_Permission item = new Domain.DataModel.User_Permission();

            item.Id = userPermissions.IdString = Common.Utility.RandomString(10);
            item.Report_eventlog = userPermissions.Report_eventlog;
            item.Report_eventlogMenuActive = userPermissions.Report_eventlogMenuActive;
            item.Report_trip = userPermissions.Report_trip;
            item.Report_tripMenuActive = userPermissions.Report_tripMenuActive;
            item.Report_violation = userPermissions.Report_violation;
            item.Report_violationMenuActive = userPermissions.Report_violationMenuActive;
            item.Rolesall = userPermissions.Rolesall;
            item.RolesMenuActive = userPermissions.RolesMenuActive;
            item.Rolesview = userPermissions.Rolesview;
            item.Usersall = userPermissions.Usersall;
            item.UsersMenuActive = userPermissions.UsersMenuActive;
            item.Usersview = userPermissions.Usersview;
            item.Vehiclesall = userPermissions.Vehiclesall;
            item.VehiclesMenuActive = userPermissions.VehiclesMenuActive;
            item.Vehiclesview = userPermissions.Vehiclesview;
            item.Deviceall = userPermissions.Deviceall;
            item.DeviceMenuActive = userPermissions.DeviceMenuActive;
            item.Deviceview = userPermissions.Deviceview;
            item.Dispatcherall = userPermissions.Dispatcherall;
            item.DispatcherMenuActive = userPermissions.DispatcherMenuActive;
            item.Dispatcherview = userPermissions.Dispatcherview;
            item.Driverall = userPermissions.Driverall;
            item.DriverMenuActive = userPermissions.DriverMenuActive;
            item.Driverview = userPermissions.Driverview;
            item.UserId = userPermissions.UserId;

            aDPEntities.User_Permission.Add(item);
            aDPEntities.SaveChanges();
           
            return userPermissions;
        }

        public User CheckUserExist(User user)
        {
           
            Domain.DataModel.User item = aDPEntities.Users.Where(c => c.C_id == user.Email).FirstOrDefault();

            List<string> role = new List<string>();
            role.Add(item.Roles);
            User result = new User()
            {
                ArabicName = item.ArabicName,
                CompanyId = item.CompanyId,
                CreatedBy = item.Created_By,
                CreationDate = item.Creation_Date,
                Email = item.Email,
                EnglishName = item.EnglishName,
                IdString = item.C_id,
                IsActive = item.IsActive,
                IsLoggedIn = item.IsLoggedIn.Value,
                LastLoginTime = item.LastLoginTime.Value,
                MobileNumber = item.MobileNumber,
                Roles = role,
                UserName = item.UserName
            };

            return result;
        }

        public User SendConfirmationEmail(User user)
        {
            User result = new User();

            Domain.DataModel.User item = aDPEntities.Users.Where(c => c.C_id == user.Email).FirstOrDefault();

            string address, subject, message, resetLink, token;

            address = "mmuravev396@gmail.com";
            subject = "Reset Password";
            token = new Crypto().Encrypt("test@test.com", "o6806642kbM7c5");

            resetLink = ConfigurationManager.AppSettings["baseURL"] + "/forget-password/" + token;

            message = "<h1>Hi, " + item.UserName + "</h1>";
            message += "<h3>We heard that you lost your password. Sorry about that!</h3><br/>";
            message += "<h3>But don't worry! You can use the following link to reset your password: <a href=\"" + resetLink + "\">" + resetLink + "</a></h3><br/>";
            message += "<h3>If you don't use this lin within 3 hours, it will expire.</h3><br/><br/><br/>";
            message += "<h3>Thanks,</h3><br/>";
            message += "<h3>The Support Team</h3><br/>";

            SendEmail(address, subject, message);
            return result;
        }

        public User GetAccess(string userName, string password)
        {

            User result = new User();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();


            try
            {
                var tt = aDPEntities.Users.Where(c => c.UserName == userName && c.Password == password).FirstOrDefault();
                List<string> role = new List<string>();

                if (tt != null)
                {
                    role.Add(tt.Roles);
                    result.UserName = tt.UserName;
                    result.Roles = role;
                    result.CompanyId = tt.CompanyId;
                    result.Email = tt.Email;
                    result.EnglishName = tt.EnglishName;
                    result.IdString = tt.C_id;
                    result.IsActive = tt.IsActive;
                    result.MobileNumber = tt.MobileNumber;
                    result.ArabicName = tt.ArabicName;
                    result.GroupId = tt.GroupId.Value;
                }
            }
            catch (Exception ex)
            {

                throw;
            }




            return result;
        }


        public User GetById(string idString)
        {
            User result = new User();
            var tt = aDPEntities.Users.Where(c => c.C_id == idString).FirstOrDefault();

            List<string> role = new List<string>();

            if (tt != null)
            {
                role.Add(tt.Roles);
                result.UserName = tt.UserName;
                result.Roles = role;
                result.CompanyId = tt.CompanyId;
                result.Email = tt.Email;
                result.EnglishName = tt.EnglishName;
                result.IdString = tt.C_id;
                result.IsActive = tt.IsActive;
                result.MobileNumber = tt.MobileNumber;
                result.ArabicName = tt.ArabicName;
                result.AccessTokenStamp = tt.AccessTokenStamp;
            }
            return result;
        }


        public void SendEmail(string address, string subject, string message)
        {
            MailMessage msg = new MailMessage();
            try
            {
                var smtp = new SmtpClient
                {
                    Host = ConfigurationManager.AppSettings["ServerHost"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["Password"])
                };

                MailMessage mailMessage = new MailMessage();

                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["DisplayEmail"], ConfigurationManager.AppSettings["DisplayName"]);
                mailMessage.To.Add(new MailAddress(address));
                mailMessage.Body += " <html>";
                mailMessage.Body += "<body>";
                mailMessage.Body += message;
                mailMessage.Body += "</body>";
                mailMessage.Body += "</html>";
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = subject;

                smtp.Send(mailMessage);
            }
            catch (Exception ex)
            {

            }
        }

        public string GeneratePassword()
        {
            Random randNum = new Random();

            int length = 8;
            string charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                   retVal = "";
            for (int i = 0, n = charset.Length; i < length; ++i)
            {
                retVal += charset[(int)Math.Floor(randNum.NextDouble() * n)];
            }
            return retVal;
        }

        public User ValidateConfirmationToken(string token)
        {
            string email;
            token = token + "==";
            email = new Crypto().Decrypt(token, "o6806642kbM7c5");

            User result = new User();
            var tt = aDPEntities.Users.Where(c => c.Email == email).FirstOrDefault();

            List<string> role = new List<string>();

            if (tt != null)
            {
                role.Add(tt.Roles);
                result.UserName = tt.UserName;
                result.Roles = role;
                result.CompanyId = tt.CompanyId;
                result.Email = tt.Email;
                result.EnglishName = tt.EnglishName;
                result.IdString = tt.C_id;
                result.IsActive = tt.IsActive;
                result.MobileNumber = tt.MobileNumber;
                result.ArabicName = tt.ArabicName;
                result.AccessTokenStamp = tt.AccessTokenStamp;
            }

            return result;
        }

        public User updatePassword(string newPassword, string token)
        {
            string email, password;
            token = token + "==";
            email = new Crypto().Decrypt(token, "o6806642kbM7c5");
            password = new Crypto().Encrypt(newPassword, "o6806642kbM7c5");

            User result = new User();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<User>.Filter.Eq(Email_FIELD, email);
            var updateCommand = Builders<User>.Update.Set("PASSWORD", password);
            db.GetCollection<User>("USER").UpdateOne(filter, updateCommand);

            var query = FilterDefinition<User>.Empty;
            query = query & Builders<User>.Filter.Eq(Email_FIELD, email);

            result = db.GetCollection<User>(COLLECTION_USER).Find<User>(query).FirstOrDefault();
            return result;
        }

        public void ChangePassword(User user)
        {

            user.Password = new Crypto().Encrypt(user.Password, "o6806642kbM7c5");
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<User>.Filter.Eq(Email_FIELD, user.Email);
            var updateCommand = Builders<User>.Update.Set("PASSWORD", user.Password);
            db.GetCollection<User>("USER").UpdateOne(filter, updateCommand);

        }
        #endregion
    }
}
