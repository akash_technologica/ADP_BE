﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Dtos;
using Raqeeb.Domain.Entities;
using Raqeeb.Domain.Filters;
using Raqeeb.Framework.Core.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class VehicleDAL : DataAccessBase
    {
        #region Constants
        #region Fields
  
        Raqeeb.Domain.DataModel.ADPEntities aDPEntities = new Domain.DataModel.ADPEntities();

        #endregion
        #endregion
        #region Methods
        public List<Vehicle> InsertBulk(List<Vehicle> vehicles)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<Vehicle>("VEHICLE").InsertMany(vehicles);
            return vehicles;
        }

        public Vehicle GetByCode(int? code)
        {
            Vehicle result = new Vehicle();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Vehicle>.Filter.Eq("Code", code);
            result = db.GetCollection<Vehicle>("VEHICLE").Find<Vehicle>(filter).FirstOrDefault();
            return result;
        }

        public string GetByCode(string code)
        {
            Domain.DataModel.Vehicle result = new Domain.DataModel.Vehicle();
           
           return result.CarLicense = aDPEntities.Vehicles.Where(c=>c.DeviceId == code).Select(c=>c.CarLicense).FirstOrDefault();
           
        }


        public List<Domain.DataModel.Vehicle> GetAll()
        {
            List<Domain.DataModel.Vehicle> vehicleDetList = aDPEntities.Vehicles.ToList();

            return vehicleDetList;
        }
        public List<Domain.DataModel.Vehicle> GetAll(int group)
        {
            List<Domain.DataModel.Vehicle> vehicleDetList = aDPEntities.Vehicles.Where(c=>c.GruoupId == group).ToList();

            return vehicleDetList;
        }
        public List<Domain.DataModel.Vehicle> Search(SearchCriteria<VehicleFilter> searchCriteria, int IncludedTime = 0)
        {
            List<Domain.DataModel.Vehicle> vehicleDetList = new List<Domain.DataModel.Vehicle>();

            try
            {
               
                var query = FilterDefinition<Vehicle>.Empty;
               
                if (searchCriteria.Filters != null)
                {
                    if (searchCriteria.Filters.GroupCode.Length > 0)
                    {
                        foreach (var group in searchCriteria.Filters.GroupCode)
                        {
                            List<Domain.DataModel.Vehicle> list = aDPEntities.Vehicles.Where(c => c.GruoupId == group).Select(c => c).ToList();
                            vehicleDetList.AddRange(list);
                        }
                    }
                    else if(searchCriteria.Filters.LicenseCode.Length > 0)
                    {
                        foreach (var license in searchCriteria.Filters.LicenseCode)
                        {
                            List<Domain.DataModel.Vehicle> list = aDPEntities.Vehicles.Where(c => c.CarLicense == license).Select(c => c).ToList();
                            vehicleDetList.AddRange(list);
                        }
                    }
                    else
                    {
                        List<Domain.DataModel.Vehicle> list = aDPEntities.Vehicles.Where(c=>c.GruoupId == searchCriteria.Filters.UserGroup).OrderBy(c=>c.GruoupId).Select(c => c).ToList();
                    }



                }            
                return vehicleDetList;

            }
            catch (Exception ex)
            {
                return vehicleDetList;
            }

        }

        public List<Vehicle> GetVehiclesByStatus(bool idle, bool reporting)
        {
            List<Vehicle> result = new List<Vehicle>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            
            var filter = Builders<Vehicle>.Filter.Empty;
            var query = FilterDefinition<Vehicle>.Empty;
           
            query &= Builders<Vehicle>.Filter.Eq("VehicleStatus", "Active");
           
            result = db.GetCollection<Vehicle>("VEHICLE").Find<Vehicle>(query).ToList();

            return result;

        }

        public List<BsonDocument> GetALlVehiclesLocations_v2()
        {
            List<BsonDocument> result = new List<BsonDocument>();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>("VEHICLE");

            // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

            var options = new AggregateOptions()
            {
                AllowDiskUse = true
            };

            PipelineDefinition<BsonDocument, BsonDocument> pipeline = new BsonDocument[]
           {
                new BsonDocument("$project", new BsonDocument()
                        .Add("_id", 0)
                        .Add("VEHICLE", "$$ROOT")),
                new BsonDocument("$lookup", new BsonDocument()
                        .Add("localField", "VEHICLE.Code")
                        .Add("from", "LIVE_POSITION")
                        .Add("foreignField", "VehicleCode")
                        .Add("as", "LIVE_POSITION")),
                new BsonDocument("$unwind", new BsonDocument()
                        .Add("path", "$LIVE_POSITION")
                        .Add("preserveNullAndEmptyArrays", new BsonBoolean(false))),
                new BsonDocument("$match", new BsonDocument()
                        .Add("LIVE_POSITION.Type", new BsonInt64(1L))),
                new BsonDocument("$project", new BsonDocument()
                        .Add("code", "$VEHICLE.Code")
                        .Add("registration", "$VEHICLE.Registration")
                        .Add("lastOdoMeter", "$VEHICLE.LastOdoMeter")
                        .Add("latitude", "$LIVE_POSITION.Latitude")
                        .Add("longitude", "$LIVE_POSITION.Longitude")
                        .Add("locationTIme", "$LIVE_POSITION.LocationTIme")
                        .Add("updateDate", "$VEHICLE.UpdateDate"))
           };

            using (var cursor = collection.Aggregate(pipeline, options))
            {
                while (cursor.MoveNext())
                {
                    var batch = cursor.Current;
                    //foreach (BsonDocument document in batch)
                    //{
                    //    Console.WriteLine(document.ToJson());
                    //}
                    result.AddRange(batch);
                }
            }
            return result;
        }

        public List<VehicleUtilization> CriticalVehicleByMonth(int month, int year)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<VehicleUtilization>.Filter.Empty;
            var query = FilterDefinition<VehicleUtilization>.Empty;
           
            query &= Builders<VehicleUtilization>.Filter.Eq("MonthName", month);
            query &= Builders<VehicleUtilization>.Filter.Eq("YearName", year);
            query &= Builders<VehicleUtilization>.Filter.Eq("Status", 3);
            return db.GetCollection<VehicleUtilization>("VEHICLE_UTILIZATION").Find<VehicleUtilization>(query).ToList();
        }

        public VehicleUtilization GetVehicleStatisticsByCode(SearchCriteria<VehicleFilter> searchCriteria,int code)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<VehicleUtilization>.Filter.Empty;
            var query = FilterDefinition<VehicleUtilization>.Empty;
            query &= Builders<VehicleUtilization>.Filter.Eq("VehicleCode", code);
            query &= Builders<VehicleUtilization>.Filter.Eq("MonthName", searchCriteria.Filters.Month);
            query &= Builders<VehicleUtilization>.Filter.Eq("YearName", searchCriteria.Filters.Year);         
            return db.GetCollection<VehicleUtilization>("VEHICLE_UTILIZATION").Find<VehicleUtilization>(query).FirstOrDefault(); 
        }
        #endregion


        public Vehicle GetByRegistration(string registration)
        {
            Vehicle result = new Vehicle();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<Vehicle>.Filter.Eq("Registration", registration);
            result = db.GetCollection<Vehicle>("VEHICLE").Find<Vehicle>(filter).FirstOrDefault();
            return result;
        }

        public Vehicle UpdateVehicle(Vehicle vehicleData)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            Vehicle result = new Vehicle();

            //var filter = Builders<Vehicle>.Filter.Eq("Code", vehicleData.Code);
            //result = db.GetCollection<Vehicle>("VEHICLE").Find<Vehicle>(filter).FirstOrDefault();


            var filter2 = Builders<Vehicle>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(vehicleData.IdString)));

            var updateCommand = Builders<Vehicle>.Update
                .Set("LastOdoMeter", vehicleData.LastOdoMeter)
                .Set("EngineOn", vehicleData.EngineOn)
                .Set("Latitude", vehicleData.Latitude)
                .Set("Longitude", vehicleData.Longitude)
                .Set("Speed", vehicleData.Speed)
                .Set("RPM", vehicleData.RPM)
                .Set("LastUpdateLocationDate", vehicleData.LastUpdateLocationDate)
                .Set("CurrentEvents", vehicleData.CurrentEvents)
                .Set("Reporting", vehicleData.Reporting)
                .Set("Counter", vehicleData.Counter)
                .Set("SM_Serial_Number", vehicleData.SM_Serial_Number)
                .Set("FMS_Serial_Number", vehicleData.FMS_Serial_Number)
                .Set("VehicleStatus", vehicleData.VehicleStatus)
                .Set("Normal", vehicleData.Normal)
                .Set("DriverCode", vehicleData.DriverCode)
                .Set("IsGeofenceViolation", vehicleData.IsGeofenceViolation)
                .Set("Idle", vehicleData.Idle)
                .Set("AcceleratorPedalPosition", vehicleData.AcceleratorPedalPosition)
                .Set("Analog_Input_1", vehicleData.Analog_Input_1)
                .Set("Axis_X", vehicleData.Axis_X)
                .Set("Axis_Y", vehicleData.Axis_Y)
                .Set("Axis_Z", vehicleData.Axis_Z)
                .Set("Battery_Current", vehicleData.Battery_Current)
                .Set("Battery_Voltage", vehicleData.Battery_Voltage)
                .Set("CNGStatus", vehicleData.CNGStatus)
                .Set("Digital_Input_1", vehicleData.Digital_Input_1)
                .Set("Digital_Output_1", vehicleData.Digital_Output_1)
                .Set("DoorStatus", vehicleData.DoorStatus)
                .Set("EngineLoad", vehicleData.EngineLoad)
                .Set("EngineTemperature", vehicleData.EngineTemperature)
                .Set("ExternalVoltage", vehicleData.ExternalVoltage)
                .Set("FuelLevelLiters", vehicleData.FuelLevelLiters)
                .Set("FuelLevelPercent", vehicleData.FuelLevelPercent)
                .Set("FuelRate", vehicleData.FuelRate)
                .Set("Fuel_Consumed", vehicleData.Fuel_Consumed)
                .Set("GNSS_HDOP", vehicleData.GNSS_HDOP)
                .Set("GNSS_PDOP", vehicleData.GNSS_PDOP)
                .Set("GreenDrivingType", vehicleData.GreenDrivingType)
                .Set("GSM_Signal", vehicleData.GSM_Signal)
                .Set("RFID", vehicleData.RFID)
                .Set("SD_Status", vehicleData.SD_Status)
                .Set("Sleep_Mode", vehicleData.Sleep_Mode)
                .Set("TotalMileage", vehicleData.TotalMileage)
                .Set("Vehicle_CAN_Speed", vehicleData.Vehicle_CAN_Speed);              
                
               db.GetCollection<Vehicle>("VEHICLE").UpdateOne(filter2, updateCommand);

            return vehicleData; 
        }
        public Vehicle UpdateVehicleClassification(Vehicle vehicleData)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            Vehicle result = new Vehicle();

            var filter = Builders<Vehicle>.Filter.Eq("Code", vehicleData.Code);
            result = db.GetCollection<Vehicle>("VEHICLE").Find<Vehicle>(filter).FirstOrDefault();


            var filter2 = Builders<Vehicle>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(result.IdString)));
            var updateCommand = Builders<Vehicle>.Update
                
                .Set("Critical", vehicleData.Critical)
                .Set("Elevated", vehicleData.Elevated)
                .Set("Normal", vehicleData.Normal)
                .Set("EventCount", vehicleData.EventCount);


            db.GetCollection<Vehicle>("VEHICLE").UpdateMany(filter2, updateCommand);

            return vehicleData;
        }

        public VehicleUtilization CheckVehicleStatisticsExists(int? code, int month, int year)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<VehicleUtilization>.Filter.Empty;
            var query = FilterDefinition<VehicleUtilization>.Empty;
            query = query & Builders<VehicleUtilization>.Filter.Eq("VehicleCode", code);
            query = query & Builders<VehicleUtilization>.Filter.Eq("MonthName", month);
            query = query & Builders<VehicleUtilization>.Filter.Eq("YearName", year);

           
            return db.GetCollection<VehicleUtilization>("VEHICLE_UTILIZATION").Find<VehicleUtilization>(query).FirstOrDefault(); ;

        }

        public void UpdateVehicleStatistics(VehicleUtilization scoreitem)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter2 = Builders<VehicleUtilization>.Filter.Eq(ID_FIELD, new BsonObjectId(new ObjectId(scoreitem.IdString)));

            var updateCommand = Builders<VehicleUtilization>.Update
                     .Set("TotalDistraction", scoreitem.TotalDistraction)
                     .Set("TotalFatigue", scoreitem.TotalFatigue)
                     .Set("TotalForwardCollisionWarning", scoreitem.TotalForwardCollisionWarning)
                      .Set("TotalFOV", scoreitem.TotalFOV)
                      .Set("TotalHarshAcceleration", scoreitem.TotalHarshAcceleration)
                      .Set("TotalHarshBraking", scoreitem.TotalHarshBraking)
                      .Set("TotalLeftLaneDeparture", scoreitem.TotalLeftLaneDeparture)
                      .Set("TotalMobileeyeTamperAlert", scoreitem.TotalMobileeyeTamperAlert)
                      .Set("TotalNearmiss", scoreitem.TotalNearmiss)
                      .Set("TotalOverRevving", scoreitem.TotalOverRevving)
                      .Set("TotalOverspeeding", scoreitem.TotalOverspeeding)
                      .Set("TotalPanicButton", scoreitem.TotalPanicButton)
                      .Set("TotalPedestrianInDangerZone", scoreitem.TotalPedestrianInDangerZone)
                      .Set("TotalPedestrianInForwardCollisionWarning", scoreitem.TotalPedestrianInForwardCollisionWarning)
                      .Set("TotalRightLaneDeparture", scoreitem.TotalRightLaneDeparture)
                      .Set("TotalTrafficSignRecognition", scoreitem.TotalTrafficSignRecognition)
                      .Set("TotalDistanceDriven", scoreitem.TotalDistanceDriven)
                      .Set("TotalDrivenTime", scoreitem.TotalDrivenTime)
                      .Set("TotalDuration", scoreitem.TotalDuration)
                      .Set("TotalStandingTime", scoreitem.TotalStandingTime)
                      .Set("TotalScore", scoreitem.TotalScore)
                      .Set("Status", scoreitem.Status)
                      .Set("TotalTrips", scoreitem.TotalTrips);


            db.GetCollection<VehicleUtilization>("VEHICLE_UTILIZATION").UpdateMany(filter2, updateCommand);

            if (scoreitem.VehicleDriverHistoryDetails.Count > 0)
            {
                var updatedCommand = Builders<VehicleUtilization>.Update
                      .Set("VehicleDriverHistoryDetails", scoreitem.VehicleDriverHistoryDetails);

                db.GetCollection<VehicleUtilization>("VEHICLE_UTILIZATION").UpdateMany(filter2, updatedCommand);
            }
        }

        public void InsertwVehicleStatistics(VehicleUtilization vehicleScore)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<VehicleUtilization>("VEHICLE_UTILIZATION").InsertOne(vehicleScore);
        }

        public Vehicle InsertOne(Vehicle vehicle)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            db.GetCollection<Vehicle>("VEHICLE").InsertOne(vehicle);
            return vehicle;
        }

        public string GetDeviceIdByLicense(string licencse)
        {
            Domain.DataModel.Vehicle result = new Domain.DataModel.Vehicle();

            return result.DeviceId = aDPEntities.Vehicles.Where(c => c.CarLicense == licencse).Select(c => c.DeviceId).FirstOrDefault();

        }
        public List<Domain.DataModel.Vehicle> GetVechilsByGroupId(int groupId)
        {
            Domain.DataModel.Vehicle result = new Domain.DataModel.Vehicle();

            return  aDPEntities.Vehicles.Where(c => c.GruoupId == groupId).ToList();

        }
    }
}
