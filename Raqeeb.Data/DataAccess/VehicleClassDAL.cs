﻿using MongoDB.Bson;
using MongoDB.Driver;
using Raqeeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raqeeb.DataAccess.DataAccess
{
    public class VehicleClassDAL : DataAccessBase
    {
        #region Methods
        public VehicleClass Insert(VehicleClass vehicleClass)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            db.GetCollection<VehicleClass>("VehicleClass").InsertOne(vehicleClass);
            return vehicleClass;
        }


        public List<VehicleClass> GetAllVehicleClass()
        {
            VehicleClass result = new VehicleClass();
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var filter = Builders<VehicleClass>.Filter.Empty;
            return db.GetCollection<VehicleClass>("VehicleClass").Find<VehicleClass>(filter).ToList();
        }

        public VehicleClass DeleteVehicleClass(VehicleClass vehicleClass)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();
            var id = BsonObjectId.Create(vehicleClass.IdString);
            var filter = Builders<VehicleClass>.Filter.Eq(ID_FIELD, id);
            db.GetCollection<VehicleClass>("VehicleClass").DeleteOne(filter);
            return vehicleClass;
        }

        public VehicleClass UpdateAsync(VehicleClass vehicleClass)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var filter = Builders<VehicleClass>
             .Filter.Eq(e => e.IdString, vehicleClass.IdString);




            return vehicleClass;
        }
        public VehicleClass UpdateVehicleClass(VehicleClass vehicleClass)
        {
            IMongoDatabase db = MongoDbManager.GetMongoDataBase();

            var filter = Builders<VehicleClass>
             .Filter.Eq(e => e.IdString, vehicleClass.IdString);


            var updateCommand = Builders<VehicleClass>.Update
                     .Set("ClassName", vehicleClass.ClassName);


            db.GetCollection<VehicleClass>("VehicleClass").UpdateMany(filter, updateCommand);

            return vehicleClass;
        }
        #endregion
    }
}
